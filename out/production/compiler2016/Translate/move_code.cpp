#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

vector<string> static_area, stack_area;
vector<string> str;

int main()
{
	string tmp;
	ifstream fin("ll.ir");
	while(getline(fin, tmp))
		str.push_back(tmp);
	fin.close();
	ofstream fout("ll_after.ir");
	int i;
	for(i = 1; i < str.size(); ++i)
	{
		if(str[i] == "##Stack area")
			break;
		static_area.push_back(str[i]);
	}
	i++;
	for(; i < str.size(); ++i)
		stack_area.push_back(str[i]);
	for(int j = 0; j < stack_area.size(); ++j)
	{
		fout << stack_area[j] << endl;
		if(stack_area[j] == "@functionLabel_main")
		{
			for(int k = 0; k < static_area.size(); ++k)
				fout << static_area[k] << endl;
		}
	}
	fout.close();
	return 0;
}