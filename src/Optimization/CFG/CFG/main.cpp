#include <vector>
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <queue>
#include <cassert>

using namespace std;
class BasicBlock
{
public:
	int leading, ending;
	int blockid;
};

class operation
{
public:
	string opcode;
	vector<string> operands;
	operation()
	{
		opcode = "";
		operands.clear();
	}
};

vector<int> mapping_line2blockid;
vector<operation> lines;
map<string, int> label_pc;
deque<BasicBlock> blocks;

class Graph
{
public:
	vector<vector<int> > nxt;
	void init(int n)
	{
		nxt.clear();
		nxt.resize(n);
	}
	void add(int x, int y)
	{
		nxt[x].push_back(y);
	}
}CFG;

operation get_context(string info, int pc)
{
	if (info == "")
	{
		return operation();
	}
	if (info[0] == '@')
	{
		operation ret;
		ret.opcode = "label";
		if (info.substr(1, 8) == "function")
		{
			int cur = 0;
			string tmp;
			bool string_mode = false;
			while (cur < info.size())
			{
				if (!string_mode && info[cur] == ' ')
				{
					break;
				}
				if (info[cur] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
				}
				else if (info[cur] == '-')
				{
					tmp = tmp + "sub_";
				}
				else if (info[cur] == ' ')
				{
					tmp = tmp + "space_";
				}
				else if (info[cur] == '\\')
				{
					tmp = tmp + "backslash_";
				}
				else if (info[cur] == ':')
				{
					tmp = tmp + "colon_";
				}
				else if (info[cur] == '!')
				{
					tmp = tmp + "exg_";
				}
				else
					tmp = tmp + info[cur];
				cur++;
			}
			ret.operands.push_back(tmp);
			label_pc[tmp] = pc;
			while (cur < info.size())
			{
				tmp = "";
				cur++;
				string_mode = false;
				while (cur < info.size())
				{
					if (!string_mode && info[cur] == ' ')
					{
						break;
					}
					if (info[cur] == '\"')
					{
						string_mode ^= 1;
						tmp = tmp + "quote_";
						cur++;
					}
					else if (info[cur] == '-')
					{
						tmp = tmp + "sub_";
						cur++;
					}
					else if (info[cur] == ' ')
					{
						tmp = tmp + "space_";
						cur++;
					}
					else if (info[cur] == '\\')
					{
						tmp = tmp + "backslash_";
						cur++;
					}
					else if (info[cur] == ':')
					{
						tmp = tmp + "colon_";
						cur++;
					}
					else if (info[cur] == '!')
					{
						tmp = tmp + "exg_";
						cur++;
					}
					else
						tmp.push_back(info[cur++]);
				}
				ret.operands.push_back(tmp);
			}
		}
		else
		{
			info.pop_back();
			label_pc[info] = pc;
			ret.operands.push_back(info);
		}
		return ret;
	}
	else
	{
		operation ret;
		int pos = 0;
		bool string_mode = false;
		while (pos < info.size())
		{
			if (info[pos] == ' ' && !string_mode)
				break;
			if (info[pos] == '\"')
			{
				string_mode ^= 1;
				ret.opcode = ret.opcode + "quote_";
			}
			else if (info[pos] == '-')
			{
				ret.opcode = ret.opcode + "sub_";
			}
			else if (info[pos] == ' ')
			{
				ret.opcode = ret.opcode + "space_";
			}
			else if (info[pos] == '\\')
			{
				ret.opcode = ret.opcode + "backslash_";
			}
			else if (info[pos] == ':')
			{
				ret.opcode = ret.opcode + "colon_";
			}
			else if (info[pos] == '!')
			{
				ret.opcode = ret.opcode + "exg_";
			}
			else
				ret.opcode.push_back(info[pos]);
			pos++;
		}
		while (pos < info.size())
		{
			string_mode = false;
			string tmp = "";
			pos++;
			while (pos < info.size())
			{
				if (info[pos] == ' ' && !string_mode)
					break;
				if (info[pos] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
					pos++;
				}
				else if (info[pos] == '-')
				{
					tmp = tmp + "sub_";
					pos++;
				}
				else if (info[pos] == ' ')
				{
					tmp = tmp + "space_";
					pos++;
				}
				else if (info[pos] == '\\')
				{
					tmp = tmp + "backslash_";
					pos++;
				}
				else if (info[pos] == ':')
				{
					tmp = tmp + "colon_";
					pos++;
				}
				else if (info[pos] == '!')
				{
					tmp = tmp + "exg_";
					pos++;
				}
				else
					tmp.push_back(info[pos++]);
				
			}
			ret.operands.push_back(tmp);
			if (ret.opcode == "call" && (ret.operands[0] == "@functionLabel_print_constant" || ret.operands[0] == "@functionLabel_println_constant"))
			{
				pos++;
				string temp;
				while (pos < info.size())
				{
					temp.push_back(info[pos]);
					pos++;
				}
				ret.operands.push_back(temp);
				return ret;
			}
		}
		return ret;
	}
}

vector<string> raw;

int main(int argc, char* argv[])
{
	/*
	if (argc != 2)
	{
		printf("Error! Arguments not valid.");
		return 1;
	}*/
	string tmp;
	ifstream ir = ifstream("ll_after.ir");
	int lineid = 0;
	while (getline(ir, tmp))
	{
		lines.push_back(get_context(tmp, lineid++));
		raw.push_back(tmp);
	}
	BasicBlock cur;
	cur.leading = 0;
	cur.blockid = blocks.size();
	for (int i = 0; i < lines.size(); ++i)
	{
		if (lines[i].opcode == "label" || lines[i].opcode == "cbr" || lines[i].opcode == "jump" || lines[i].opcode == "jumpI" || lines[i].opcode == "return" || lines[i].opcode == "call"
			|| lines[i].opcode == "cbr_lt" || lines[i].opcode == "cbr_le" || lines[i].opcode == "cbr_gt" || lines[i].opcode == "cbr_ge" || lines[i].opcode == "cbr_eq" || lines[i].opcode == "cbr_ne")
		{
			if (lines[i].opcode == "jump")
			{
				assert(false);
			}
			if(lines[i].opcode == "label")
			{
				if (cur.leading == i)
				{
					mapping_line2blockid.push_back(cur.blockid);
				}
				else
				{
					cur.ending = i - 1;
					blocks.push_back(cur);
					cur.leading = i;
					cur.blockid = blocks.size();
					mapping_line2blockid.push_back(cur.blockid);
				}
			}
			else
			{
				cur.ending = i;
				mapping_line2blockid.push_back(cur.blockid);
				blocks.push_back(cur);

				cur.leading = i + 1;
				cur.blockid = blocks.size();
			}
		}
		else
			mapping_line2blockid.push_back(cur.blockid);
	}
	cur.ending = lines.size() - 1;
	if (cur.ending >= cur.leading)
		blocks.push_back(cur);

	CFG.init(blocks.size());

	for (int i = 0; i < lines.size(); ++i)
	{
		if (lines[i].opcode == "cbr" || lines[i].opcode == "jump" || lines[i].opcode == "jumpI" || lines[i].opcode == "call"
			|| lines[i].opcode == "cbr_lt" || lines[i].opcode == "cbr_le" || lines[i].opcode == "cbr_gt" || lines[i].opcode == "cbr_ge" || lines[i].opcode == "cbr_eq" || lines[i].opcode == "cbr_ne")
		{ //return edges are added by call instruction
			if (lines[i].opcode == "cbr")
			{
				int nxt1, nxt2;
				nxt1 = label_pc[lines[i].operands[1]];
				nxt2 = label_pc[lines[i].operands[2]];
				CFG.add(mapping_line2blockid[i], mapping_line2blockid[nxt1]);
				CFG.add(mapping_line2blockid[i], mapping_line2blockid[nxt2]);
			}
			else if (lines[i].opcode == "jumpI")
			{
				int nxt = label_pc[lines[i].operands[0]];
				CFG.add(mapping_line2blockid[i], mapping_line2blockid[nxt]);
			}
			else if (lines[i].opcode == "call")
			{
				int nxt = label_pc[lines[i].operands[0]];
				CFG.add(mapping_line2blockid[i], mapping_line2blockid[nxt]);
				if(i + 1 < lines.size())
					CFG.add(mapping_line2blockid[i], mapping_line2blockid[i + 1]);
				for (int j = nxt + 1; j < lines.size(); ++j)
				{
					if (lines[j].opcode == "label" && lines[j].operands[0].substr(0, 9) == "@function")
					{
						break;
					}
					if (lines[j].opcode == "return")
					{
						if (i + 1 < lines.size())
							CFG.add(mapping_line2blockid[j], -mapping_line2blockid[i + 1]);
					}
				}
			}
			else if (lines[i].opcode == "cbr_lt" || lines[i].opcode == "cbr_le" || lines[i].opcode == "cbr_gt" || lines[i].opcode == "cbr_ge" || lines[i].opcode == "cbr_eq" || lines[i].opcode == "cbr_ne")
			{
				int nxt = label_pc[lines[i].operands[2]];
				CFG.add(mapping_line2blockid[i], mapping_line2blockid[nxt]);
				nxt = i + 1;
				CFG.add(mapping_line2blockid[i], mapping_line2blockid[nxt]);

			}
			else
			{
				assert(false);
			}
		}
		else if (i == blocks[mapping_line2blockid[i]].ending && lines[i].opcode != "return")
		{
			if(i + 1 < lines.size())
				CFG.add(mapping_line2blockid[i], mapping_line2blockid[i + 1]);
		}
	}
	for (int i = 0; i < blocks.size(); ++i)
	{
		if (lines[blocks[i].ending].opcode == "label")
		{
			if(i + 1 < blocks.size())
				CFG.add(i, i + 1);
		}
	}
	ofstream blo, cfg;
	blo = ofstream("blocks");
	cfg = ofstream("control_flow_graph");
	for (int i = 0; i < blocks.size(); ++i)
	{
		blo << blocks[i].blockid << " " << blocks[i].leading << " " << blocks[i].ending << endl;
	}
	cfg << CFG.nxt.size() << endl;
	for (int i = 0; i < CFG.nxt.size(); ++i)
	{
		cfg << i << " " << CFG.nxt[i].size();
		for (int j = 0; j < CFG.nxt[i].size(); ++j)
			cfg << " " << CFG.nxt[i][j];
		cfg << endl;
	}
	return 0;
}