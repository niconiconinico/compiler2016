#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <fstream>
#include <map>
#include <cassert>
#include <sstream>
#include <queue>

using namespace std;

class Block
{
public:
	int leading, ending;
	int blockid;
	bool isreachable;
};

class operation
{
public:
	string opcode;
	vector<string> operands;
	operation()
	{
		opcode = "";
		operands.clear();
	}
};
class Graph
{
public:
	vector<vector<int> > nxt;
	void init(int n)
	{
		nxt.clear();
		nxt.resize(n);
	}
	void add(int x, int y)
	{
		nxt[x].push_back(y);
	}
}CFG;

map<string, int> label_pc;
vector<operation> lines;
vector<string> raw;
vector<Block> blocks;
int S;
vector<int> mapping_lines2block;

operation get_context(string info, int pc)
{
	if (info == "")
	{
		return operation();
	}
	if (info[0] == '@')
	{
		operation ret;
		ret.opcode = "label";
		if (info.substr(1, 8) == "function")
		{
			int cur = 0;
			string tmp;
			bool string_mode = false;
			while (cur < info.size())
			{
				if (!string_mode && info[cur] == ' ')
				{
					break;
				}
				if (info[cur] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
				}
				else if (info[cur] == '-')
				{
					tmp = tmp + "sub_";
				}
				else if (info[cur] == ' ')
				{
					tmp = tmp + "space_";
				}
				else if (info[cur] == '\\')
				{
					tmp = tmp + "backslash_";
				}
				else if (info[cur] == ':')
				{
					tmp = tmp + "colon_";
				}
				else if (info[cur] == '!')
				{
					tmp = tmp + "exg_";
				}
				else
					tmp = tmp + info[cur];
				cur++;
			}
			ret.operands.push_back(tmp);
			label_pc[tmp] = pc + 1;
			while (cur < info.size())
			{
				tmp = "";
				cur++;
				string_mode = false;
				while (cur < info.size())
				{
					if (!string_mode && info[cur] == ' ')
					{
						break;
					}
					if (info[cur] == '\"')
					{
						string_mode ^= 1;
						tmp = tmp + "quote_";
						cur++;
					}
					else if (info[cur] == '-')
					{
						tmp = tmp + "sub_";
						cur++;
					}
					else if (info[cur] == ' ')
					{
						tmp = tmp + "space_";
						cur++;
					}
					else if (info[cur] == '\\')
					{
						tmp = tmp + "backslash_";
						cur++;
					}
					else if (info[cur] == ':')
					{
						tmp = tmp + "colon_";
						cur++;
					}
					else if (info[cur] == '!')
					{
						tmp = tmp + "exg_";
						cur++;
					}
					else
						tmp.push_back(info[cur++]);
				}
				ret.operands.push_back(tmp);
			}
		}
		else
		{
			info.pop_back();
			label_pc[info] = pc + 1;
			ret.operands.push_back(info);
		}
		return ret;
	}
	else
	{
		operation ret;
		int pos = 0;
		bool string_mode = false;
		while (pos < info.size())
		{
			if (info[pos] == ' ' && !string_mode)
				break;
			if (info[pos] == '\"')
			{
				string_mode ^= 1;
				ret.opcode = ret.opcode + "quote_";
			}
			else if (info[pos] == '-')
			{
				ret.opcode = ret.opcode + "sub_";
			}
			else if (info[pos] == ' ')
			{
				ret.opcode = ret.opcode + "space_";
			}
			else if (info[pos] == '\\')
			{
				ret.opcode = ret.opcode + "backslash_";
			}
			else if (info[pos] == ':')
			{
				ret.opcode = ret.opcode + "colon_";
			}
			else if (info[pos] == '!')
			{
				ret.opcode = ret.opcode + "exg_";
			}
			else
				ret.opcode.push_back(info[pos]);
			pos++;
		}
		while (pos < info.size())
		{
			string_mode = false;
			string tmp = "";
			pos++;
			while (pos < info.size())
			{
				if (info[pos] == ' ' && !string_mode)
					break;
				if (info[pos] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
					pos++;
				}
				else if (info[pos] == '-')
				{
					tmp = tmp + "sub_";
					pos++;
				}
				else if (info[pos] == ' ')
				{
					tmp = tmp + "space_";
					pos++;
				}
				else if (info[pos] == '\\')
				{
					tmp = tmp + "backslash_";
					pos++;
				}
				else if (info[pos] == ':')
				{
					tmp = tmp + "colon_";
					pos++;
				}
				else if (info[pos] == '!')
				{
					tmp = tmp + "exg_";
					pos++;
				}
				else
					tmp.push_back(info[pos++]);
			}
			ret.operands.push_back(tmp);
			if (ret.opcode == "call" && (ret.operands[0] == "@functionLabel_print_constant" || ret.operands[0] == "@functionLabel_println_constant"))
			{
				pos++;
				string temp;
				while (pos < info.size())
				{
					temp.push_back(info[pos]);
					pos++;
				}
				ret.operands.push_back(temp);
				return ret;
			}
		}
		return ret;
	}
}

vector<bool> iskey;

void block_init()
{
	ifstream block_in("blocks");
	int a, b, c;
	int mx = 0;
	while (block_in >> a >> b >> c)
	{
		Block bb;
		bb.blockid = a;
		bb.leading = b;
		bb.ending = c;
		bb.isreachable = false;
		mx = max(mx, c);
		blocks.push_back(bb);
	}
	mapping_lines2block.resize(mx + 1);
	for (int i = 0; i < blocks.size(); ++i)
	{
		for (int j = blocks[i].leading; j <= blocks[i].ending; ++j)
			mapping_lines2block[j] = blocks[i].blockid;
	}
	block_in.close();
}

void init()
{
	ifstream ir_in("ll_after.ir");
	ifstream cfg_in("control_flow_graph");
	
	block_init();
	string tmp;
	int pc = 0;
	while (getline(ir_in, tmp))
	{
		lines.push_back(get_context(tmp, pc++));
		raw.push_back(tmp);
		if (lines[lines.size() - 1].opcode == "label" && lines[lines.size() - 1].operands[0] == "@functionLabel_main")
		{
			S = pc - 1;
			S = mapping_lines2block[S];
		}
	}

	blocks[S].isreachable = true;

	int n;
	cfg_in >> n;
	CFG.init(n);
	for (int i = 0; i < n; ++i)
	{
		int s, t, m;
		cfg_in >> s >> m;
		for (int j = 0; j < m; ++j)
		{
			cfg_in >> t;
			CFG.add(s, t);
		}
	}
	iskey.resize(lines.size());
	for (int i = 0; i < iskey.size(); ++i)
		iskey[i] = true;
	ir_in.close();
	cfg_in.close();
}

void unreachable_elimination()
{
	queue<int> q;
	q.push(S);
	while (!q.empty())
	{
		int cur = q.front();
		q.pop();
		blocks[cur].isreachable = true;
		for (int j = 0; j < CFG.nxt[cur].size(); ++j)
		{
			int y = CFG.nxt[cur][j];
			if (y < 0) continue;
			if (blocks[y].isreachable == false)
			{
				blocks[y].isreachable = true;
				q.push(y);
			}
		}
	}
}

void useless_elimination()
{

}

void useless_control_flow_elimination()
{
	for (int i = 0; i < blocks.size(); ++i)
	{
		if (lines[blocks[i].ending].opcode == "jumpI")
		{
			if (CFG.nxt[blocks[i].blockid].size() == 1 && CFG.nxt[blocks[i].blockid][0] == blocks[i].blockid + 1)
			{
				iskey[blocks[i].ending] = false;
			}
		}
	}
}

void output()
{
	ofstream out("ll_after.ir");
	for (int i = 0; i < lines.size(); ++i)
		if (iskey[i] && blocks[mapping_lines2block[i]].isreachable)
			out << raw[i] << endl;
	out.close();
}

void reorder()
{
	for (int i = 0; i + 1 < lines.size(); ++i)
	{
		if (lines[i].opcode == "i2i" && lines[i + 1].opcode == "store")
		{
			if (lines[i].operands[1] == lines[i + 1].operands[0])
			{
				raw[i] = "store " + lines[i].operands[0] + " " + lines[i + 1].operands[1];
				raw[i + 1] = "i2i " + lines[i].operands[0] + " " + lines[i].operands[1];
				lines[i] = get_context(raw[i], i);
				lines[i + 1] = get_context(raw[i + 1], i + 1);
			}
		}
		if ((lines[i].opcode == "add" || lines[i].opcode == "sub" || lines[i].opcode == "mul" || lines[i].opcode == "div") && lines[i + 1].opcode == "i2i")
		{
			if (lines[i].operands[2] == lines[i + 1].operands[0])
			{
				raw[i] = lines[i].opcode + " " + lines[i].operands[0] + " " + lines[i].operands[1] + " " + lines[i + 1].operands[1];
				raw[i + 1] = lines[i + 1].opcode + " " + lines[i + 1].operands[1] + " " + lines[i].operands[2];
				lines[i] = get_context(raw[i], i);
				lines[i + 1] = get_context(raw[i + 1], i + 1);
			}
		}if ((lines[i].opcode == "addI" || lines[i].opcode == "subI" || lines[i].opcode == "mulI" || lines[i].opcode == "divI") && lines[i + 1].opcode == "i2i")
		{
			if (lines[i].operands[2] == lines[i + 1].operands[0])
			{
				raw[i] = lines[i].opcode + " " + lines[i].operands[0] + " " + lines[i].operands[1] + " " + lines[i + 1].operands[1];
				raw[i + 1] = lines[i + 1].opcode + " " + lines[i + 1].operands[1] + " " + lines[i].operands[2];
				lines[i] = get_context(raw[i], i);
				lines[i + 1] = get_context(raw[i + 1], i + 1);
			}
		}
		if (i + 1 < lines.size() && lines[i].opcode == "loadI" && lines[i + 1].opcode == "i2i")
		{
			if (lines[i].operands[1] == lines[i + 1].operands[0])
			{
				raw[i] = "loadI " + lines[i].operands[0] + " " + lines[i + 1].operands[1];
				raw[i + 1] = "i2i " + lines[i + 1].operands[1] + " " + lines[i].operands[1];
				lines[i] = get_context(raw[i], i);
				lines[i + 1] = get_context(raw[i + 1], i + 1);
			}
		}
	}
}

int main()
{
	init();
	reorder();
	unreachable_elimination();
	useless_elimination();
	useless_control_flow_elimination();
	output();
	return 0;
}