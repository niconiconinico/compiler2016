#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <map>
#include <set>
#include <queue>

using namespace std;

//初始关键语句
/*
	1. 函数返回
	2. 堆的写入
	3. 输入输出
	4. 参数
	5. main入口
*/

class operation
{
public:
	string opcode;
	vector<string> operands;
	operation()
	{
		opcode = "";
		operands.clear();
	}
};
class Block
{
public:
	int leading, ending;
	int blockid;
};
class Graph
{
public:
	vector<vector<int> > nxt;
	void init(int n)
	{
		nxt.clear();
		nxt.resize(n);
	}
	void add(int x, int y)
	{
		nxt[x].push_back(y);
	}
}rCFG;


map<string, int> label_pc;
vector<int> mapping_lines2block;
vector<Block> blocks;
vector<string> raw;
int S;
vector<operation> lines;

operation get_context(string info, int pc)
{
	if (info == "")
	{
		return operation();
	}
	if (info[0] == '@')
	{
		operation ret;
		ret.opcode = "label";
		if (info.substr(1, 8) == "function")
		{
			int cur = 0;
			string tmp;
			bool string_mode = false;
			while (cur < info.size())
			{
				if (!string_mode && info[cur] == ' ')
				{
					break;
				}
				if (info[cur] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
				}
				else if (info[cur] == '-')
				{
					tmp = tmp + "sub_";
				}
				else if (info[cur] == ' ')
				{
					tmp = tmp + "space_";
				}
				else if (info[cur] == '\\')
				{
					tmp = tmp + "backslash_";
				}
				else if (info[cur] == ':')
				{
					tmp = tmp + "colon_";
				}
				else if (info[cur] == '!')
				{
					tmp = tmp + "exg_";
				}
				else
					tmp = tmp + info[cur];
				cur++;
			}
			ret.operands.push_back(tmp);
			label_pc[tmp] = pc;
			while (cur < info.size())
			{
				tmp = "";
				cur++;
				string_mode = false;
				while (cur < info.size())
				{
					if (!string_mode && info[cur] == ' ')
					{
						break;
					}
					if (info[cur] == '\"')
					{
						string_mode ^= 1;
						tmp = tmp + "quote_";
						cur++;
					}
					else if (info[cur] == '-')
					{
						tmp = tmp + "sub_";
						cur++;
					}
					else if (info[cur] == ' ')
					{
						tmp = tmp + "space_";
						cur++;
					}
					else if (info[cur] == '\\')
					{
						tmp = tmp + "backslash_";
						cur++;
					}
					else if (info[cur] == ':')
					{
						tmp = tmp + "colon_";
						cur++;
					}
					else if (info[cur] == '!')
					{
						tmp = tmp + "exg_";
						cur++;
					}
					else
						tmp.push_back(info[cur++]);
				}
				ret.operands.push_back(tmp);
			}
		}
		else
		{
			info.pop_back();
			label_pc[info] = pc;
			ret.operands.push_back(info);
		}
		return ret;
	}
	else
	{
		operation ret;
		int pos = 0;
		bool string_mode = false;
		while (pos < info.size())
		{
			if (info[pos] == ' ' && !string_mode)
				break;
			if (info[pos] == '\"')
			{
				string_mode ^= 1;
				ret.opcode = ret.opcode + "quote_";
			}
			else if (info[pos] == '-')
			{
				ret.opcode = ret.opcode + "sub_";
			}
			else if (info[pos] == ' ')
			{
				ret.opcode = ret.opcode + "space_";
			}
			else if (info[pos] == '\\')
			{
				ret.opcode = ret.opcode + "backslash_";
			}
			else if (info[pos] == ':')
			{
				ret.opcode = ret.opcode + "colon_";
			}
			else if (info[pos] == '!')
			{
				ret.opcode = ret.opcode + "exg_";
			}
			else
				ret.opcode.push_back(info[pos]);
			pos++;
		}
		while (pos < info.size())
		{
			string_mode = false;
			string tmp = "";
			pos++;
			while (pos < info.size())
			{
				if (info[pos] == ' ' && !string_mode)
					break;
				if (info[pos] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
					pos++;
				}
				else if (info[pos] == '-')
				{
					tmp = tmp + "sub_";
					pos++;
				}
				else if (info[pos] == ' ')
				{
					tmp = tmp + "space_";
					pos++;
				}
				else if (info[pos] == '\\')
				{
					tmp = tmp + "backslash_";
					pos++;
				}
				else if (info[pos] == ':')
				{
					tmp = tmp + "colon_";
					pos++;
				}
				else if (info[pos] == '!')
				{
					tmp = tmp + "exg_";
					pos++;
				}
				else
					tmp.push_back(info[pos++]);
			}
			ret.operands.push_back(tmp);
			if (ret.opcode == "call" && (ret.operands[0] == "@functionLabel_print_constant" || ret.operands[0] == "@functionLabel_println_constant"))
			{
				pos++;
				string temp;
				while (pos < info.size())
				{
					temp.push_back(info[pos]);
					pos++;
				}
				ret.operands.push_back(temp);
				return ret;
			}
		}
		return ret;
	}
}

ifstream ir_in("ll_after.ir");
ifstream cfg_in("control_flow_graph");
ifstream blocks_in("blocks");
ofstream out;
vector<bool> isKey;

void initir()
{
	string tmp;
	int pc = 0;
	while (getline(ir_in, tmp))
	{
		raw.push_back(tmp);
		lines.push_back(get_context(tmp, pc++));
	}
	isKey.resize(lines.size());
	for (int i = 0; i < isKey.size(); ++i)
		isKey[i] = false;
	ir_in.close();
}

void initblocks()
{
	int a, b, c;
	int id;
	while (blocks_in >> a >> b >> c)
	{
		Block tmp;
		tmp.blockid = a;
		tmp.leading = b;
		tmp.ending = c;
		blocks.push_back(tmp);
		for (int j = b; j <= c; ++j)
			mapping_lines2block.push_back(a);
	}
	blocks_in.close();
}

void initcfg()
{
	int n;
	cfg_in >> n;
	rCFG.init(n);
	for (int i = 0; i < n; ++i)
	{
		int m, id;
		cfg_in >> id >> m;
		for (int j = 0; j < m; ++j)
		{
			int y;
			cfg_in >> y;
			y = abs(y);
			rCFG.add(y, i);
		}
	}
}


void init()
{
	initir();
	initblocks();
	initcfg();
}

void pass1()
{
	for (int i = 0; i < lines.size(); ++i)
	{
		//output or input
		//parameter
		if (lines[i].opcode == "call")
		{
			isKey[i] = true;
		}
		//heap write
		if (lines[i].opcode == "malloc" || lines[i].opcode == "mallocI" || lines[i].opcode == "store" || lines[i].opcode == "storeAI" || lines[i].opcode == "storeA0" || lines[i].opcode == "mallocStatic")
		{
			isKey[i] = true;
		}
		//return
		if (lines[i].opcode == "return")
			isKey[i] = true;
		//main entry
		if (lines[i].opcode == "label")
		{
			isKey[i] = true;
		}
		//branch
		if (lines[i].opcode == "jumpI" || lines[i].opcode == "jump" || lines[i].opcode == "cbr" || lines[i].opcode == "cbr_lt" || lines[i].opcode == "cbr_le" || lines[i].opcode == "cbr_gt" || lines[i].opcode == "cbr_ge" || lines[i].opcode == "cbr_eq" || lines[i].opcode == "cbr_ne")
		{
			isKey[i] = true;
		}
	}
}

pair<vector<string>, vector<string> > src_tar(operation instr)
{
	vector<string> src_regs;
	vector<string> tar_regs;
	if (instr.opcode == "")
	{
		return make_pair(src_regs, tar_regs);
	}
	if (instr.opcode == "label")
	{
		return make_pair(src_regs, tar_regs);
	}
	else if (instr.opcode == "mallocStatic")
	{
		tar_regs.push_back("r_{global_base_address_register^_^}");
	}
	else if (instr.opcode == "mallocI")
	{
		tar_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "malloc")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "call")
	{
		for (int i = 1; i < instr.operands.size(); ++i)
			src_regs.push_back(instr.operands[i]);
		tar_regs.push_back("r_{global_return_register}");
	}
	else if (instr.opcode == "return")
	{
		src_regs.push_back(instr.operands[0]);
	}
	else if (instr.opcode == "nop")
	{
		return make_pair(src_regs, tar_regs);
	}
	else if (instr.opcode == "add")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "sub")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "mult")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "div")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "addI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "subI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "rsubI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "multI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "divI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "rdivI")
	{
		assert(false);
	}
	else if (instr.opcode == "lshift")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "rshift")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "lshiftI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "rshiftI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "and")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "or")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "xor")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "andI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "orI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "xorI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "loadI")
	{
		tar_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "loadAI")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "loadA0")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "store")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "storeAI")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "storeA0")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		src_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "i2i")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "jump")
	{
		assert(false);
	}
	else if (instr.opcode == "jumpI")
	{
		return make_pair(src_regs, tar_regs);
	}
	else if (instr.opcode == "cbr")
	{
		src_regs.push_back(instr.operands[0]);
	}
	else if (instr.opcode == "cmp_LT")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "cmp_LE")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "cmp_EQ")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "cmp_GE")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "cmp_GT")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "cmp_NE")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
		tar_regs.push_back(instr.operands[2]);
	}
	else if (instr.opcode == "load")
	{
		src_regs.push_back(instr.operands[0]);
		tar_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "cbr_lt")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "cbr_le")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "cbr_gt")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "cbr_ge")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "cbr_eq")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
	}
	else if (instr.opcode == "cbr_ne")
	{
		src_regs.push_back(instr.operands[0]);
		src_regs.push_back(instr.operands[1]);
	}
	else
		assert(false);
	return make_pair(src_regs, tar_regs);
}

vector<bool> visit;

void track(int pc, set<string> keyreg)
{
	if (visit[pc])
		return;
	visit[pc] = true;
	pair<vector<string>, vector<string> > info = src_tar(lines[pc]);
	for (int i = 0; i < info.second.size(); ++i)
		if (keyreg.find(info.second[i]) != keyreg.end())
		{
			isKey[pc] = true;
			for (int j = 0; j < info.first.size(); ++j)
				keyreg.insert(info.first[j]);
		}
	int curblk = mapping_lines2block[pc];
	if (pc == blocks[curblk].leading)
	{
		for (int i = 0; i < rCFG.nxt[curblk].size(); ++i)
		{
			int y = rCFG.nxt[curblk][i];
			track(blocks[y].ending , keyreg);
		}
	}
	else
	{
		track(pc - 1, keyreg);
	}
}

void pass2()
{
	for (int i = 0; i < lines.size(); ++i)
	{
		if (isKey[i])
		{
			set<string> keyreg;
			pair<vector<string>, vector<string> > info = src_tar(lines[i]);
			for (int i = 0; i < info.first.size(); ++i)
				keyreg.insert(info.first[i]);
			int blk = mapping_lines2block[i];
			visit.resize(lines.size());
			for (int j = 0; j < lines.size(); ++j)
				visit[j] = false;
			if (i == blocks[blk].leading)
			{
				for (int j = 0; j < rCFG.nxt[blk].size(); ++j)
				{
					int y = rCFG.nxt[blk][j];
					track(blocks[y].ending, keyreg);
				}
			}
			else
				track(i - 1, keyreg);
		}
	}
}

void output()
{
	out = ofstream("ll_after.ir");
	for (int i = 0; i < lines.size(); ++i)
		if (isKey[i])
			out << raw[i] << endl;
	out.close();
}

int main()
{
	init();
	pass1();
	pass2();
	output();
	return 0;
}