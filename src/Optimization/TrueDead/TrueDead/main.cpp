#include <cstdio>
#include <cstring>
#include <iostream>
#include <cstring>
#include <vector>
#include <string>
#include <map>
#include <fstream>

using namespace std;

class operation
{
public:
	string opcode;
	vector<string> operands;
	vector<int> real_operands;
};

operation get_context(string info, int pc)
{
	//remeber, there is ANOTHER COPY in CFG
	if (info == "")
	{
		operation ret;
		ret.opcode = "null";
		return ret;
	}
	if (info[info.size() - 1] == ':')
	{
		operation ret;
		ret.opcode = "label";
		ret.operands.push_back(info);
		return ret;
	}
	else
	{
		operation ret;
		int pos = 0;
		bool string_mode = false;
		while (pos < info.size())
		{
			if (info[pos] == ' ' && !string_mode)
				break;
			if (info[pos] == '\"')
			{
				string_mode ^= 1;
				ret.opcode = ret.opcode + "quote_";
			}
			ret.opcode.push_back(info[pos]);
			pos++;
		}
		while (pos < info.size())
		{
			string_mode = false;
			string tmp = "";
			pos++;
			while (pos < info.size())
			{
				if (info[pos] == ' ' && !string_mode)
					break;
				if (info[pos] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
					pos++;
				}
				tmp.push_back(info[pos++]);
			}
			while (tmp.size() > 0 && tmp[tmp.size() - 1] == ',')
				tmp.pop_back();
			ret.operands.push_back(tmp);
		}
		return ret;
	}
}

vector<bool> del;

map<int, string> f;
map<string, int> label_pc;

string find(string label)
{
	int pc = label_pc[label];
	if (f[pc] != "")
		return find(f[pc]);
	else
		return label;
}

int main()
{
	vector<string> lines;
	vector<operation> ops;
	ifstream s_in("lovelive.s");
	ofstream s_out;
	string tmp;
	while (getline(s_in, tmp))
	{
		if (tmp != "nop")
		{
			lines.push_back(tmp);
			ops.push_back(get_context(tmp, -1));
		}
	}
	del.resize(lines.size());
	s_in.close();

	for (int i = 0; i < lines.size(); ++i)
	{
		if (ops[i].opcode == "add" || ops[i].opcode == "sub")
		{
			if (ops[i].operands[2] == "0"  && ops[i].operands[0] == ops[i].operands[1])
				del[i] = true;
		}
		if (ops[i].opcode == "mul" || ops[i].opcode == "div")
		{
			if (ops[i].operands[2] == "1" && ops[i].operands[0] == ops[i].operands[1])
				del[i] = true;
		}

		if (i + 1 < lines.size() && ops[i].opcode == "lw" && ops[i + 1].opcode == "move")
		{
			if (ops[i].operands[0] == ops[i + 1].operands[0])
				del[i] = true;
		}
	}

	for (int i = 0; i < lines.size(); )
	{
		if (ops[i].opcode == "label" && ops[i].operands[0].size() >= 10 && ops[i].operands[0].substr(0, 10) == "Atfunction")
		{
			bool isLeaf = true;
			int j;
			int isave = i;
			for (j = i + 1; j < lines.size(); ++j)
			{
				if (ops[j].opcode == "label" && ((ops[j].operands[0].size() >= 10 && ops[j].operands[0].substr(0, 10) == "Atfunction") || ops[j].operands[0] == "main:"))
				{
					i = j;
					break;
				}
				if (ops[j].opcode == "jal")
					isLeaf = false;
			}
			if (isLeaf)
			{
				for (int p = isave; p < i && p < lines.size(); ++p)
				{
					if (lines[p] == "sw $31, ($sp)")
						del[p] = true;
					if (lines[p] == "lw $31, 4($sp)")
						del[p] = true;
				}
			}
			if (j == lines.size())
				break;
		}
		else
			i++;
	}

	for (int i = 0; i < lines.size(); ++i)
	{
		if (ops[i].opcode == "j")
		{
			f[i] = ops[i].operands[0];
		}
		if (i + 1 < lines.size() && ops[i].opcode == "label")
		{
			if (ops[i].operands[0].size() < 10 || (ops[i].operands[0].substr(0, 10) != "Atfunction"))
			{
				label_pc[ops[i].operands[0].substr(0, ops[i].operands[0].size() - 1)] = i;
				if (ops[i + 1].opcode == "label")
				{
					if (ops[i + 1].operands[0].size() < 10 || (ops[i].operands[0].substr(0, 10) != "Atfunction"))
					{
						f[i] = ops[i + 1].operands[0].substr(0, ops[i + 1].operands[0].size() - 1);
					}
				}
				if (ops[i + 1].opcode == "j")
				{
					f[i] = ops[i + 1].operands[0];
				}
			}
		}
	}
	
	for (int i = 0; i < lines.size(); ++i)
	{
		if (ops[i].opcode == "j")
		{
			string tag = find(ops[i].operands[0]);
			lines[i] = "j " + tag;
			ops[i] = get_context(lines[i], i);
		}
	}

	s_out = ofstream("lovelive.s");

	for (int i = 0; i < lines.size(); ++i)
	{
		if(!del[i])
			s_out << lines[i] << endl;
	}

	s_out.close();
	return 0;
}