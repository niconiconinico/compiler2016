#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <fstream>
#include <cassert>

using namespace std;

const int hashSize = 10;

unsigned long long hashModlist[] = { 1000000007, 1000000009, 367040799, 21054143321LL, 21473678599LL };

class Hash
{
public:
	vector<unsigned long long> hashVal;
	vector<unsigned long long> hashMod;
	string reg;
	void init()
	{
		hashMod.resize(hashSize);
		hashVal.resize(hashSize);
		for (int i = 0; i < min(hashSize, 5); ++i)
			hashMod[i] = (hashModlist[i]);
		if (hashSize > 5)
		{
			for (int i = 5; i < hashSize; ++i)
			{
				hashMod[i] = (i + hashModlist[4]);
			}
		}
	}
	bool operator < (const Hash &b) const
	{
		return hashVal < b.hashVal;
	}

	bool operator == (const Hash &b) const
	{
		return hashVal == b.hashVal;
	}
	Hash(string x)
	{
		init();
		long long pow = 1;
		for (int i = 0; i < hashSize; ++i)
		{
			pow = 1;
			for (int j = 0; j < x.size(); ++j)
				hashVal[i] += x[j] * pow, pow = pow * 121;
			hashVal[i] %= hashMod[i];
		}
	}
	Hash(int x)
	{
		init();
		for (int i = 0; i < hashSize; ++i)
			hashVal[i] = (long long)x * x % hashMod[i];
	}
	Hash() { init(); }
};

Hash assignNew(string reg_name)
{
	Hash ret;
	ret.reg = reg_name;
	for (int i = 0; i < ret.hashVal.size(); ++i)
		ret.hashVal[i] = (rand() * rand()) % ret.hashMod[i];
	return ret;
}

Hash add(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = ((a.hashVal[i] ^ b.hashVal[i]) + (a.hashVal[i] * b.hashVal[i])) % ret.hashMod[i];
	return ret;
}
Hash sub(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = ((a.hashVal[i] ^ b.hashVal[i]) + (a.hashVal[i] * b.hashVal[i]) - (a.hashVal[i] ^ i) ^ (b.hashVal[i] ^ i) + b.hashVal[i]) % ret.hashMod[i];
	return ret;
}
Hash mult(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = ((a.hashVal[i] + b.hashVal[i]) + (a.hashVal[i] * b.hashVal[i])) % ret.hashMod[i];
	return ret;
}
Hash div(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = ((a.hashVal[i] & b.hashVal[i]) + (a.hashVal[i] + b.hashVal[i]) * (a.hashVal[i] - b.hashVal[i]) + a.hashVal[i] * a.hashVal[i]) % ret.hashMod[i];
	return ret;
}
Hash lshift(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = ((a.hashVal[i] & b.hashVal[i]) + (a.hashVal[i] + b.hashVal[i]) * (a.hashVal[i] ^ b.hashVal[i]) - b.hashVal[i]) % ret.hashMod[i];
	return ret;

}
Hash rshift(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = ((a.hashVal[i] & b.hashVal[i]) ^ (a.hashVal[i] | b.hashVal[i]) * (a.hashVal[i] & b.hashVal[i]) + (a.hashVal[i] - b.hashVal[i])) % ret.hashMod[i];
	return ret;

}
Hash And (Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = (a.hashVal[i] * a.hashVal[i] + b.hashVal[i] * b.hashVal[i]) % ret.hashMod[i];
	return ret;

}
Hash Or (Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = ((a.hashVal[i] / b.hashVal[i]) + (a.hashVal[i] % b.hashVal[i]) * (a.hashVal[i] + b.hashVal[i])) % ret.hashMod[i];
	return ret;

}
Hash Xor(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = ((a.hashVal[i] & b.hashVal[i]) ^ (a.hashVal[i] % b.hashVal[i]) * (a.hashVal[i] & b.hashVal[i])) % ret.hashMod[i];
	return ret;

}
Hash cmp_LT(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = (a.hashVal[i] * a.hashVal[i] * a.hashVal[i] + b.hashVal[i] * b.hashVal[i] + a.hashVal[i] ^ b.hashVal[i] - a.hashVal[i]) % ret.hashMod[i];
	return ret;
}
Hash cmp_LE(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = (a.hashVal[i] ^ (a.hashVal[i] * a.hashVal[i] * a.hashVal[i] * a.hashVal[i] - b.hashVal[i] * b.hashVal[i]) + (a.hashVal[i] ^ b.hashVal[i]) * (a.hashVal[i] & b.hashVal[i])) % ret.hashMod[i];
	return ret;
}
Hash cmp_GT(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = (a.hashVal[i] * a.hashVal[i] * a.hashVal[i] * a.hashVal[i] - b.hashVal[i] * b.hashVal[i] + (a.hashVal[i] ^ b.hashVal[i]) * (a.hashVal[i] & b.hashVal[i])) % ret.hashMod[i];
	return ret;
}
Hash cmp_GE(Hash a, Hash b)
{

	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = (a.hashVal[i] * a.hashVal[i] * a.hashVal[i] * a.hashVal[i] + b.hashVal[i] * b.hashVal[i] + (a.hashVal[i] ^ b.hashVal[i]) * (a.hashVal[i] & b.hashVal[i]) + (a.hashVal[i] ^ i) % b.hashVal[i]) % ret.hashMod[i];
	return ret;
}
Hash cmp_eq(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = (a.hashVal[i] & a.hashVal[i] * a.hashVal[i] * a.hashVal[i] + b.hashVal[i] * b.hashVal[i] + (a.hashVal[i] ^ b.hashVal[i]) * (a.hashVal[i] & b.hashVal[i]) + (a.hashVal[i] ^ i) % b.hashVal[i] + b.hashVal[i] * i) % ret.hashMod[i];
	return ret;
}
Hash cmp_ne(Hash a, Hash b)
{
	Hash ret;
	for (int i = 0; i < a.hashVal.size(); ++i)
		ret.hashVal[i] = (a.hashVal[i] * a.hashVal[i] * a.hashVal[i] * a.hashVal[i] + b.hashVal[i] * b.hashVal[i] + (a.hashVal[i] ^ b.hashVal[i]) * (a.hashVal[i] & b.hashVal[i]) + (a.hashVal[i] ^ i) % b.hashVal[i] - b.hashVal[i] * i) % ret.hashMod[i];
	return ret;
}

class operation
{
public:
	string opcode;
	vector<string> operands;
	operation()
	{
		opcode = "";
		operands.clear();
	}
};
class Block
{
public:
	int leading, ending;
	int blockid;
};


operation get_context(string info, int pc)
{
	if (info == "")
	{
		return operation();
	}
	if (info[0] == '@')
	{
		operation ret;
		ret.opcode = "label";
		if (info.substr(1, 8) == "function")
		{
			int cur = 0;
			string tmp;
			bool string_mode = false;
			while (cur < info.size())
			{
				if (!string_mode && info[cur] == ' ')
				{
					break;
				}
				if (info[cur] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
				}
				else if (info[cur] == '-')
				{
					tmp = tmp + "sub_";
				}
				else if (info[cur] == ' ')
				{
					tmp = tmp + "space_";
				}
				else if (info[cur] == '\\')
				{
					tmp = tmp + "backslash_";
				}
				else if (info[cur] == ':')
				{
					tmp = tmp + "colon_";
				}
				else if (info[cur] == '!')
				{
					tmp = tmp + "exg_";
				}
				else
					tmp = tmp + info[cur];
				cur++;
			}
			ret.operands.push_back(tmp);
			while (cur < info.size())
			{
				tmp = "";
				cur++;
				string_mode = false;
				while (cur < info.size())
				{
					if (!string_mode && info[cur] == ' ')
					{
						break;
					}
					if (info[cur] == '\"')
					{
						string_mode ^= 1;
						tmp = tmp + "quote_";
						cur++;
					}
					else if (info[cur] == '-')
					{
						tmp = tmp + "sub_";
						cur++;
					}
					else if (info[cur] == ' ')
					{
						tmp = tmp + "space_";
						cur++;
					}
					else if (info[cur] == '\\')
					{
						tmp = tmp + "backslash_";
						cur++;
					}
					else if (info[cur] == ':')
					{
						tmp = tmp + "colon_";
						cur++;
					}
					else if (info[cur] == '!')
					{
						tmp = tmp + "exg_";
						cur++;
					}
					else
						tmp.push_back(info[cur++]);
				}
				ret.operands.push_back(tmp);
			}
		}
		else
		{
			info.pop_back();
			ret.operands.push_back(info);
		}
		return ret;
	}
	else
	{
		operation ret;
		int pos = 0;
		bool string_mode = false;
		while (pos < info.size())
		{
			if (info[pos] == ' ' && !string_mode)
				break;
			if (info[pos] == '\"')
			{
				string_mode ^= 1;
				ret.opcode = ret.opcode + "quote_";
			}
			else if (info[pos] == '-')
			{
				ret.opcode = ret.opcode + "sub_";
			}
			else if (info[pos] == ' ')
			{
				ret.opcode = ret.opcode + "space_";
			}
			else if (info[pos] == '\\')
			{
				ret.opcode = ret.opcode + "backslash_";
			}
			else if (info[pos] == ':')
			{
				ret.opcode = ret.opcode + "colon_";
			}
			else if (info[pos] == '!')
			{
				ret.opcode = ret.opcode + "exg_";
			}
			else
				ret.opcode.push_back(info[pos]);
			pos++;
		}
		while (pos < info.size())
		{
			string_mode = false;
			string tmp = "";
			pos++;
			while (pos < info.size())
			{
				if (info[pos] == ' ' && !string_mode)
					break;
				if (info[pos] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
					pos++;
				}
				else if (info[pos] == '-')
				{
					tmp = tmp + "sub_";
					pos++;
				}
				else if (info[pos] == ' ')
				{
					tmp = tmp + "space_";
					pos++;
				}
				else if (info[pos] == '\\')
				{
					tmp = tmp + "backslash_";
					pos++;
				}
				else if (info[pos] == ':')
				{
					tmp = tmp + "colon_";
					pos++;
				}
				else if (info[pos] == '!')
				{
					tmp = tmp + "exg_";
					pos++;
				}
				else
					tmp.push_back(info[pos++]);
			}
			ret.operands.push_back(tmp);
		}
		return ret;
	}
}


vector<int> mapping_lines2block;
vector<Block> blocks;
vector<string> raw;
vector<operation> lines;

void init()
{
	ifstream blo_in("blocks");
	ifstream ir_in("ll_after.ir");
	string tmp;
	while (getline(ir_in, tmp))
	{
		raw.push_back(tmp);
		lines.push_back(get_context(tmp, -1));
	}
	ir_in.close();
	int a, b, c;
	mapping_lines2block.resize(lines.size());
	while (blo_in >> a >> b >> c)
	{
		Block bb;
		bb.blockid = a;
		bb.leading = b;
		bb.ending = c;
		blocks.push_back(bb);
		for (int i = b; i <= c; ++i)
			mapping_lines2block[i] = a;
	}
}

Hash find(string name, vector<Hash> &x)
{
	for (int i = 0; i < x.size(); ++i)
		if (x[i].reg == name)
			return x[i];
	Hash h = assignNew(name);
	x.push_back(h);
	return h;
}

vector<bool> isKey;

int main()
{
	srand(10);
	init();
	isKey.resize(lines.size());
	for (int i = 0; i < isKey.size(); ++i)
		isKey[i] = true;
	for (int j = 0; j < blocks.size(); ++j)
	{
		vector<Hash> cached_regs;
		for (int i = blocks[j].leading; i <= blocks[j].ending; ++i)
		{
			int ii = i;
			operation &instr = lines[ii];
			if (instr.opcode == "")
				continue;
			if (instr.opcode == "label")
			{
				continue;
			}
			else if (instr.opcode == "mallocStatic")
			{
				Hash h;
				h = assignNew("r_{global_base_address_register^_^}");
				cached_regs.push_back(h);
				continue;
			}
			else if (instr.opcode == "mallocI")
			{
				bool found = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (cached_regs[i].reg == lines[ii].operands[1])
						cached_regs[i] = assignNew(cached_regs[i].reg), found = true;
				if (!found)
				{
					Hash h = assignNew(instr.operands[1]);
					cached_regs.push_back(h);
				}
			}
			else if (instr.opcode == "malloc")
			{
				bool found = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (cached_regs[i].reg == lines[ii].operands[1])
						cached_regs[i] = assignNew(cached_regs[i].reg), found = true;
				if (!found)
				{
					Hash h = assignNew(lines[ii].operands[1]);
					cached_regs.push_back(h);
				}
			}
			else if (instr.opcode == "call")
			{
				bool found = false;
				string name = "r_{global_return_register}";
				for (int i = 0; i < cached_regs.size(); ++i)
					if (cached_regs[i].reg == name)
						cached_regs[i] = assignNew(name), found = true;
				if (!found)
				{
					Hash h = assignNew(name);
					cached_regs.push_back(h);
				}
			}
			else if (instr.opcode == "return")
			{
				continue;
			}
			else if (instr.opcode == "nop")
			{
				continue;
			}
			else if (instr.opcode == "add")
			{
				Hash res = add(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "sub")
			{
				Hash res = sub(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "mult")
			{
				Hash res = mult(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "div")
			{
				Hash res = div(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "addI")
			{
				Hash res = add(find(instr.operands[0], cached_regs), instr.operands[1]);
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "subI")
			{
				Hash res = sub(find(instr.operands[0], cached_regs), instr.operands[1]);
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "rsubI")
			{
				Hash res = sub(instr.operands[1], find(instr.operands[0], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "multI")
			{
				Hash res = mult(find(instr.operands[0], cached_regs), instr.operands[1]);
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "divI")
			{
				Hash res = div(find(instr.operands[0], cached_regs), instr.operands[1]);
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "rdivI")
			{
				assert(false);
			}
			else if (instr.opcode == "lshift")
			{
				Hash res = lshift(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "rshift")
			{
				Hash res = rshift(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "lshiftI")
			{
				Hash res = lshift(find(instr.operands[0], cached_regs), instr.operands[1]);
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "rshiftI")
			{
				Hash res = rshift(find(instr.operands[0], cached_regs), instr.operands[1]);
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "and")
			{
				Hash res = And(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "or")
			{
				Hash res = Or(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "xor")
			{
				Hash res = Xor(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "andI")
			{
				Hash res = And(find(instr.operands[0], cached_regs), (instr.operands[1]));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "orI")
			{
				Hash res = Or(find(instr.operands[0], cached_regs), (instr.operands[1]));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "xorI")
			{
				Hash res = Xor(find(instr.operands[0], cached_regs), (instr.operands[1]));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "loadI")
			{
				Hash res = instr.operands[0];
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
				{
					if (cached_regs[i] == res)
					{
						foundReplace = true;
					}
				}
				if (!foundReplace)
				{
					res.reg = instr.operands[1];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "loadAI")
			{
				Hash res = assignNew(instr.operands[2]);
				bool found = false;
				for (int i = 0; i < cached_regs.size(); ++i)
				{
					if (cached_regs[i].reg == res.reg)
					{
						cached_regs[i] = res;
						found = true;
					}
				}
				if (!found)
				{
					cached_regs.push_back(res);
				}
			}
			else if (instr.opcode == "loadA0")
			{
				Hash res = assignNew(instr.operands[2]);
				bool found = false;
				for (int i = 0; i < cached_regs.size(); ++i)
				{
					if (cached_regs[i].reg == res.reg)
					{
						cached_regs[i] = res;
						found = true;
					}
				}
				if (!found)
				{
					cached_regs.push_back(res);
				}
			}
			else if (instr.opcode == "store")
			{
				continue;
			}
			else if (instr.opcode == "storeAI")
			{
				continue;
			}
			else if (instr.opcode == "storeA0")
			{
				continue;
			}
			else if (instr.opcode == "i2i")
			{
				Hash r0, r1;
				bool foundr0 = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (cached_regs[i].reg == instr.operands[0])
					{
						foundr0 = true;
						r0 = cached_regs[i];
					}
				if (!foundr0)
				{
					r0 = assignNew(instr.operands[0]);
					cached_regs.push_back(r0);
				}
				bool foundr1 = false;
				for (int i = 0; i < cached_regs.size(); ++i)
				{
					if (cached_regs[i].reg == instr.operands[1])
					{
						foundr1 = true;
						if (cached_regs[i] == r0)
						{
							isKey[ii] = false;
						}
						else
						{
							for (vector<Hash>::iterator p = cached_regs.begin(); p != cached_regs.end(); p++)
							{
								if (p->reg == instr.operands[1])
								{
									cached_regs.erase(p);
									break;
								}
							}
						}
					}
				}
				if (!foundr1)
				{
					//nothing to do, since r0 has a copy
				}
			}
			else if (instr.opcode == "jump")
			{
				assert(false);
			}
			else if (instr.opcode == "jumpI")
			{
				continue;
			}
			else if (instr.opcode == "cbr")
			{
				continue;
			}
			else if (instr.opcode == "cmp_LT")
			{
				Hash res = cmp_LT(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "cmp_LE")
			{
				Hash res = cmp_LE(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "cmp_EQ")
			{
				Hash res = cmp_eq(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "cmp_GE")
			{
				Hash res = cmp_GE(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "cmp_GT")
			{
				Hash res = cmp_GT(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "cmp_NE")
			{
				Hash res = cmp_ne(find(instr.operands[0], cached_regs), find(instr.operands[1], cached_regs));
				bool foundReplace = false;
				for (int i = 0; i < cached_regs.size(); ++i)
					if (res == cached_regs[i])
					{
						foundReplace = true;
						raw[ii] = "i2i " + cached_regs[i].reg + " " + instr.operands[2];
					}
				if (!foundReplace)
				{
					res.reg = instr.operands[2];
					bool found = false;
					for (int i = 0; i < cached_regs.size(); ++i)
					{
						if (cached_regs[i].reg == res.reg)
						{
							cached_regs[i] = res;
							found = true;
						}
					}
					if (!found)
					{
						cached_regs.push_back(res);
					}
				}
			}
			else if (instr.opcode == "load")
			{
				Hash res = assignNew(instr.operands[1]);
				bool found = false;
				for (int i = 0; i < cached_regs.size(); ++i)
				{
					if (cached_regs[i].reg == res.reg)
					{
						found = true;
						cached_regs[i] = res;
					}
				}
				if (!found)
				{
					cached_regs.push_back(res);
				}
			}
			else if (instr.opcode == "cbr_lt")
			{
				continue;
			}
			else if (instr.opcode == "cbr_le")
			{
				continue;
			}
			else if (instr.opcode == "cbr_gt")
			{
				continue;
			}
			else if (instr.opcode == "cbr_ge")
			{
				continue;
			}
			else if (instr.opcode == "cbr_eq")
			{
				continue;
			}
			else if (instr.opcode == "cbr_ne")
			{
				continue;
			}
			else
				assert(false);
		}
	}
	ofstream out("ll_after.ir");
	for (int i = 0; i < lines.size(); ++i)
	{
		if (isKey[i])
			out << raw[i] << endl;
	}
	out.close();
	return 0;
}