#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <fstream>
#include <map>
#include <cassert>
#include <sstream>
#include <queue>

using namespace std;

const int Size = 32;

ofstream out;



ifstream in;

class operation
{
public:
	string opcode;
	vector<string> operands;
	operation()
	{
		opcode = "";
		operands.clear();
	}
};
class Block
{
public:
	int leading, ending;
	int blockid;
}; 
class Graph
{
public:
	vector<vector<int> > nxt;
	void init(int n)
	{
		nxt.clear();
		nxt.resize(n);
	}
	void add(int x, int y)
	{
		nxt[x].push_back(y);
	}
}CFG;

map<string, int> label_pc;
vector<int> mapping_lines2block;
vector<Block> blocks;
vector<string> raw;
int S;
vector<operation> lines;

operation get_context(string info, int pc)
{
	if (info == "")
	{
		return operation();
	}
	if (info[0] == '@')
	{
		operation ret;
		ret.opcode = "label";
		if (info.substr(1, 8) == "function")
		{
			int cur = 0;
			string tmp;
			bool string_mode = false;
			while (cur < info.size())
			{
				if (!string_mode && info[cur] == ' ')
				{
					break;
				}
				if (info[cur] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
				}
				else if (info[cur] == '-')
				{
					tmp = tmp + "sub_";
				}
				else if (info[cur] == ' ')
				{
					tmp = tmp + "space_";
				}
				else if (info[cur] == '\\')
				{
					tmp = tmp + "backslash_";
				}
				else if (info[cur] == ':')
				{
					tmp = tmp + "colon_";
				}
				else if (info[cur] == '!')
				{
					tmp = tmp + "exg_";
				}
				else
					tmp = tmp + info[cur];
				cur++;
			}
			ret.operands.push_back(tmp);
			label_pc[tmp] = pc;
			while (cur < info.size())
			{
				tmp = "";
				cur++;
				string_mode = false;
				while (cur < info.size())
				{
					if (!string_mode && info[cur] == ' ')
					{
						break;
					}
					if (info[cur] == '\"')
					{
						string_mode ^= 1;
						tmp = tmp + "quote_";
						cur++;
					}
					else if (info[cur] == '-')
					{
						tmp = tmp + "sub_";
						cur++;
					}
					else if (info[cur] == ' ')
					{
						tmp = tmp + "space_";
						cur++;
					}
					else if (info[cur] == '\\')
					{
						tmp = tmp + "backslash_";
						cur++;
					}
					else if (info[cur] == ':')
					{
						tmp = tmp + "colon_";
						cur++;
					}
					else if (info[cur] == '!')
					{
						tmp = tmp + "exg_";
						cur++;
					}
					else
						tmp.push_back(info[cur++]);
				}
				ret.operands.push_back(tmp);
			}
		}
		else
		{
			info.pop_back();
			label_pc[info] = pc;
			ret.operands.push_back(info);
		}
		return ret;
	}
	else
	{
		operation ret;
		int pos = 0;
		bool string_mode = false;
		while (pos < info.size())
		{
			if (info[pos] == ' ' && !string_mode)
				break;
			if (info[pos] == '\"')
			{
				string_mode ^= 1;
				ret.opcode = ret.opcode + "quote_";
			}
			else if (info[pos] == '-')
			{
				ret.opcode = ret.opcode + "sub_";
			}
			else if (info[pos] == ' ')
			{
				ret.opcode = ret.opcode + "space_";
			}
			else if (info[pos] == '\\')
			{
				ret.opcode = ret.opcode + "backslash_";
			}
			else if (info[pos] == ':')
			{
				ret.opcode = ret.opcode + "colon_";
			}
			else if (info[pos] == '!')
			{
				ret.opcode = ret.opcode + "exg_";
			}
			else
				ret.opcode.push_back(info[pos]);
			pos++;
		}
		while (pos < info.size())
		{
			string_mode = false;
			string tmp = "";
			pos++;
			while (pos < info.size())
			{
				if (info[pos] == ' ' && !string_mode)
					break;
				if (info[pos] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
					pos++;
				}
				else if (info[pos] == '-')
				{
					tmp = tmp + "sub_";
					pos++;
				}
				else if (info[pos] == ' ')
				{
					tmp = tmp + "space_";
					pos++;
				}
				else if (info[pos] == '\\')
				{
					tmp = tmp + "backslash_";
					pos++;
				}
				else if (info[pos] == ':')
				{
					tmp = tmp + "colon_";
					pos++;
				}
				else if (info[pos] == '!')
				{
					tmp = tmp + "exg_";
					pos++;
				}
				else
					tmp.push_back(info[pos++]);
			}
			ret.operands.push_back(tmp);
			if (ret.opcode == "call" && (ret.operands[0] == "@functionLabel_print_constant" || ret.operands[0] == "@functionLabel_println_constant"))
			{
				pos++;
				string temp;
				while (pos < info.size())
				{
					temp.push_back(info[pos]);
					pos++;
				}
				ret.operands.push_back(temp);
				return ret;
			}
		}
		return ret;
	}
}

vector<bool> visit;

pair<bool, int> isneed(string reg_id, int lineid, int stps)
{
	int blk = mapping_lines2block[lineid];
	if (visit[blk])
		return make_pair(false, -1);
	if (lineid == blocks[blk].leading)
		visit[blk] = true;
	for (int j = lineid; j <= blocks[blk].ending; ++j)
	{
		operation &instr = lines[j];
		if (instr.opcode == "")
			continue;
		vector<string> src_regs;
		vector<string> tar_regs;
		if (instr.opcode == "label")
		{
			continue;
		}
		else if (instr.opcode == "mallocStatic")
		{
			continue;
		}
		else if (instr.opcode == "mallocI")
		{
			if (reg_id == instr.operands[1])
				return make_pair(false, -1);
		}
		else if (instr.opcode == "malloc")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "call")
		{
			for (int i = 1; i < instr.operands.size(); ++i)
				src_regs.push_back(instr.operands[i]);
			tar_regs.push_back("r_{global_return_register}");
		}
		else if (instr.opcode == "return")
		{
			src_regs.push_back(instr.operands[0]);
		}
		else if (instr.opcode == "nop")
		{
			continue;
		}
		else if (instr.opcode == "add")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "sub")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "mult")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "div")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "addI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "subI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "rsubI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "multI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "divI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "rdivI")
		{
			assert(false);
		}
		else if (instr.opcode == "lshift")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "rshift")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "lshiftI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "rshiftI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "and")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "or")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "xor")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "andI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "orI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "xorI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "loadI")
		{
			tar_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "loadAI")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "loadA0")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "store")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "storeAI")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "storeA0")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			src_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "i2i")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "jump")
		{
			assert(false);
		}
		else if (instr.opcode == "jumpI")
		{
			continue;
		}
		else if (instr.opcode == "cbr")
		{
			src_regs.push_back(instr.operands[0]);
		}
		else if (instr.opcode == "cmp_LT")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "cmp_LE")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "cmp_EQ")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "cmp_GE")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "cmp_GT")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "cmp_NE")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
			tar_regs.push_back(instr.operands[2]);
		}
		else if (instr.opcode == "load")
		{
			src_regs.push_back(instr.operands[0]);
			tar_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "cbr_lt")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "cbr_le")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "cbr_gt")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "cbr_ge")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "cbr_eq")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
		}
		else if (instr.opcode == "cbr_ne")
		{
			src_regs.push_back(instr.operands[0]);
			src_regs.push_back(instr.operands[1]);
		}
		else
			assert(false);

		for (int i = 0; i < src_regs.size(); ++i)
			if (src_regs[i] == reg_id)
				return make_pair(true, stps);
		for (int i = 0; i < tar_regs.size(); ++i)
			if (tar_regs[i] == reg_id)
				return make_pair(false, -1);
		stps++;
	}
	int minres = 0x3f3f3f3f;
	for (int i = 0; i < CFG.nxt[blk].size(); ++i)
	{
		int y = CFG.nxt[blk][i];
		pair<bool, int> res = isneed(reg_id, blocks[y].leading, stps + 1);
		if(res.first)
			minres = min(minres, res.second);
	}
	if (minres != 0x3f3f3f3f)
		return make_pair(true, minres);
	return make_pair(false, -1);
}

bool needed(string reg_id, int lineid)
{
	visit.clear();
	visit.resize(blocks.size());
	return isneed(reg_id, lineid, 0).first;
}

int Dist(string reg_id, int lineid)
{
	visit.clear();
	visit.resize(blocks.size());
	int ret = isneed(reg_id, lineid, 0).second;
	if (ret == -1)
		ret = 0x3f3f3f3f;
	return ret + 1;
}

class Class
{
public:
	string Name[Size];
	long long Next[Size];
	bool Free[Size];
	bool forbid[Size];
	int Stack[Size];
	int StackTop;
	bool changed[Size];
	void init()
	{
		StackTop = -1;
		for (int i = 0; i < Size; ++i)
			Free[i] = true, forbid[i] = false, changed[i] = false;
		//forbidden to allocate
		forbid[7] = true;
		Free[7] = false;
		forbid[6] = true;
		Free[6] = false;
		Free[0] = Free[1] = Free[2] = Free[3] = Free[4] = forbid[5] = Free[8] = Free[9] = Free[10] = Free[26] = Free[27] = Free[28] = Free[29] = Free[30] = Free[31] = false;
		forbid[0] = forbid[1] = forbid[2] = forbid[3] = forbid[4] = forbid[5] = forbid[8] = forbid[9] = forbid[10] = forbid[26] = forbid[27] = forbid[28] = forbid[29] = forbid[30] = forbid[31] = true;
		for (int i = 0; i < Size; ++i)
			if (Free[i])
				Stack[++StackTop] = i;
		for (int i = 0; i < Size; ++i)
			Next[i] = 0x3f3f3f3f;
	}
	Class()
	{
		init();
	}
	int contain(string vr)
	{
		for (int i = 0; i < Size; ++i)
			if (!Free[i] && Name[i] == vr)
				return i;
		return -1;
	}
	void kick()
	{
		int mxpos = -1;
		for (int i = 0; i < Size; ++i)
		{
			if (forbid[i]) continue;
			if (mxpos == -1)
				mxpos = i;
			if (Next[i] > Next[mxpos])
				mxpos = i;
		}
		stringstream ss;
		ss << mxpos;
		if(changed[mxpos])
			out << "unassign " + Name[mxpos] + " " + ss.str() << endl;
		Free[mxpos] = true;
	//	changed[mxpos] = false;
		Stack[++StackTop] = mxpos;
		Next[mxpos] = 0x3f3f3f3f;
	}
	void kickAll(int line, bool force, bool isfuncCall = false)
	{
		for (int i = 0; i < Size; ++i)
		{
			if (forbid[i]) continue;
			if (!Free[i])
			{
				if (force || (needed(Name[i], line) && changed[i]))
				{
					if(isfuncCall)
						out << "stackunassign " << Name[i] << " " << i << endl;
					else
						out << "unassign " << Name[i] << " " << i << endl;
				}
				Free[i] = true;
			//	changed[i] = false;
				Stack[++StackTop] = i;
				Next[i] = 0x3f3f3f3f;
			}
		}
	}
	void doFree(int id)
	{
		if (Name[id] == "r_{global_return_register}")
		{
			out << "unassign " << Name[id] << " " << id << endl;
		}
		assert(!Free[id]);
		Free[id] = true;
		Stack[++StackTop] = id;
	//	changed[id] = false;
		Next[id] = 0x3f3f3f3f;
	}
};


int allocate(string vr, Class &cla, bool needlw)
{
	if (cla.StackTop == -1)
		cla.kick();
	stringstream ss;
	int res = cla.Stack[cla.StackTop--];
	ss << res;
	if(needlw || vr == "r_{global_return_register}")
		out << "assign " + vr + " " + ss.str() << endl;
	else
		out << "virtualassign " + vr + " " + ss.str() << endl;
	cla.Free[res] = false;
	cla.changed[res] = false;
	cla.Name[res] = vr;
	cla.Next[res] = -1;
	return res;
}

int Ensure(string vr, Class &cla, bool needlw)
{
	int ret;
	if ((ret = cla.contain(vr)) != -1)
	{
		return ret;
	}
	else
	{
		ret = allocate(vr, cla, needlw);
		return ret;
	}
}

void block_init()
{
	ifstream block_in("blocks");
	int a, b, c;
	int mx = 0;
	while (block_in >> a >> b >> c)
	{
		Block bb;
		bb.blockid = a;
		bb.leading = b;
		bb.ending = c;
		mx = max(mx, c);
		blocks.push_back(bb);
	}
	mapping_lines2block.resize(mx + 1);
	for (int i = 0; i < blocks.size(); ++i)
	{
		for (int j = blocks[i].leading; j <= blocks[i].ending; ++j)
			mapping_lines2block[j] = blocks[i].blockid;
	}
}

void init()
{
	ifstream cfg_in("control_flow_graph");
	ifstream ir_in("ll_after.ir");
	block_init();
	
	string tmp;
	int pc = 0;
	while (getline(ir_in, tmp))
	{
		raw.push_back(tmp);
		lines.push_back(get_context(tmp, pc++));
	}
	int n;
	cfg_in >> n;
	CFG.init(n);
	for (int i = 0; i < n; ++i)
	{
		int id, m;
		cfg_in >> id >> m;
		for (int j = 0; j < m; ++j)
		{
			int x;
			cfg_in >> x;
			CFG.add(id, abs(x));
		}
	}
}

int main()
{
	out = ofstream("ll_risc.ir");
	in = ifstream("ll_after_new.ir");

	init();
	for (int i = 0; i < blocks.size(); ++i)
	{
		Class cla;
		bool ending_with_branch = false;
		bool form1_need_lw = true;
		for (int j = blocks[i].leading; j <= blocks[i].ending; ++j)
		{
			operation &instr = lines[j];
			if (instr.opcode == "")
				continue;
			//1 for vr_i1 -> vr_i2
			//2 for vr_i1 vr_i2 -> vr_i3
			//3 for imm -> vr_i1
			int form; 
			string vr_i1, vr_i2, vr_i3;

			if (instr.opcode == "label")
			{
				out << raw[j] << endl;
				continue;
			}
			else if (instr.opcode == "mallocStatic")
			{
				out << raw[j] << endl;
				continue;
			}
			else if (instr.opcode == "mallocI")
			{
				form = 3;
				vr_i1 = instr.operands[1];
			}
			else if (instr.opcode == "malloc")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
			}
			else if (instr.opcode == "call")
			{
				if (lines[blocks[i].ending].operands[0] == "@functionLabel_print"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel_println"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel_getString"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel_getInt"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel___builtin__string_add"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel_toString"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel___builtin_printInt"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel___builtin_printIntln"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel___builtin_string_generate_quote_space_quote_"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel___builtin__length"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel___builtin__substring"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel___builtin__ord"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel_print_constant"
					|| lines[blocks[i].ending].operands[0] == "@functionLabel_println_constant"
					)
					cla.kickAll(blocks[i].ending, true);
				else
					cla.kickAll(blocks[i].ending, true, true);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "return")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "nop")
			{
				out << raw[j] << endl;
				continue;
			}
			else if (instr.opcode == "add")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "sub")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "mult")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "div")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "addI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "subI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "rsubI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "multI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "divI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "rdivI")
			{
				assert(false);
			}
			else if (instr.opcode == "lshift")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "rshift")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "lshiftI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "rshiftI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "and")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "or")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "xor")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "andI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "orI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "xorI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "loadI")
			{
				form = 3;
				vr_i1 = instr.operands[1];
			}
			else if (instr.opcode == "loadAI")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "loadA0")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "store")
			{
				form = 1;
				form1_need_lw = true;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
			}
			else if (instr.opcode == "storeAI")
			{
				form = 1;
				form1_need_lw = true;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[2];
			}
			else if (instr.opcode == "storeA0")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "i2i")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
			}
			else if (instr.opcode == "jump")
			{
				assert(false);
			}
			else if (instr.opcode == "jumpI")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "cbr")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "cmp_LT")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "cmp_LE")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "cmp_EQ")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "cmp_GE")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "cmp_GT")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "cmp_NE")
			{
				form = 2;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
				vr_i3 = instr.operands[2];
			}
			else if (instr.opcode == "load")
			{
				form = 1;
				form1_need_lw = false;
				vr_i1 = instr.operands[0];
				vr_i2 = instr.operands[1];
			}
			else if (instr.opcode == "cbr_lt")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "cbr_le")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "cbr_gt")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "cbr_ge")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "cbr_eq")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else if (instr.opcode == "cbr_ne")
			{
				cla.kickAll(blocks[i].ending, false);
				out << raw[j] << endl;
				ending_with_branch = true;
				continue;
			}
			else
				assert(false);

			//1 for vr_i1 -> vr_i2
			//2 for vr_i1 vr_i2 -> vr_i3
			//3 for imm -> vr_i1
			switch (form)
			{
			case 1:
			{
				int rx = Ensure(vr_i1, cla, true);
			//	cla.changed[rx] = true;
				cla.forbid[rx] = true;
				visit.clear();
				visit.resize(lines.size());
				pair<bool, int> res = isneed(vr_i1, j + 1, 0);
				int rz = Ensure(vr_i2, cla, form1_need_lw);
				cla.forbid[rx] = false;
				cla.changed[rz] = true;
				out << raw[j] << endl;
				if (!res.first)
					cla.doFree(rx);
				if (res.first)
					cla.Next[rx] = res.second;
				cla.Next[rz] = Dist(vr_i2, j + 1);
			}
				break;
			case 2:
			{
				int rx = Ensure(vr_i1, cla, true);
			//	cla.changed[rx] = true;
				cla.forbid[rx] = true;
				int ry = Ensure(vr_i2, cla, true);
			//	cla.changed[ry] = true;
				cla.forbid[ry] = true;
				visit.clear();
				visit.resize(lines.size());
				pair<bool, int> res1 = isneed(vr_i1, j + 1, 0);
				visit.clear();
				visit.resize(lines.size());
				pair<bool, int> res2 = isneed(vr_i2, j + 1, 0);
				int rz = Ensure(vr_i3, cla, false);
				cla.changed[rz] = true;
				cla.forbid[rx] = cla.forbid[ry] = false;
				out << raw[j] << endl;
				if (!res1.first)
					cla.doFree(rx);
				if (!res2.first)
					cla.doFree(ry);
				if (res1.first)
					cla.Next[rx] = res1.second;
				if (res2.first)
					cla.Next[ry] = res2.second;
				cla.Next[rz] = Dist(vr_i3, j + 1);
			}
				break;
			case 3:
			{
				int rx = Ensure(vr_i1, cla, false);
				cla.changed[rx] = true;
				out << raw[j] << endl;
				cla.Next[rx] = Dist(vr_i1, j + 1);
			}
				break;
			default:
				assert(false);
			}
		}
		if (!ending_with_branch)
		{
			cla.kickAll(blocks[i].ending + 1, true);
		}
	}

	return 0;
}