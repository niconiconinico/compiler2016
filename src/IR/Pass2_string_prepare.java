package IR;

/**
 * Created by xietiancheng on 16/4/28.
 */
// Generated from Muse.g4 by ANTLR 4.5.2

import IR.Memory_Model.Reg;
import IR.symbol_table.Property;
import IR.symbol_table.Symbol_table;
import Parser.MuseParser;
import Parser.MuseVisitor;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Pass2_string_prepare extends AbstractParseTreeVisitor<information> implements MuseVisitor<information> {
    Symbol_table symbol_table;
    Reg reg_allocator;
    LabelAllocator label_allocator;
    BufferedWriter out;
    public Integer static_area_size, used_static_area;
    HashMap<String, Boolean> generated;

    private void write_string(String x)
    {
        try
        {
            out.write(x);
            out.newLine();
        }
        catch(Exception e)
        {
            throw new RuntimeException("IO Error");
        }
    }

    private void write_direct(String opcode, information[] src, information[] target) throws IOException
    {
        out.write(opcode);
        for(int i = 0; i < src.length; ++i)
            out.write(" " + src[i].varInfo.varP.reg);
        for(int i = 0; i < target.length; ++i)
            out.write(" " + target[i].varInfo.varP.reg);
        out.newLine();
        for(int i = 0; i < target.length; ++i)
        {
            if(target[i].varInfo.varP.is_static)
            {
                if(target[i].varInfo.varP.addr_reg != null)
                    write("store", new information[]{new information(target[i].varInfo.varP.reg)}, new information[]{new information(target[i].varInfo.varP.addr_reg)});
                else
                    write("storeAI", new information[]{new information(target[i].varInfo.varP.reg)}, new information[]{new information("r_{global_base_address_register^_^}"), new information(target[i].varInfo.varP.offset)});
            }
        }
    }

    private void write(String opcode, information[] src, information[] target)
    {
        try
        {
            write_direct(opcode, src, target);
        }
        catch(Exception e)
        {
            throw new RuntimeException("IO Error " + e.getMessage());
        }
    }

    @Override public information visitProgram(MuseParser.ProgramContext ctx) {
        write_string("\n\n\n\n\n\n##Stack area");
        generated = new HashMap<>();
        return visitChildren(ctx);
    }
    
    @Override public information visitDeclaration(MuseParser.DeclarationContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInit_declarators(MuseParser.Init_declaratorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitDeclarators(MuseParser.DeclaratorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInit_declarator(MuseParser.Init_declaratorContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType(MuseParser.TypeContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitArray_decl(MuseParser.Array_declContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitArray_new(MuseParser.Array_newContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType_non_array(MuseParser.Type_non_arrayContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitClass_declaration(MuseParser.Class_declarationContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitDeclarator(MuseParser.DeclaratorContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInitializer(MuseParser.InitializerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFunction_definition(MuseParser.Function_definitionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitParameters(MuseParser.ParametersContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitParameter(MuseParser.ParameterContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitStatement(MuseParser.StatementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_statement(MuseParser.Assignment_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitStructured_statement(MuseParser.Structured_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitCompond_statement(MuseParser.Compond_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLoop_statement(MuseParser.Loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBranch_statement(MuseParser.Branch_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFor_loop_statement(MuseParser.For_loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFirst_expression(MuseParser.First_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSecond_expression(MuseParser.Second_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitThird_expression(MuseParser.Third_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitWhile_loop_statement(MuseParser.While_loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitIf_statement(MuseParser.If_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitExpression(MuseParser.ExpressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_expression(MuseParser.Assignment_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitEqual(MuseParser.EqualContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_operators(MuseParser.Assignment_operatorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitCalculation_expression(MuseParser.Calculation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitIsequal(MuseParser.IsequalContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNotequal(MuseParser.NotequalContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitEquality_expression(MuseParser.Equality_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitEqu_op(MuseParser.Equ_opContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBigger(MuseParser.BiggerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSmaller(MuseParser.SmallerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBigger_e(MuseParser.Bigger_eContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSmaller_e(MuseParser.Smaller_eContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitRela_op(MuseParser.Rela_opContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitRelation_expression(MuseParser.Relation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLshift(MuseParser.LshiftContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitRshift(MuseParser.RshiftContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPlus(MuseParser.PlusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMinus(MuseParser.MinusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitShift_op(MuseParser.Shift_opContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAdd_op(MuseParser.Add_opContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitShift_expression(MuseParser.Shift_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAdd_expression(MuseParser.Add_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMultiply(MuseParser.MultiplyContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitDivision(MuseParser.DivisionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMod(MuseParser.ModContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMul_op(MuseParser.Mul_opContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMul_expression(MuseParser.Mul_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPlusplus(MuseParser.PlusplusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMinusminus(MuseParser.MinusminusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnary_expression(MuseParser.Unary_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitClass_new(MuseParser.Class_newContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNew_operation(MuseParser.New_operationContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitArguements(MuseParser.ArguementsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitValue(MuseParser.ValueContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNot_sign(MuseParser.Not_signContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnary_operation(MuseParser.Unary_operationContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_not(MuseParser.Bitwise_notContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPostfix_expression(MuseParser.Postfix_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitGetMember(MuseParser.GetMemberContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPostfix(MuseParser.PostfixContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPrimary_expression(MuseParser.Primary_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType_name(MuseParser.Type_nameContext ctx) { return visitChildren(ctx); }

    private Pair<Integer, ArrayList<Integer> > str_2_mem_data(String cur)
    {
        ArrayList<Integer> ret = new ArrayList<>();
        int last = 1, size = 0;
        for(int i = 1; i + 4 < cur.length() - 1; i += 4, last = i)
        {
            int result = 0;
            for(int k = 0; k < 4; ++k)
            {
                size++;
                int res = 0;
                if(cur.charAt(i + k) == '\\')
                {
                    i++;
                    if(cur.charAt(i + k) == '\\')
                    {
                        res = (int) '\\';
                    }
                    if(cur.charAt(i + k) == '\"')
                    {
                        res = (int) '\"';
                    }
                    if(cur.charAt(i + k) == 'n')
                    {
                        res = (int) '\n';
                    }
                }
                else
                {
                    res = (int) cur.charAt(i + k);
                }
                result += (res << (8 * (k)));
            }
            ret.add(result);
        }
        if(last < cur.length() - 1)
        {
            int result = 0;
            for(int i = last; i < cur.length() - 1; ++i)
            {
                size++;
                int res = 0;
                if(cur.charAt(i) == '\\')
                {
                    i++;
                    last++;
                    if(cur.charAt(i) == '\\')
                    {
                        res = (int) '\\';
                    }
                    if(cur.charAt(i) == '\"')
                    {
                        res = (int) '\"';
                    }
                    if(cur.charAt(i) == 'n')
                    {
                        res = (int) '\n';
                    }
                }
                else
                {
                    res = (int) cur.charAt(i);
                }
                result += (res << (8 * ((i - last))));
            }
            ret.add(result);
        }
        return new Pair<>(size, ret);
    }


    @Override public information visitConstant(MuseParser.ConstantContext ctx) {
        //for string, the 1st one is size
        //I decide one char one byte
        //note cur -> "str"
        if(ctx.string_constant() == null)
            return null;
        String cur = ctx.string_constant().getText();
        Pair<Integer, ArrayList<Integer>> str = str_2_mem_data(cur);

        String func_label_name = label_allocator.allocate_func("__builtin_string_generate_" + cur);
        if(generated.get(func_label_name) != null)
            return null;
        generated.put(func_label_name, true);
        write_string(func_label_name + "\nnop");
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(true, "");

        write("mallocI", new information[]{new information((2 + str.b.size()) * RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{ret});
        String tempReg = reg_allocator.allocate(true, "str_constant_tmp"), pointer = reg_allocator.allocate(true, "str_constant_ptr");
        write("loadI", new information[]{new information(str.a)}, new information[]{new information(tempReg, null)});
        write("store", new information[]{new information(tempReg, null)}, new information[]{ret});
        write("i2i", new information[]{ret}, new information[]{new information(pointer, null)});
        for(int i = 0; i < str.b.size(); ++i)
        {
            write("addI", new information[]{new information(pointer, null), new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(pointer, null)});
            write("loadI", new information[]{new information(str.b.get(i))}, new information[]{new information(tempReg, null)});
            write("store", new information[]{new information(tempReg, null)}, new information[]{new information(pointer, null)});
        }
        write("i2i", new information[]{ret}, new information[]{new information("r_{global_return_register}")});
        write("return", new information[]{new information("r_{global_return_register}")}, new information[]{});
        return null;
    }
    
    @Override public information visitNumeric_constant(MuseParser.Numeric_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInteger_constant(MuseParser.Integer_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitString_constant(MuseParser.String_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitTypenametokens(MuseParser.TypenametokensContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNumber(MuseParser.NumberContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnion_class(MuseParser.Union_classContext ctx) { return visitChildren(ctx); }
}