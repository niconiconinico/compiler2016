package IR;

import IR.Memory_Model.Reg;
import IR.symbol_table.Name;
import IR.symbol_table.Property;
import IR.symbol_table.Symbol_table;
import Parser.MuseParser;
import Parser.MuseVisitor;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by xietiancheng on 16/4/23.
 */

/**
 * This File
 * Determine all label of functions
 * Determine all name of classes, also the size of it.
 * And put all of them into symbol_table
 */

public class Pass1 extends AbstractParseTreeVisitor<information> implements MuseVisitor<information> {
    Symbol_table symbol_table;
    Reg reg_allocator;
    LabelAllocator label_allocator;
    
    @Override public information visitProgram(MuseParser.ProgramContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitDeclaration(MuseParser.DeclarationContext ctx) {
        if(ctx.class_declaration() != null)
        {
            return visit(ctx.class_declaration());
        }
        else
        {
            return null;
        }
        //return visitChildren(ctx);
    }
    
    @Override public information visitInit_declarators(MuseParser.Init_declaratorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitDeclarators(MuseParser.DeclaratorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInit_declarator(MuseParser.Init_declaratorContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType(MuseParser.TypeContext ctx) {
        //TODO in future type specifier
        return visitChildren(ctx);
    }
    
    @Override public information visitArray_decl(MuseParser.Array_declContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitArray_new(MuseParser.Array_newContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType_non_array(MuseParser.Type_non_arrayContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitClass_declaration(MuseParser.Class_declarationContext ctx) {
        information ret = new information();
        ret.typeInfo = new Property();
        ret.typeInfo.typeP = new Property.TypeP();
        ret.typeInfo.id = Name.getSymbolName(ctx.Identifier().getText());
        ret.typeInfo.typeP.pointerSize = RegisiterInfo.Register_size / RegisiterInfo.byte_size;
        ret.typeInfo.typeP.realSize = ctx.declarators().size() * RegisiterInfo.Register_size / RegisiterInfo.byte_size;
        ret.typeInfo.typeP.ctx = new ArrayList<>();
        symbol_table.put(ret.typeInfo.id, ret.typeInfo);
        //TODO in future pass, complete ctx
        return visitChildren(ctx);
    }
    
    @Override public information visitDeclarator(MuseParser.DeclaratorContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInitializer(MuseParser.InitializerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFunction_definition(MuseParser.Function_definitionContext ctx) {
        //TODO in future pass, determine the parameter list and label of function
        information ret = new information();
        ret.funcInfo = new Property();
        ret.funcInfo.funcP = new Property.FuncP();
        ret.funcInfo.id = Name.getSymbolName(ctx.Identifier().getText());
        ret.funcInfo.funcP.label = label_allocator.allocate(ret.funcInfo.id.name);
        ret.funcInfo.funcP.retType = ctx.type().type_non_array().getText();
        symbol_table.put(ret.funcInfo.id, ret.funcInfo);
        return ret;
    }
    
    @Override public information visitParameters(MuseParser.ParametersContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitParameter(MuseParser.ParameterContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitStatement(MuseParser.StatementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_statement(MuseParser.Assignment_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitStructured_statement(MuseParser.Structured_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitCompond_statement(MuseParser.Compond_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLoop_statement(MuseParser.Loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBranch_statement(MuseParser.Branch_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFor_loop_statement(MuseParser.For_loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFirst_expression(MuseParser.First_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSecond_expression(MuseParser.Second_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitThird_expression(MuseParser.Third_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitWhile_loop_statement(MuseParser.While_loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitIf_statement(MuseParser.If_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitExpression(MuseParser.ExpressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_expression(MuseParser.Assignment_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitEqual(MuseParser.EqualContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_operators(MuseParser.Assignment_operatorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitCalculation_expression(MuseParser.Calculation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitIsequal(MuseParser.IsequalContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNotequal(MuseParser.NotequalContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitEquality_expression(MuseParser.Equality_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBigger(MuseParser.BiggerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSmaller(MuseParser.SmallerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBigger_e(MuseParser.Bigger_eContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSmaller_e(MuseParser.Smaller_eContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitRelation_expression(MuseParser.Relation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLshift(MuseParser.LshiftContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitRshift(MuseParser.RshiftContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPlus(MuseParser.PlusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMinus(MuseParser.MinusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitShift_expression(MuseParser.Shift_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAdd_expression(MuseParser.Add_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMultiply(MuseParser.MultiplyContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitDivision(MuseParser.DivisionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMod(MuseParser.ModContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMul_expression(MuseParser.Mul_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPlusplus(MuseParser.PlusplusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMinusminus(MuseParser.MinusminusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnary_expression(MuseParser.Unary_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitClass_new(MuseParser.Class_newContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNew_operation(MuseParser.New_operationContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitArguements(MuseParser.ArguementsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitValue(MuseParser.ValueContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNot_sign(MuseParser.Not_signContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnary_operation(MuseParser.Unary_operationContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_not(MuseParser.Bitwise_notContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPostfix_expression(MuseParser.Postfix_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitGetMember(MuseParser.GetMemberContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPostfix(MuseParser.PostfixContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPrimary_expression(MuseParser.Primary_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType_name(MuseParser.Type_nameContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitConstant(MuseParser.ConstantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNumeric_constant(MuseParser.Numeric_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInteger_constant(MuseParser.Integer_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitString_constant(MuseParser.String_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitTypenametokens(MuseParser.TypenametokensContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNumber(MuseParser.NumberContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnion_class(MuseParser.Union_classContext ctx) { return visitChildren(ctx); }
    @Override public information visitMul_op(MuseParser.Mul_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitAdd_op(MuseParser.Add_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitShift_op(MuseParser.Shift_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitRela_op(MuseParser.Rela_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitEqu_op(MuseParser.Equ_opContext ctx) { return visitChildren(ctx); }

}
