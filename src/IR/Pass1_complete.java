package IR;
import IR.Memory_Model.Reg;
import IR.information;
import IR.symbol_table.Name;
import IR.symbol_table.Property;
import IR.symbol_table.Symbol_table;
import Parser.MuseParser;
import Parser.MuseVisitor;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

import java.util.LinkedList;

/**
 * This file
 * Determined the structure of class, including the member of it. Allocate the offset and the register
 * Determined the parameter list of function, allocate the register
 * allocate global var's register
 */

public class Pass1_complete extends AbstractParseTreeVisitor<information> implements MuseVisitor<information> {
    Symbol_table symbol_table;
    Reg reg_allocator;
    LabelAllocator label_allocator;
    Integer static_area_size;
    
    @Override public information visitProgram(MuseParser.ProgramContext ctx) { static_area_size = 0;return visitChildren(ctx); }
    
    @Override public information visitDeclaration(MuseParser.DeclarationContext ctx) {
        if(ctx.class_declaration() != null)
            return visit(ctx.class_declaration());
        else
        {
            static_area_size += RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            information type = visit(ctx.type());
            MuseParser.Init_declaratorsContext init_ctx = ctx.init_declarators();
            Name id = Name.getSymbolName(init_ctx.init_declarator().declarator().getText());
            Property varPo = new Property();
            varPo.varP = new Property.VarP();
            varPo.varP.type = type.typeInfo;
            varPo.varP.reg = reg_allocator.allocate_global(false, id.name);
            //varPo.varP.addr_reg = reg_allocator.allocate(false, id.name + "@addr");
            varPo.varP.is_static = true;
            varPo.varP.offset = static_area_size - RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            varPo.id = id;

            information ret = new information();
            ret.varInfo = varPo;

            symbol_table.put(varPo.id, varPo);
        }
        return visitChildren(ctx);
    }
    
    @Override public information visitInit_declarators(MuseParser.Init_declaratorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitDeclarators(MuseParser.DeclaratorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInit_declarator(MuseParser.Init_declaratorContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType(MuseParser.TypeContext ctx) {
        information ret = new information();
        ret.typeInfo = new Property();
        if(ctx.type_non_array() != null)
        {
            ret.typeInfo = symbol_table.get(Name.getSymbolName(ctx.getText()));
            return ret;
        }
        else
        {
            ret.typeInfo = symbol_table.get(Name.getSymbolName(ctx.array_decl().type_non_array().getText()));
            return ret;
        }
    }
    
    @Override public information visitArray_decl(MuseParser.Array_declContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitArray_new(MuseParser.Array_newContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType_non_array(MuseParser.Type_non_arrayContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitClass_declaration(MuseParser.Class_declarationContext ctx) {
        Property cur = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()));
        for(int i = 0; i < ctx.type().size(); ++i)
        {
            information ctx_type = visit(ctx.type(i));
            Name id = Name.getSymbolName(ctx.declarators(i).getText());
            Property pro = new Property();
            pro.varP = new Property.VarP();
            pro.varP.type = ctx_type.typeInfo;
            pro.id = id;
            pro.varP.offset = i * RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            cur.typeP.ctx.add(pro);
        }
        return null;
    }
    
    @Override public information visitDeclarator(MuseParser.DeclaratorContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInitializer(MuseParser.InitializerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFunction_definition(MuseParser.Function_definitionContext ctx) {
        Property curp = new Property();
        curp.funcP = new Property.FuncP();
        curp.funcP.label = label_allocator.allocate_func(ctx.Identifier().getText());
        curp.id = Name.getSymbolName(ctx.Identifier().getText());
        curp.funcP.parameters = new LinkedList<>();
        curp.funcP.retType = ctx.type().getText();
        if(ctx.parameters() != null)
        {
            for(int i = 0; i < ctx.parameters().parameter().size(); ++i)
            {
                MuseParser.ParameterContext curPara = ctx.parameters().parameter(i);
                information ctype = visit(curPara.type());
                Name id = Name.getSymbolName(curPara.declarator().getText());
                Property curParameter = new Property();;
                curParameter.varP = new Property.VarP();
                curParameter.id = id;
                curParameter.varP.type = ctype.typeInfo;
                curParameter.varP.reg = reg_allocator.allocate(false, id.name);
                curp.funcP.parameters.add(curParameter);
            }
        }
        symbol_table.put(curp.id, curp);
        return null;
    }
    
    @Override public information visitParameters(MuseParser.ParametersContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitParameter(MuseParser.ParameterContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitStatement(MuseParser.StatementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_statement(MuseParser.Assignment_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitStructured_statement(MuseParser.Structured_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitCompond_statement(MuseParser.Compond_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLoop_statement(MuseParser.Loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBranch_statement(MuseParser.Branch_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFor_loop_statement(MuseParser.For_loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFirst_expression(MuseParser.First_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSecond_expression(MuseParser.Second_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitThird_expression(MuseParser.Third_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitWhile_loop_statement(MuseParser.While_loop_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitIf_statement(MuseParser.If_statementContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitExpression(MuseParser.ExpressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_expression(MuseParser.Assignment_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitEqual(MuseParser.EqualContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAssignment_operators(MuseParser.Assignment_operatorsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitCalculation_expression(MuseParser.Calculation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitIsequal(MuseParser.IsequalContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNotequal(MuseParser.NotequalContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitEquality_expression(MuseParser.Equality_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBigger(MuseParser.BiggerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSmaller(MuseParser.SmallerContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBigger_e(MuseParser.Bigger_eContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitSmaller_e(MuseParser.Smaller_eContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitRelation_expression(MuseParser.Relation_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitLshift(MuseParser.LshiftContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitRshift(MuseParser.RshiftContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPlus(MuseParser.PlusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMinus(MuseParser.MinusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitShift_expression(MuseParser.Shift_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitAdd_expression(MuseParser.Add_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMultiply(MuseParser.MultiplyContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitDivision(MuseParser.DivisionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMod(MuseParser.ModContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMul_expression(MuseParser.Mul_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPlusplus(MuseParser.PlusplusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitMinusminus(MuseParser.MinusminusContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnary_expression(MuseParser.Unary_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitClass_new(MuseParser.Class_newContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNew_operation(MuseParser.New_operationContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitArguements(MuseParser.ArguementsContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitValue(MuseParser.ValueContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNot_sign(MuseParser.Not_signContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnary_operation(MuseParser.Unary_operationContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitBitwise_not(MuseParser.Bitwise_notContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPostfix_expression(MuseParser.Postfix_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitGetMember(MuseParser.GetMemberContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPostfix(MuseParser.PostfixContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitPrimary_expression(MuseParser.Primary_expressionContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitType_name(MuseParser.Type_nameContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitConstant(MuseParser.ConstantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNumeric_constant(MuseParser.Numeric_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitInteger_constant(MuseParser.Integer_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitString_constant(MuseParser.String_constantContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitTypenametokens(MuseParser.TypenametokensContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitNumber(MuseParser.NumberContext ctx) { return visitChildren(ctx); }
    
    @Override public information visitUnion_class(MuseParser.Union_classContext ctx) { return visitChildren(ctx); }
    @Override public information visitMul_op(MuseParser.Mul_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitAdd_op(MuseParser.Add_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitShift_op(MuseParser.Shift_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitRela_op(MuseParser.Rela_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitEqu_op(MuseParser.Equ_opContext ctx) { return visitChildren(ctx); }

}