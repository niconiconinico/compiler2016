package IR;

/**
 * Created by xietiancheng on 16/4/24.
 */
public class LabelAllocator {
    Integer label_cnt = 0;
    String allocate()
    {
        return "@Label_"+label_cnt++;
    }
    String allocate(String name)
    {
        return "@Label_"+name+"_"+label_cnt++;
    }
    String allocate_func(String name) { return "@functionLabel_"+name;}
}
