package IR.Memory_Model;

import java.util.Objects;

/**
 * Created by xietiancheng on 16/4/23.
 */
public class Reg {
    Integer regCount;
    public Reg(){regCount = -1;}
    public String allocate(boolean isTemp, String id)
    {
        regCount++;
        if(isTemp)
            return "r_{" + regCount + "_*temp}";
        else
            return "r_{" + regCount + "_" + id+"}";
    }
    public String allocate_global(boolean isTemp, String id)
    {
        regCount++;
        if(isTemp)
            return "r_{global_" + regCount + "_*temp}";
        else
            return "r_{global_" + regCount + "_" + id+"}";
    }
}
