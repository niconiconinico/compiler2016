package IR;

/**
 * Created by xietiancheng on 16/4/24.
 */

import IR.Memory_Model.Reg;
import IR.symbol_table.Name;
import IR.symbol_table.Property;
import IR.symbol_table.Symbol_table;
import Parser.MuseParser;
import Parser.MuseVisitor;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

/**
 * This file
 * I want initialize all global vars
 */
public class Pass2_prepare extends AbstractParseTreeVisitor<information> implements MuseVisitor<information> {
    Symbol_table symbol_table;
    Reg reg_allocator;
    LabelAllocator label_allocator;
    BufferedWriter out;
    public Integer static_area_size, used_static_area;

    private void write_string(String x)
    {
        try
        {
            out.write(x);
            out.newLine();
        }
        catch(Exception e)
        {
            throw new RuntimeException("IO Error");
        }
    }

    private void write_direct(String opcode, information[] src, information[] target) throws IOException
    {
        out.write(opcode);
        for(int i = 0; i < src.length; ++i)
            out.write(" " + src[i].varInfo.varP.reg);
        for(int i = 0; i < target.length; ++i)
            out.write(" " + target[i].varInfo.varP.reg);
        out.newLine();
        for(int i = 0; i < target.length; ++i)
        {
            if(target[i].varInfo.varP.is_static)
            {
                if(target[i].varInfo.varP.addr_reg != null)
                    write("store", new information[]{new information(target[i].varInfo.varP.reg)}, new information[]{new information(target[i].varInfo.varP.addr_reg)});
                else
                    write("storeAI", new information[]{new information(target[i].varInfo.varP.reg)}, new information[]{new information(target[i].varInfo.varP.offset), new information("r_{global_base_address_register^_^}")});
            }
        }
    }

    private void write(String opcode, information[] src, information[] target)
    {
        try
        {
            write_direct(opcode, src, target);
        }
        catch(Exception e)
        {
            throw new RuntimeException("IO Error " + e.getMessage());
        }
    }

    @Override public information visitProgram(MuseParser.ProgramContext ctx) {
        //True, false, null
        write_string("##Static_area");
        used_static_area = 0;
        //write_string("//static area size = " + (static_area_size + RegisiterInfo.Register_size / RegisiterInfo.byte_size * 3));
        write("mallocStatic", new information[]{new information(static_area_size + RegisiterInfo.Register_size / RegisiterInfo.byte_size * 4)}, new information[]{});
        String tmpReg = reg_allocator.allocate(true, "");
        write("loadI", new information[]{new information(static_area_size + RegisiterInfo.Register_size / RegisiterInfo.byte_size * 3)}, new information[]{new information(tmpReg)});
        write("store", new information[]{new information(tmpReg)}, new information[]{new information("r_{global_base_address_register^_^}")});
        {
            Property True = symbol_table.get(Name.getSymbolName("true"));
            True.varP.reg = reg_allocator.allocate(false, "true");
            True.varP.offset = static_area_size;
            information[] s = {new information(1)}, t = {new information(True.varP.reg)};
            write("loadI", s, t);
            write("storeAI", t, new information[]{new information(True.varP.offset + RegisiterInfo.Register_size / RegisiterInfo.byte_size), new information("r_{global_base_address_register^_^}")});
        }
        {
            Property False = symbol_table.get(Name.getSymbolName("false"));
            False.varP.reg = reg_allocator.allocate(false, "false");
            False.varP.offset = static_area_size + RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            //write("loadI 0 " + False.varP.reg);
            information[] s = {new information(1)}, t = {new information(False.varP.reg)};
            write("loadI", s, t);
            write("storeAI", t, new information[]{new information(False.varP.offset + RegisiterInfo.Register_size / RegisiterInfo.byte_size), new information("r_{global_base_address_register^_^}")});
        }
        {
            Property Null = symbol_table.get(Name.getSymbolName("null"));
            Null.varP.reg = reg_allocator.allocate(false, "null");
            Null.varP.offset = static_area_size + RegisiterInfo.Register_size / RegisiterInfo.byte_size * 2;
            //write("loadI 0 " + Null.varP.reg);
            information[] s = {new information(1)}, t = {new information(Null.varP.reg)};
            write("loadI", s, t);
            write("storeAI", t, new information[]{new information(Null.varP.offset + RegisiterInfo.Register_size / RegisiterInfo.byte_size), new information("r_{global_base_address_register^_^}")});
        }
        for(Map.Entry<Name, Property> x : symbol_table.symbol_table.get(0).entrySet())
        {
            if(x.getValue().varP != null)
            {
                write("addI", new information[]{new information("r_{global_base_address_register^_^}"), new information(x.getValue().varP.offset + RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(x.getValue().varP.reg)});
            }
        }
        return visitChildren(ctx);
    }

    @Override public information visitDeclaration(MuseParser.DeclarationContext ctx) {
        if(ctx.class_declaration() == null)
        {
            Name id = Name.getSymbolName(ctx.init_declarators().init_declarator().declarator().getText());
            Property var = symbol_table.get(id);
            if(ctx.init_declarators().init_declarator().equal() == null)
                return null;
            information rhs = visit(ctx.init_declarators().init_declarator().initializer());
            information tmp = new information();
            tmp.varInfo = var;
            information[] s = {rhs}, t = {tmp};
            write("i2i", s, t);
            //write("storeAI", s, new information[]{new information(tmp.varInfo.varP.offset), new information("r_{global_base_address_register^_^}")});
            return null;
        }
        return null;
    }

    @Override public information visitInit_declarators(MuseParser.Init_declaratorsContext ctx) { return visitChildren(ctx); }

    @Override public information visitDeclarators(MuseParser.DeclaratorsContext ctx) { return visitChildren(ctx); }

    @Override public information visitInit_declarator(MuseParser.Init_declaratorContext ctx) { return visitChildren(ctx); }

    @Override public information visitType(MuseParser.TypeContext ctx) { return visitChildren(ctx); }

    @Override public information visitArray_decl(MuseParser.Array_declContext ctx) { return visitChildren(ctx); }

    @Override public information visitArray_new(MuseParser.Array_newContext ctx) { return visitChildren(ctx); }

    @Override public information visitType_non_array(MuseParser.Type_non_arrayContext ctx) { return visitChildren(ctx); }

    @Override public information visitClass_declaration(MuseParser.Class_declarationContext ctx) { return visitChildren(ctx); }

    @Override public information visitDeclarator(MuseParser.DeclaratorContext ctx) { return visitChildren(ctx); }

    @Override public information visitInitializer(MuseParser.InitializerContext ctx) {
        return visit(ctx.assignment_expression());
    }

    @Override public information visitFunction_definition(MuseParser.Function_definitionContext ctx) {
        return null;
    }

    @Override public information visitParameters(MuseParser.ParametersContext ctx) { return visitChildren(ctx); }

    @Override public information visitParameter(MuseParser.ParameterContext ctx) { return visitChildren(ctx); }

    @Override public information visitStatement(MuseParser.StatementContext ctx) { return visitChildren(ctx); }

    @Override public information visitAssignment_statement(MuseParser.Assignment_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitStructured_statement(MuseParser.Structured_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitCompond_statement(MuseParser.Compond_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitLoop_statement(MuseParser.Loop_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitBranch_statement(MuseParser.Branch_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitFor_loop_statement(MuseParser.For_loop_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitFirst_expression(MuseParser.First_expressionContext ctx) { return visitChildren(ctx); }

    @Override public information visitSecond_expression(MuseParser.Second_expressionContext ctx) { return visitChildren(ctx); }

    @Override public information visitThird_expression(MuseParser.Third_expressionContext ctx) { return visitChildren(ctx); }

    @Override public information visitWhile_loop_statement(MuseParser.While_loop_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitIf_statement(MuseParser.If_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitExpression(MuseParser.ExpressionContext ctx) { return visitChildren(ctx); }

    @Override public information visitAssignment_expression(MuseParser.Assignment_expressionContext ctx) {
        if(ctx.unary_expression() != null)
        {
            information rhs = visit(ctx.calculation_expression());
            for(int i = ctx.unary_expression().size() - 1; i >= 0; --i)
            {
                information lhs = visit(ctx.unary_expression(i));
                information[] s = {rhs}, t = {lhs};
                write("i2i", s, t);
                rhs = lhs;
            }
            return rhs;
        }
        else
        {
            return visit(ctx.calculation_expression());
        }
    }

    @Override public information visitEqual(MuseParser.EqualContext ctx) { return visitChildren(ctx); }

    @Override public information visitAssignment_operators(MuseParser.Assignment_operatorsContext ctx) { return visitChildren(ctx); }

    @Override public information visitCalculation_expression(MuseParser.Calculation_expressionContext ctx) {
        return visit(ctx.logical_caclulation_expression());
    }

    @Override public information visitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx) {
        return visit(ctx.logical_or_expression());
    }

    @Override public information visitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx) {
        if(ctx.logical_and_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
            ret.varInfo.varP.reg = reg_allocator.allocate(true, "bool");

            //短路求值是支持的

            //write("loadI 0 " + ret.varInfo.varP.reg);
            information[] s = {new information(0)}, t = {ret};
            write("loadI", s, t);
            String exit_true_label = label_allocator.allocate();
            String exit_label = label_allocator.allocate();
            for(int i = 0; i < ctx.logical_and_expression().size(); ++i)
            {
                information curExpr = visit(ctx.logical_and_expression(i));
                String nextLabel = label_allocator.allocate();
                //write("cbr " + curExpr.varInfo.varP.reg + " " + exit_true_label + " " + nextLabel);
                information[] ss = {curExpr}, tt = {new information(exit_true_label), new information(nextLabel)};
                write("cbr", ss, tt);
                write_string(nextLabel + ": \nnop");
            }
            //write("jumpI " + exit_label);
            information[] ss = {}, tt = {new information(exit_label)};
            write("jumpI", ss, tt);
            write_string(exit_true_label + ":\nnop");
            //write("loadI 1 " + ret.varInfo.varP.reg);
            information[] sss = {new information(1)}, ttt = {ret};
            write("loadI", sss, ttt);
            write_string(exit_label + ":\nnop");
            return ret;
        }
        else
        {
            return visit(ctx.logical_and_expression(0));
        }
    }

    @Override public information visitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx) {
        if(ctx.bitwise_or_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
            ret.varInfo.varP.reg = reg_allocator.allocate(true, "bool");

            //短路求值是支持的

            //write("loadI 1 " + ret.varInfo.varP.reg);
            write("loadI", new information[]{new information(1)}, new information[]{ret});
            String exit_false_label = label_allocator.allocate();
            String exit_label = label_allocator.allocate();
            for(int i = 0; i < ctx.bitwise_or_expression().size(); ++i)
            {
                information curExpr = visit(ctx.bitwise_or_expression(i));
                String nextLabel = label_allocator.allocate();
                //write("cbr " + curExpr.varInfo.varP.reg + " " + nextLabel + " " + exit_false_label);
                write("cbr", new information[]{curExpr}, new information[]{new information(nextLabel), new information(exit_false_label)});
                write_string(nextLabel + ": \nnop");
            }
            //write("jumpI " + exit_label);
            write("jumpI", new information[]{}, new information[]{new information(exit_label)});
            write_string(exit_false_label + ":\nnop");
            //write("loadI 0 " + ret.varInfo.varP.reg);
            write("loadI", new information[]{new information(0)}, new information[]{ret});
            write_string(exit_label + ":\nnop");
            return ret;
        }
        else
        {
            return visit(ctx.bitwise_or_expression(0));
        }
    }

    @Override public information visitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx) {
        if(ctx.bitwise_xor_expression().size() > 1)
        {
            information ans = new information();
            ans.varInfo = new Property();
            ans.varInfo.varP = new Property.VarP();
            ans.varInfo.varP.reg = reg_allocator.allocate(true, "bitwise_or");
            ans.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            //write("loadI 0 " + ans.varInfo.varP.reg);
            write("loadI", new information[]{new information(0)}, new information[]{ans});
            for(int i = 0; i < ctx.bitwise_xor_expression().size(); ++i)
            {
                information lhs = visit(ctx.bitwise_xor_expression(i));
                //write("or " + ans.varInfo.varP.reg + " " + lhs.varInfo.varP.reg + " " + ans.varInfo.varP.reg);
                write("or", new information[]{ans, lhs}, new information[]{ans});
            }
            return ans;
        }
        else
        {
            return visit(ctx.bitwise_xor_expression(0));
        }
    }

    @Override public information visitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx) {
        if(ctx.bitwise_and_expression().size() > 1)
        {
            information ans = new information();
            ans.varInfo = new Property();
            ans.varInfo.varP = new Property.VarP();
            ans.varInfo.varP.reg = reg_allocator.allocate(true, "bitwise_xor");
            ans.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            //write("loadI 0 " + ans.varInfo.varP.reg);
            write("loadI", new information[]{new information(0)}, new information[]{ans});
            for(int i = 0; i < ctx.bitwise_and_expression().size(); ++i)
            {
                information lhs = visit(ctx.bitwise_and_expression(i));
                //write("xor " + ans.varInfo.varP.reg + " " + lhs.varInfo.varP.reg + " " + ans.varInfo.varP.reg);
                write("xor", new information[]{ans, lhs}, new information[]{ans});
            }
            return ans;
        }
        else
        {
            return visit(ctx.bitwise_and_expression(0));
        }
    }

    @Override public information visitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx) {
        if(ctx.equality_expression().size() > 1)
        {
            information ans = new information();
            ans.varInfo = new Property();
            ans.varInfo.varP = new Property.VarP();
            ans.varInfo.varP.reg = reg_allocator.allocate(true, "bitwise_and");
            ans.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            //write("loadI -1 " + ans.varInfo.varP.reg);
            write("loadI", new information[]{new information(-1)}, new information[]{ans});
            for(int i = 0; i < ctx.equality_expression().size(); ++i)
            {
                information lhs = visit(ctx.equality_expression(i));
                //write("and " + ans.varInfo.varP.reg + " " + lhs.varInfo.varP.reg + " " + ans.varInfo.varP.reg);
                write("and", new information[]{ans, lhs}, new information[]{ans});
            }
            return ans;
        }
        else
        {
            return visit(ctx.equality_expression(0));
        }
    }

    @Override public information visitIsequal(MuseParser.IsequalContext ctx) { return visitChildren(ctx); }

    @Override public information visitNotequal(MuseParser.NotequalContext ctx) { return visitChildren(ctx); }

    @Override public information visitEquality_expression(MuseParser.Equality_expressionContext ctx) {
        if(ctx.relation_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.reg = reg_allocator.allocate(true, "eq_e");
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
            String instr;
            if(ctx.equ_op(0).isequal() != null)
                instr = "cmp_EQ";
            else
                instr = "cmp_NE";
            information a = visit(ctx.relation_expression(0)), b = visit(ctx.relation_expression(1));

            //write(instr + " " + a.varInfo.varP.reg + " " + b.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
            if(a.varInfo.varP.type.id.name.equals("string"))
            {
                if(ctx.equ_op(0).isequal() != null)
                {
                    write("call @functionLabel____builtin__string_cmp_EQ", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else
                {
                    write("call @functionLabel____builtin__string_cmp_EQ", new information[]{a, b}, new information[]{});
                    write("rsubI", new information[]{new information("r_{global_return_register}"), new information(1)}, new information[]{ret});
                }
            }
            else {
                write(instr, new information[]{a, b}, new information[]{ret});
            }
            for(int i = 1; i < ctx.equ_op().size(); ++i)
            {
                if(ctx.equ_op(i).isequal() != null)
                    instr = "cmp_EQ";
                else
                    instr = "cmp_NE";
                b = visit(ctx.relation_expression(i + 1));
                //write(instr + " " + b.varInfo.varP.reg + " " + ret.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write(instr, new information[]{b, ret}, new information[]{ret});
            }
            return ret;
        }
        else
        {
            return visit(ctx.relation_expression(0));
        }
    }

    @Override public information visitBigger(MuseParser.BiggerContext ctx) { return visitChildren(ctx); }

    @Override public information visitSmaller(MuseParser.SmallerContext ctx) { return visitChildren(ctx); }

    @Override public information visitBigger_e(MuseParser.Bigger_eContext ctx) { return visitChildren(ctx); }

    @Override public information visitSmaller_e(MuseParser.Smaller_eContext ctx) { return visitChildren(ctx); }

    @Override public information visitRelation_expression(MuseParser.Relation_expressionContext ctx) {
        if(ctx.shift_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.reg = reg_allocator.allocate(true, "rela_e");
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
            information a = visit(ctx.shift_expression(0)), b = visit(ctx.shift_expression(1));
            if(ctx.rela_op().smaller_e() != null) {
                if(a.varInfo.varP.type.id.name.equals("string"))
                {
                    write("call @functionLabel____builtin__string_cmp_LE", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else {
                    write("cmp_LE", new information[]{a, b}, new information[]{ret});
                }
            }
            if(ctx.rela_op().bigger_e() != null) {
                if(a.varInfo.varP.type.id.name.equals("string"))
                {
                    write("call @functionLabel____builtin__string_cmp_GE", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else
                    write("cmp_GE", new information[]{a, b}, new information[]{ret});
            }
            if(ctx.rela_op().bigger() != null) {
                if(a.varInfo.varP.type.id.name.equals("string"))
                {
                    write("call @functionLabel____builtin__string_cmp_GT", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else
                    write("cmp_GT", new information[]{a, b}, new information[]{ret});
            }
            if(ctx.rela_op().smaller() != null) {
                if(a.varInfo.varP.type.id.name.equals("string"))
                {
                    write("call @functionLabel____builtin__string_cmp_LT", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else
                    write("cmp_LT", new information[]{a, b}, new information[]{ret});
            }
            return ret;
        }
        else
        {
            return visit(ctx.shift_expression(0));
        }
    }

    @Override public information visitLshift(MuseParser.LshiftContext ctx) { return visitChildren(ctx); }

    @Override public information visitRshift(MuseParser.RshiftContext ctx) { return visitChildren(ctx); }

    @Override public information visitPlus(MuseParser.PlusContext ctx) { return visitChildren(ctx); }

    @Override public information visitMinus(MuseParser.MinusContext ctx) { return visitChildren(ctx); }

    @Override public information visitShift_expression(MuseParser.Shift_expressionContext ctx) {
        if(ctx.add_expression().size() > 1)
        {
            information ret = new information(); ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.reg = reg_allocator.allocate(false, "shift_e");
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            information a = visit(ctx.add_expression(0)), b = visit(ctx.add_expression(1));
            String instr;
            if(ctx.shift_op(0).lshift() != null)
                instr = "lshift";
            else
                instr = "rshift";
            //write(instr + " " + a.varInfo.varP.reg + " " + b.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
            write(instr, new information[]{a, b}, new information[]{ret});
            for(int i = 1; i < ctx.shift_op().size(); ++i)
            {
                if(ctx.shift_op(i).lshift() != null)
                    instr = "lshift";
                else
                    instr = "rshift";
                a = visit(ctx.add_expression(i + 1));
                //write(instr + " " + ret.varInfo.varP.reg + " " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write(instr, new information[]{ret, a}, new information[]{ret});
            }
            return ret;
        }
        else
        {
            return visit(ctx.add_expression(0));
        }
    }

    @Override public information visitAdd_expression(MuseParser.Add_expressionContext ctx) {
        if(ctx.mul_expression().size() > 1)
        {
            information ret = new information(); ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.reg = reg_allocator.allocate(false, "add_e");
            information a = visit(ctx.mul_expression(0)), b = visit(ctx.mul_expression(1));
            ret.varInfo.varP.type = a.varInfo.varP.type;
            if(a.varInfo.varP.type.id.name.equals("string"))
            {
                write("call", new information[]{new information("@functionLabel___builtin__string_add"), a, b}, new information[]{});
                write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                for(int i = 1; i < ctx.add_op().size(); ++i)
                {
                    a = visit(ctx.mul_expression(i + 1));
                    write("call", new information[]{new information("@functionLabel___builtin__string_add"), ret, a}, new information[]{ret});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
            }
            else {
                String instr;
                if (ctx.add_op(0).plus() != null)
                    instr = "add";
                else
                    instr = "sub";
                //write(instr + " " + a.varInfo.varP.reg + " " + b.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write(instr, new information[]{a, b}, new information[]{ret});
                for (int i = 1; i < ctx.add_op().size(); ++i) {
                    if (ctx.add_op(i).plus() != null)
                        instr = "add";
                    else
                        instr = "sub";
                    a = visit(ctx.mul_expression(i + 1));
                    //write(instr + " " + ret.varInfo.varP.reg + " " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                    write(instr, new information[]{ret, a}, new information[]{ret});
                }
            }
            return ret;
        }
        else
        {
            return visit(ctx.mul_expression(0));
        }
    }

    @Override public information visitMultiply(MuseParser.MultiplyContext ctx) { return visitChildren(ctx); }

    @Override public information visitDivision(MuseParser.DivisionContext ctx) { return visitChildren(ctx); }

    @Override public information visitMod(MuseParser.ModContext ctx) { return visitChildren(ctx); }

    @Override public information visitMul_expression(MuseParser.Mul_expressionContext ctx) {
        if(ctx.unary_expression().size() > 1)
        {
            information ret = new information(); ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            ret.varInfo.varP.reg = reg_allocator.allocate(false, "mul_e");
            information a = visit(ctx.unary_expression(0)), b = visit(ctx.unary_expression(1));
            if(ctx.mul_op(0).multiply() != null)
            {
                //write("mult " + a.varInfo.varP.reg + " " + b.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write("mult", new information[]{a, b}, new information[]{ret});
            }
            else if(ctx.mul_op(0).mod() != null)
            {
                String tempReg = reg_allocator.allocate(true, "modtmp");
                //write("div " + a.varInfo.varP.reg + " " + b.varInfo.varP.reg + " " + tempReg);
                //write("mult " + b.varInfo.varP.reg + " " + tempReg + " " + tempReg);
                //write("sub " + a.varInfo.varP.reg + " " + tempReg + " " + ret.varInfo.varP.reg);
                write("div", new information[]{a, b}, new information[]{new information(tempReg)});
                write("mult", new information[]{b, new information(tempReg)}, new information[]{new information(tempReg)});
                write("sub", new information[]{a, new information(tempReg)}, new information[]{ret});
            }
            else
            {
                //write("div " + a.varInfo.varP.reg + " " + b.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write("div", new information[]{a, b}, new information[]{ret});
            }
            for(int i = 1; i < ctx.mul_op().size(); ++i)
            {
                a = visit(ctx.unary_expression(i + 1));
                if(ctx.mul_op(i).multiply() != null)
                {
                    //write("mult " + ret.varInfo.varP.reg + " " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                    write("mult", new information[]{ret, a}, new information[]{ret});
                }
                else if(ctx.mul_op(i).mod() != null)
                {
                    String tempReg = reg_allocator.allocate(true, "modtmp");
                    //write("div " + ret.varInfo.varP.reg + " " + a.varInfo.varP.reg + " " + tempReg);
                    //write("mult " + a.varInfo.varP.reg + " " + tempReg + " " + tempReg);
                    //write("sub " + ret.varInfo.varP.reg + " " + tempReg + " " + ret.varInfo.varP.reg);
                    write("div", new information[]{ret, a}, new information[]{new information(tempReg)});
                    write("mult", new information[]{a, new information(tempReg)}, new information[]{new information(tempReg)});
                    write("sub", new information[]{ret, new information(tempReg)}, new information[]{ret});
                }
                else
                {
                    //write("div " + ret.varInfo.varP.reg + " " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                    write("div", new information[]{ret, a}, new information[]{ret});
                }
            }
            return ret;
        }
        else
        {
            return visit(ctx.unary_expression(0));
        }
    }

    @Override public information visitPlusplus(MuseParser.PlusplusContext ctx) { return visitChildren(ctx); }

    @Override public information visitMinusminus(MuseParser.MinusminusContext ctx) { return visitChildren(ctx); }

    private Integer toInt(String x)
    {
        Integer ret = 0;
        for(int i = 0; i < x.length(); ++i)
            ret = ret * 10 + ((int)x.charAt(i)) - 48;
        return ret;
    }

    @Override public information visitUnary_expression(MuseParser.Unary_expressionContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(false, "unary_e");
        if(ctx.postfix_expression() != null)
        {
            return visit(ctx.postfix_expression());
        }
        else if(ctx.unary_operation() != null)
        {
            information a = visit(ctx.unary_expression());
            ret.varInfo.varP.type = a.varInfo.varP.type;
            if(ctx.unary_operation().not_sign() != null)
            {
                //write("xorI 1 " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write("xorI", new information[]{a, new information(1)}, new information[]{ret});
            }
            else if(ctx.unary_operation().minus() != null)
            {
                //write("rsubI 0 " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write("rsubI", new information[]{a, new information(0)}, new information[]{ret});
            }
            else if(ctx.unary_operation().minusminus() != null)
            {
                //write("subI " + ret.varInfo.varP.reg + " 1 " + ret.varInfo.varP.reg);
                write("subI", new information[]{a, new information(1)}, new information[]{ret});
                write("subI", new information[]{a, new information(1)}, new information[]{a});
            }
            else if(ctx.unary_operation().plusplus() != null)
            {
                //write("addI " + ret.varInfo.varP.reg + " 1 " + ret.varInfo.varP.reg);
                write("addI", new information[]{a, new information(1)}, new information[]{ret});
                write("addI", new information[]{a, new information(1)}, new information[]{a});
            }
            else if(ctx.unary_operation().type_name() != null)
            {
                throw new RuntimeException("WTF type transform is not allowed");
            }
            else if(ctx.unary_operation().bitwise_not() != null)
            {
                //write("xorI -1 " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write("xorI", new information[]{a, new information(-1)}, new information[]{ret});
            }
            else
            {
                throw new RuntimeException("This code cannot be executed.");
            }
        }
        else if(ctx.number() != null)
        {
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            //write("loadI " + ctx.number().getText() + " " + ret.varInfo.varP.reg);
            write("loadI", new information[]{new information(toInt(ctx.number().getText()))}, new information[]{ret});
        }
        else if(ctx.functionCall_expression() != null)
        {
            return visit(ctx.functionCall_expression());
        }
        else if(ctx.new_operation() != null)
        {
            return visit(ctx.new_operation());
        }
        else if(ctx.pre_defined_constants() != null)
        {
            if(ctx.pre_defined_constants().getText().equals("true"))
            {
                ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
                //write("loadI 1 " + ret.varInfo.varP.reg);
                write("loadI", new information[]{new information(1)}, new information[]{ret});
            }
            if(ctx.pre_defined_constants().getText().equals("false"))
            {
                ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
                //write("loadI 0 " + ret.varInfo.varP.reg);
                write("loadI", new information[]{new information(0)}, new information[]{ret});
            }
            if(ctx.pre_defined_constants().getText().equals("null"))
            {
                ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("1122special"));
                //write("loadI 0 " + ret.varInfo.varP.reg);
                write("loadI", new information[]{new information(0)}, new information[]{ret});
            }
        }
        else
        {
            throw new RuntimeException("This code cannot be executed");
        }
        return ret;
    }

    @Override public information visitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx) { return visitChildren(ctx); }

    @Override public information visitClass_new(MuseParser.Class_newContext ctx) { return visitChildren(ctx); }

    @Override public information visitNew_operation(MuseParser.New_operationContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(true, "new_op");
        if(ctx.class_new() != null)
        {
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName(ctx.class_new().Identifier().getText()));
            write("mallocI", new information[]{new information(ret.varInfo.varP.type.typeP.realSize + RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{ret});
            write("mark_pointerI", new information[]{ret, new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size), new information(ret.varInfo.varP.type.typeP.realSize)}, new information[]{});
            String tempReg = reg_allocator.allocate(true, "");
            write("loadI", new information[]{new information(ret.varInfo.varP.type.typeP.realSize)}, new information[]{new information(tempReg)});
            write("store", new information[]{new information(tempReg)}, new information[]{ret});
        }
        else
        {
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName(ctx.array_new().type_non_array().getText()));
            //MR assume, ctx.array_new().expression().size == 1 is always true
            information b = visit(ctx.array_new().expression(0));
            String arraySize = reg_allocator.allocate(true, "");
            String byteArraySize = reg_allocator.allocate(true, "");
            write("i2i", new information[]{b}, new information[]{new information(arraySize)});
            write("multI", new information[]{new information(arraySize), new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(byteArraySize)});
            write("addI", new information[]{new information(byteArraySize), new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(byteArraySize)});
            write("malloc", new information[]{new information(byteArraySize)}, new information[]{ret});
            write("mark_pointer", new information[]{ret, new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size), b}, new information[]{});
            write("store", new information[]{new information(arraySize)}, new information[]{ret});
        }
        return ret;
    }

    @Override public information visitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = "r_{global_return_register}";
        Property func = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()));
        ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName(func.funcP.retType));
        //String toWrite = "call " + func.funcP.label + " " + ret.varInfo.varP.reg;
        String opcode = "call";
        information[] src, tar = new information[]{};
        if(ctx.arguements() != null)
        {
            src = new information[ctx.arguements().assignment_expression().size() + 1];
            src[0] = new information(func.funcP.label);
            for(int i = 0; i < ctx.arguements().assignment_expression().size(); ++i)
            {
                information b = visit(ctx.arguements().assignment_expression(i));
                //toWrite = toWrite + " " + b.varInfo.varP.reg;
                src[i + 1] = b;
            }
        }
        else
            src = new information[]{new information(func.funcP.label)};
        write(opcode, src, tar);
        //write(toWrite);
        return ret;
    }

    @Override public information visitArguements(MuseParser.ArguementsContext ctx) { return visitChildren(ctx); }

    @Override public information visitValue(MuseParser.ValueContext ctx) { return visitChildren(ctx); }

    @Override public information visitNot_sign(MuseParser.Not_signContext ctx) { return visitChildren(ctx); }

    @Override public information visitUnary_operation(MuseParser.Unary_operationContext ctx) { return visitChildren(ctx); }

    @Override public information visitBitwise_not(MuseParser.Bitwise_notContext ctx) { return visitChildren(ctx); }

    @Override public information visitPostfix_expression(MuseParser.Postfix_expressionContext ctx) {
        information ret = (information) visit(ctx.primary_expression()).clone();
        String tempReg = reg_allocator.allocate(true, "array_addr_finder");
        for(int i = 0; i < ctx.postfix().size(); ++i)
        {
            if(ctx.postfix(i).expression() != null)
            {
                information b = visit(ctx.postfix(i).expression());

                information newRet = new information();
                newRet.varInfo = new Property();
                newRet.varInfo.varP = new Property.VarP();
                newRet.varInfo.varP.type = ret.varInfo.varP.type;
                newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");

                write("multI", new information[]{b, new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(tempReg)});
                write("add", new information[]{new information(tempReg), ret}, new information[]{new information(tempReg)});
                //the 1st element is the size, so add one unit
                write("addI", new information[]{new information(tempReg), new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(tempReg)});
                ret = newRet;
                write("load", new information[]{new information(tempReg)}, new information[]{ret});
                newRet.varInfo.varP.addr_reg = reg_allocator.allocate(false, "addr");
                newRet.varInfo.varP.is_static = true;
                write("i2i", new information[]{new information(tempReg, null)},new information[]{new information(ret.varInfo.varP.addr_reg, null)});
            }
            if(ctx.postfix(i).getMember() != null)
            {
                if(ctx.postfix(i).functionCall_expression() != null)
                {
                    if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("size"))
                    {
                        //ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
                        write("load", new information[]{ret}, new information[]{newRet});
                        ret = newRet;
                    }
                    else if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("ord") && ret.varInfo.varP.type.id.name.equals("string"))
                    {
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
                        if(ctx.postfix(i).functionCall_expression().arguements().assignment_expression().size() != 1)
                        {
                            throw new RuntimeException("parameters not correct\n");
                        }
                        write("call", new information[]{new information("@functionLabel___builtin__ord"), ret, visit(ctx.postfix(i).functionCall_expression().arguements().assignment_expression(0))}, new information[]{});
                        write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{newRet});
                        ret = newRet;
                    }
                    else if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("length") && ret.varInfo.varP.type.id.name.equals("string"))
                    {
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
                        write("load", new information[]{ret}, new information[]{newRet});
                        ret = newRet;
                    }
                    else
                    {
                        throw new RuntimeException("builtin member not found or not completed\n");
                    }
                }
                else {
                    ArrayList<Property> members = ret.varInfo.varP.type.typeP.ctx;
                    for (int j = 0; j < members.size(); ++j) {
                        if (members.get(j).id.name.equals(ctx.postfix(i).Identifier().getText())) {
                            information newRet = new information();
                            newRet.varInfo = new Property();
                            newRet.varInfo.varP = new Property.VarP();
                            newRet.varInfo.varP.type = ret.varInfo.varP.type;
                            newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");

                            write("loadI", new information[]{new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size * j)}, new information[]{new information(tempReg, null)});
                            write("add", new information[]{new information(tempReg, null), ret}, new information[]{new information(tempReg, null)});
                            ret = newRet;
                            write("load", new information[]{new information(tempReg, null)}, new information[]{ret});
                            ret.varInfo.varP.addr_reg = reg_allocator.allocate(false, "addr");
                            write("i2i", new information[]{new information(tempReg, null)}, new information[]{new information(ret.varInfo.varP.addr_reg, null)});
                            ret.varInfo.varP.type = members.get(j);
                            ret.varInfo.varP.is_static = true;
                            break;
                        }
                    }
                }
            }
            if(ctx.postfix(i).plusplus() != null)
            {
                information newRet = new information();
                newRet.varInfo = new Property(); newRet.varInfo.varP = new Property.VarP();
                newRet.varInfo.varP.reg = reg_allocator.allocate(true, "renaming");
                write("i2i", new information[]{ret}, new information[]{newRet});
                write("addI", new information[]{ret}, new information[]{new information(1)});
                return newRet;
            }
            if(ctx.postfix(i).minusminus() != null)
            {
                information newRet = new information();
                newRet.varInfo = new Property(); newRet.varInfo.varP = new Property.VarP();
                newRet.varInfo.varP.reg = reg_allocator.allocate(true, "renaming");
                write("i2i", new information[]{ret}, new information[]{newRet});
                write("subI", new information[]{ret, new information(1)}, new information[]{ret});
                return newRet;
            }
        }
        return ret;
    }

    @Override public information visitGetMember(MuseParser.GetMemberContext ctx) { return visitChildren(ctx); }

    @Override public information visitPostfix(MuseParser.PostfixContext ctx) { return visitChildren(ctx); }

    @Override public information visitPrimary_expression(MuseParser.Primary_expressionContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(true, "prim_ex");
        if(ctx.Identifier() != null)
        {
            Property b = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()));
            write("i2i", new information[]{new information(b.varP.reg, b.varP.addr_reg)}, new information[]{ret});
            ret.varInfo.varP.addr_reg = b.varP.addr_reg;
            ret.varInfo.varP.type = b.varP.type;
        }
        if(ctx.constant() != null)
        {
            information b = visit(ctx.constant());
            ret.varInfo.varP.reg = b.varInfo.varP.reg;
            ret.varInfo.varP.addr_reg = b.varInfo.varP.addr_reg;
            ret.varInfo.varP.type = b.varInfo.varP.type;
        }
        if(ctx.expression() != null)
        {
            information b = visit(ctx.expression());
            write("i2i", new information[]{b}, new information[]{ret});
            ret.varInfo.varP.addr_reg = b.varInfo.varP.addr_reg;
            ret.varInfo.varP.type = b.varInfo.varP.type;
        }
        if(ctx.functionCall_expression() != null)
        {
            information b = visit(ctx.functionCall_expression());
            write("i2i", new information[]{b}, new information[]{ret});
            ret.varInfo.varP.addr_reg = b.varInfo.varP.addr_reg;
            ret.varInfo.varP.type = b.varInfo.varP.type;
        }
        return ret;
    }

    @Override public information visitType_name(MuseParser.Type_nameContext ctx) { return visitChildren(ctx); }

    @Override public information visitConstant(MuseParser.ConstantContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(true, "constant");
        if(ctx.numeric_constant() != null)
        {
            write("loadI", new information[]{new information(ctx.numeric_constant().getText(), null)}, new information[]{ret});
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
        }
        else
        {
            write("call", new information[]{new information("@functionLabel___builtin_string_generate_" + ctx.string_constant().getText())}, new information[]{});
            write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("string"));
        }
        return ret;
    }

    @Override public information visitNumeric_constant(MuseParser.Numeric_constantContext ctx) { return visitChildren(ctx); }

    @Override public information visitInteger_constant(MuseParser.Integer_constantContext ctx) { return visitChildren(ctx); }

    @Override public information visitString_constant(MuseParser.String_constantContext ctx) { return visitChildren(ctx); }

    @Override public information visitTypenametokens(MuseParser.TypenametokensContext ctx) { return visitChildren(ctx); }

    @Override public information visitNumber(MuseParser.NumberContext ctx) { return visitChildren(ctx); }

    @Override public information visitUnion_class(MuseParser.Union_classContext ctx) { return visitChildren(ctx); }
    @Override public information visitMul_op(MuseParser.Mul_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitAdd_op(MuseParser.Add_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitShift_op(MuseParser.Shift_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitRela_op(MuseParser.Rela_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitEqu_op(MuseParser.Equ_opContext ctx) { return visitChildren(ctx); }

}