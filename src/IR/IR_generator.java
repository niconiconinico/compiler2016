package IR;

import IR.Memory_Model.Reg;
import IR.symbol_table.Symbol_table;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
/* 约定
 * 所有的局部变量都有寄存器(如果是非int,bool,则是一个指针)
 * 所有的类和数组,只拥有一个地址寄存器,其内容在内存中选取
 * 所有的全局变量都有寄存器
 */
public class IR_generator {
    public void generate(ParseTree tree) throws IOException
    {
        //generate IR according to tree
        //the result is saved in .ll.ir
        Symbol_table symbol_table = new Symbol_table();
        Reg reg_allocator = new Reg();
        LabelAllocator label_allocator = new LabelAllocator();
        BufferedWriter out = new BufferedWriter(new FileWriter("ll.ir"));
        Integer static_area_size = 0;

        Pass1 p1 = new Pass1();
        p1.reg_allocator = reg_allocator;
        p1.symbol_table = symbol_table;
        p1.label_allocator = label_allocator;
        p1.visit(tree);

        Pass1_complete p1c = new Pass1_complete();
        p1c.reg_allocator = reg_allocator;
        p1c.symbol_table = symbol_table;
        p1c.label_allocator = label_allocator;
        p1c.visit(tree);

        static_area_size = p1c.static_area_size;

        Pass2_prepare p2p = new Pass2_prepare();
        p2p.reg_allocator = reg_allocator;
        p2p.static_area_size = static_area_size;
        p2p.symbol_table = symbol_table;
        p2p.label_allocator = label_allocator;
        p2p.out = out;
        p2p.visit(tree);

        Pass2_string_prepare p2sp = new Pass2_string_prepare();
        p2sp.reg_allocator = reg_allocator;
        p2sp.static_area_size = static_area_size;
        p2sp.symbol_table = symbol_table;
        p2sp.label_allocator = label_allocator;
        p2sp.out = out;
        p2sp.visit(tree);

        Pass2 p2 = new Pass2();
        p2.out = out;
        p2.reg_allocator = reg_allocator;
        p2.symbol_table = symbol_table;
        p2.label_allocator = label_allocator;
        p2.visit(tree);

        out.close();
    }
}