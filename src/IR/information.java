package IR;

import IR.symbol_table.Property;

/**
 * Created by xietiancheng on 16/4/23.
 */
public class information implements Cloneable {
    //TODO think about this
    Property varInfo; //Should contain register or memory location
    Property typeInfo;
    Property funcInfo;
    information()
    {
        varInfo = null;
        typeInfo = null;
        funcInfo = null;
    }
    information(int x)
    {
        Integer t = x;
        varInfo = new Property();
        varInfo.varP = new Property.VarP();
        varInfo.varP.reg = t.toString();
    }
    information(String x)
    {
        varInfo = new Property();
        varInfo.varP = new Property.VarP();
        varInfo.varP.reg = x;
    }
    information(String x, String y)
    {
        varInfo = new Property();
        varInfo.varP = new Property.VarP();
        varInfo.varP.reg = x;
        varInfo.varP.addr_reg = y;
        if(y != null)
            varInfo.varP.is_static = true;
    }
    @Override
    public Object clone()
    {
        information sc = null;
        try
        {
            sc = (information) super.clone();
            if(varInfo != null)
                sc.varInfo = (Property)varInfo.clone();
            if(funcInfo != null)
                sc.funcInfo = (Property)funcInfo.clone();
            if(typeInfo != null)
                sc.typeInfo = (Property)typeInfo.clone();
            return sc;
        } catch(Exception e)
        {
            throw new RuntimeException("cannot clone");
        }
    }
}
