// Generated from Muse.g4 by ANTLR 4.5.2
package IR;
import IR.Memory_Model.Reg;
import IR.symbol_table.Name;
import IR.symbol_table.Property;
import IR.symbol_table.Symbol_table;
import Parser.MuseParser;
import Parser.MuseVisitor;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * What I have done?
 *  1. Class and it's context, offset is ok
 *  2. Function's parameter list, register is ok
 * What I want to do?
 *  1. global vars
 *  2. statements and else in function
 *  3. In a word, I have to generate IR in this file
 *  4. consider use call to call a function. avoid some details.
 */

public class Pass2 extends AbstractParseTreeVisitor<information> implements MuseVisitor<information> {
    Symbol_table symbol_table;
    Reg reg_allocator;
    LabelAllocator label_allocator;
    BufferedWriter out;

    private void write_string(String x)
    {
        try
        {
            out.write(x);
            out.newLine();
        }
        catch(Exception e)
        {
            throw new RuntimeException("IO Error");
        }
    }

    private void write_direct(String opcode, information[] src, information[] target) throws IOException
    {
        out.write(opcode);
        for(int i = 0; i < src.length; ++i)
            out.write(" " + src[i].varInfo.varP.reg);
        for(int i = 0; i < target.length; ++i)
            out.write(" " + target[i].varInfo.varP.reg);
        out.newLine();
        for(int i = 0; i < target.length; ++i)
        {
            if(target[i].varInfo.varP.addr_reg != null)
            {
                information s, t;
                s = new information();
                s.varInfo = new Property(); s.varInfo.varP = new Property.VarP();
                t = new information();
                t.varInfo = new Property(); t.varInfo.varP = new Property.VarP();

                s.varInfo.varP.reg = target[i].varInfo.varP.reg;
                t.varInfo.varP.reg = target[i].varInfo.varP.addr_reg;
                information[] Src = {s}, Tar = {t};
                write("store", Src, Tar);
            }
        }
    }

    private void write(String opcode, information[] src, information[] target)
    {
        try
        {
            write_direct(opcode, src, target);
        }
        catch(Exception e)
        {
            throw new RuntimeException("IO Error");
        }
    }

    @Override public information visitProgram(MuseParser.ProgramContext ctx) {
        return visitChildren(ctx);
    }

    @Override public information visitDeclaration(MuseParser.DeclarationContext ctx) {
        if(ctx.getParent() instanceof MuseParser.ProgramContext)
        {
            //Global vars or class, skip
            return null;
        }
        else
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.id = Name.getSymbolName(ctx.init_declarators().init_declarator().declarator().getText());
            ret.varInfo.varP.reg = reg_allocator.allocate(false, ctx.init_declarators().init_declarator().declarator().getText());
            ret.varInfo.varP.type = visit(ctx.type()).typeInfo;
            symbol_table.put(ret.varInfo.id, ret.varInfo);
            if(ctx.init_declarators().init_declarator().equal() == null)
                return null;
            information rhs = visit(ctx.init_declarators().init_declarator().initializer());
            information tmp = new information();
            tmp.varInfo = ret.varInfo;
            write("i2i", new information[]{rhs}, new information[]{tmp});
            return ret;
        }
    }

    @Override public information visitInit_declarators(MuseParser.Init_declaratorsContext ctx) { return visitChildren(ctx); }

    @Override public information visitDeclarators(MuseParser.DeclaratorsContext ctx) { return visitChildren(ctx); }

    @Override public information visitInit_declarator(MuseParser.Init_declaratorContext ctx) { return visitChildren(ctx); }

    @Override public information visitType(MuseParser.TypeContext ctx) {
        information ret = new information();
        if(ctx.array_decl() != null)
            ret.typeInfo = symbol_table.get(Name.getSymbolName(ctx.array_decl().type_non_array().getText()));
        else
            ret.typeInfo = symbol_table.get(Name.getSymbolName(ctx.type_non_array().getText()));
        return ret;
    }

    @Override public information visitArray_decl(MuseParser.Array_declContext ctx) { return visitChildren(ctx); }

    @Override public information visitArray_new(MuseParser.Array_newContext ctx) { return visitChildren(ctx); }

    @Override public information visitType_non_array(MuseParser.Type_non_arrayContext ctx) { return visitChildren(ctx); }

    @Override public information visitClass_declaration(MuseParser.Class_declarationContext ctx) { return visitChildren(ctx); }

    @Override public information visitDeclarator(MuseParser.DeclaratorContext ctx) { return visitChildren(ctx); }

    @Override public information visitInitializer(MuseParser.InitializerContext ctx) {
        return visit(ctx.assignment_expression());
    }

    @Override public information visitFunction_definition(MuseParser.Function_definitionContext ctx) {
        symbol_table.beginScope();
        String headString = ("@functionLabel_"+ctx.Identifier().getText());
        if(ctx.parameters() != null) {
            for (int i = 0; i < ctx.parameters().parameter().size(); ++i) {
                Property cur = new Property();
                cur.id = Name.getSymbolName(ctx.parameters().parameter(i).declarator().getText());
                cur.varP = new Property.VarP();
                cur.varP.is_static = false;
                cur.varP.reg = reg_allocator.allocate(false, "arg_{"+i+"}_"+cur.id.name);
                headString = headString + " " + cur.varP.reg;
                information ty = visit(ctx.parameters().parameter(i).type());
                cur.varP.type = ty.typeInfo;
                symbol_table.put(cur.id, cur);
            }
        }
        write_string(headString);
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = "r_{global_return_register}";
        information ty = visit(ctx.type());
        ret.varInfo.varP.type = ty.typeInfo;
        ret.varInfo.varP.is_static = false;
        for(int i = 0; i < ctx.statement().size(); ++i)
        {
            visit(ctx.statement(i));
        }
        symbol_table.endScope();
        write("return", new information[]{ret}, new information[]{});
        return ret;
    }

    @Override public information visitParameters(MuseParser.ParametersContext ctx) { return visitChildren(ctx); }

    @Override public information visitParameter(MuseParser.ParameterContext ctx) { return visitChildren(ctx); }

    @Override public information visitStatement(MuseParser.StatementContext ctx) {
        if(ctx.compond_statement() != null)
            return visit(ctx.compond_statement());
        if(ctx.declaration() != null)
            return visit(ctx.declaration());
        if(ctx.assignment_statement() != null)
            return visit(ctx.assignment_statement());
        if(ctx.structured_statement() != null)
            return visit(ctx.structured_statement());
        return null;
    }

    @Override public information visitAssignment_statement(MuseParser.Assignment_statementContext ctx) {
        if(ctx.expression() != null)
            return visit(ctx.expression());
        else
            return null;
    }

    @Override public information visitStructured_statement(MuseParser.Structured_statementContext ctx) {
        if(ctx.loop_statement() != null)
            return visit(ctx.loop_statement());
        else
            return visit(ctx.branch_statement());
    }

    @Override public information visitCompond_statement(MuseParser.Compond_statementContext ctx) {
        symbol_table.beginScope();
        for(int i = 0; i < ctx.statement().size(); ++i)
            visit(ctx.statement(i));
        symbol_table.endScope();
        return null;
    }


    String curLoopExit, curLoopStart;

    boolean cond_simplify(MuseParser.ExpressionContext exp, String true_label, String false_label)
    {
        try
        {
            MuseParser.Equality_expressionContext equ_ex;
            MuseParser.Relation_expressionContext rla_ex;
            List<MuseParser.Logical_and_expressionContext> land = exp.assignment_expression().calculation_expression().logical_caclulation_expression().logical_or_expression().logical_and_expression();
            if(land.size() != 1)
                return false;
            if(land.get(0).bitwise_or_expression().size() != 1)
                return false;
            if(land.get(0).bitwise_or_expression(0).bitwise_xor_expression().size() != 1)
                return false;
            if(land.get(0).bitwise_or_expression(0).bitwise_xor_expression(0).bitwise_and_expression().size() != 1)
                return false;
            MuseParser.Bitwise_and_expressionContext bwa = land.get(0).bitwise_or_expression(0).bitwise_xor_expression(0).bitwise_and_expression(0);
            if(bwa.equality_expression().size() != 1)
                return false;
            if(bwa.equality_expression(0).equ_op().size() > 1)
                return false;
            if(bwa.equality_expression(0).equ_op().size() == 1)
            {
                String instr;
                equ_ex = bwa.equality_expression(0);
                information a, b;
                a = visit(equ_ex.relation_expression(0)); b = visit(equ_ex.relation_expression(1));
                if(equ_ex.equ_op(0).getText().equals("=="))
                {
                    instr = "cbr_ne";
                }
                else
                    instr = "cbr_eq";
                write(instr, new information[]{a, b}, new information[]{new information(false_label)});
                return true;
            }
            if(bwa.equality_expression(0).relation_expression().size() != 1)
                return false;
            rla_ex = bwa.equality_expression(0).relation_expression(0);
            if(rla_ex.rela_op() == null)
                return false;
            String rla_op = rla_ex.rela_op().getText();
            String instr;
            if(rla_op.equals("<"))
                instr = "cbr_ge";
            else if(rla_op.equals("<="))
                instr = "cbr_gt";
            else if(rla_op.equals(">"))
                instr = "cbr_le";
            else
                instr = "cbr_lt";
            information a, b;
            a = visit(rla_ex.shift_expression(0)); b = visit(rla_ex.shift_expression(1));
            write(instr, new information[]{a, b}, new information[]{new information(false_label)});
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }


    @Override public information visitLoop_statement(MuseParser.Loop_statementContext ctx) {
        if(ctx.while_loop_statement() != null) {
            String loopSave1 = curLoopExit, loopSave2 = curLoopStart;
            String labelCond = label_allocator.allocate("while_cond");
            String labelEnd = label_allocator.allocate("while_end");
            String labelStart = label_allocator.allocate("while_start");
            curLoopExit = labelEnd;
            curLoopStart = labelCond;
            write_string(labelCond + ":\nnop");
            if (!cond_simplify(ctx.while_loop_statement().expression(), labelStart, labelEnd))
            {
                information b = visit(ctx.while_loop_statement().expression());
                write("cbr", new information[]{b}, new information[]{new information(labelStart), new information(labelEnd)});
            }
            write_string(labelStart+":\nnop");
            visit(ctx.while_loop_statement().statement());
            write("jumpI", new information[]{new information(labelCond)}, new information[]{});
            write_string(labelEnd + ":\nnop");
            curLoopExit = loopSave1; curLoopStart = loopSave2;
        }
        else
        {
            String loopSave1 = curLoopExit, loopSave2 = curLoopStart;
            symbol_table.beginScope();
            if(ctx.for_loop_statement().first_expression() != null)
                visit(ctx.for_loop_statement().first_expression());

            String labelCond = label_allocator.allocate("for_cond");
            String labelBody = label_allocator.allocate("for_body");
            String labelExp3 = label_allocator.allocate("for_exp3");
            String labelExit = label_allocator.allocate("for_exit");
            curLoopExit = labelExit;
            curLoopStart = labelExp3;

            write("jumpI", new information[]{new information(labelCond)}, new information[]{});
            write_string(labelExp3 + ":\nnop");
            if(ctx.for_loop_statement().third_expression() != null)
                visit(ctx.for_loop_statement().third_expression());
            write_string(labelCond+":\nnop");
            if(ctx.for_loop_statement().second_expression() != null) {
                if(!cond_simplify(ctx.for_loop_statement().second_expression().expression(), labelBody, labelExit)) {
                    information b = visit(ctx.for_loop_statement().second_expression());
                    write("cbr", new information[]{b}, new information[]{new information(labelBody), new information(labelExit)});
                }
            }
            else
            {
                write("jumpI", new information[]{new information(labelBody)}, new information[]{});
            }
            write_string(labelBody + ":\nnop");
            visit(ctx.for_loop_statement().statement());
            write("jumpI", new information[]{new information(labelExp3)}, new information[]{});
            write_string(labelExit + ":\nnop");

            symbol_table.endScope();
            curLoopExit = loopSave1; curLoopStart = loopSave2;
        }
        return null;
    }
    @Override public information visitBranch_statement(MuseParser.Branch_statementContext ctx) {
        if(ctx.if_statement() != null)
        {
            visit(ctx.if_statement());
        }
        if(ctx.BREAK() != null)
        {
            write("jumpI", new information[]{new information(curLoopExit)}, new information[]{});
        }
        if(ctx.CONTINUE() != null)
        {
            write("jumpI", new information[]{new information(curLoopStart)}, new information[]{});
        }
        if(ctx.RETURN() != null)
        {
            if(ctx.expression() != null) {
                information b = visit(ctx.expression());
                write("i2i", new information[]{b}, new information[]{new information("r_{global_return_register}")});
            }
            write("return", new information[]{new information("r_{global_return_register}")}, new information[]{});
        }
        return null;
    }

    @Override public information visitFor_loop_statement(MuseParser.For_loop_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitFirst_expression(MuseParser.First_expressionContext ctx) { return visitChildren(ctx); }

    @Override public information visitSecond_expression(MuseParser.Second_expressionContext ctx) { return visitChildren(ctx); }

    @Override public information visitThird_expression(MuseParser.Third_expressionContext ctx) { return visitChildren(ctx); }

    @Override public information visitWhile_loop_statement(MuseParser.While_loop_statementContext ctx) { return visitChildren(ctx); }

    @Override public information visitIf_statement(MuseParser.If_statementContext ctx) {
        if(ctx.ELSE() != null)
        {
            String body1 = label_allocator.allocate("if_body1"), body2 = label_allocator.allocate("if_body2");
            String exit = label_allocator.allocate("if_exit");
            try
            {
                //try to find simple branch statment
                //a > b, a < b, a == b, a <= b, a >= b, a != b
                List<MuseParser.Logical_and_expressionContext> land = ctx.expression().assignment_expression().calculation_expression().logical_caclulation_expression().logical_or_expression().logical_and_expression();

                if(land.size() == 1 && land.get(0).bitwise_or_expression().size() == 1 && land.get(0).bitwise_or_expression(0).bitwise_xor_expression().size() == 1)
                {
                    if(land.get(0).bitwise_or_expression(0).bitwise_xor_expression(0).bitwise_and_expression().size() == 1)
                    {
                        MuseParser.Bitwise_and_expressionContext bwa = land.get(0).bitwise_or_expression(0).bitwise_xor_expression(0).bitwise_and_expression(0);
                        if(bwa.equality_expression().size() == 1)
                        {
                            if(bwa.equality_expression(0).equ_op().size() == 1)
                            {
                                String equ_op = bwa.equality_expression(0).equ_op(0).getText();
                                information a, b;
                                a = visit(bwa.equality_expression(0).relation_expression(0));
                                b = visit(bwa.equality_expression(0).relation_expression(1));
                                if(a.varInfo.varP.type.id.name.equals("string"))
                                {
                                    write("call", new information[]{new information("@functionLabel___builtin__string_cmp_EQ"), a, b}, new information[]{});
                                    if (equ_op.equals("==")) {
                                        write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(body2)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write("jumpI", new information[]{new information(exit)}, new information[]{});
                                        write_string(body2 + ":\nnop");
                                        visit(ctx.statement(1));
                                        write_string(exit + ":\nnop");
                                    } else {
                                        write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body2), new information(body1)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write("jumpI", new information[]{new information(exit)}, new information[]{});
                                        write_string(body2 + ":\nnop");
                                        visit(ctx.statement(1));
                                        write_string(exit + ":\nnop");
                                    }
                                }
                                else {
                                    if (equ_op.equals("==")) {
                                        write("cbr_ne", new information[]{a, b}, new information[]{new information(body2)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write("jumpI", new information[]{new information(exit)}, new information[]{});
                                        write_string(body2 + ":\nnop");
                                        visit(ctx.statement(1));
                                        write_string(exit + ":\nnop");
                                    } else {
                                        write("cbr_eq", new information[]{a, b}, new information[]{new information(body2)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write("jumpI", new information[]{new information(exit)}, new information[]{});
                                        write_string(body2 + ":\nnop");
                                        visit(ctx.statement(1));
                                        write_string(exit + ":\nnop");
                                    }
                                }
                                return null;
                            }
                            else if(bwa.equality_expression().size() == 1 && bwa.equality_expression(0).relation_expression().size() == 1)
                            {
                                MuseParser.Relation_expressionContext rla = bwa.equality_expression(0).relation_expression(0);
                                if(rla.rela_op() != null)
                                {
                                    String rela_op = rla.rela_op().getText();
                                    String instr;
                                    if(rela_op.equals(">"))
                                    {
                                        instr = "cbr_le";
                                    }
                                    else if(rela_op.equals(">="))
                                    {
                                        instr = "cbr_lt";
                                    }
                                    else if(rela_op.equals("<"))
                                    {
                                        instr = "cbr_ge";
                                    }
                                    else if(rela_op.equals("<="))
                                    {
                                        instr = "cbr_gt";
                                    }
                                    else
                                    {
                                        throw new RuntimeException("simplify error");
                                    }
                                    information a, b;
                                    a = visit(rla.shift_expression(0)); b = visit(rla.shift_expression(1));
                                    if(a.varInfo.varP.type.id.name.equals("string"))
                                    {
                                        if(rela_op.equals(">"))
                                        {
                                            write("call", new information[]{new information("@functionLabel___builtin__string_cmp_GT"), a, b}, new information[]{});
                                            write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(body2)});
                                            write_string(body1 + ":\nnop");
                                            visit(ctx.statement(0));
                                            write("jumpI", new information[]{new information(exit)}, new information[]{});
                                            write_string(body2 + ":\nnop");
                                            visit(ctx.statement(1));
                                            write_string(exit + ":\nnop");
                                        }
                                        else if(rela_op.equals(">="))
                                        {
                                            write("call", new information[]{new information("@functionLabel___builtin__string_cmp_GE"), a, b}, new information[]{});
                                            write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(body2)});
                                            write_string(body1 + ":\nnop");
                                            visit(ctx.statement(0));
                                            write("jumpI", new information[]{new information(exit)}, new information[]{});
                                            write_string(body2 + ":\nnop");
                                            visit(ctx.statement(1));
                                            write_string(exit + ":\nnop");
                                        }
                                        else if(rela_op.equals("<"))
                                        {

                                            write("call", new information[]{new information("@functionLabel___builtin__string_cmp_LT"), a, b}, new information[]{});
                                            write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(body2)});
                                            write_string(body1 + ":\nnop");
                                            visit(ctx.statement(0));
                                            write("jumpI", new information[]{new information(exit)}, new information[]{});
                                            write_string(body2 + ":\nnop");
                                            visit(ctx.statement(1));
                                            write_string(exit + ":\nnop");
                                        }
                                        else if(rela_op.equals("<="))
                                        {

                                            write("call", new information[]{new information("@functionLabel___builtin__string_cmp_LE"), a, b}, new information[]{});
                                            write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(body2)});
                                            write_string(body1 + ":\nnop");
                                            visit(ctx.statement(0));
                                            write("jumpI", new information[]{new information(exit)}, new information[]{});
                                            write_string(body2 + ":\nnop");
                                            visit(ctx.statement(1));
                                            write_string(exit + ":\nnop");
                                        }
                                        else
                                        {
                                            throw new RuntimeException("simplify error");
                                        }
                                    }
                                    else {
                                        write(instr, new information[]{a, b}, new information[]{new information(body2)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write("jumpI", new information[]{new information(exit)}, new information[]{});
                                        write_string(body2 + ":\nnop");
                                        visit(ctx.statement(1));
                                        write_string(exit + ":\nnop");
                                    }
                                    return null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            information b = visit(ctx.expression());
            write("cbr", new information[]{b}, new information[]{new information(body1), new information(body2)});
            write_string(body1 + ":\nnop");
            visit(ctx.statement(0));
            write("jumpI", new information[]{new information(exit)}, new information[]{});
            write_string(body2 + ":\nnop");
            visit(ctx.statement(1));
            write("jumpI", new information[]{new information(exit)}, new information[]{});
            write_string(exit + ":\nnop");
        }
        else
        {
            String body1 = label_allocator.allocate("if_body1");
            String exit = label_allocator.allocate("if_exit");
            try
            {
                //try to find simple branch statment
                //a > b, a < b, a == b, a <= b, a >= b, a != b
                List<MuseParser.Logical_and_expressionContext> land = ctx.expression().assignment_expression().calculation_expression().logical_caclulation_expression().logical_or_expression().logical_and_expression();

                if(land.size() == 1 && land.get(0).bitwise_or_expression().size() == 1 && land.get(0).bitwise_or_expression(0).bitwise_xor_expression().size() == 1)
                {
                    if(land.get(0).bitwise_or_expression(0).bitwise_xor_expression(0).bitwise_and_expression().size() == 1)
                    {
                        MuseParser.Bitwise_and_expressionContext bwa = land.get(0).bitwise_or_expression(0).bitwise_xor_expression(0).bitwise_and_expression(0);
                        if(bwa.equality_expression().size() == 1)
                        {
                            if(bwa.equality_expression(0).equ_op().size() == 1)
                            {
                                String equ_op = bwa.equality_expression(0).equ_op(0).getText();
                                information a, b;
                                a = visit(bwa.equality_expression(0).relation_expression(0));
                                b = visit(bwa.equality_expression(0).relation_expression(1));
                                if(a.varInfo.varP.type.id.name.equals("string"))
                                {
                                    write("call", new information[]{new information("@functionLabel___builtin__string_cmp_EQ"), a, b}, new information[]{});
                                    if (equ_op.equals("==")) {
                                        write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(exit)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write_string(exit + ":\nnop");
                                    } else {
                                        write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(exit), new information(body1)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write_string(exit + ":\nnop");
                                    }
                                }
                                else {
                                    if (equ_op.equals("==")) {
                                        write("cbr_ne", new information[]{a, b}, new information[]{new information(exit)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write_string(exit + ":\nnop");
                                    } else {
                                        write("cbr_eq", new information[]{a, b}, new information[]{new information(exit)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write_string(exit + ":\nnop");
                                    }
                                }
                                return null;
                            }
                            else if(bwa.equality_expression().size() == 1 && bwa.equality_expression(0).relation_expression().size() == 1)
                            {
                                MuseParser.Relation_expressionContext rla = bwa.equality_expression(0).relation_expression(0);
                                if(rla.rela_op() != null)
                                {
                                    String rela_op = rla.rela_op().getText();
                                    String instr;
                                    if(rela_op.equals(">"))
                                    {
                                        instr = "cbr_le";
                                    }
                                    else if(rela_op.equals(">="))
                                    {
                                        instr = "cbr_lt";
                                    }
                                    else if(rela_op.equals("<"))
                                    {
                                        instr = "cbr_ge";
                                    }
                                    else if(rela_op.equals("<="))
                                    {
                                        instr = "cbr_gt";
                                    }
                                    else
                                    {
                                        throw new RuntimeException("simplify error");
                                    }
                                    information a, b;
                                    a = visit(rla.shift_expression(0)); b = visit(rla.shift_expression(1));
                                    if(a.varInfo.varP.type.id.name.equals("string"))
                                    {
                                        if(rela_op.equals(">"))
                                        {
                                            write("call", new information[]{new information("@functionLabel___builtin__string_cmp_GT"), a, b}, new information[]{});
                                            write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(exit)});

                                            write_string(body1 + ":\nnop");
                                            visit(ctx.statement(0));
                                            write_string(exit + ":\nnop");
                                        }
                                        else if(rela_op.equals(">="))
                                        {
                                            write("call", new information[]{new information("@functionLabel___builtin__string_cmp_GE"), a, b}, new information[]{});
                                            write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(exit)});

                                            write_string(body1 + ":\nnop");
                                            visit(ctx.statement(0));
                                            write_string(exit + ":\nnop");
                                        }
                                        else if(rela_op.equals("<"))
                                        {
                                            write("call", new information[]{new information("@functionLabel___builtin__string_cmp_LT"), a, b}, new information[]{});
                                            write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(exit)});

                                            write_string(body1 + ":\nnop");
                                            visit(ctx.statement(0));
                                            write_string(exit + ":\nnop");
                                        }
                                        else if(rela_op.equals("<="))
                                        {
                                            write("call", new information[]{new information("@functionLabel___builtin__string_cmp_LE"), a, b}, new information[]{});
                                            write("cbr", new information[]{new information("r_{global_return_register}")}, new information[]{new information(body1), new information(exit)});

                                            write_string(body1 + ":\nnop");
                                            visit(ctx.statement(0));
                                            write_string(exit + ":\nnop");
                                        }
                                        else
                                        {
                                            throw new RuntimeException("simplify error");
                                        }
                                    }
                                    else {
                                        write(instr, new information[]{a, b}, new information[]{new information(exit)});
                                        write_string(body1 + ":\nnop");
                                        visit(ctx.statement(0));
                                        write_string(exit + ":\nnop");
                                    }
                                    return null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {

            }
            information b = visit(ctx.expression());
            write("cbr", new information[]{b}, new information[]{new information(body1), new information(exit)});
            write_string(body1 + ":\nnop");
            visit(ctx.statement(0));
            write_string(exit + ":\nnop");
        }
        return null;
    }

    @Override public information visitExpression(MuseParser.ExpressionContext ctx) {
        return visit(ctx.assignment_expression());
    }

    @Override public information visitAssignment_expression(MuseParser.Assignment_expressionContext ctx) {
        if(ctx.unary_expression() != null)
        {
            information rhs = visit(ctx.calculation_expression());
            for(int i = ctx.unary_expression().size() - 1; i >= 0; --i)
            {
                information lhs = visit(ctx.unary_expression(i));
                write("i2i", new information[]{rhs}, new information[]{lhs});
                rhs = lhs;
            }
            return rhs;
        }
        else
        {
            return visit(ctx.calculation_expression());
        }
    }

    @Override public information visitEqual(MuseParser.EqualContext ctx) { return visitChildren(ctx); }

    @Override public information visitAssignment_operators(MuseParser.Assignment_operatorsContext ctx) { return visitChildren(ctx); }

    @Override public information visitCalculation_expression(MuseParser.Calculation_expressionContext ctx) {
        return visit(ctx.logical_caclulation_expression());
    }

    @Override public information visitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx) {
        return visit(ctx.logical_or_expression());
    }

    @Override public information visitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx) {
        if(ctx.logical_and_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
            ret.varInfo.varP.reg = reg_allocator.allocate(true, "");

            write("loadI", new information[]{new information(0)}, new information[]{ret});
            String exit_true_label = label_allocator.allocate();
            String exit_label = label_allocator.allocate();
            for(int i = 0; i < ctx.logical_and_expression().size(); ++i)
            {
                information curExpr = visit(ctx.logical_and_expression(i));
                String nextLabel = label_allocator.allocate();

                write("cbr", new information[]{curExpr}, new information[]{new information(exit_true_label), new information(nextLabel)});
                write_string(nextLabel + ":\nnop");
            }
            write("jumpI", new information[]{}, new information[]{new information(exit_label)});
            write_string(exit_true_label + ":\nnop");
            write("loadI", new information[]{new information(1)}, new information[]{ret});
            write_string(exit_label+":\nnop");
            return ret;
        }
        else
        {
            return visit(ctx.logical_and_expression(0));
        }
    }

    boolean can_be_simplify_logical_and(MuseParser.Bitwise_or_expressionContext ctx, String exitLabel)
    {
        if(ctx.bitwise_xor_expression().size() == 1 && ctx.bitwise_xor_expression(0).bitwise_and_expression().size() == 1)
        {
            MuseParser.Bitwise_and_expressionContext bwa = ctx.bitwise_xor_expression(0).bitwise_and_expression(0);
            if(bwa.equality_expression().size() == 1)
            {
                if(bwa.equality_expression(0).equ_op().size() == 1)
                {
                    String instr;
                    MuseParser.Equality_expressionContext equ = bwa.equality_expression(0);
                    if(equ.equ_op(0).getText().equals("=="))
                        instr = "cbr_ne";
                    else
                        instr = "cbr_eq";
                    information a = visit(equ.relation_expression(0)), b = visit(equ.relation_expression(1));
                    write(instr, new information[]{a, b}, new information[]{new information(exitLabel)});
                    return true;
                }
                else
                {
                    if(bwa.equality_expression(0).relation_expression().size() == 1 && bwa.equality_expression(0).relation_expression(0).rela_op() != null)
                    {
                        String instr;
                        MuseParser.Relation_expressionContext rla = bwa.equality_expression(0).relation_expression(0);
                        if(rla.rela_op().getText().equals("<"))
                            instr = "cbr_ge";
                        else if(rla.rela_op().getText().equals("<="))
                            instr = "cbr_gt";
                        else if(rla.rela_op().getText().equals(">"))
                            instr = "cbr_le";
                        else
                            instr = "cbr_lt";
                        information a = visit(rla.shift_expression(0)), b = visit(rla.shift_expression(1));
                        write(instr, new information[]{a, b}, new information[]{new information(exitLabel)});
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override public information visitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx) {
        if(ctx.bitwise_or_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
            ret.varInfo.varP.reg = reg_allocator.allocate(true, "");

            //短路求值是支持的

            write("loadI", new information[]{new information(1)}, new information[]{ret});
            String exit_false_label = label_allocator.allocate();
            String exit_label = label_allocator.allocate();
            for(int i = 0; i < ctx.bitwise_or_expression().size(); ++i)
            {
                if(!can_be_simplify_logical_and(ctx.bitwise_or_expression(i), exit_false_label))
                {
                    information curExpr = visit(ctx.bitwise_or_expression(i));
                    String nextLabel = label_allocator.allocate();

                    write("cbr", new information[]{curExpr}, new information[]{new information(nextLabel), new information(exit_false_label)});
                    write_string(nextLabel + ":\nnop");
                }
            }
            write("jumpI", new information[]{}, new information[]{new information(exit_label)});
            write_string(exit_false_label+":\nnop");
            write("loadI", new information[]{new information(0)}, new information[]{ret});
            write_string(exit_label+":\nnop");
            return ret;
        }
        else
        {
            return visit(ctx.bitwise_or_expression(0));
        }
    }

    @Override public information visitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx) {
        if(ctx.bitwise_xor_expression().size() > 1)
        {
            information ans = new information();
            ans.varInfo = new Property();
            ans.varInfo.varP = new Property.VarP();
            ans.varInfo.varP.reg = reg_allocator.allocate(true, "");
            ans.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));

            write("loadI", new information[]{new information(0)}, new information[]{ans});
            for(int i = 0; i < ctx.bitwise_xor_expression().size(); ++i)
            {
                information lhs = visit(ctx.bitwise_xor_expression(i));

                write("or", new information[]{ans, lhs}, new information[]{ans});
            }
            return ans;
        }
        else
        {
            return visit(ctx.bitwise_xor_expression(0));
        }
    }

    @Override public information visitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx) {
        if(ctx.bitwise_and_expression().size() > 1)
        {
            information ans = new information();
            ans.varInfo = new Property();
            ans.varInfo.varP = new Property.VarP();
            ans.varInfo.varP.reg = reg_allocator.allocate(true, "");
            ans.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));

            write("loadI", new information[]{new information(0)}, new information[]{ans});
            for(int i = 0; i < ctx.bitwise_and_expression().size(); ++i)
            {
                information lhs = visit(ctx.bitwise_and_expression(i));

                write("xor", new information[]{ans, lhs}, new information[]{ans});
            }
            return ans;
        }
        else
        {
            return visit(ctx.bitwise_and_expression(0));
        }
    }

    @Override public information visitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx) {
        if(ctx.equality_expression().size() > 1)
        {
            information ans = new information();
            ans.varInfo = new Property();
            ans.varInfo.varP = new Property.VarP();
            ans.varInfo.varP.reg = reg_allocator.allocate(true, "");
            ans.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));

            write("loadI", new information[]{new information(-1)}, new information[]{ans});
            for(int i = 0; i < ctx.equality_expression().size(); ++i)
            {
                information lhs = visit(ctx.equality_expression(i));

                write("and", new information[]{ans, lhs}, new information[]{ans});
            }
            return ans;
        }
        else
        {
            return visit(ctx.equality_expression(0));
        }
    }

    @Override public information visitIsequal(MuseParser.IsequalContext ctx) { return visitChildren(ctx); }

    @Override public information visitNotequal(MuseParser.NotequalContext ctx) { return visitChildren(ctx); }

    @Override public information visitEquality_expression(MuseParser.Equality_expressionContext ctx) {
        if(ctx.relation_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.reg = reg_allocator.allocate(true, "");
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
            String instr;
            if(ctx.equ_op(0).isequal() != null)
                instr = "cmp_EQ";
            else
                instr = "cmp_NE";
            information a = visit(ctx.relation_expression(0)), b = visit(ctx.relation_expression(1));

            if(a.varInfo.varP.type.id.name.equals("string"))
            {
                if(ctx.equ_op(0).isequal() != null)
                {
                    write("call @functionLabel____builtin__string_cmp_EQ", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else
                {
                    write("call @functionLabel____builtin__string_cmp_EQ", new information[]{a, b}, new information[]{});
                    write("rsubI", new information[]{new information("r_{global_return_register}"), new information(1)}, new information[]{ret});
                }
            }
            else
            {
                write(instr, new information[]{a, b}, new information[]{ret});
            }
            for(int i = 1; i < ctx.equ_op().size(); ++i)
            {
                if(ctx.equ_op(i).isequal() != null)
                    instr = "cmp_EQ";
                else
                    instr = "cmp_NE";
                b = visit(ctx.relation_expression(i + 1));
                //write(instr + " " + b.varInfo.varP.reg + " " + ret.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write(instr, new information[]{b, ret}, new information[]{ret});
            }
            return ret;
        }
        else
        {
            return visit(ctx.relation_expression(0));
        }
    }

    @Override public information visitBigger(MuseParser.BiggerContext ctx) { return visitChildren(ctx); }

    @Override public information visitSmaller(MuseParser.SmallerContext ctx) { return visitChildren(ctx); }

    @Override public information visitBigger_e(MuseParser.Bigger_eContext ctx) { return visitChildren(ctx); }

    @Override public information visitSmaller_e(MuseParser.Smaller_eContext ctx) { return visitChildren(ctx); }

    @Override public information visitRelation_expression(MuseParser.Relation_expressionContext ctx) {
        if(ctx.shift_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.reg = reg_allocator.allocate(true, "rela_e");
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
            information a = visit(ctx.shift_expression(0)), b = visit(ctx.shift_expression(1));
            if(ctx.rela_op().smaller_e() != null) {
                if(a.varInfo.varP.type.id.name.equals("string"))
                {
                    write("call @functionLabel____builtin__string_cmp_LE", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else {
                    write("cmp_LE", new information[]{a, b}, new information[]{ret});
                }
            }
            if(ctx.rela_op().bigger_e() != null) {
                if(a.varInfo.varP.type.id.name.equals("string"))
                {
                    write("call @functionLabel____builtin__string_cmp_GE", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else
                    write("cmp_GE", new information[]{a, b}, new information[]{ret});
            }
            if(ctx.rela_op().bigger() != null) {
                if(a.varInfo.varP.type.id.name.equals("string"))
                {
                    write("call @functionLabel____builtin__string_cmp_GT", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else
                    write("cmp_GT", new information[]{a, b}, new information[]{ret});
            }
            if(ctx.rela_op().smaller() != null) {
                if(a.varInfo.varP.type.id.name.equals("string"))
                {
                    write("call @functionLabel____builtin__string_cmp_LT", new information[]{a, b}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
                else
                    write("cmp_LT", new information[]{a, b}, new information[]{ret});
            }
            return ret;
        }
        else
        {
            return visit(ctx.shift_expression(0));
        }
    }

    @Override public information visitLshift(MuseParser.LshiftContext ctx) { return visitChildren(ctx); }

    @Override public information visitRshift(MuseParser.RshiftContext ctx) { return visitChildren(ctx); }

    @Override public information visitPlus(MuseParser.PlusContext ctx) { return visitChildren(ctx); }

    @Override public information visitMinus(MuseParser.MinusContext ctx) { return visitChildren(ctx); }

    @Override public information visitShift_expression(MuseParser.Shift_expressionContext ctx) {
        if(ctx.add_expression().size() > 1)
        {
            information ret = new information(); ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.reg = reg_allocator.allocate(false, "shift_e");
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            information a = visit(ctx.add_expression(0)), b = visit(ctx.add_expression(1));
            String instr;
            if(ctx.shift_op(0).lshift() != null)
                instr = "lshift";
            else
                instr = "rshift";

            write(instr, new information[]{a, b}, new information[]{ret});
            for(int i = 1; i < ctx.shift_op().size(); ++i)
            {
                if(ctx.shift_op(i).lshift() != null)
                    instr = "lshift";
                else
                    instr = "rshift";
                a = visit(ctx.add_expression(i + 1));

                write(instr, new information[]{ret, a}, new information[]{ret});
            }
            return ret;
        }
        else
        {
            return visit(ctx.add_expression(0));
        }
    }

    @Override public information visitAdd_expression(MuseParser.Add_expressionContext ctx) {
        //System.out.println(ctx.getText());
        if(ctx.mul_expression().size() > 1)
        {
            information ret = new information();
            ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.reg = reg_allocator.allocate(false, "add_e");
            information a = visit(ctx.mul_expression(0)), b;
            ret.varInfo.varP.type = a.varInfo.varP.type;
            if(a.varInfo.varP.type.id.name.equals("string"))
            {
                b = visit(ctx.mul_expression(1));
                write("call", new information[]{new information("@functionLabel___builtin__string_add"), a, b}, new information[]{});
                write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                for(int i = 1; i < ctx.add_op().size(); ++i)
                {
                    a = visit(ctx.mul_expression(i + 1));
                    write("call", new information[]{new information("@functionLabel___builtin__string_add"), ret, a}, new information[]{});
                    write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
                }
            }
            else
            {
                String instr;
                if (ctx.add_op(0).plus() != null)
                    instr = "add";
                else
                    instr = "sub";

                try {
                    if (ctx.mul_expression(1).unary_expression(0).postfix_expression().primary_expression().constant() != null) {
                        if(instr.equals("add"))
                            instr = "addI";
                        else
                            instr = "subI";
                        write(instr, new information[]{a, new information(ctx.mul_expression(1).getText())}, new information[]{ret});
                    }
                    else
                        throw new RuntimeException("haha");
                }
                catch (Exception e)
                {
                    b = visit(ctx.mul_expression(1));
                    write(instr, new information[]{a, b}, new information[]{ret});
                }
                for (int i = 1; i < ctx.add_op().size(); ++i) {
                    if (ctx.add_op(i).plus() != null)
                        instr = "add";
                    else
                        instr = "sub";
                    if(ctx.mul_expression(i + 1).unary_expression().size() == 1)
                    {
                        try {
                            if (ctx.mul_expression(i + 1).unary_expression(0).postfix_expression().primary_expression().constant() != null) {
                                if(instr.equals("add"))
                                    instr = "addI";
                                else
                                    instr = "subI";
                                write(instr, new information[]{ret, new information(ctx.mul_expression(i + 1).getText())}, new information[]{ret});
                                continue;
                            }
                        }
                        catch (Exception e)
                        {

                        }
                    }
                    a = visit(ctx.mul_expression(i + 1));
                    write(instr, new information[]{ret, a}, new information[]{ret});
                }
            }
            return ret;
        }
        else
        {
            return visit(ctx.mul_expression(0));
        }
    }

    @Override public information visitMultiply(MuseParser.MultiplyContext ctx) { return visitChildren(ctx); }

    @Override public information visitDivision(MuseParser.DivisionContext ctx) { return visitChildren(ctx); }

    @Override public information visitMod(MuseParser.ModContext ctx) { return visitChildren(ctx); }

    @Override public information visitMul_expression(MuseParser.Mul_expressionContext ctx) {
        if(ctx.unary_expression().size() > 1)
        {
            information ret = new information(); ret.varInfo = new Property();
            ret.varInfo.varP = new Property.VarP();
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            ret.varInfo.varP.reg = reg_allocator.allocate(false, "mul_e");
            information a = visit(ctx.unary_expression(0)), b = visit(ctx.unary_expression(1));
            if(ctx.mul_op(0).multiply() != null)
            {
                write("mult", new information[]{a, b}, new information[]{ret});
            }
            else if(ctx.mul_op(0).mod() != null)
            {
                String tempReg = reg_allocator.allocate(true, "modtmp");
                write("div", new information[]{a, b}, new information[]{new information(tempReg)});
                write("mult", new information[]{b, new information(tempReg)}, new information[]{new information(tempReg)});
                write("sub", new information[]{a, new information(tempReg)}, new information[]{ret});
            }
            else
            {
                write("div", new information[]{a, b}, new information[]{ret});
            }
            for(int i = 1; i < ctx.mul_op().size(); ++i)
            {
                a = visit(ctx.unary_expression(i + 1));
                if(ctx.mul_op(i).multiply() != null)
                {
                    write("mult", new information[]{ret, a}, new information[]{ret});
                }
                else if(ctx.mul_op(i).mod() != null)
                {
                    String tempReg = reg_allocator.allocate(true, "modtmp");
                    write("div", new information[]{ret, a}, new information[]{new information(tempReg)});
                    write("mult", new information[]{a, new information(tempReg)}, new information[]{new information(tempReg)});
                    write("sub", new information[]{ret, new information(tempReg)}, new information[]{ret});
                }
                else
                {
                    write("div", new information[]{ret, a}, new information[]{ret});
                }
            }
            return ret;
        }
        else
        {
            return visit(ctx.unary_expression(0));
        }
    }

    @Override public information visitPlusplus(MuseParser.PlusplusContext ctx) { return visitChildren(ctx); }

    @Override public information visitMinusminus(MuseParser.MinusminusContext ctx) { return visitChildren(ctx); }

    private Integer toInt(String x)
    {
        Integer ret = 0;
        for(int i = 0; i < x.length(); ++i)
            ret = ret * 10 + ((int)x.charAt(i)) - 48;
        return ret;
    }

    @Override public information visitUnary_expression(MuseParser.Unary_expressionContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(false, "unary_e");
        if(ctx.postfix_expression() != null)
        {
            return visit(ctx.postfix_expression());
        }
        else if(ctx.unary_operation() != null)
        {
            information a = visit(ctx.unary_expression());
            ret.varInfo.varP.type = a.varInfo.varP.type;
            if(ctx.unary_operation().not_sign() != null)
            {
                //write("xorI 1 " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write("xorI", new information[]{a, new information(1)}, new information[]{ret});
            }
            else if(ctx.unary_operation().minus() != null)
            {
                //write("rsubI 0 " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write("rsubI", new information[]{a, new information(0)}, new information[]{ret});
            }
            else if(ctx.unary_operation().minusminus() != null)
            {
                //write("subI " + ret.varInfo.varP.reg + " 1 " + ret.varInfo.varP.reg);
                write("subI", new information[]{a, new information(1)}, new information[]{ret});
                write("subI", new information[]{a, new information(1)}, new information[]{a});
            }
            else if(ctx.unary_operation().plusplus() != null)
            {
                //write("addI " + ret.varInfo.varP.reg + " 1 " + ret.varInfo.varP.reg);
                write("addI", new information[]{a, new information(1)}, new information[]{ret});
                write("addI", new information[]{a, new information(1)}, new information[]{a});
            }
            else if(ctx.unary_operation().type_name() != null)
            {
                throw new RuntimeException("WTF type transform is not allowed");
            }
            else if(ctx.unary_operation().bitwise_not() != null)
            {
                //write("xorI -1 " + a.varInfo.varP.reg + " " + ret.varInfo.varP.reg);
                write("xorI", new information[]{a, new information(-1)}, new information[]{ret});
            }
            else
            {
                throw new RuntimeException("This code cannot be executed.");
            }
        }
        else if(ctx.number() != null)
        {
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
            //write("loadI " + ctx.number().getText() + " " + ret.varInfo.varP.reg);
            write("loadI", new information[]{new information(toInt(ctx.number().getText()))}, new information[]{ret});
        }
        else if(ctx.functionCall_expression() != null)
        {
            return visit(ctx.functionCall_expression());
        }
        else if(ctx.new_operation() != null)
        {
            return visit(ctx.new_operation());
        }
        else if(ctx.pre_defined_constants() != null)
        {
            if(ctx.pre_defined_constants().getText().equals("true"))
            {
                ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
                //write("loadI 1 " + ret.varInfo.varP.reg);
                write("loadI", new information[]{new information(1)}, new information[]{ret});
            }
            if(ctx.pre_defined_constants().getText().equals("false"))
            {
                ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("bool"));
                //write("loadI 0 " + ret.varInfo.varP.reg);
                write("loadI", new information[]{new information(0)}, new information[]{ret});
            }
            if(ctx.pre_defined_constants().getText().equals("null"))
            {
                ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("1122special"));
                //write("loadI 0 " + ret.varInfo.varP.reg);
                write("loadI", new information[]{new information(0)}, new information[]{ret});
            }
        }
        else
        {
            throw new RuntimeException("This code cannot be executed");
        }
        return ret;
    }

    @Override public information visitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx) { return visitChildren(ctx); }

    @Override public information visitClass_new(MuseParser.Class_newContext ctx) { return visitChildren(ctx); }

    @Override public information visitNew_operation(MuseParser.New_operationContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(true, "new_op");
        if(ctx.class_new() != null)
        {
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName(ctx.class_new().Identifier().getText()));
            write("mallocI", new information[]{new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size + ret.varInfo.varP.type.typeP.realSize)}, new information[]{ret});
            write("mark_pointerI", new information[]{ret, new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size), new information(ret.varInfo.varP.type.typeP.realSize)}, new information[]{});
            String tempReg = reg_allocator.allocate(true, "");
            write("loadI", new information[]{new information(ret.varInfo.varP.type.typeP.realSize)}, new information[]{new information(tempReg)});
            write("store", new information[]{new information(tempReg)}, new information[]{ret});
        }
        else
        {
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName(ctx.array_new().type_non_array().getText()));
            //MR assume, ctx.array_new().expression().size == 1 is always true
            information b = visit(ctx.array_new().expression(0));
            String arraySize = reg_allocator.allocate(true, "");
            String byteArraySize = reg_allocator.allocate(true, "");
            write("i2i", new information[]{b}, new information[]{new information(arraySize)});
            write("multI", new information[]{new information(arraySize), new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(byteArraySize)});
            write("addI", new information[]{new information(byteArraySize), new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(byteArraySize)});
            write("malloc", new information[]{new information(byteArraySize)}, new information[]{ret});
            write("mark_pointer", new information[]{ret, new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size), b}, new information[]{});
            write("store", new information[]{new information(arraySize)}, new information[]{ret});
        }
        return ret;
    }

    boolean can_be_inlined(MuseParser.FunctionCall_expressionContext ctx)
    {
        //TODO
        return false;
    }

    @Override public information visitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx) {
        if(ctx.Identifier().getText().equals("println") || ctx.Identifier().getText().equals("print"))
        {
            try
            {
                MuseParser.Add_expressionContext add_ctx = ctx.arguements().assignment_expression(0).calculation_expression().logical_caclulation_expression().logical_or_expression().logical_and_expression(0).bitwise_or_expression(0).bitwise_xor_expression(0).bitwise_and_expression(0).equality_expression(0).relation_expression(0).shift_expression(0).add_expression(0);
                for(int i = 0; i < add_ctx.mul_expression().size(); ++i)
                {
                    if(add_ctx.mul_expression(i).unary_expression(0).postfix_expression().primary_expression().functionCall_expression() != null)
                    {
                        MuseParser.FunctionCall_expressionContext f_ctx = add_ctx.mul_expression(i).unary_expression(0).postfix_expression().primary_expression().functionCall_expression();
                        if(f_ctx.Identifier().getText().equals("toString"))
                        {
                            information[] src = new information[2];
                            if(i + 1 == add_ctx.mul_expression().size() && ctx.Identifier().getText().equals("println"))
                                src[0] = new information("@functionLabel___builtin_printIntln");
                            else
                                src[0] = new information("@functionLabel___builtin_printInt");
                            information b = visit(f_ctx.arguements().assignment_expression(0));
                            src[1] = b;
                            information[] tar = new information[]{};
                            write("call", src, tar);
                        }
                    }
                    else
                    {
                        information[] src = new information[2];

                        if(i + 1 == add_ctx.mul_expression().size() && ctx.Identifier().getText().equals("println"))
                            src[0] = new information("@functionLabel_println");
                        else
                            src[0] = new information("@functionLabel_print");

                        try
                        {
                            if(add_ctx.mul_expression(i).unary_expression().size() == 1 &&
                                    add_ctx.mul_expression(i).unary_expression(0).postfix_expression().primary_expression().constant() != null
                                    && add_ctx.mul_expression(i).unary_expression(0).postfix_expression().postfix().size() == 0)
                            {

                                if(i + 1 == add_ctx.mul_expression().size() && ctx.Identifier().getText().equals("println")) {
                                    src[0] = new information("@functionLabel_println_constant"); src[1] = new information(add_ctx.mul_expression(i).getText());
                                }else {
                                    src[0] = new information("@functionLabel_print_constant"); src[1] = new information(add_ctx.mul_expression(i).getText());
                                }
                                information[] tar = new information[]{};
                                write("call", src, tar);
                            }
                            else
                                throw new RuntimeException("it's not constant");
                        }
                        catch (Exception e) {
                            src[1] = visit(add_ctx.mul_expression(i));
                            information[] tar = new information[]{};
                            write("call", src, tar);
                        }
                    }
                }
                information ret = new information();
                ret.varInfo = new Property();
                ret.varInfo.varP = new Property.VarP();
                ret.varInfo.varP.reg = "r_{global_return_register}";
                ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("void"));
                return ret;
            }
            catch (Exception e)
            {

            }
        }
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = "r_{global_return_register}";
        Property func = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()));
        ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName(func.funcP.retType));
        if(can_be_inlined(ctx))
        {
            return ret;
        }
        //String toWrite = "call " + func.funcP.label + " " + ret.varInfo.varP.reg;
        String opcode = "call";
        information[] src, tar = new information[]{};
        if(ctx.arguements() != null)
        {
            src = new information[ctx.arguements().assignment_expression().size() + 1];
            src[0] = new information(func.funcP.label);
            for(int i = 0; i < ctx.arguements().assignment_expression().size(); ++i)
            {
                information b = visit(ctx.arguements().assignment_expression(i));
                //toWrite = toWrite + " " + b.varInfo.varP.reg;
                src[i + 1] = b;
            }
        }
        else
            src = new information[]{new information(func.funcP.label)};
        write(opcode, src, tar);
        //write(toWrite);
        return ret;
    }

    @Override public information visitArguements(MuseParser.ArguementsContext ctx) { return visitChildren(ctx); }

    @Override public information visitValue(MuseParser.ValueContext ctx) { return visitChildren(ctx); }

    @Override public information visitNot_sign(MuseParser.Not_signContext ctx) { return visitChildren(ctx); }

    @Override public information visitUnary_operation(MuseParser.Unary_operationContext ctx) { return visitChildren(ctx); }

    @Override public information visitBitwise_not(MuseParser.Bitwise_notContext ctx) { return visitChildren(ctx); }

    @Override public information visitPostfix_expression(MuseParser.Postfix_expressionContext ctx) {
        //System.out.println(ctx.getText());
        information ret = (information) visit(ctx.primary_expression()).clone();
        String tempReg = reg_allocator.allocate(true, "array_addr_finder");
        for(int i = 0; i < ctx.postfix().size(); ++i)
        {
            if(ctx.postfix(i).expression() != null)
            {
                information newRet = new information();
                newRet.varInfo = new Property();
                newRet.varInfo.varP = new Property.VarP();
                newRet.varInfo.varP.type = ret.varInfo.varP.type;
                newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");

                information b = visit(ctx.postfix(i).expression());
                write("multI", new information[]{b, new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{new information(tempReg)});
                write("add", new information[]{new information(tempReg), ret}, new information[]{new information(tempReg)});
                //the 1st element is the size, so add one unit
                ret = newRet;
                write("loadAI", new information[]{new information(tempReg), new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)}, new information[]{ret});
                newRet.varInfo.varP.addr_reg = reg_allocator.allocate(false, "addr");
                newRet.varInfo.varP.is_static = true;
                write("addI", new information[]{new information(tempReg, null), new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size)},new information[]{new information(ret.varInfo.varP.addr_reg, null)});
            }
            if(ctx.postfix(i).getMember() != null)
            {
                if(ctx.postfix(i).functionCall_expression() != null)
                {
                    if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("size"))
                    {
                        //ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
                        write("load", new information[]{ret}, new information[]{newRet});
                        ret = newRet;
                    }
                    else if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("ord") && ret.varInfo.varP.type.id.name.equals("string"))
                    {
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
                        if(ctx.postfix(i).functionCall_expression().arguements().assignment_expression().size() != 1)
                        {
                            throw new RuntimeException("parameters not correct\n");
                        }
                        write("call", new information[]{new information("@functionLabel___builtin__ord"), ret, visit(ctx.postfix(i).functionCall_expression().arguements().assignment_expression(0))}, new information[]{});
                        write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{newRet});
                        ret = newRet;
                    }
                    else if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("length") && ret.varInfo.varP.type.id.name.equals("string"))
                    {
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
                        write("load", new information[]{ret}, new information[]{newRet});
                        ret = newRet;
                    }
                    else if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("substring") && ret.varInfo.varP.type.id.name.equals("string"))
                    {
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("string"));
                        if(ctx.postfix(i).functionCall_expression().arguements().assignment_expression().size() != 2)
                        {
                            throw new RuntimeException("parameter list error");
                        }
                        write("call", new information[]{new information("@functionLabel___builtin__substring"), ret, visit(ctx.postfix(i).functionCall_expression().arguements().assignment_expression(0)), visit(ctx.postfix(i).functionCall_expression().arguements().assignment_expression(1))}, new information[]{});
                        write("i2i", new information[]{ new information("r_{global_return_register}")}, new information[]{newRet});
                        ret = newRet;
                    }
                    else if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("length") && ret.varInfo.varP.type.id.name.equals("string"))
                    {
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("string"));
                        write("call", new information[]{new information("@functionLabel___builtin__length"), ret}, new information[]{});
                        write("i2i", new information[]{ new information("r_{global_return_register}")}, new information[]{newRet});
                        ret = newRet;
                    }
                    else if(ctx.postfix(i).functionCall_expression().Identifier().getText().equals("parseInt"))
                    {
                        information newRet = new information();
                        newRet.varInfo = new Property();
                        newRet.varInfo.varP = new Property.VarP();
                        newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");
                        newRet.varInfo.varP.type = symbol_table.get(Name.getSymbolName("string"));
                        write("call", new information[]{new information("@functionLabel___builtin__parseInt"), ret}, new information[]{});
                        write("i2i", new information[]{ new information("r_{global_return_register}")}, new information[]{newRet});
                        ret = newRet;
                    }
                    else
                    {
                        throw new RuntimeException("builtin member not found or not completed\n");
                    }
                }
                else {
                    ArrayList<Property> members = ret.varInfo.varP.type.typeP.ctx;
                    for (int j = 0; j < members.size(); ++j) {
                        if (members.get(j).id.name.equals(ctx.postfix(i).Identifier().getText())) {
                            information newRet = new information();
                            newRet.varInfo = new Property();
                            newRet.varInfo.varP = new Property.VarP();
                            newRet.varInfo.varP.type = ret.varInfo.varP.type;
                            newRet.varInfo.varP.reg = reg_allocator.allocate(true, "");

                            write("loadI", new information[]{new information(RegisiterInfo.Register_size / RegisiterInfo.byte_size * j)}, new information[]{new information(tempReg, null)});
                            write("add", new information[]{new information(tempReg, null), ret}, new information[]{new information(tempReg, null)});
                            ret = newRet;
                            write("load", new information[]{new information(tempReg, null)}, new information[]{ret});
                            ret.varInfo.varP.addr_reg = reg_allocator.allocate(false, "addr");
                            write("i2i", new information[]{new information(tempReg, null)}, new information[]{new information(ret.varInfo.varP.addr_reg, null)});
                            ret.varInfo.varP.type = members.get(j).varP.type;
                            ret.varInfo.varP.is_static = true;
                            break;
                        }
                    }
                }
            }
            if(ctx.postfix(i).plusplus() != null)
            {
                information newRet = new information();
                newRet.varInfo = new Property(); newRet.varInfo.varP = new Property.VarP();
                newRet.varInfo.varP.reg = reg_allocator.allocate(true, "renaming");
                write("i2i", new information[]{ret}, new information[]{newRet});
                write("addI", new information[]{ret, new information(1)}, new information[]{ret});
                return newRet;
            }
            if(ctx.postfix(i).minusminus() != null)
            {
                information newRet = new information();
                newRet.varInfo = new Property(); newRet.varInfo.varP = new Property.VarP();
                newRet.varInfo.varP.reg = reg_allocator.allocate(true, "renaming");
                write("i2i", new information[]{ret}, new information[]{newRet});
                write("subI", new information[]{ret, new information(1)}, new information[]{ret});
                return newRet;
            }
        }
        return ret;
    }

    @Override public information visitGetMember(MuseParser.GetMemberContext ctx) { return visitChildren(ctx); }

    @Override public information visitPostfix(MuseParser.PostfixContext ctx) { return visitChildren(ctx); }

    @Override public information visitPrimary_expression(MuseParser.Primary_expressionContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(true, "prim_ex");
        if(ctx.Identifier() != null)
        {
            Property b = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()));
        //    write("i2i", new information[]{new information(b.varP.reg, b.varP.addr_reg)}, new information[]{ret});
        //    ret.varInfo.varP.addr_reg = b.varP.addr_reg;
        //    ret.varInfo.varP.type = b.varP.type;
            ret.varInfo = b;
            return ret;
        }
        if(ctx.constant() != null)
        {
            information b = visit(ctx.constant());
            ret.varInfo.varP.reg = b.varInfo.varP.reg;
            ret.varInfo.varP.addr_reg = b.varInfo.varP.addr_reg;
            ret.varInfo.varP.type = b.varInfo.varP.type;
        }
        if(ctx.expression() != null)
        {
            information b = visit(ctx.expression());
            return b;
        /*    write("i2i", new information[]{b}, new information[]{ret});
            ret.varInfo.varP.addr_reg = b.varInfo.varP.addr_reg;
            ret.varInfo.varP.type = b.varInfo.varP.type;*/
        }
        if(ctx.functionCall_expression() != null)
        {
            information b = visit(ctx.functionCall_expression());
            return b;
        /*    write("i2i", new information[]{b}, new information[]{ret});
            ret.varInfo.varP.addr_reg = b.varInfo.varP.addr_reg;
            ret.varInfo.varP.type = b.varInfo.varP.type;*/
        }
        return ret;
    }

    @Override public information visitType_name(MuseParser.Type_nameContext ctx) { return visitChildren(ctx); }

    @Override public information visitConstant(MuseParser.ConstantContext ctx) {
        information ret = new information();
        ret.varInfo = new Property();
        ret.varInfo.varP = new Property.VarP();
        ret.varInfo.varP.reg = reg_allocator.allocate(true, "constant");
        if(ctx.numeric_constant() != null)
        {
            write("loadI", new information[]{new information(ctx.numeric_constant().getText(), null)}, new information[]{ret});
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("int"));
        }
        else
        {
            write("call", new information[]{new information("@functionLabel___builtin_string_generate_" + ctx.string_constant().getText())}, new information[]{});
            write("i2i", new information[]{new information("r_{global_return_register}")}, new information[]{ret});
            ret.varInfo.varP.type = symbol_table.get(Name.getSymbolName("string"));
        }
        return ret;
    }

    @Override public information visitNumeric_constant(MuseParser.Numeric_constantContext ctx) { return visitChildren(ctx); }

    @Override public information visitInteger_constant(MuseParser.Integer_constantContext ctx) { return visitChildren(ctx); }

    @Override public information visitString_constant(MuseParser.String_constantContext ctx) { return visitChildren(ctx); }

    @Override public information visitTypenametokens(MuseParser.TypenametokensContext ctx) { return visitChildren(ctx); }

    @Override public information visitNumber(MuseParser.NumberContext ctx) { return visitChildren(ctx); }

    @Override public information visitUnion_class(MuseParser.Union_classContext ctx) { return visitChildren(ctx); }
    @Override public information visitMul_op(MuseParser.Mul_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitAdd_op(MuseParser.Add_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitShift_op(MuseParser.Shift_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitRela_op(MuseParser.Rela_opContext ctx) { return visitChildren(ctx); }
    @Override public information visitEqu_op(MuseParser.Equ_opContext ctx) { return visitChildren(ctx); }

}
