package IR.symbol_table;

import IR.Memory_Model.Reg;
import IR.RegisiterInfo;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by xietiancheng on 16/4/23.
 */
public class Symbol_table {

    public ArrayList<HashMap<Name, Property>> symbol_table;
    public Symbol_table(){
        symbol_table = new ArrayList<>();
        symbol_table.add(new HashMap<>());
        {
            Property Int = new Property();
            Int.id = Name.getSymbolName("int");
            Int.typeP = new Property.TypeP();
            Int.typeP.pointerSize = RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            Int.typeP.realSize = RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            symbol_table.get(0).put(Int.id, Int);
        }
        {
            Property Int = new Property();
            Int.id = Name.getSymbolName("void");
            Int.typeP = new Property.TypeP();
            symbol_table.get(0).put(Int.id, Int);
        }
        {
            Property Int = new Property();
            Int.id = Name.getSymbolName("bool");
            Int.typeP = new Property.TypeP();
            Int.typeP.pointerSize = RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            Int.typeP.realSize = RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            symbol_table.get(0).put(Int.id, Int);
        }
        {
            Property True = new Property();
            True.id = Name.getSymbolName("true");
            True.varP = new Property.VarP();
            True.varP.type = get(Name.getSymbolName("bool"));
            symbol_table.get(0).put(True.id, True);
        }
        {
            Property False = new Property();
            False.id = Name.getSymbolName("false");
            False.varP = new Property.VarP();
            False.varP.type = get(Name.getSymbolName("bool"));
            symbol_table.get(0).put(False.id, False);
        }
        {
            Property special = new Property();
            special.id = Name.getSymbolName("1122special");
            special.typeP = new Property.TypeP();
            special.typeP.pointerSize = RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            special.typeP.realSize = RegisiterInfo.Register_size / RegisiterInfo.byte_size;
            symbol_table.get(0).put(special.id, special);
        }
        {
            Property Null = new Property();
            Null.id = Name.getSymbolName("null");
            Null.varP = new Property.VarP();
            Null.varP.type = get(Name.getSymbolName("1122special"));
            symbol_table.get(0).put(Null.id, Null);
        }
    }
    private int lastIndexOf(Name key) {
        for (int i = symbol_table.size() - 1; i >= 0; --i)
            if (symbol_table.get(i).get(key) != null)
                return i;
        return -1;
    }

    public void put(Name key, Property value) {
        symbol_table.get(symbol_table.size() - 1).put(key, value);
    }

    public Property get(Name key) {
        return symbol_table.get(lastIndexOf(key)).get(key);
    }

    public void beginScope() {
        symbol_table.add(new HashMap<>());
    }

    public void endScope() {
        symbol_table.remove(symbol_table.size() - 1);
    }
}
