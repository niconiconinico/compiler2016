package IR.symbol_table;

import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Created by xietiancheng on 16/3/29.
 */
public class Name {
    private static Dictionary<String, Name> dict
            = new Hashtable<String, Name>();
    public String name;

    private Name(String text) {
        name = text;
    }          // 私有化构造函数

    public static Name getSymbolName(String text) {
        String unique = text.intern();
        Name s = dict.get(unique);
        if (s == null) {
            s = new Name(unique);
            dict.put(unique, s);
        }
        return s;
    }
}
