package IR.symbol_table;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.LinkedList;

import static IR.RegisiterInfo.Register_size;
import static IR.RegisiterInfo.byte_size;

/**
 * Created by xietiancheng on 16/4/23.
 */
public class Property implements Cloneable{
    //TODO think about this
    public Name id;
    public static class VarP implements Cloneable
    {
        public Property type;
        public Integer offset;
        public String reg, addr_reg;
        public boolean is_static;
        public VarP() {
            type = null;
            offset = null;
            reg = null;
            is_static = false;
        }
        @Override public Object clone()
        {
            try
            {
                VarP sc = (VarP) super.clone();
                if(type != null)
                    sc.type = (Property)type.clone();
                return sc;
            }
            catch(Exception e)
            {
                throw new RuntimeException("cannot clone");
            }
        }
    }
    public static class FuncP implements Cloneable
    {
        public LinkedList<Property> parameters;
        public String label;
        public String retType;
        public FuncP()
        {
            parameters = null;
            label = null;
            retType = null;
        }
        @Override public Object clone()
        {
            try
            {
                FuncP sc = (FuncP) super.clone();
                if(parameters != null)
                    sc.parameters = (LinkedList<Property>)parameters.clone();
                return sc;
            }
            catch(Exception e)
            {
                throw new RuntimeException("cannot clone");
            }
        }
    }
    public static class TypeP implements Cloneable
    {
        public Integer pointerSize;
        public Integer realSize;
        public Integer dim;
        public Integer[] dim_info;

        public ArrayList<Property> ctx;

        public TypeP(){
            pointerSize = Register_size / byte_size;
            realSize = 0;
            dim = 0;
            dim_info = null;
            ctx = null;
        }
        @Override public Object clone()
        {
            try
            {
                TypeP sc = (TypeP) super.clone();
                if(ctx != null)
                    sc.ctx = (ArrayList<Property>)ctx.clone();
                if(dim_info != null)
                    sc.dim_info = dim_info.clone();
                return sc;
            }
            catch(Exception e)
            {
                throw new RuntimeException("cannot clone");
            }
        }
    }
    public VarP varP; public FuncP funcP;
    public TypeP typeP;
    public Property()
    {
        varP = null;
        funcP = null;
        typeP = null;
    }
    @Override public Object clone()
    {
        try
        {
            Property sc = null;
            sc = (Property) super.clone();
            if(varP != null)
                sc.varP = (VarP)varP.clone();
            if(funcP != null)
                sc.funcP = (FuncP) funcP.clone();
            if(typeP != null)
                sc.typeP = (TypeP) typeP.clone();
            return sc;
        }
        catch(Exception e)
        {
            throw new RuntimeException("cannot clone");
        }
    }
}
