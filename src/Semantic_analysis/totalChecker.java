package Semantic_analysis;

/**
 * Created by 天成 on 2016/3/31.
 */

import Parser.MuseParser;
import Parser.MuseVisitor;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import symbol_table.Name;
import symbol_table.Property;
import symbol_table.Symbol_table;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class totalChecker extends AbstractParseTreeVisitor<LinkedList<Pair<Property, Boolean>>> implements MuseVisitor<LinkedList<Pair<Property, Boolean>>> {
    public Symbol_table symbol_table;

    public totalChecker() {
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitProgram(MuseParser.ProgramContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitDeclaration(MuseParser.DeclarationContext ctx) {
        if (ctx.class_declaration() == null) {
            Property ty = visit(ctx.type()).getFirst().a;
            MuseParser.Init_declaratorsContext ctxInit = ctx.init_declarators();
            //for(Iterator<MuseParser.Init_declaratorContext> i = ctxInit.init_declarator().iterator(); i.hasNext(); )
            {
                //    MuseParser.DeclaratorContext tmp = i.next().declarator();
                MuseParser.DeclaratorContext tmp = ctxInit.init_declarator().declarator();
                String txt = tmp.getText();
                Name id = Name.getSymbolName(txt);
                Property current_var = new Property();
                current_var.id = id;
                current_var.varP = new Property.varProperty();
                current_var.varP.type = ty;
                if (ty.equal(symbol_table.get(Name.getSymbolName("void"), -1)))
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", var cannot be defined as void.");
                symbol_table.put(id, current_var, tmp.getStart().getLine());
            }
            visit(ctx.init_declarators());
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitInit_declarators(MuseParser.Init_declaratorsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitDeclarators(MuseParser.DeclaratorsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitInit_declarator(MuseParser.Init_declaratorContext ctx) {
        Name id = Name.getSymbolName(ctx.declarator().getText());
        Property curVar = symbol_table.get(id, ctx.getStart().getLine());
        if (ctx.equal() != null) {
            LinkedList<Pair<Property, Boolean>> initializer = visit(ctx.initializer());
            if (initializer.size() != 1 || !initializer.getFirst().a.equal(curVar.varP.type)) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type mismatch.");
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitType(MuseParser.TypeContext ctx) {
        if (ctx.array_decl() != null) {
            return visit(ctx.array_decl());
        } else {
            return visit(ctx.type_non_array());
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitArray_decl(MuseParser.Array_declContext ctx) {
        int dim = ctx.LMidbracket().size();
        LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.type_non_array());
        Property myProperty = new Property();
        myProperty.id = tmp.getFirst().a.id;
        myProperty.typeP = new Property.typeProperty();
        myProperty.typeP.isBuiltin = tmp.getFirst().a.typeP.isBuiltin;
        myProperty.typeP.isArray = true;
        myProperty.typeP.dim = dim;
        myProperty.typeP.ctx = tmp.getFirst().a.typeP.ctx;
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        ret.add(new Pair<>(myProperty, false));
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitArray_new(MuseParser.Array_newContext ctx) {
        Property myProperty = new Property();
        myProperty.id = Name.getSymbolName(ctx.type_non_array().getText());
        myProperty.typeP = new Property.typeProperty();
        myProperty.typeP.isArray = true;
        myProperty.typeP.dim = ctx.LMidbracket().size();
        myProperty.typeP.isBuiltin = false;
        myProperty.typeP.dim_info = new ArrayList<>();
        for (int i = 0; i < ctx.expression().size(); ++i) {
            LinkedList<Pair<Property, Boolean>> res = visit(ctx.expression(i));
            Property indextype = res.getFirst().a;
            if (res.size() != 1 || !indextype.id.name.equals("int")) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", subscript must be integer.");
            }
        }
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        ret.add(new Pair<>(myProperty, false));
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitType_non_array(MuseParser.Type_non_arrayContext ctx) {
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        ret.add(new Pair<>(symbol_table.get(Name.getSymbolName(ctx.getText()), ctx.getStart().getLine()), false));
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitClass_declaration(MuseParser.Class_declarationContext ctx) {
        //done in previous
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitDeclarator(MuseParser.DeclaratorContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitInitializer(MuseParser.InitializerContext ctx) {
        return visit(ctx.assignment_expression());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitFunction_definition(MuseParser.Function_definitionContext ctx) {
        symbol_table.beginScope();
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();

        LinkedList<Pair<Property, Boolean>> retType = visit(ctx.type());

        Property thisFunc = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()), ctx.getStart().getLine());

        if (thisFunc.funcP.parameters != null) {
            for (Pair<Name, Property> x :
                    thisFunc.funcP.parameters) {
                symbol_table.put(x.a, x.b, ctx.getStart().getLine());
            }
        }

        for (MuseParser.DeclarationContext x :
                ctx.declaration()) {
            visit(x);
        }

        for (MuseParser.StatementContext x :
                ctx.statement()) {
            LinkedList<Pair<Property, Boolean>> tmp = visit(x);
            if (tmp != null)
                ret.addAll(tmp);
        }

        if (retType.size() != 1) {
            throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", it gives me a big supurise.");
        }

    /*    if (ret.size() == 0 && !retType.getFirst().a.equal(symbol_table.get(Name.getSymbolName("void"), -1))) {
            throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", must return something.");
        }*/

        for (Pair<Property, Boolean> x :
                ret) {
            if (!x.a.equal(retType.getFirst().a)) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", return type mismatch.");
            }
        }

        symbol_table.endScope();
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitParameters(MuseParser.ParametersContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitParameter(MuseParser.ParameterContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitStatement(MuseParser.StatementContext ctx) {
        if (ctx.compond_statement() != null)
            return visit(ctx.compond_statement());
        if (ctx.assignment_statement() != null)
            return visit(ctx.assignment_statement());
        if (ctx.structured_statement() != null)
            return visit(ctx.structured_statement());
        if (ctx.declaration() != null)
            visit(ctx.declaration());
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitAssignment_statement(MuseParser.Assignment_statementContext ctx) {
        if(ctx.expression() != null)
            visit(ctx.expression());
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitStructured_statement(MuseParser.Structured_statementContext ctx) {
        if (ctx.branch_statement() != null)
            return visit(ctx.branch_statement());
        if (ctx.loop_statement() != null)
            return visit(ctx.loop_statement());
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitCompond_statement(MuseParser.Compond_statementContext ctx) {
        symbol_table.beginScope();
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();

        for (MuseParser.DeclarationContext x :
                ctx.declaration()) {
            visit(x);
        }

        for (MuseParser.StatementContext x :
                ctx.statement()) {
            LinkedList<Pair<Property, Boolean>> tmp = visit(x);
            if (tmp != null)
                ret.addAll(tmp);
        }

        symbol_table.endScope();
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitLoop_statement(MuseParser.Loop_statementContext ctx) {
        if (ctx.for_loop_statement() != null)
            return visit(ctx.for_loop_statement());
        else
            return visit(ctx.while_loop_statement());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitBranch_statement(MuseParser.Branch_statementContext ctx) {
        if (ctx.if_statement() != null)
            return visit(ctx.if_statement());
        if (ctx.RETURN() != null) {
            if (ctx.expression() != null)
                return visit(ctx.expression());
            else
                return null;
        }
        if (ctx.BREAK() != null || ctx.CONTINUE() != null) {
            boolean flag = false;

            ParserRuleContext curCtx = ctx;

            ParserRuleContext forloop = new MuseParser.For_loop_statementContext(ctx, -1);
            ParserRuleContext whileloop = new MuseParser.While_loop_statementContext(ctx, -1);

            while (curCtx.depth() != 1) {
                if (curCtx.getRuleIndex() == forloop.getRuleIndex() || curCtx.getRuleIndex() == whileloop.getRuleIndex())
                    flag = true;
                curCtx = curCtx.getParent();
            }

            if (!flag) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", no loop found, nothing to break or continue.");
            }

            return null;
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitFor_loop_statement(MuseParser.For_loop_statementContext ctx) {
        if (ctx.first_expression() != null) {
            LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.first_expression());
        }
        if (ctx.second_expression() != null) {
            LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.second_expression());
            if (/*tmp.size() > 1 || */(tmp.size() == 1 && !tmp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("bool"), -1)))) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", bool is required.");
            }
        }
        if (ctx.third_expression() != null) {
            LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.third_expression());
        }
        symbol_table.beginScope();
        LinkedList<Pair<Property, Boolean>> ret = visit(ctx.statement());
        symbol_table.endScope();
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitFirst_expression(MuseParser.First_expressionContext ctx) {
        return visit(ctx.expression());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitSecond_expression(MuseParser.Second_expressionContext ctx) {
        return visit(ctx.expression());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitThird_expression(MuseParser.Third_expressionContext ctx) {
        return visit(ctx.expression());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitWhile_loop_statement(MuseParser.While_loop_statementContext ctx) {
        LinkedList<Pair<Property, Boolean>> ret = visit(ctx.expression());
        if (ret.size() != 1 || !ret.getFirst().a.equal(symbol_table.get(Name.getSymbolName("bool"), -1))) {
            throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", bool is required.");
        }
        symbol_table.beginScope();
        ret = visit(ctx.statement());
        symbol_table.endScope();
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitIf_statement(MuseParser.If_statementContext ctx) {
        LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.expression());
        if (tmp.size() != 1 || !tmp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("bool"), -1))) {
            throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", bool is required.");
        }
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        symbol_table.beginScope();
        tmp = visit(ctx.statement(0));
        symbol_table.endScope();
        if (tmp != null)
            ret.addAll(tmp);
        if (ctx.ELSE() != null) {
            symbol_table.beginScope();
            tmp = visit(ctx.statement(1));
            symbol_table.endScope();
            if (tmp != null)
                ret.addAll(tmp);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitExpression(MuseParser.ExpressionContext ctx) {
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        /*for(MuseParser.Assignment_expressionContext x :
                ctx.assignment_expression())*/
        {
            MuseParser.Assignment_expressionContext x = ctx.assignment_expression();
            LinkedList<Pair<Property, Boolean>> tmp = visit(x);
            if (tmp != null)
                ret.addAll(tmp);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitAssignment_expression(MuseParser.Assignment_expressionContext ctx) {
        LinkedList<Pair<Property, Boolean>> rhs = visit(ctx.calculation_expression());
        List<MuseParser.Unary_expressionContext> lhs_ctx = ctx.unary_expression();
        boolean isLvalue = rhs.getFirst().b;
        for (Iterator<MuseParser.Unary_expressionContext> i = lhs_ctx.iterator(); i.hasNext(); ) {
            MuseParser.Unary_expressionContext cctx = i.next();
            LinkedList<Pair<Property, Boolean>> curPro = visit(cctx);
            if (!curPro.getFirst().a.equal(rhs.getFirst().a)) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type mismatch.");
            }
            if (!curPro.getFirst().b) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", the left hand side is not left value.");
            }
        }
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        Pair<Property, Boolean> tmp = new Pair<>(rhs.getFirst().a, isLvalue);
        ret.add(tmp);
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitEqual(MuseParser.EqualContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitAssignment_operators(MuseParser.Assignment_operatorsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitCalculation_expression(MuseParser.Calculation_expressionContext ctx) {
        return visit(ctx.logical_caclulation_expression());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx) {
        return visit(ctx.logical_or_expression());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx) {
        if (ctx.logical_and_expression().size() == 1) {
            return visit(ctx.logical_and_expression(0));
        } else {
            Property retType = symbol_table.get(Name.getSymbolName("bool"), -1);
            for (int i = 0; i < ctx.logical_and_expression().size(); ++i) {
                LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.logical_and_expression(i));
                if (tmp.size() != 1 || !tmp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("bool"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", bool is required here.");
                }
            }
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(retType, false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx) {
        if (ctx.bitwise_or_expression().size() == 1) {
            return visit(ctx.bitwise_or_expression(0));
        } else {
            Property retType = symbol_table.get(Name.getSymbolName("bool"), -1);
            for (int i = 0; i < ctx.bitwise_or_expression().size(); ++i) {
                LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.bitwise_or_expression(i));
                if (tmp.size() != 1 || !tmp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("bool"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", bool is required here.");
                }
            }
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(retType, false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx) {
        if (ctx.bitwise_xor_expression().size() == 1) {
            return visit(ctx.bitwise_xor_expression(0));
        } else {
            Property retType = symbol_table.get(Name.getSymbolName("int"), -1);
            for (int i = 0; i < ctx.bitwise_xor_expression().size(); ++i) {
                LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.bitwise_xor_expression(i));
                if (tmp.size() != 1 || !tmp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer is required here.");
                }
            }
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(retType, false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx) {
        if (ctx.bitwise_and_expression().size() == 1) {
            return visit(ctx.bitwise_and_expression(0));
        } else {
            Property retType = symbol_table.get(Name.getSymbolName("int"), -1);
            for (int i = 0; i < ctx.bitwise_and_expression().size(); ++i) {
                LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.bitwise_and_expression(i));
                if (tmp.size() != 1 || !tmp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer is required here.");
                }
            }
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(retType, false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx) {
        if (ctx.equality_expression().size() == 1) {
            return visit(ctx.equality_expression(0));
        } else {
            Property retType = symbol_table.get(Name.getSymbolName("int"), -1);
            for (int i = 0; i < ctx.equality_expression().size(); ++i) {
                LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.equality_expression(i));
                if (tmp.size() != 1 || !tmp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer is required here.");
                }
            }
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(retType, false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitIsequal(MuseParser.IsequalContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitNotequal(MuseParser.NotequalContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitEquality_expression(MuseParser.Equality_expressionContext ctx) {
        if (ctx.relation_expression().size() == 1) {
            return visit(ctx.relation_expression(0));
        } else {
            Property retType = symbol_table.get(Name.getSymbolName("bool"), -1);
            LinkedList<Pair<Property, Boolean>> retChain = new LinkedList<>();
            for (int i = 0; i < ctx.relation_expression().size(); ++i) {
                LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.relation_expression(i));
                retChain.add(tmp.getFirst());
                if (tmp.size() != 1) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type error.");
                }
            }
            if (!retChain.get(0).a.equal(retChain.get(1).a)) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type error.");
            }
            for (int i = 2; i < ctx.relation_expression().size(); ++i) {
                if (!retChain.get(i).a.equal(symbol_table.get(Name.getSymbolName("bool"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type error.");
                }
            }
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(retType, false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitBigger(MuseParser.BiggerContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitSmaller(MuseParser.SmallerContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitBigger_e(MuseParser.Bigger_eContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitSmaller_e(MuseParser.Smaller_eContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitRelation_expression(MuseParser.Relation_expressionContext ctx) {
        if (ctx.shift_expression().size() == 1) {
            return visit(ctx.shift_expression(0));
        } else {
            LinkedList<Pair<Property, Boolean>> a, b;
            a = visit(ctx.shift_expression(0));
            b = visit(ctx.shift_expression(1));

            if (!a.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1)) && !a.getFirst().a.equal(symbol_table.get(Name.getSymbolName("string"), -1))) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type mismatch. (Expected int or string)");
            }

            if (a.size() != 1 || b.size() != 1) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type mismatch.");
            }
            if (!a.getFirst().a.equal(b.getFirst().a)) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type mismatch.");
            }

            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(symbol_table.get(Name.getSymbolName("bool"), -1), false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitLshift(MuseParser.LshiftContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitRshift(MuseParser.RshiftContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitPlus(MuseParser.PlusContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitMinus(MuseParser.MinusContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitShift_expression(MuseParser.Shift_expressionContext ctx) {
        if (ctx.add_expression().size() == 1) {
            return visit(ctx.add_expression(0));
        } else {
            for (int i = 0; i < ctx.add_expression().size(); ++i) {
                LinkedList<Pair<Property, Boolean>> tmp = visit(ctx.add_expression(i));
                if (tmp.size() != 1 || !tmp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer is required.");
                }
            }
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(symbol_table.get(Name.getSymbolName("int"), -1), false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitAdd_expression(MuseParser.Add_expressionContext ctx) {
        if (ctx.mul_expression().size() == 1) {
            return visit(ctx.mul_expression(0));
        }
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        LinkedList<Pair<Property, Boolean>> a, b;
        a = visit(ctx.mul_expression(0));
        for(int i = 0; i < ctx.add_op().size(); ++i) {
            if (ctx.add_op(i).plus() != null) { //string can use operator +
                b = visit(ctx.mul_expression(i + 1));
                if (a.size() != 1 || b.size() != 1) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer or string is required.");
                }
                if (!a.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1)) && !a.getFirst().a.equal(symbol_table.get(Name.getSymbolName("string"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer or string is required.");
                }
                if (!a.getFirst().a.equal(b.getFirst().a)) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type mismatch.");
                }
            } else {
                b = visit(ctx.mul_expression(i + 1));
                if (a.size() != 1 || b.size() != 1) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer is required.");
                }
                if (!a.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer is required.");
                }
                if (!a.getFirst().a.equal(b.getFirst().a)) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type mismatch.");
                }
            }
        }
        ret.add(new Pair<>(a.getFirst().a, false));
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitMultiply(MuseParser.MultiplyContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitDivision(MuseParser.DivisionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitMod(MuseParser.ModContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitMul_expression(MuseParser.Mul_expressionContext ctx) {
        if (ctx.unary_expression().size() == 1) {
            return visit(ctx.unary_expression(0));
        } else {
            LinkedList<Pair<Property, Boolean>> a, b;
            a = visit(ctx.unary_expression(0));
            b = visit(ctx.unary_expression(1));
            if (a.size() != 1 || b.size() != 1 || !a.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1))) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", integer is required.");
            }
            if (!a.getFirst().a.equal(b.getFirst().a)) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", type mismatch.");
            }
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(symbol_table.get(Name.getSymbolName("int"), -1), false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitPlusplus(MuseParser.PlusplusContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitMinusminus(MuseParser.MinusminusContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitUnary_expression(MuseParser.Unary_expressionContext ctx) {
        if (ctx.postfix_expression() != null)
            return visit(ctx.postfix_expression());
        if (ctx.unary_operation() != null)
            return visit(ctx.unary_expression());
        if (ctx.number() != null)
            return visit(ctx.number());
        if (ctx.functionCall_expression() != null)
            return visit(ctx.functionCall_expression());
        if (ctx.new_operation() != null)
            return visit(ctx.new_operation());
        if (ctx.pre_defined_constants() != null)
            return visit(ctx.pre_defined_constants());
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx) {
        if (ctx.getText().equals("true") || ctx.getText().equals("false")) {
            Property myProperty = symbol_table.get(Name.getSymbolName("bool"), ctx.getStart().getLine());
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(myProperty, false));
            return ret;
        } else {
            Property myProperty = symbol_table.get(Name.getSymbolName("1122special"), ctx.getStart().getLine());
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(myProperty, false));
            return ret;
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitClass_new(MuseParser.Class_newContext ctx) {
        Property myProperty;
        myProperty = symbol_table.get(Name.getSymbolName(ctx.getText()), ctx.getStart().getLine());
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        ret.add(new Pair<>(myProperty, false));
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitNew_operation(MuseParser.New_operationContext ctx) {
        if (ctx.array_new() == null)
            return visit(ctx.class_new());
        else
            return visit(ctx.array_new());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx) {
        if (ctx.arguements() != null) {
            Property tmp = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()), ctx.getStart().getLine());
            if (tmp.funcP == null) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", function not found.");
            }
            LinkedList<Pair<Name, Property>> paralist = tmp.funcP.parameters;
            Iterator<MuseParser.Assignment_expressionContext> j = ctx.arguements().assignment_expression().iterator();
            for (Iterator<Pair<Name, Property>> i = paralist.iterator(); i.hasNext(); ) {
                Property t = i.next().b;
                if (!j.hasNext()) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", parameter list length mismatch.");
                }
                MuseParser.Assignment_expressionContext m = j.next();

                LinkedList<Pair<Property, Boolean>> v = visit(m);
                if (!t.varP.type.equal(v.getFirst().a)) {
                    throw new RuntimeException("Line: " + m.getStart().getLine() + ", type mismatch.");
                }
            }
            if (j.hasNext()) {
                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", parameter list length mismatch.");
            }
        }
        Property retType = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()), ctx.getStart().getLine()).funcP.retType;
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        ret.add(new Pair<>(retType, false));
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitArguements(MuseParser.ArguementsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitValue(MuseParser.ValueContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitNot_sign(MuseParser.Not_signContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitUnary_operation(MuseParser.Unary_operationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitBitwise_not(MuseParser.Bitwise_notContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitPostfix_expression(MuseParser.Postfix_expressionContext ctx) {
        LinkedList<Pair<Property, Boolean>> primary_info = visit(ctx.primary_expression());
        List<MuseParser.PostfixContext> postfixs = ctx.postfix();
        Property curType = primary_info.getFirst().a;
        Boolean isLvalue = primary_info.getFirst().b;
        for (Iterator<MuseParser.PostfixContext> i = postfixs.iterator(); i.hasNext(); ) {
            MuseParser.PostfixContext cur = i.next();
            if (cur.LMidbracket() != null) {
                LinkedList<Pair<Property, Boolean>> exp = visit(cur.expression());
                if (exp.size() != 1 || !exp.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), ctx.getStart().getLine()))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", subscript is not integer.");
                }
                if (curType.typeP.dim < 0) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", dimension is not correct.");
                }
                Property tmp = curType;
                curType = new Property();
                curType.id = tmp.id;
                curType.typeP = new Property.typeProperty();
                curType.typeP.dim = tmp.typeP.dim - 1;
                curType.typeP.ctx = tmp.typeP.ctx;
                curType.typeP.isArray = (curType.typeP.dim != 0);
                curType.typeP.dim_info = tmp.typeP.dim_info;
                curType.typeP.isBuiltin = tmp.typeP.isBuiltin;
                isLvalue &= true;
            }
            if (cur.getMember() != null) {
                if (curType.typeP.dim > 0) {
                    if (cur.functionCall_expression() == null) {
                        throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", no such member.");
                    }
                    if (!cur.functionCall_expression().Identifier().getText().equals("size")) {
                        throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", niconiconi?" + cur.functionCall_expression().Identifier().getText() + "function 見て無い");
                    }
                    if (cur.functionCall_expression().arguements() != null) {
                        throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", makimakima~, argument list mismatch.(length)");
                    }
                    curType = symbol_table.get(Name.getSymbolName("int"), ctx.getStart().getLine());
                    isLvalue &= false;
                } else {
                    if (cur.functionCall_expression() != null) {
                        if (!curType.equal(symbol_table.get(Name.getSymbolName("string"), -1))) {
                            throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", no such member.");
                        }
                        if (cur.functionCall_expression().Identifier().getText().equals("length")) {
                            if (cur.functionCall_expression().arguements() != null) {
                                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", argument list mismatch.(length)");
                            }
                            curType = symbol_table.get(Name.getSymbolName("int"), ctx.getStart().getLine());
                            isLvalue &= false;
                        } else if (cur.functionCall_expression().Identifier().getText().equals("substring")) {
                            LinkedList<Pair<Property, Boolean>> exp1, exp2;
                            if (cur.functionCall_expression().arguements().assignment_expression().size() != 2) {
                                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", argument list mismatch.(length)");
                            }
                            exp1 = visit(cur.functionCall_expression().arguements().assignment_expression(0));
                            exp2 = visit(cur.functionCall_expression().arguements().assignment_expression(1));
                            if (exp1.size() != 1 || exp2.size() != 1 || !exp1.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1)) || !exp2.getFirst().a.equal(symbol_table.get(Name.getSymbolName("int"), -1))) {
                                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", argument list mismatch.(type)");
                            }
                            curType = symbol_table.get(Name.getSymbolName("string"), -1);
                            isLvalue &= false;
                        } else if (cur.functionCall_expression().Identifier().getText().equals("parseInt")) {
                            if (cur.functionCall_expression().arguements() != null) {
                                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", argument list mismatch.(length)");
                            }
                            curType = symbol_table.get(Name.getSymbolName("int"), -1);
                            isLvalue &= false;
                        } else if (cur.functionCall_expression().Identifier().getText().equals("ord")) {
                            if (cur.functionCall_expression().arguements().assignment_expression().size() != 1) {
                                throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", argument list mismatch.(length)");
                            }
                            curType = symbol_table.get(Name.getSymbolName("int"), -1);
                            isLvalue &= false;
                        } else {
                            throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", no such member.");
                        }
                    } else {
                        curType.typeP.ctx = symbol_table.getForClass(curType.id, ctx.getStart().getLine()).typeP.ctx;
                        if (curType.typeP.ctx == null || curType.typeP.ctx.get(Name.getSymbolName(cur.Identifier().getText())) == null) {
                            throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", no such member.");
                        }
                        curType = curType.typeP.ctx.get(Name.getSymbolName(cur.Identifier().getText())).varP.type;
                        isLvalue &= true;
                    }
                }
            }
            if (cur.plusplus() != null || cur.minusminus() != null) {
                if (!curType.equal(symbol_table.get(Name.getSymbolName("int"), -1))) {
                    throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", ++/-- operator only suit for integer.");
                }
                isLvalue &= false;
            }
        }

        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        ret.add(new Pair<>(curType, isLvalue));
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitGetMember(MuseParser.GetMemberContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitPostfix(MuseParser.PostfixContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitPrimary_expression(MuseParser.Primary_expressionContext ctx) {
        if (ctx.Identifier() != null) {
            Property var = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()), ctx.getStart().getLine());
            LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
            ret.add(new Pair<>(var.varP.type, true));
            return ret;
        }
        if (ctx.constant() != null) {
            if (ctx.constant().string_constant() != null) {
                LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
                ret.add(new Pair<>(symbol_table.get(Name.getSymbolName("string"), -1), false));
                return ret;
            } else {
                LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
                ret.add(new Pair<>(symbol_table.get(Name.getSymbolName("int"), -1), false));
                return ret;
            }
        }
        if (ctx.expression() != null) {
            return visit(ctx.expression());
        }
        if (ctx.functionCall_expression() != null) {
            return visit(ctx.functionCall_expression());
        }
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitType_name(MuseParser.Type_nameContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitConstant(MuseParser.ConstantContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitNumeric_constant(MuseParser.Numeric_constantContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitInteger_constant(MuseParser.Integer_constantContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitString_constant(MuseParser.String_constantContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitTypenametokens(MuseParser.TypenametokensContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitNumber(MuseParser.NumberContext ctx) {
        LinkedList<Pair<Property, Boolean>> ret = new LinkedList<>();
        ret.add(new Pair<>(symbol_table.get(Name.getSymbolName("int"), ctx.getStart().getLine()), false));
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Pair<Property, Boolean>> visitUnion_class(MuseParser.Union_classContext ctx) {
        return visitChildren(ctx);
    }
    @Override public LinkedList<Pair<Property, Boolean>> visitMul_op(MuseParser.Mul_opContext ctx) { return visitChildren(ctx); }
    @Override public LinkedList<Pair<Property, Boolean>> visitAdd_op(MuseParser.Add_opContext ctx) { return visitChildren(ctx); }
    @Override public LinkedList<Pair<Property, Boolean>> visitShift_op(MuseParser.Shift_opContext ctx) { return visitChildren(ctx); }
    @Override public LinkedList<Pair<Property, Boolean>> visitRela_op(MuseParser.Rela_opContext ctx) { return visitChildren(ctx); }
    @Override public LinkedList<Pair<Property, Boolean>> visitEqu_op(MuseParser.Equ_opContext ctx) { return visitChildren(ctx); }

}
