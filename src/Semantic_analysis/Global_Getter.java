// Generated from M.g4 by ANTLR 4.5.2
package Semantic_analysis;

import Parser.MuseParser;
import Parser.MuseVisitor;
import org.antlr.v4.runtime.misc.Pair;
import org.antlr.v4.runtime.tree.AbstractParseTreeVisitor;
import symbol_table.Name;
import symbol_table.Property;
import symbol_table.Symbol_table;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Global_Getter extends AbstractParseTreeVisitor<LinkedList<Property>> implements MuseVisitor<LinkedList<Property>> {
    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */

    public Symbol_table symbol_table;
    private boolean working;

    public Global_Getter() {
        working = false;
    }


    @Override
    public LinkedList<Property> visitProgram(MuseParser.ProgramContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitDeclaration(MuseParser.DeclarationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitInit_declarators(MuseParser.Init_declaratorsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitDeclarators(MuseParser.DeclaratorsContext ctx) {
        /*LinkedList<Property> ret = new LinkedList<Property>();
        for(Iterator<MuseParser.DeclaratorContext> i = ctx.declarator().iterator(); i.hasNext();)
		{
			ret.addAll(visit(i.next()));
		}
		return ret;*/
        return visit(ctx.declarator());
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitInit_declarator(MuseParser.Init_declaratorContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitType(MuseParser.TypeContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitArray_decl(MuseParser.Array_declContext ctx) {
        if (!working) {
            return visitChildren(ctx);
        }
        int dim = ctx.LMidbracket().size();
        LinkedList<Property> tmp = visit(ctx.type_non_array());
        Property myProperty = new Property();
        myProperty.id = tmp.getFirst().id;
        myProperty.typeP = new Property.typeProperty();
        myProperty.typeP.isBuiltin = tmp.getFirst().typeP.isBuiltin;
        myProperty.typeP.isArray = true;
        myProperty.typeP.dim = dim;
        LinkedList<Property> ret = new LinkedList<>();
        ret.add(myProperty);
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitArray_new(MuseParser.Array_newContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitType_non_array(MuseParser.Type_non_arrayContext ctx) {
        if (!working) {
            return visitChildren(ctx);
        }
        LinkedList<Property> ret = new LinkedList<>();
        Property tmp;
        String txt = ctx.typenametokens().getText();
        tmp = symbol_table.get(Name.getSymbolName(txt), ctx.getStart().getLine());
        ret.add(tmp);
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitClass_declaration(MuseParser.Class_declarationContext ctx) {
        if (ctx.depth() != 3) {
            throw new RuntimeException("Line: " + ctx.getStart().getLine() + ", nested class declaration is not supported");
        }

        symbol_table.beginScope();

        working = true;

        LinkedList<Property> ret = new LinkedList<>();

        Iterator<MuseParser.DeclaratorsContext> j = ctx.declarators().iterator();
        for (Iterator<MuseParser.TypeContext> i = ctx.type().iterator(); i.hasNext(); ) {
            int Lineid;
            MuseParser.TypeContext ttmp = i.next();
            Lineid = ttmp.getStart().getLine();
            LinkedList<Property> ty = visit(ttmp);
            LinkedList<Property> vars = visit(j.next());
            for (Iterator<Property> k = vars.iterator(); k.hasNext(); ) {
                Property myProperty = new Property(), tmp = k.next();
                myProperty.id = tmp.id;
                myProperty.varP = new Property.varProperty();
                myProperty.varP.type = ty.getFirst();
                symbol_table.put(tmp.id, myProperty, Lineid);
                ret.add(myProperty);
            }
        }

        symbol_table.endScope();

        Property myProperty = symbol_table.get(Name.getSymbolName(ctx.Identifier().getText()), ctx.getStart().getLine());
        for (Iterator<Property> i = ret.iterator(); i.hasNext(); ) {
            Property tmp = i.next();
            myProperty.typeP.ctx.put(tmp.id, tmp);
        }
        working = false;
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitDeclarator(MuseParser.DeclaratorContext ctx) {
        Property myProperty = new Property();
        myProperty.id = Name.getSymbolName(ctx.getText());
        LinkedList<Property> ret = new LinkedList<>();
        ret.add(myProperty);
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitInitializer(MuseParser.InitializerContext ctx) {
        return visitChildren(ctx);
    }
    /**
     * {@inheritDoc}
     *
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitFunction_definition(MuseParser.Function_definitionContext ctx) {
        working = true;
        LinkedList<Property> ret, para = null;
        ret = visit(ctx.type());
        symbol_table.beginScope();
        if (ctx.parameters() != null)
            para = visit(ctx.parameters());
        symbol_table.endScope();
        Property result = new Property();
        result.funcP = new Property.funcProperty();

        if (para != null) {
            result.funcP.parameters = new LinkedList<>();
            for (ListIterator<Property> i = para.listIterator(); i.hasNext(); ) {
                Property tmp = i.next();
                result.funcP.parameters.add(new Pair<>(tmp.id, tmp));
            }
        }

        result.funcP.retType = ret.getFirst();
        result.id = Name.getSymbolName(ctx.Identifier().getText());

        symbol_table.put(result.id, result, ctx.getStart().getLine());
        working = false;
        return null;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitParameters(MuseParser.ParametersContext ctx) {
        LinkedList<Property> ret = new LinkedList<>();

        List<MuseParser.ParameterContext> tmp = ctx.parameter();
        for (int i = 0; i < tmp.size(); ++i) {
            ret.addAll(visit(tmp.get(i)));
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitParameter(MuseParser.ParameterContext ctx) {
        Property typePro = visit(ctx.type()).getFirst();
        Property varPro = visit(ctx.declarator()).getFirst();
        varPro.varP = new Property.varProperty();
        varPro.varP.type = typePro;
        LinkedList<Property> ret = new LinkedList<>();
        ret.add(varPro);
        symbol_table.put(varPro.id, varPro, ctx.getStart().getLine());
        return ret;
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitStatement(MuseParser.StatementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitAssignment_statement(MuseParser.Assignment_statementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitStructured_statement(MuseParser.Structured_statementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitCompond_statement(MuseParser.Compond_statementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitLoop_statement(MuseParser.Loop_statementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitBranch_statement(MuseParser.Branch_statementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitFor_loop_statement(MuseParser.For_loop_statementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitFirst_expression(MuseParser.First_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitSecond_expression(MuseParser.Second_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitThird_expression(MuseParser.Third_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitWhile_loop_statement(MuseParser.While_loop_statementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitIf_statement(MuseParser.If_statementContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitExpression(MuseParser.ExpressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitAssignment_expression(MuseParser.Assignment_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitEqual(MuseParser.EqualContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitAssignment_operators(MuseParser.Assignment_operatorsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitCalculation_expression(MuseParser.Calculation_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitIsequal(MuseParser.IsequalContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitNotequal(MuseParser.NotequalContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitEquality_expression(MuseParser.Equality_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitBigger(MuseParser.BiggerContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitSmaller(MuseParser.SmallerContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitBigger_e(MuseParser.Bigger_eContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitSmaller_e(MuseParser.Smaller_eContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitRelation_expression(MuseParser.Relation_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitLshift(MuseParser.LshiftContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitRshift(MuseParser.RshiftContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitPlus(MuseParser.PlusContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitMinus(MuseParser.MinusContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitShift_expression(MuseParser.Shift_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitAdd_expression(MuseParser.Add_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitMultiply(MuseParser.MultiplyContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitDivision(MuseParser.DivisionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitMod(MuseParser.ModContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitMul_expression(MuseParser.Mul_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitPlusplus(MuseParser.PlusplusContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitMinusminus(MuseParser.MinusminusContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitUnary_expression(MuseParser.Unary_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */

    @Override
    public LinkedList<Property> visitBitwise_not(MuseParser.Bitwise_notContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitClass_new(MuseParser.Class_newContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitNew_operation(MuseParser.New_operationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitArguements(MuseParser.ArguementsContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitValue(MuseParser.ValueContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitNot_sign(MuseParser.Not_signContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitUnary_operation(MuseParser.Unary_operationContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitPostfix_expression(MuseParser.Postfix_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitGetMember(MuseParser.GetMemberContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitPostfix(MuseParser.PostfixContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitPrimary_expression(MuseParser.Primary_expressionContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitType_name(MuseParser.Type_nameContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitConstant(MuseParser.ConstantContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitNumeric_constant(MuseParser.Numeric_constantContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitInteger_constant(MuseParser.Integer_constantContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitString_constant(MuseParser.String_constantContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitTypenametokens(MuseParser.TypenametokensContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitNumber(MuseParser.NumberContext ctx) {
        return visitChildren(ctx);
    }

    /**
     * {@inheritDoc}
     * <p>
     * <p>The default implementation returns the result of calling
     * {@link #visitChildren} on {@code ctx}.</p>
     */
    @Override
    public LinkedList<Property> visitUnion_class(MuseParser.Union_classContext ctx) {
        return visitChildren(ctx);
    }
    @Override public LinkedList<Property> visitMul_op(MuseParser.Mul_opContext ctx) { return visitChildren(ctx); }
    @Override public LinkedList<Property> visitAdd_op(MuseParser.Add_opContext ctx) { return visitChildren(ctx); }
    @Override public LinkedList<Property> visitShift_op(MuseParser.Shift_opContext ctx) { return visitChildren(ctx); }
    @Override public LinkedList<Property> visitRela_op(MuseParser.Rela_opContext ctx) { return visitChildren(ctx); }
    @Override public LinkedList<Property> visitEqu_op(MuseParser.Equ_opContext ctx) { return visitChildren(ctx); }

}