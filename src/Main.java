import IR.IR_generator;
import Parser.MuseLexer;
import Parser.MuseParser;
import Semantic_analysis.Global_Getter;
import Semantic_analysis.Global_class_getter;
import Semantic_analysis.totalChecker;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import symbol_table.Name;
import symbol_table.Symbol_table;

import java.io.*;

public class Main {

    static Symbol_table symbol_table;

    private static void phase1(InputStream src) throws IOException {

        InputStreamReader Src = new InputStreamReader(src);
        ANTLRInputStream input = new ANTLRInputStream(Src);
        MuseLexer lexer = new MuseLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        MuseParser parser = new MuseParser(tokens);

        ParseTree tree = parser.program();
        if (parser.getNumberOfSyntaxErrors() > 0) {
            throw new RuntimeException("Syntax Error");
        }

        Global_class_getter global_class_getter = new Global_class_getter();
        global_class_getter.symbol_table = symbol_table;
        global_class_getter.visit(tree);


        Global_Getter global_getter = new Global_Getter();
        global_getter.symbol_table = symbol_table;
        global_getter.visit(tree);

        totalChecker tot_check = new totalChecker();
        tot_check.symbol_table = symbol_table;
        tot_check.visit(tree);

       /* AST_printer ast_printer = new AST_printer();
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(ast_printer, tree);
        */
        if(symbol_table.get(Name.getSymbolName("main"), -1).funcP == null ||
                !symbol_table.get(Name.getSymbolName("main"), -1).funcP.retType.equal(symbol_table.get(Name.getSymbolName("int"), -1)))
        {
            throw new RuntimeException("main not found, or return type of main is not int");
        }

        //symbol_table.print();

        IR_generator ir_gen = new IR_generator();
        ir_gen.generate(tree);
    }

    private static FileInputStream add_built_in_to_input(FileInputStream src) throws IOException
    {
        BufferedReader user = new BufferedReader(new InputStreamReader(src));
        BufferedReader builtin = new BufferedReader(new InputStreamReader(new FileInputStream("built_in.ll")));

        BufferedWriter new_src = new BufferedWriter(new FileWriter(".temp.ll.swp"));

        String line;
        while((line = user.readLine()) != null)
        {
            new_src.write(line);
            new_src.newLine();
        }
        while((line = builtin.readLine()) != null)
        {
            new_src.write(line);
            new_src.newLine();
        }
        new_src.close();
        user.close();
        builtin.close();
        return new FileInputStream(new File(".temp.ll.swp"));
    }


    public static void main(String[] args) throws Exception { //args specified the file location
        FileInputStream src = add_built_in_to_input(new FileInputStream("test.ll"));
        src.close();
        File f = new File(".temp.ll.swp");
        symbol_table = new Symbol_table();
        try {
            phase1(new FileInputStream(f));
        } catch (Exception e) {
            //f.delete();
            throw e;
        }
        f.delete();
        System.exit(0);
    }
}
