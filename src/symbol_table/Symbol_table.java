package symbol_table;

import org.antlr.v4.runtime.misc.Pair;

import java.util.*;

/**
 * Created by xietiancheng on 16/3/29.
 * Wota gei
 */
public class Symbol_table {
    private ArrayList<HashMap<Name, Property>> symbol_table;

    public Symbol_table() {
        symbol_table = new ArrayList<>();
        symbol_table.add(new HashMap<>());
        {
            Property Int = new Property();
            Int.id = Name.getSymbolName("int");
            Int.typeP = new Property.typeProperty();
            Int.typeP.isBuiltin = true;
            symbol_table.get(0).put(Int.id, Int);
        }
        {
            Property Int = new Property();
            Int.id = Name.getSymbolName("void");
            Int.typeP = new Property.typeProperty();
            Int.typeP.isBuiltin = true;
            symbol_table.get(0).put(Int.id, Int);
        }
        {
            Property Int = new Property();
            Int.id = Name.getSymbolName("bool");
            Int.typeP = new Property.typeProperty();
            Int.typeP.isBuiltin = true;
            symbol_table.get(0).put(Int.id, Int);
        }
        {
            Property True = new Property();
            True.id = Name.getSymbolName("true");
            True.varP = new Property.varProperty();
            True.varP.type = get(Name.getSymbolName("bool"), -1);
            symbol_table.get(0).put(True.id, True);
        }
        {
            Property False = new Property();
            False.id = Name.getSymbolName("false");
            False.varP = new Property.varProperty();
            False.varP.type = get(Name.getSymbolName("bool"), -1);
            symbol_table.get(0).put(False.id, False);
        }
        {
            Property special = new Property();
            special.id = Name.getSymbolName("1122special");
            special.typeP = new Property.typeProperty();
            special.typeP.isArray = false;
            special.typeP.isBuiltin = true;
            symbol_table.get(0).put(special.id, special);
        }
        {
            Property Null = new Property();
            Null.id = Name.getSymbolName("null");
            Null.varP = new Property.varProperty();
            Null.varP.type = get(Name.getSymbolName("1122special"), -1);
            symbol_table.get(0).put(Null.id, Null);
        }
    }

    private int lastIndexOf(Name key) {
        for (int i = symbol_table.size() - 1; i >= 0; --i)
            if (symbol_table.get(i).get(key) != null)
                return i;
        return -1;
    }

    public void put(Name key, Property value, int Line_id) {
        if (lastIndexOf(key) == symbol_table.size() - 1)
            throw new RuntimeException("Line " + Line_id + ", redefinition of " + key.name + " with a different type.");
        symbol_table.get(symbol_table.size() - 1).put(key, value);
    }

    public Property get(Name key, int Line_id) {
        if (lastIndexOf(key) == -1)
            throw new RuntimeException("Line " + Line_id + ", no such name/type of " + key.name + ".");
        return symbol_table.get(lastIndexOf(key)).get(key);
    }

    public Property getForClass(Name key, int Line_id) {
        for(int i = symbol_table.size() - 1; i >= 0; --i)
        {
            Property res = symbol_table.get(i).get(key);
            if(res != null && res.typeP != null)
            {
                return res;
            }
        }
        throw new RuntimeException("Line " + Line_id + ", no such name/type of " + key.name + ".");
    }

    public void beginScope() {
        symbol_table.add(new HashMap<>());
    }

    public void endScope() {
        symbol_table.remove(symbol_table.size() - 1);
    }

    /*public void print() {
        for (HashMap<Name, Property> cur : symbol_table) {
            Set<Map.Entry<Name, Property>> curS = cur.entrySet();
            for (Map.Entry<Name, Property> ready_to_print : curS) {
                System.out.println("Name " + ready_to_print.getKey().name);
                System.out.println("Property ");
                ready_to_print.getValue().print();
            }
        }
    }*/
}
