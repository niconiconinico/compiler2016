package symbol_table;

import org.antlr.v4.runtime.misc.Pair;

import java.util.*;

/**
 * Created by tiancheng on 16-3-25.
 */

public class Property {
    public Name id;
    public varProperty varP;
    public funcProperty funcP;
    public typeProperty typeP;

    public boolean equal(Property e) {
        if (typeP != null) {
            if (typeP.isArray) {
                return id.name.equals(e.id.name) && typeP.dim.equals(e.typeP.dim);
            }
        }
        if (id.name.equals("1122special") || e.id.name.equals("1122special")) {
            if (id.name.equals("string") || e.id.name.equals("string"))
                return false;
            if (id.name.equals("int") || e.id.name.equals("int"))
                return false;
            if (id.name.equals("bool") || e.id.name.equals("bool"))
                return false;
            return true;
        }
        return id.name.equals(e.id.name);
    }
/*
    public void print() {
        System.out.println("    Name: " + id.name);
        if (varP != null) {
            System.out.println("    Var, type = " + varP.type.id.name);
        }
        if (funcP != null) {
            System.out.println("    Func");
            System.out.println("        return type: " + funcP.retType.id.name);
            System.out.println("        Parameters");
            if (funcP.parameters != null) {
                LinkedList<Pair<Name, Property>> par = funcP.parameters;
                for (Iterator<Pair<Name, Property>> i = par.iterator(); i.hasNext(); ) {
                    Pair<Name, Property> cur = i.next();
                    System.out.println("            name: " + cur.a.name + " type: " + cur.b.varP.type.id.name);
                }
            } else {
                System.out.println("            no parameter");
            }
        }
        if (typeP != null) {
            System.out.print("      ");
            if (typeP.isBuiltin)
                System.out.print("builtin ");
            else
                System.out.print("user-defined(class) ");
            System.out.println(typeP.dim + " dimension type (0 for non-array)");
            if (!typeP.isBuiltin) {
                Set<Map.Entry<Name, Property>> par = typeP.ctx.entrySet();
                for (Iterator<Map.Entry<Name, Property>> i = par.iterator(); i.hasNext(); ) {
                    Map.Entry<Name, Property> cur = i.next();
                    System.out.print("            name: " + cur.getKey().name + " type: " + cur.getValue().varP.type.id.name);
                    System.out.println(" dimension " + cur.getValue().varP.type.typeP.dim);
                }
            }
        }
    }*/

    public static class varProperty {
        public Property type;
    }

    public static class funcProperty {
        public LinkedList<Pair<Name, Property>> parameters;
        public Property retType;
    }

    public static class typeProperty {
        public boolean isBuiltin;
        public HashMap<Name, Property> ctx; //for class
        public boolean isArray;
        public Integer dim;
        public ArrayList<Integer> dim_info;

        public typeProperty() {
            isArray = isBuiltin = false;
            dim = 0;
            ctx = null;
        }
    }
}
