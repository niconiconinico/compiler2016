#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <fstream>
#include <map>
#include <cassert>
#include <sstream>

using namespace std;

map<string, int> label_pc;
class operation
{
public:
	string opcode;
	vector<string> operands;
	vector<int> real_operands;
};

operation get_context(string info, int pc)
{
	//remeber, there is ANOTHER COPY in CFG
	if (info[0] == '@')
	{
		operation ret;
		ret.opcode = "label";
		if (info.substr(1, 8) == "function")
		{
			int cur = 0;
			string tmp;
			bool string_mode = false;
			while (cur < info.size())
			{
				if (!string_mode && info[cur] == ' ')
				{
					break;
				}
				if (info[cur] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
				}
				else if (info[cur] == '-')
				{
					tmp = tmp + "sub_";
				}
				else if (info[cur] == ' ')
				{
					tmp = tmp + "space_";
				}
				else if (info[cur] == '\\')
				{
					tmp = tmp + "backslash_";
				}
				else if (info[cur] == ':')
				{
					tmp = tmp + "colon_";
				}
				else if (info[cur] == '!')
				{
					tmp = tmp + "exg_";
				}
				else if (info[cur] == '>')
				{
					tmp = tmp + "dayu_";
				}
				else if (info[cur] == '+')
				{
					tmp = tmp + "plus_";
				}
				else if (info[cur] == '=')
				{
					tmp = tmp + "equ_";
				}
				else if (info[cur] == '.')
				{
					tmp = tmp + "dot_";
				}
				else if (info[cur] == ',')
				{
					tmp = tmp + "comma_";
				}
				else if (info[cur] == '\'')
				{
					tmp = tmp + "hyphen_";
				}
				else if (info[cur] == '(')
				{
					tmp = tmp + "lbracket_";
				}
				else if (info[cur] == ')')
				{
					tmp = tmp + "rbracket_";
				}
				else
					tmp = tmp + info[cur];
				cur++;
			}
			ret.operands.push_back(tmp);
			while (cur < info.size())
			{
				tmp = "";
				cur++;
				string_mode = false;
				while (cur < info.size())
				{
					if (!string_mode && info[cur] == ' ')
					{
						break;
					}
					if (info[cur] == '\"')
					{
						string_mode ^= 1;
						tmp = tmp + "quote_";
						cur++;
					}
					else if (info[cur] == '-')
					{
						tmp = tmp + "sub_";
						cur++;
					}
					else if (info[cur] == ' ')
					{
						tmp = tmp + "space_";
						cur++;
					}
					else if (info[cur] == '\\')
					{
						tmp = tmp + "backslash_";
						cur++;
					}
					else if (info[cur] == ':')
					{
						tmp = tmp + "colon_";
						cur++;
					}
					else if (info[cur] == '!')
					{
						tmp = tmp + "exg_";
						cur++;
					}
					else if (info[cur] == '>')
					{
						tmp = tmp + "dayu_";
						cur++;
					}
					else if (info[cur] == '+')
					{
						tmp = tmp + "plus_";
						cur++;
					}
					else if (info[cur] == '=')
					{
						tmp = tmp + "equ_";
						cur++;
					}
					else if (info[cur] == '.')
					{
						tmp = tmp + "dot_";
						cur++;
					}
					else if (info[cur] == ',')
					{
						tmp = tmp + "comma_";
						cur++;
					}
					else if (info[cur] == '\'')
					{
						tmp = tmp + "hyphen_";
						cur++;
					}
					else if (info[cur] == '(')
					{
						tmp = tmp + "lbracket_";
						cur++;
					}
					else if (info[cur] == ')')
					{
						tmp = tmp + "rbracket_";
						cur++;
					}
					else
						tmp.push_back(info[cur++]);
				}
				ret.operands.push_back(tmp);
			}
			label_pc[tmp] = pc + 1;
		}
		else
		{
			info.pop_back();
			label_pc[info] = pc + 1;
			ret.operands.push_back(info);
		}
		return ret;
	}
	else
	{
		operation ret;
		int pos = 0;
		bool string_mode = false;
		while (pos < info.size())
		{
			if (info[pos] == ' ' && !string_mode)
				break;
			if (info[pos] == '\"')
			{
				string_mode ^= 1;
				ret.opcode = ret.opcode + "quote_";
			}
			else if(info[pos] == '-')
			{
				ret.opcode = ret.opcode + "sub_";
			}
			else if (info[pos] == ' ')
			{
				ret.opcode = ret.opcode + "space_";
			}
			else if (info[pos] == '\\')
			{
				ret.opcode = ret.opcode + "backslash_";
			}
			else if (info[pos] == ':')
			{
				ret.opcode = ret.opcode + "colon_";
			}
			else if (info[pos] == '!')
			{
				ret.opcode = ret.opcode + "exg_";
			}
			else if (info[pos] == '>')
			{
				ret.opcode = ret.opcode + "dayu_";
			}
			else if (info[pos] == '+')
			{
				ret.opcode = ret.opcode + "plus_";
			}
			else if (info[pos] == '=')
			{
				ret.opcode = ret.opcode + "equ_";
			}
			else if (info[pos] == '.')
			{
				ret.opcode = ret.opcode + "dot_";
			}
			else if (info[pos] == ',')
			{
				ret.opcode = ret.opcode + "comma_";
			}
			else if (info[pos] == '\'')
			{
				ret.opcode = ret.opcode + "hyphen_";
			}
			else if (info[pos] == '(')
			{
				ret.opcode = ret.opcode + "lbracket_";
			}
			else if (info[pos] == ')')
			{
				ret.opcode = ret.opcode + "rbracket_";
			}
			else
				ret.opcode.push_back(info[pos]);
			pos++;
		}
		while (pos < info.size())
		{
			string_mode = false;
			string tmp = "";
			pos++;
			while (pos < info.size())
			{
				if (info[pos] == ' ' && !string_mode)
					break;
				if (info[pos] == '\"')
				{
					string_mode ^= 1;
					tmp = tmp + "quote_";
					pos++;
				}
				else if (info[pos] == '-')
				{
					tmp = tmp + "sub_";
					pos++;
				}
				else if (info[pos] == ' ')
				{
					tmp = tmp + "space_";
					pos++;
				}
				else if (info[pos] == '\\')
				{
					tmp = tmp + "backslash_";
					pos++;
				}
				else if (info[pos] == ':')
				{
					tmp = tmp + "colon_";
					pos++;
				}
				else if (info[pos] == '!')
				{
					tmp = tmp + "exg_";
					pos++;
				}
				else if (info[pos] == '>')
				{
					tmp = tmp + "dayu_";
					pos++;
				}
				else if (info[pos] == '+')
				{
					tmp = tmp + "plus_";
					pos++;
				}
				else if (info[pos] == '=')
				{
					tmp = tmp + "equ_";
					pos++;
				}
				else if (info[pos] == '.')
				{
					tmp = tmp + "dot_";
					pos++;
				}
				else if (info[pos] == ',')
				{
					tmp = tmp + "comma_";
					pos++;
				}
				else if (info[pos] == '\'')
				{
					tmp = tmp + "hyphen_";
					pos++;
				}
				else if (info[pos] == '(')
				{
					tmp = tmp + "lbracket_";
					pos++;
				}
				else if (info[pos] == ')')
				{
					tmp = tmp + "rbracket_";
					pos++;
				}
				else
					tmp.push_back(info[pos++]);
			}
			ret.operands.push_back(tmp);
			if (ret.opcode == "call" && (ret.operands[0] == "@functionLabel_print_constant" || ret.operands[0] == "@functionLabel_println_constant"))
			{
				pos += 2;
				string temp;
				while (pos < info.size())
				{
					temp.push_back(info[pos]);
					pos++;
				}
				temp.pop_back();
				ret.operands.push_back(temp);
				return ret;
			}
		}
		return ret;
	}
}

int string_to_int(string x)
{
	int delta = 0, mul = 1, ret = 0;
	if (x[0] == '-')
		mul = -1, delta = 1;
	for (int i = delta; i < x.size(); ++i)
	{
		ret = ret * 10 + x[i] - '0';
	}
	return ret * mul;
}

class frame
{
public:
	vector<pair<int, int> > caller_saved_regs, callee_saved_regs;
	vector<int> local_vars;
	vector<int> parameters;
	int *ra;
	int callee_return_address;
	int return_pc;
	frame() {}
};

vector<operation> lines;
vector<string> raw;

stack<frame> func_stack;
vector<unsigned char> heap;
vector<pair<int, bool> > regs;


int return_reg_id;
int static_base_reg;

map<string, int> dis;

class call_graph
{
public:
	vector<vector<int> > nxt;
	int main_func;
	void dfs(int x, set<int> &res)
	{
		if (res.find(x) != res.end())
			return;
		res.insert(x);
		for (int i = 0; i < nxt[x].size(); ++i)
			dfs(nxt[x][i], res);
	}
	void init(int n)
	{
		nxt.clear();
		nxt.resize(n + 10);
	}
}G;

void read_code(string file_path)
{
	ifstream fin(file_path);
	string tmp;
	int pc = 0, func_count = 0;
	while (getline(fin, tmp))
	{
		if (tmp == "")
			continue;
		operation cur_op = get_context(tmp, pc++);

		{
			if (cur_op.opcode == "label")
			{
				cur_op.real_operands.resize(cur_op.operands.size());
				if (tmp.substr(1, 8) == "function")
				{
					for (int i = 1; i < cur_op.operands.size(); ++i)
						dis[cur_op.operands[i]] = 0;
					func_count++;
				}
				label_pc[cur_op.operands[0]] = pc; //point to next
			}
			else if (cur_op.opcode == "mallocStatic")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = (string_to_int(cur_op.operands[0]));
			}
			else if (cur_op.opcode == "mallocI")
			{
				cur_op.real_operands.resize(2);
				cur_op.real_operands[0] = string_to_int(cur_op.operands[0]);
				dis[cur_op.operands[1]] = 0;
			}
			else if (cur_op.opcode == "malloc")
			{
				cur_op.real_operands.resize(2);
				dis[cur_op.operands[0]] = 0;
				dis[cur_op.operands[1]] = 0;
			}
			else if (cur_op.opcode == "call")
			{
				cur_op.real_operands.resize(cur_op.operands.size() - 1);
				for (int i = 1; i < cur_op.operands.size(); ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "return")
			{
				cur_op.real_operands.resize(1);
				dis[cur_op.operands[0]] = 0;
			}
			else if (cur_op.opcode == "nop")
			{
			}
			else if (cur_op.opcode == "add")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "sub")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "mult")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "div")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "addI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "subI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "rsubI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "multI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "divI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "rdivI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "lshift")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "rshift")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "lshiftI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "rshiftI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "and")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "or")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "xor")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "andI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "orI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "xorI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "loadI")
			{
				cur_op.real_operands.resize(2);
				dis[cur_op.operands[1]] = 0;
				cur_op.real_operands[0] = string_to_int(cur_op.operands[0]);
			}
			else if (cur_op.opcode == "loadAI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "loadA0")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "store")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "storeAI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "storeA0")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "i2i")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "jump")
			{
				cur_op.real_operands.resize(1);
				dis[cur_op.operands[0]] = 0;
			}
			else if (cur_op.opcode == "jumpI")
			{
				cur_op.real_operands.resize(1);
			}
			else if (cur_op.opcode == "cbr")
			{
				cur_op.real_operands.resize(1);
				dis[cur_op.operands[0]] = 0;
			}
			else if (cur_op.opcode == "cmp_LT")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_LE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_EQ")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_GE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_GT")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_NE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "load")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cbr_lt")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cbr_le")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cbr_gt")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cbr_ge")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cbr_eq")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cbr_ne")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "assign")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 1; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "virtualassign")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 1; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "unassign")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 1; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "stackunassign")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 1; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else
				assert(false);
		}
		lines.push_back(cur_op);
	}
	fin.close();
	int curId = 0;
	regs.resize(dis.size() + 10);
	for (map<string, int>::iterator i = dis.begin(); i != dis.end(); ++i)
	{
		i->second = curId++;
		if (i->first == "r_{global_return_register}")
			return_reg_id = curId - 1;
		if (i->first == "r_{global_base_address_register^_^}")
		{
			static_base_reg = curId - 1;
			regs[curId - 1].first = 0;
		}
		if (((i->first).size() >= 9 && (i->first).substr(3, 6) == "global") || (i->first.size() >= 6 && i->first.substr(i->first.size() - 6, 6) == "*temp}"))
		{
			regs[curId - 1].second = true;
		}
	}
	int this_function = -1;
	G.init(lines.size() + 10);
	for (int i = 0; i < lines.size(); ++i)
	{
		operation &cur_op = lines[i];

		{
			if (lines[i].opcode == "label")
			{
				if (lines[i].operands[0].size() >= 9 && lines[i].operands[0].substr(1, 8) == "function")
				{
					for (int i = 1; i < cur_op.operands.size(); ++i)
						cur_op.real_operands[i] = dis[cur_op.operands[i]];
					this_function = i + 1;
				}
			}
			else if (lines[i].opcode == "mallocStatic")
			{
			}
			else if (lines[i].opcode == "mallocI")
			{
				cur_op.real_operands.resize(2);
				cur_op.real_operands[1] = dis[cur_op.operands[1]];
			}
			else if (lines[i].opcode == "malloc")
			{
				cur_op.real_operands.resize(2);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[1] = dis[cur_op.operands[1]];
			}
			else if (lines[i].opcode == "call")
			{
				cur_op.real_operands.resize(cur_op.operands.size() - 1);
				for (int i = 1; i < cur_op.operands.size(); ++i)
					cur_op.real_operands[i - 1] = dis[cur_op.operands[i]];
				G.nxt[this_function].push_back(label_pc[cur_op.operands[0]]);
			}
			else if (lines[i].opcode == "return")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "nop")
			{
			}
			else if (lines[i].opcode == "add")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "sub")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "mult")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "div")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "addI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "subI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "rsubI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "multI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "divI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "rdivI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "lshift")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "rshift")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "lshiftI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "rshiftI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "and")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "or")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "xor")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "andI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "orI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "xorI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "loadI")
			{
				cur_op.real_operands.resize(2);
				cur_op.real_operands[1] = dis[cur_op.operands[1]];
				cur_op.real_operands[0] = string_to_int(cur_op.operands[0]);
			}
			else if (lines[i].opcode == "loadAI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "loadA0")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "store")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "storeAI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "storeA0")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "i2i")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "jump")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "jumpI")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = label_pc[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "cbr")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "cmp_LT")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_LE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_EQ")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_GE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_GT")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_NE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "load")
			{
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (cur_op.opcode == "cbr_lt")
			{
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (cur_op.opcode == "cbr_le")
			{
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (cur_op.opcode == "cbr_gt")
			{
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (cur_op.opcode == "cbr_ge")
			{
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (cur_op.opcode == "cbr_eq")
			{
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (cur_op.opcode == "cbr_ne")
			{
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (cur_op.opcode == "assign")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 1; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "virtualassign")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 1; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "unassign")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 1; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "stackunassign")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 1; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else
				assert(false);
		}
	}
}

int pc = 0;

void init_frame(frame &new_frame, int nextPc, int curPc)
{
//	new_frame.return_pc = pc + 1;
//	new_frame.ra = &func_stack.top().callee_return_address;
	set<int> local_vars;
	for (int i = nextPc; i < lines.size() && i <= curPc && (lines[i].opcode != "label" || (lines[i].opcode == "label" && lines[i].operands[0].substr(0, 9) != "@function")); i++)
	{
		operation &cur_op = lines[i];
		{
			if (lines[i].opcode == "label")
			{
				for (int j = 1; j < lines[i].real_operands.size(); ++j)
				{
					local_vars.insert(lines[i].real_operands[j]);
				}
			}
			else if (lines[i].opcode == "mallocStatic")
			{
			}
			else if (lines[i].opcode == "mallocI")
			{
				local_vars.insert(lines[i].real_operands[1]);
			}
			else if (lines[i].opcode == "malloc")
			{
				local_vars.insert(lines[i].real_operands[0]);
				local_vars.insert(lines[i].real_operands[1]);
			}
			else if (lines[i].opcode == "call")
			{
				for (int i = 1; i < cur_op.operands.size(); ++i)
					local_vars.insert(cur_op.real_operands[i - 1]);
			}
			else if (lines[i].opcode == "return")
			{
				local_vars.insert(cur_op.real_operands[0]);
			}
			else if (lines[i].opcode == "nop")
			{
			}
			else if (lines[i].opcode == "add")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "sub")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "mult")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "div")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "addI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "subI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "rsubI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "multI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "divI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "rdivI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "lshift")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "rshift")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "lshiftI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "rshiftI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "and")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "or")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "xor")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "andI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "orI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "xorI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "loadI")
			{
				local_vars.insert(cur_op.real_operands[1]);
			}
			else if (lines[i].opcode == "loadAI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "loadA0")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "store")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "storeAI")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "storeA0")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "i2i")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "jump")
			{
				cur_op.real_operands.resize(1);
				local_vars.insert(cur_op.real_operands[0]);
			}
			else if (lines[i].opcode == "jumpI")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = label_pc[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "cbr")
			{
				local_vars.insert(cur_op.real_operands[0]);
			}
			else if (lines[i].opcode == "cmp_LT")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_LE")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_EQ")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_GE")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_GT")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_NE")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "load")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "cbr_lt")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "cbr_le")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "cbr_gt")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "cbr_ge")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "cbr_eq")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "cbr_ne")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "assign")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "virtualassign")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "unassign")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (cur_op.opcode == "stackunassign")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else
				assert(false);
		}
	}
	for (set<int>::iterator i = local_vars.begin(); i != local_vars.end(); i++)
	{
		if (!regs[*i].second)
			new_frame.local_vars.push_back(*i);
	}
	func_stack.push(new_frame);
	pc = nextPc;
}

string dot_text, dot_data;
string dot_data_tail;

int physical_reg[40];

int assigned(int vr)
{
	for (int i = 0; i < 40; ++i)
		if (physical_reg[i] == vr)
			return i;
	return -1;
}

class stackUnassignInfo
{
public:
	int offset;
	int vrreg, prreg;
};

stack<vector<stackUnassignInfo> > st;

void process_instruction(operation instr, int &this_function, int lineid)
{
	operation &cur_op = instr;
	{
		if (instr.opcode == "label")
		{
			if (instr.operands[0].substr(0, 9) == "@function")
			{
				if (instr.operands[0] == "@functionLabel_main")
				{
					dot_text = dot_text + "main:\nmove $fp, $sp\n";
					dot_text = dot_text + "li $7, 4\n";
					dot_text = dot_text + "li $6, 1\n";
					dot_text = dot_text + "li $t0, 32\n";
					dot_text = dot_text + "li $t1, 1\n";
					dot_text = dot_text + "la $a0, __CharSpace\n";
					dot_text = dot_text + "sw $t1, ($a0)\n";
					dot_text = dot_text + "sw $t0, 4($a0)\n";
					stringstream enter;
					enter << (int)('\n');
					dot_text = dot_text + "li $t0, " + enter.str() + "\n";
					dot_text = dot_text + "li $t1, 1\n";
					dot_text = dot_text + "la $a0, __CharEnter\n";
					dot_text = dot_text + "sw $t1, ($a0)\n";
					dot_text = dot_text + "sw $t0, 4($a0)\n";
				}
				else
					dot_text = dot_text + "At" + instr.operands[0].substr(1, instr.operands[0].size() - 1) + ":\n";
				dot_text = dot_text + "sw $31, ($sp)\n";
				dot_text = dot_text + "sub $sp, $sp, 4\n";
				for (int i = 1; i < instr.real_operands.size(); ++i)
				{
					stringstream ss; ss << -4 * (i);
					stringstream reg; reg << instr.real_operands[i];
					dot_text = dot_text + "lw $t0, " + ss.str() + "($fp)\n";
					dot_text = dot_text + "sw $t0, reg_" + reg.str() + "\n";
				}
				this_function = lineid + 1;
			}
			else
			{
				dot_text = dot_text + "At" + instr.operands[0].substr(1, instr.operands[0].size() - 1) + ":\n";
			}
		}
		else if (instr.opcode == "mallocStatic")
		{
			//regs[static_base_reg].first = heap.size();
			//heap.resize(heap.size() + instr.real_operands[0]);
			//pc++;

			stringstream ss, base;
			ss << instr.real_operands[0];
			dot_data = dot_data + "static:\n\t.space " + ss.str() + "\n";
			dot_text = dot_text + "la $t0, static\n";
			base << dis["r_{global_base_address_register^_^}"];
			dot_text = dot_text + "sw $t0, " + "reg_" + base.str() + "\n";
		}
		else if (instr.opcode == "mallocI")
		{
			//regs[instr.real_operands[1]].first = heap.size();
			//heap.resize(heap.size() + instr.real_operands[0]);
			//pc++;
			stringstream ss, r1;
			ss << instr.real_operands[0];
			dot_text = dot_text + "li $v0, 9\n";
			dot_text = dot_text + "li $a0, " + ss.str() + "\n";
			dot_text = dot_text + "syscall\n";
			r1 << instr.real_operands[1];
			if (assigned(instr.real_operands[1]) != -1)
			{
				stringstream reg_num;
				reg_num << assigned(instr.real_operands[1]);
				dot_text = dot_text + "move $" + reg_num.str() + ", $v0\n";
			}
			else
				dot_text = dot_text + "sw $v0, " + "reg_" + r1.str() + "\n";
		}
		else if (instr.opcode == "malloc")
		{
			//regs[instr.real_operands[1]].first = heap.size();
			//heap.resize(heap.size() + regs[instr.real_operands[0]].first);
			//pc++;
			stringstream ss, r1;
			dot_text = dot_text + "li $v0, 9\n";
			ss << instr.real_operands[0];
			if (assigned(instr.real_operands[0]) != -1)
			{
				stringstream reg_num;
				reg_num << assigned(instr.real_operands[0]);
				dot_text = dot_text + "move $a0, $" + reg_num.str() + "\n";
			}
			else
				dot_text = dot_text + "lw $a0, " + "reg_" + ss.str() + "\n";
			dot_text = dot_text + "syscall\n";
			r1 << instr.real_operands[1];
			if (assigned(instr.real_operands[1]) != -1)
			{
				stringstream reg_num;
				reg_num << assigned(instr.real_operands[1]);
				dot_text = dot_text + "move $" + reg_num.str() + ", $v0\n";
			}
			else
				dot_text = dot_text + "sw $v0, " + "reg_" + r1.str() + "\n";
		}
		else if (instr.opcode == "call")
		{
			if (instr.operands[0] == "@functionLabel_print")
			{
				/*	int size = 0;
					int pointer = regs[instr.real_operands[0]].first;
					size = ((int)heap[pointer]) + ((int)heap[pointer + 1] << 8) + ((int)heap[pointer + 2] << 16) + ((int)heap[pointer + 3] << 24);
					for (int j = 0; j < size; ++j)
						printf("%c", heap[pointer + 4 + j]);
					pc++;*/
				stringstream po; po << instr.real_operands[0];
				dot_text = dot_text + "li $v0, 4\n";
				if (assigned(instr.real_operands[0]) != -1)
				{
					stringstream reg_num;
					reg_num << instr.real_operands[0];
					dot_text = dot_text + "move $a0, $" + reg_num.str() + "\n";
				}
				else
					dot_text = dot_text + "lw $a0, reg_" + po.str() + "\n";
				dot_text = dot_text + "add $a0, $a0, 4\n";
				dot_text = dot_text + "syscall\n";
			}
			else if (instr.operands[0] == "@functionLabel_println")
			{
				/*	int size = 0;
					int pointer = regs[instr.real_operands[0]].first;
					size = ((int)heap[pointer]) + ((int)heap[pointer + 1] << 8) + ((int)heap[pointer + 2] << 16) + ((int)heap[pointer + 3] << 24);
					for (int j = 0; j < size; ++j)
						printf("%c", heap[pointer + 4 + j]);
					printf("\n");
					pc++;*/
				stringstream po; po << instr.real_operands[0];
				dot_text = dot_text + "li $v0, 4\n";
				if (assigned(instr.real_operands[0]) != -1)
				{
					stringstream reg_num;
					reg_num << instr.real_operands[0];
					dot_text = dot_text + "move $a0, $" + reg_num.str() + "\n";
				}
				else
					dot_text = dot_text + "lw $a0, reg_" + po.str() + "\n";
				dot_text = dot_text + "add $a0, $a0, 4\n";
				dot_text = dot_text + "syscall\n";
				dot_text = dot_text + "la $a0, __CharEnter\n";
				dot_text = dot_text + "add $a0, $a0, 4\n";
				dot_text = dot_text + "syscall\n";
			}
			
			else if (instr.operands[0] == "@functionLabel_getInt")
			{
				/*	int tmp;
					cin >> tmp;
					int return_ptr = tmp;
					regs[return_reg_id].first = return_ptr;
					func_stack.top().callee_return_address = return_ptr;
					pc++;*/

				dot_text = dot_text + "li $v0, 5\n";
				dot_text = dot_text + "syscall\n";
				stringstream ss; ss << return_reg_id;
				if (assigned(return_reg_id) != -1)
				{
					stringstream reg_num;
					reg_num << assigned(return_reg_id);
					dot_text = dot_text + "move $v0, $" + reg_num.str() + "\n";
				}
				else
					dot_text = dot_text + "sw $v0, reg_" + ss.str() + "\n";
			}
			else if (instr.operands[0] == "@functionLabel___builtin__string_add")
			{
				/*	int size = 0;
					int str1_ptr = regs[instr.real_operands[0]].first, str2_ptr = regs[instr.real_operands[1]].first;
					int len1 = ((int)heap[str1_ptr] << 0) + ((int)heap[str1_ptr + 1] << 8) + ((int)heap[str1_ptr + 2] << 16) + ((int)heap[str1_ptr + 3] << 24);
					int len2 = ((int)heap[str2_ptr] << 0) + ((int)heap[str2_ptr + 1] << 8) + ((int)heap[str2_ptr + 2] << 16) + ((int)heap[str2_ptr + 3] << 24);
					size = len1 + len2;
					int return_ptr = heap.size();
					if (size % 4 == 0)
						heap.resize(heap.size() + size + 4);
					else
						heap.resize(heap.size() + size / 4 * 4 + 8);
					heap[return_ptr + 0] = (size >> 0) & 255;
					heap[return_ptr + 1] = (size >> 8) & 255;
					heap[return_ptr + 2] = (size >> 16) & 255;
					heap[return_ptr + 3] = (size >> 24) & 255;
					for (int i = 0; i < len1; ++i)
						heap[return_ptr + 4 + i] = heap[str1_ptr + 4 + i];
					for (int i = 0; i < len2; ++i)
						heap[return_ptr + 4 + len1 + i] = heap[str2_ptr + 4 + i];
					regs[return_reg_id].first = return_ptr;
					func_stack.top().callee_return_address = return_ptr;
					pc++;*/

				stringstream pa1, pa2;
				pa1 << instr.real_operands[0];
				pa2 << instr.real_operands[1];
				//len calc
				dot_text = dot_text + "lw $t0, reg_" + pa1.str() + "\n";
				dot_text = dot_text + "lw $t1, reg_" + pa2.str() + "\n";
				dot_text = dot_text + "lw $t0, ($t0)\n";
				dot_text = dot_text + "lw $t1, ($t1)\n";
				dot_text = dot_text + "add $t0, $t0, $t1\n";

				//allocate
				dot_text = dot_text + "li $v0, 9\n";
				dot_text = dot_text + "add $a0, $t0, 16\n";
				dot_text = dot_text + "div $a0, $a0, 4\n";
				dot_text = dot_text + "mul $a0, $a0, 4\n";
				dot_text = dot_text + "syscall\n";

				//save result
				stringstream ret;
				int reg_id = dis["r_{global_return_register}"];
				if (assigned(reg_id) != -1)
				{
					ret << assigned(reg_id);
					dot_text = dot_text + "move $" + ret.str() + ", $v0\n";
				}
				else
				{
					ret << reg_id;
					dot_text = dot_text + "sw $v0, reg_" + ret.str() + "\n";
				}

				//save len
				dot_text = dot_text + "sw $t0, ($v0)\n";

				//pointer init 1
				dot_text = dot_text + "lw $t0, reg_" + pa1.str() + "\n";
				dot_text = dot_text + "lw $t1, ($t0)\n";
				dot_text = dot_text + "add $t0, $t0, 4\n";
				dot_text = dot_text + "move $a0, $v0\n";
				dot_text = dot_text + "add $a0, $a0, 4\n";

				//main loop 1
				string labelWhileStart1, labelWhileExit1;
				int lws1 = label_pc.size(), lwe1 = label_pc.size() + 1;
				stringstream Lws1, Lwe1;
				Lws1 << lws1; Lwe1 << lwe1;
				labelWhileStart1 = "AtLabelWhileStart" + Lws1.str();
				labelWhileExit1 = "AtLabelWhileExit" + Lwe1.str();
				label_pc[labelWhileExit1] = label_pc[labelWhileStart1] = -1;

				dot_text = dot_text + labelWhileStart1 + ":\n";
				dot_text = dot_text + "beq $t1, $0, " + labelWhileExit1 + "\n";
				dot_text = dot_text + "lb $t2, ($t0)\n";
				dot_text = dot_text + "sb $t2, ($a0)\n";
				dot_text = dot_text + "add $t0, $t0, 1\n";
				dot_text = dot_text + "add $a0, $a0, 1\n";
				dot_text = dot_text + "sub $t1, $t1, 1\n";
				dot_text = dot_text + "j " + labelWhileStart1 + "\n";
				dot_text = dot_text + labelWhileExit1 + ":\n";

				//main loop 2
				//init
				dot_text = dot_text + "lw $t0, reg_" + pa2.str() + "\n";
				dot_text = dot_text + "lw $t1, ($t0)\n";
				dot_text = dot_text + "add $t0, $t0, 4\n";


				string labelWhileStart2, labelWhileExit2;
				int lws2 = label_pc.size(), lwe2 = label_pc.size() + 1;
				stringstream Lws2, Lwe2;
				Lws2 << lws2; Lwe2 << lwe2;
				labelWhileStart2 = "AtLabelWhileStart" + Lws2.str();
				labelWhileExit2 = "AtLabelWhileExit" + Lwe2.str();
				label_pc[labelWhileExit2] = label_pc[labelWhileStart2] = -1;

				dot_text = dot_text + labelWhileStart2 + ":\n";
				dot_text = dot_text + "beq $t1, $0, " + labelWhileExit2 + "\n";
				dot_text = dot_text + "lb $t2, ($t0)\n";
				dot_text = dot_text + "sb $t2, ($a0)\n";
				dot_text = dot_text + "add $t0, $t0, 1\n";
				dot_text = dot_text + "add $a0, $a0, 1\n";
				dot_text = dot_text + "sub $t1, $t1, 1\n";
				dot_text = dot_text + "j " + labelWhileStart2 + "\n";
				dot_text = dot_text + labelWhileExit2 + ":\n";
			}
			else if (instr.operands[0] == "@functionLabel_toString")
			{
				dot_text = dot_text + "\n";
				string labelWhileStart, labelWhileExit, labelWhileBody, labelIfExit, labelToStringExit;
				string labelEquZero;
				int stLabel = label_pc.size(), exLabel = stLabel + 1, bdLabel = stLabel + 2, ieLabel = stLabel + 3, feLabel = stLabel + 4;
				int ezLabel = stLabel + 5;
				stringstream st, ex, bd, ie, fe, ez;
				st << stLabel; ex << exLabel; bd << bdLabel; fe << feLabel; ez << ezLabel;
				ie << ieLabel;
				labelWhileStart = "AtLabelWhileStart" + st.str();
				labelWhileExit = "AtLabelWhileExit" + ex.str();
				labelWhileBody = "AtLabelWhileBody" + bd.str();
				labelIfExit = "AtLabelIfExit" + ie.str();
				labelToStringExit = "AtLabelToStringExit" + fe.str();
				labelEquZero = "AtLabelEquZero" + ez.str();
				label_pc[labelIfExit] = label_pc[labelToStringExit] = label_pc[labelWhileBody] = label_pc[labelWhileExit] = label_pc[labelWhileStart] = -1;
				label_pc[labelEquZero] = -1;


				dot_text = dot_text + "li $v0, 9\n";
				stringstream po; po << instr.real_operands[0];
				if (assigned(instr.real_operands[0]) != -1)
				{
					stringstream reg_num;
					reg_num << assigned(instr.real_operands[0]);
					dot_text = dot_text + "move $t0, $" + reg_num.str() + "\n";
				}
				else
					dot_text = dot_text + "lw $t0, reg_" + po.str() + "\n";
				dot_text = dot_text + "li $a0, 4\n";

				//equal zero?
				dot_text = dot_text + "bne $t0, 0, " + labelEquZero + "\n";
				dot_text = dot_text + "add $a0, 4\n";
				dot_text = dot_text + "syscall\n";
				dot_text = dot_text + "li $t1, 1\n";
				dot_text = dot_text + "sw $t1, ($v0)\n";
				dot_text = dot_text + "li $t1, 48\n"; //zero
				dot_text = dot_text + "sw $t1, 4($v0)\n";
				dot_text = dot_text + "j " + labelToStringExit + "\n";

				dot_text = dot_text + labelEquZero + ":\n";
				dot_text = dot_text + "move $t2, $0\n";

				dot_text = dot_text + labelWhileStart + ":\n";
				dot_text = dot_text + "bne $t0, 0, " + labelWhileBody + "\n";
				dot_text = dot_text + "j " + labelWhileExit + "\n";
				dot_text = dot_text + labelWhileBody + ":\n";
				dot_text = dot_text + "add $a0, $a0, 1\n";
				dot_text = dot_text + "div $t0, $t0, 10\n";
				dot_text = dot_text + "add $t2, $t2, 1\n";
				dot_text = dot_text + "j " + labelWhileStart + "\n";
				dot_text = dot_text + labelWhileExit + ":\n";

				dot_text = dot_text + "add $a0, $a0, 2\n";
				dot_text = dot_text + "rem $t1, $a0, 4\n";
				dot_text = dot_text + "add $t1, $t1, -4\n";
				dot_text = dot_text + "neg $t1, $t1\n";
				dot_text = dot_text + "add $a0, $a0, $t1\n";
				dot_text = dot_text + "syscall\n";

				//below zero?
				dot_text = dot_text + "lw $t0, reg_" + po.str() + "\n";
				dot_text = dot_text + "bge $t0, 0, " + labelIfExit + "\n";
				dot_text = dot_text + "add $t2, $t2, 1\n";
				dot_text = dot_text + "sw $t2, ($v0)\n";
				dot_text = dot_text + "li $t1, 45\n";
				dot_text = dot_text + "sb $t1, 4($v0)\n";
				dot_text = dot_text + "neg $t0, $t0\n";
				dot_text = dot_text + labelIfExit + ":\n";

				dot_text = dot_text + "sw $t2, ($v0)\n";

				//main while
				stringstream ws, wb, we;
				int labelws = label_pc.size();
				int labelwb = labelws + 1;
				int labelwe = labelws + 2;

				ws << labelws; wb << labelwb; we << labelwe;

				string lws, lwe;
				lws = "AtLabelWhileStart" + ws.str();
				lwe = "AtLabelWhileEnd" + we.str();
				label_pc[lws] = label_pc[lwe] = -1;

				dot_text = dot_text + "AtLabelWhileStart" + ws.str() + ":\n";
				dot_text = dot_text + "beq $t0, 0, AtLabelWhileEnd" + we.str() + "\n";
				dot_text = dot_text + "rem $t1, $t0, 10\n";
				dot_text = dot_text + "add $t1, $t1, 48\n";
				dot_text = dot_text + "add $t2, $t2, -1\n";
				dot_text = dot_text + "add $a0, $t2, $v0\n";
				dot_text = dot_text + "sb $t1, 4($a0)\n";
				dot_text = dot_text + "div $t0, $t0, 10\n";
				dot_text = dot_text + "j AtLabelWhileStart" + ws.str() + "\n";
				dot_text = dot_text + "AtLabelWhileEnd" + we.str() + ":\nnop\n";

				dot_text = dot_text + labelToStringExit + ":\n";
				stringstream ret; ret << dis["r_{global_return_register}"];
				dot_text = dot_text + "sw $v0, reg_" + ret.str() + "\n";
				dot_text = dot_text + "\n";
			}
			else if (instr.operands[0] == "@functionLabel___builtin_printInt")
			{
				dot_text = dot_text + "li $v0, 1\n";
				stringstream ss; ss << instr.real_operands[0];
				if (assigned(instr.real_operands[0]) != -1)
				{
					stringstream reg_num;
					reg_num << assigned(instr.real_operands[0]);
					dot_text = dot_text + "move $a0, $" + reg_num.str() + "\n";
				}
				else
					dot_text = dot_text + "lw $a0, reg_" + ss.str() + "\n";
				dot_text = dot_text + "syscall\n";
			}
			else if (instr.operands[0] == "@functionLabel___builtin_printIntln")
			{
				dot_text = dot_text + "li $v0, 1\n";
				stringstream ss; ss << instr.real_operands[0]; 
				if (assigned(instr.real_operands[0]) != -1)
				{
					stringstream reg_num;
					reg_num << assigned(instr.real_operands[0]);
					dot_text = dot_text + "move $a0, $" + reg_num.str() + "\n";
				}
				else
					dot_text = dot_text + "lw $a0, reg_" + ss.str() + "\n";
				dot_text = dot_text + "syscall\n";
				dot_text = dot_text + "li $v0, 4\n";
				dot_text = dot_text + "la $a0, __CharEnter\n";
				dot_text = dot_text + "add $a0, $a0, 4\n";
				dot_text = dot_text + "syscall\n";
			}
			else if (instr.operands[0] == "@functionLabel___builtin_string_generate_quote_space_quote_")
			{
				stringstream ss; ss << dis["r_{global_return_register}"];
				dot_text = dot_text + "la $t0, __CharSpace\n";
				if (assigned(dis["r_{global_return_register}"]) != -1)
				{
					stringstream reg_num;
					reg_num << assigned(dis["r_{global_return_register}"]);
					dot_text = dot_text + "move $" + reg_num.str() + ", $t0\n";
				}
				else
					dot_text = dot_text + "sw $t0, reg_" + ss.str() + "\n";
			}
			else if (instr.operands[0] == "@functionLabel_getString")
			{
				//allocate
				dot_text = dot_text + "li $v0, 9\n";
				dot_text = dot_text + "li $a0, 310\n";
				dot_text = dot_text + "syscall\n";

				//save address
				dot_text = dot_text + "move $t0, $v0\n";

				stringstream ss; ss << dis["r_{global_return_register}"];
				if (assigned(dis["r_{global_return_register}"]) != -1)
				{
					stringstream reg_num;
					reg_num << assigned(dis["r_{global_return_register}"]);
					dot_text = dot_text + "move $" + reg_num.str() + ", $t0\n";
				}
				else
					dot_text = dot_text + "sw $t0, reg_" + ss.str() + "\n";

				//read in
				dot_text = dot_text + "li $v0, 8\n";
				dot_text = dot_text + "move $a0, $t0\n";
				dot_text = dot_text + "add $a0, $a0, 4\n";
				dot_text = dot_text + "li $a1, 300\n";
				dot_text = dot_text + "syscall\n";

				//calc len
				dot_text = dot_text + "move $t1, $0\n";
				dot_text = dot_text + "move $t2, $a0\n";

				stringstream ws, we, wee;
				ws << label_pc.size(); we << label_pc.size() + 1;
				wee << label_pc.size() + 2;
				string whileStartLabel = "AtWhileLabelStart" + ws.str(), whileEndLabel = "AtWhileLabelEnd" + we.str(),
					whileEndEnterLabel = "AtWhileLabelEndEnter" + wee.str();
				label_pc[whileStartLabel] = label_pc[whileEndLabel] = label_pc[whileEndEnterLabel] = -1;

				dot_text = dot_text + whileStartLabel + ":\n";
				dot_text = dot_text + "lb $t2, ($a0)\n";
				dot_text = dot_text + "beq $t2, 10, " + whileEndEnterLabel + "\n";
				dot_text = dot_text + "beq $t2, 0, " + whileEndLabel + "\n";
				dot_text = dot_text + "add $a0, $a0, 1\n";
				dot_text = dot_text + "add $t1, $t1, 1\n";
				dot_text = dot_text + "j " + whileStartLabel + "\n";
				dot_text = dot_text + whileEndEnterLabel + ":\n";
				dot_text = dot_text + "move $t2, $0\n";
				dot_text = dot_text + "sb $t2, ($a0)\n";
				dot_text = dot_text + whileEndLabel + ":\n";
				dot_text = dot_text + "sw $t1, ($t0)\n";
			}
			else if (instr.operands[0] == "@functionLabel___builtin__ord")
			{
				stringstream pa1, pa2;
				pa1 << instr.real_operands[0];
				pa2 << instr.real_operands[1];
				
				//str & pos
				dot_text = dot_text + "lw $a0, reg_" + pa1.str() + "\n";
				dot_text = dot_text + "lw $a1, reg_" + pa2.str() + "\n";
				dot_text = dot_text + "add $a0, $a0, $a1\n";
				dot_text = dot_text + "lb $t0, 4($a0)\n";
				stringstream ret;
				int reg_id = dis["r_{global_return_register}"];
				if (assigned(reg_id) != -1)
				{
					ret << assigned(reg_id);
					dot_text = dot_text + "move $" + ret.str() + ", $t0\n";
				}
				else
				{
					ret << reg_id;
					dot_text = dot_text + "sw $t0, reg_" + ret.str() + "\n";
				}
			}
			else if(instr.operands[0] == "@functionLabel___builtin__substring")
			{
				stringstream pa1, pa2, pa3;
				pa1 << instr.real_operands[0];
				pa2 << instr.real_operands[1];
				pa3 << instr.real_operands[2];
				
				//calc len
				dot_text = dot_text + "lw $t0, reg_" + pa2.str() + "\n";
				dot_text = dot_text + "lw $t1, reg_" + pa3.str() + "\n";
				dot_text = dot_text + "sub $t0, $t1, $t0\n";
				dot_text = dot_text + "add $t0, $t0, 1\n";

				//allocate
				dot_text = dot_text + "li $v0, 9\n";
				dot_text = dot_text + "add $a0, $t0, 12\n";
				dot_text = dot_text + "div $a0, $a0, 4\n";
				dot_text = dot_text + "mul $a0, $a0, 4\n";
				dot_text = dot_text + "syscall\n";

				//save result
				dot_text = dot_text + "move $a1, $v0\n";

				//save len
				dot_text = dot_text + "sw $t0, ($v0)\n";

				//main loop init part1
				dot_text = dot_text + "add $a0, $v0, 4\n";

				//re config offset
				dot_text = dot_text + "lw $t0, reg_" + pa2.str() + "\n";
				dot_text = dot_text + "lw $t1, reg_" + pa3.str() + "\n";
				dot_text = dot_text + "sub $v0, $t1, $t0\n";
				dot_text = dot_text + "add $v0, $v0, 1\n";

				//main loop
				stringstream ws, we;
				ws << label_pc.size(); we << label_pc.size() + 1;
				string whileStartLabel = "AtWhileLabelStart" + ws.str();
				string whileEndLabel = "AtWhileLabelEnd" + we.str();
				label_pc[whileStartLabel] = label_pc[whileEndLabel] = -1;
				//main loop init part2
				dot_text = dot_text + "lw $t2, reg_" + pa1.str() + "\n";
				dot_text = dot_text + "add $t2, $t2, 4\n";
				dot_text = dot_text + "add $t2, $t2, $t0\n";
				
				dot_text = dot_text + whileStartLabel + ":\n";
				dot_text = dot_text + "beq $v0, 0, " + whileEndLabel + "\n";
				dot_text = dot_text + "lb $t0, ($t2)\n";
				dot_text = dot_text + "sb $t0, ($a0)\n";
				dot_text = dot_text + "add $t2, $t2, 1\n";
				dot_text = dot_text + "add $a0, $a0, 1\n";
				dot_text = dot_text + "sub $v0, $v0, 1\n";
				dot_text = dot_text + "j " + whileStartLabel + "\n";
				dot_text = dot_text + whileEndLabel + ":\n";
				int reg_id = dis["r_{global_return_register}"];
				stringstream ret;
				if (assigned(reg_id) != -1)
				{
					ret << assigned(reg_id);
					dot_text = dot_text + "move $" + ret.str() + ", $a1\n";
				}
				else
				{
					ret << reg_id;
					dot_text = dot_text + "sw $a1, reg_" + ret.str() + "\n";
				}
			}
			else if (instr.operands[0] == "@functionLabel___builtin__length")
			{
				stringstream pa1;
				pa1 << instr.real_operands[0];
				dot_text = dot_text + "lw $t0, reg_" + pa1.str() + "\n"; 
				dot_text = dot_text + "lw $t0, ($t0)\n";
				int reg_id = dis["r_{global_return_register}"];
				stringstream ret;
				if (assigned(reg_id) != -1)
				{
					ret << assigned(reg_id);
					dot_text = dot_text + "move $" + ret.str() + ", $t0\n";
				}
				else
				{
					ret << reg_id;
					dot_text = dot_text + "sw $t0, reg_" + ret.str() + "\n";
				}
			}
			else if (instr.operands[0] == "@functionLabel_println_constant")
			{
				int randomLabel = rand() * rand();
				randomLabel = abs(randomLabel);
				stringstream rL;
				rL << randomLabel;
				dot_data_tail = dot_data_tail + "Label" + rL.str() + ": .asciiz \"" + instr.operands[1] + "\\n\"\n";
				dot_text = dot_text + "li $v0, 4\n";
				dot_text = dot_text + "la $a0, Label" + rL.str() + "\n";
				dot_text = dot_text + "syscall\n";
			}
			else if (instr.operands[0] == "@functionLabel_print_constant")
			{
				int randomLabel = rand() * rand();
				randomLabel = abs(randomLabel);
				stringstream rL;
				rL << randomLabel;
				dot_data_tail = dot_data_tail + "Label" + rL.str() + ": .asciiz \"" + instr.operands[1] + "\"\n";
				dot_text = dot_text + "li $v0, 4\n";
				dot_text = dot_text + "la $a0, Label" + rL.str() + "\n";
				dot_text = dot_text + "syscall\n";
			}
			else
			{
				bool need_save_reg = true;
				set<int> childs;
				G.dfs(label_pc[instr.operands[0]], childs);
				if (childs.find(this_function) == childs.end())
					need_save_reg = false;
				frame cur_frame;
				int saved_count = 0;
				if (need_save_reg)
				{
					init_frame(cur_frame, this_function + 1, lineid);
					for (int i = 0; i < cur_frame.local_vars.size(); ++i)
					{
						int reg = cur_frame.local_vars[i];
						if (!regs[reg].second)
						{
							saved_count++;
						}
					}
					int cnt = 0;
					for (int i = 0; i < cur_frame.local_vars.size(); ++i)
					{
						int reg = cur_frame.local_vars[i];
						if (!regs[reg].second)
						{
							stringstream ss, toStr_cnt;
							ss << reg;
							toStr_cnt << -cnt * 4;
							cnt++;
							if (assigned(reg) != -1)
							{
								stringstream reg_num;
								reg_num << assigned(reg);
								dot_text = dot_text + "sw $" + reg_num.str() + ", " + toStr_cnt.str() + "($sp)\n";
							}
							else
							{
								bool instackunassign = false;
								int pr;
								for (vector<stackUnassignInfo>::iterator j = st.top().begin(); j != st.top().end(); j++)
								{
									if ((*j).vrreg == reg)
									{
										(*j).offset = -cnt * 4;
										pr = (*j).prreg;
										instackunassign = true;
										st.top().erase(j);
										break;
									}
								}
								if (!instackunassign)
								{
									dot_text = dot_text + "lw $t0, reg_" + ss.str() + "\n";
									dot_text = dot_text + "sw $t0, " + toStr_cnt.str() + "($sp)\n";
								}
								else
								{
									stringstream ppr;
									ppr << pr;
									dot_text = dot_text + "sw $" + ppr.str() + ", " + toStr_cnt.str() + "($sp)\n";
									physical_reg[pr] = -1;
								}
							}
						}
					}
					stringstream ss; ss << saved_count * 4;
					dot_text = dot_text + "sub $sp, $sp, " + ss.str() + "\n";
				}
				for (int j = 0; j < st.top().size(); ++j)
				{
					stringstream ppr; ppr << st.top()[j].prreg;
					stringstream vvr; vvr << st.top()[j].vrreg;
					dot_text = dot_text + "sw $" + ppr.str() + ", reg_" + vvr.str() + "\n";
					physical_reg[st.top()[j].prreg] = -1;
				}
				st.top().clear();
				dot_text = dot_text + "sw $fp, ($sp)\n";
				dot_text = dot_text + "move $fp, $sp\n";
				int nextPc = label_pc[instr.operands[0]];
				int funcinfo = nextPc - 1;
				int delta = 4 + 4 * (lines[funcinfo].real_operands.size() - 1);

				for (int i = 1; i < lines[funcinfo].real_operands.size(); ++i)
				{
					int reg = instr.real_operands[i - 1];
					stringstream __reg; __reg << reg;
					stringstream delta; delta << -(i)* 4;
					
					if (assigned(reg) != -1)
					{
						stringstream reg_num;
						reg_num << assigned(reg);
						dot_text = dot_text + "sw $" + reg_num.str() + ", " + delta.str() + "($fp)\n";
					}
					else
					{
						dot_text = dot_text + "lw $t0, reg_" + __reg.str() + "\n";
						dot_text = dot_text + "sw $t0, " + delta.str() + "($fp)\n";
					}
				}
				stringstream delta_delta; delta_delta << delta;
				dot_text = dot_text + "sub $sp, $sp, " + delta_delta.str() + "\n";
				dot_text = dot_text + "jal At" + instr.operands[0].substr(1, instr.operands[0].size() - 1) + "\n";
				
				if (need_save_reg)
				{
					stringstream ss; ss << saved_count * 4;
					dot_text = dot_text + "add $sp, $sp, " + ss.str() + "\n";
					int cnt = 0;
					for (int i = 0; i < cur_frame.local_vars.size(); ++i)
					{
						int reg = cur_frame.local_vars[i];
						if (!regs[reg].second)
						{
							stringstream ss, toStr_cnt;
							ss << reg;
							toStr_cnt << -cnt * 4;
							cnt++;
							if (assigned(reg) != -1)
							{
								stringstream reg_num;
								reg_num << assigned(reg);
								dot_text = dot_text + "lw $" + reg_num.str() + ", " + toStr_cnt.str() + "($sp)\n";
							}
							else
							{
								dot_text = dot_text + "lw $t0, " + toStr_cnt.str() + "($sp)\n";
								dot_text = dot_text + "sw $t0, reg_" + ss.str() + "\n";
							}
						}
					}
				}
			}
		}
		else if (instr.opcode == "return")
		{
		/*	if (func_stack.size() == 1)
			{
				func_stack.pop();
				return;
			}
			*func_stack.top().ra = regs[instr.real_operands[0]].first;
			pc = func_stack.top().return_pc;
			func_stack.pop();
			for (int i = 0; i < func_stack.top().caller_saved_regs.size(); ++i)
			{
				pair<int, int> v = func_stack.top().caller_saved_regs[i];
				regs[v.first].first = v.second;
			}*/
			dot_text = dot_text + "lw $31, 4($sp)\n";
			dot_text = dot_text + "move $sp, $fp\n";
			dot_text = dot_text + "lw $fp, ($fp)\n";
			dot_text = dot_text + "jr $31\n";
		}
		else if (instr.opcode == "nop")
		{
			//pc++;
			dot_text = dot_text + "nop\n";
		}
		else if (instr.opcode == "add")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first + regs[instr.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);
			dot_text = dot_text + "add $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";
			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, " + "reg_" + r1.str() + "\n";
			dot_text = dot_text + "add $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "sub")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first - regs[instr.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);
			dot_text = dot_text + "sub $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";
			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, " + "reg_" + r1.str() + "\n";
			dot_text = dot_text + "sub $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "mult")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first * regs[instr.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);
			dot_text = dot_text + "mul $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";
			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, " + "reg_" + r1.str() + "\n";
			dot_text = dot_text + "mul $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";*/
		}
		else if (instr.opcode == "div")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first / regs[instr.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);
			dot_text = dot_text + "div $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, " + "reg_" + r1.str() + "\n";
			dot_text = dot_text + "div $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";*/
		}
		else if (instr.opcode == "addI")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first + instr.real_operands[1];
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r2 << assigned(instr.real_operands[2]);
			r1 << instr.real_operands[1];
			if (r1.str() == "4")
			{
				dot_text = dot_text + "add $" + r2.str() + ", $" + r0.str() + ", $7\n";
			}
			else if (r1.str() == "1")
			{
				dot_text = dot_text + "add $" + r2.str() + ", $" + r0.str() + ", $6\n";
			}
			else
				dot_text = dot_text + "add $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "add $t2, $t0, " + r1.str() + "\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "subI")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first - instr.real_operands[1];
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r2 << assigned(instr.real_operands[2]);
			r1 << instr.real_operands[1];
			if (r1.str() == "4")
			{
				dot_text = dot_text + "sub $" + r2.str() + ", $" + r0.str() + ", $7\n";
			}
			else if (r1.str() == "1")
			{
				dot_text = dot_text + "sub $" + r2.str() + ", $" + r0.str() + ", $6\n";
			}
			else
			dot_text = dot_text + "sub $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "sub $t2, $t0, " + r1.str() + "\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "rsubI")
		{
			//regs[instr.real_operands[2]].first = -regs[instr.real_operands[0]].first + instr.real_operands[1];
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r2 << assigned(instr.real_operands[2]);
			r1 << instr.real_operands[1];
			
			if (r1.str() == "1")
			{
				dot_text = dot_text + "sub $" + r2.str() + ", $6, " + "$" + r0.str() + "\n";
			}
			else if (r1.str() == "4")
			{
				dot_text = dot_text + "sub $" + r2.str() + ", $7, " + "$" + r0.str() + "\n";
			}
			else
			{
				dot_text = dot_text + "sub $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";
				dot_text = dot_text + "neg $" + r2.str() + ", $" + r2.str() + "\n";
			}
			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "neg $t0, $t0\n";
			dot_text = dot_text + "add $t2, $t0, " + r1.str() + "\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "multI")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first * instr.real_operands[1];
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r2 << assigned(instr.real_operands[2]);
			r1 << instr.real_operands[1];
			if (r1.str() == "4")
			{
				dot_text = dot_text + "mul $" + r2.str() + ", $" + r0.str() + ", $7\n";
			}
			else if (r1.str() == "1")
			{
				dot_text = dot_text + "mul $" + r2.str() + ", $" + r0.str() + ", $6\n";
			}
			else
				dot_text = dot_text + "mul $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "mul $t2, $t0, " + r1.str() + "\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "divI")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first / instr.real_operands[1];
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r2 << assigned(instr.real_operands[2]);
			r1 << instr.real_operands[1];
			if (r1.str() == "4")
			{
				dot_text = dot_text + "div $" + r2.str() + ", $" + r0.str() + ", $7\n";
			}
			else if (r1.str() == "1")
			{
				dot_text = dot_text + "div $" + r2.str() + ", $" + r0.str() + ", $6\n";
			}
			else
				dot_text = dot_text + "div $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, " + "reg_" + r0.str() + "\n";
			dot_text = dot_text + "div $t2, $t0, " + r1.str() + "\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "rdivI")
		{
			//regs[instr.real_operands[2]].first = instr.real_operands[0] / regs[instr.real_operands[1]].first;
			//pc++;
			assert(false);
		}
		else if (instr.opcode == "lshift")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first << regs[instr.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);
			dot_text = dot_text + "sll $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "sll $t2, $t0, $t1" + "\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "rshift")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first >> regs[instr.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);
			dot_text = dot_text + "srl $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
            dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
            dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
            dot_text = dot_text + "srl $t2, $t0, $t1" + "\n";
            dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "lshiftI")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first << instr.real_operands[1];
			//pc++;
			stringstream r0, r1, r2;
            r0 << assigned(instr.real_operands[0]);
            r1 << instr.real_operands[1];
            r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "sll $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";


			/*
            dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
            dot_text = dot_text + "sll $t2, $t0, " + r1.str() + "\n";
            dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "rshiftI")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first >> instr.real_operands[1];
			//pc++;

			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << instr.real_operands[1];
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "srl $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";

			/*
            dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
            dot_text = dot_text + "srl $t2, $t0, " + r1.str() + "\n";
            dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "and")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first & regs[instr.real_operands[1]].first;
			//pc++;

			stringstream r0, r1, r2;
            r0 << assigned(instr.real_operands[0]);
            r1 << assigned(instr.real_operands[1]);
            r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "and $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
            dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
            dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
            dot_text = dot_text + "and $t2, $t0, $t1" + "\n";
            dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "or")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first | regs[instr.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "or $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";
			/*
            dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
            dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
            dot_text = dot_text + "or $t2, $t0, $t1" + "\n";
            dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "xor")
		{
			//regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first ^ regs[instr.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "xor $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
            dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
            dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
            dot_text = dot_text + "xor $t2, $t0, $t1" + "\n";
            dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "andI")
		{
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << instr.real_operands[1];
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "and $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";

		}
		else if (instr.opcode == "orI")
		{
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << instr.real_operands[1];
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "or $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";

		}
		else if (instr.opcode == "xorI")
		{
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << instr.real_operands[1];
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "xor $" + r2.str() + ", $" + r0.str() + ", " + r1.str() + "\n";

		}
		else if (instr.opcode == "loadI")
		{
			stringstream r0, r1, r2;
            r0 << instr.real_operands[0];
            r1 << instr.real_operands[1];

			if (assigned(instr.real_operands[1]) != -1)
			{
				stringstream reg_num;
				reg_num << assigned(instr.real_operands[1]);
				if (r0.str() == "0")
					dot_text = dot_text + "move $" + reg_num.str() + ", $" + r0.str() + "\n";
				else if (r0.str() == "1")
				{
					dot_text = dot_text + "move $" + reg_num.str() + ", $6" + "\n";
				}
				else if (r0.str() == "4")
				{
					dot_text = dot_text + "move $" + reg_num.str() + ", $7" + "\n";
				}
				else
					dot_text = dot_text + "li $" + reg_num.str() + ", " + r0.str() + "\n";
			}
			else
			{
				if (r0.str() == "0")
					dot_text = dot_text + "move $t1, $0\n";
				else
					dot_text = dot_text + "li $t1, " + r0.str() + "\n";
				dot_text = dot_text + "sw $t1, reg_" + r1.str() + "\n";
			}
		}
		else if (instr.opcode == "loadAI")
		{

			stringstream r0, r1, r2;
            r0 << assigned(instr.real_operands[0]);
            r1 << instr.real_operands[1];
            r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "lw $" + r2.str() + ", " + r1.str() + "($" + r0.str() +  ")\n";

		}
		else if (instr.opcode == "loadA0")
		{

			stringstream r0, r1, r2;
            r0 << assigned(instr.real_operands[0]);
            r1 << assigned(instr.real_operands[1]);
            r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "add $t1, $" + r0.str() + ", $" + r1.str() + "\n";
			dot_text = dot_text + "lw $" + r2.str() + ", ($t1)\n";

		}
		else if (instr.opcode == "store")
		{
		    stringstream r0, r1, r2;
            r0 << assigned(instr.real_operands[0]);
            r1 << assigned(instr.real_operands[1]);

			dot_text = dot_text + "sw $" + r0.str() + ", ($" + r1.str() + ")\n";

		}
		else if (instr.opcode == "storeAI")
		{
		    stringstream r0, r1, r2;
            r0 << assigned(instr.real_operands[0]);
            r1 << instr.real_operands[1];
            r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "sw $" + r0.str() + ", " + r1.str() + "($" + r2.str() + ")\n";

		}
		else if (instr.opcode == "storeA0")
		{
            stringstream r0, r1, r2;
            r0 << assigned(instr.real_operands[0]);
            r1 << assigned(instr.real_operands[1]);
            r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "add $t0, $" + r1.str() + ", $" + r2.str() + "\n";
			dot_text = dot_text + "sw $" + r0.str() + ", ($t0)\n";

		}
		else if (instr.opcode == "i2i")
		{
			stringstream r0, r1, r2;
            r0 << assigned(instr.real_operands[0]);
            r1 << assigned(instr.real_operands[1]);

			dot_text = dot_text + "move $" + r1.str() + ", $" + r0.str() + "\n";

		}
		else if (instr.opcode == "jump")
		{
			//pc = regs[cur_op.real_operands[0]].first;
			stringstream r0;
			r0 << instr.real_operands[0];
			if (assigned(instr.real_operands[0]) != -1)
			{
				stringstream reg_num;
				reg_num << assigned(instr.real_operands[0]);
				dot_text = dot_text + "jr $" + reg_num.str() + "\n";
			}
			else
			{
				dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
				dot_text = dot_text + "jr $t0";
			}
		}
		else if (instr.opcode == "jumpI")
		{
			stringstream r0;
			r0 << cur_op.real_operands[0];
			dot_text = dot_text + "j At" + instr.operands[0].substr(1, instr.operands[0].size() - 1) + "\n";
		}
		else if (instr.opcode == "cbr")
		{
			stringstream r0;
			r0 << cur_op.real_operands[0];
			if (assigned(cur_op.real_operands[0]) != -1)
			{
				stringstream reg_num;
				reg_num << assigned(cur_op.real_operands[0]);
				dot_text = dot_text + "beq $" + reg_num.str() + ", 1, At" + instr.operands[1].substr(1, instr.operands[1].size() - 1) + "\n";
				dot_text = dot_text + "j At" + instr.operands[2].substr(1, instr.operands[2].size() - 1) + "\n";
			}
			else
			{
				dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
				dot_text = dot_text + "beq $t0, 1, At" + instr.operands[1].substr(1, instr.operands[1].size() - 1) + "\n";
				dot_text = dot_text + "j At" + instr.operands[2].substr(1, instr.operands[2].size() - 1) + "\n";
			}
		}
		else if (instr.opcode == "cmp_LT")
		{
		//	regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first < regs[cur_op.real_operands[1]].first;
		//	pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "slt $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "slt $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "cmp_LE")
		{
			//regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first <= regs[cur_op.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "sle $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "sle $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "cmp_EQ")
		{
			//regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first == regs[cur_op.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "seq $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "seq $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "cmp_GE")
		{
			//regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first >= regs[cur_op.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "sge $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "sge $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "cmp_GT")
		{
			//regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first > regs[cur_op.real_operands[1]].first;
			//pc++;

			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "sgt $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "sgt $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "cmp_NE")
		{
			//regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first != regs[cur_op.real_operands[1]].first;
			//pc++;
			stringstream r0, r1, r2;
			r0 << assigned(instr.real_operands[0]);
			r1 << assigned(instr.real_operands[1]);
			r2 << assigned(instr.real_operands[2]);

			dot_text = dot_text + "sne $" + r2.str() + ", $" + r0.str() + ", $" + r1.str() + "\n";

			/*
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "sne $t2, $t0, $t1\n";
			dot_text = dot_text + "sw $t2, reg_" + r2.str() + "\n";
			*/
		}
		else if (instr.opcode == "load")
		{
			//int ptr = regs[cur_op.real_operands[0]].first;
			//regs[cur_op.real_operands[1]].first =
			//	((int)heap[ptr] << 0) +
			//	((int)heap[ptr + 1] << 8) +
			//	((int)heap[ptr + 2] << 16) +
			//	((int)heap[ptr + 3] << 24);
			//pc++;
			stringstream r0, r1;
			r0 << assigned(cur_op.real_operands[0]);
			r1 << assigned(cur_op.real_operands[1]);

			dot_text = dot_text + "lw $" + r1.str() + ", ($" + r0.str() + ")\n";

			/*
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, ($t0)\n";
			dot_text = dot_text + "sw $t1, reg_" + r1.str() + "\n";
			*/
		}
		else if (cur_op.opcode == "cbr_lt")
		{
			stringstream r0, r1;
			r0 << cur_op.real_operands[0];
			r1 << cur_op.real_operands[1];
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "blt $t0, $t1, At" + instr.operands[2].substr(1, instr.operands[2].size() - 1) + "\n";
		}
		else if (cur_op.opcode == "cbr_le")
		{
			stringstream r0, r1;
			r0 << cur_op.real_operands[0];
			r1 << cur_op.real_operands[1];
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "ble $t0, $t1, At" + instr.operands[2].substr(1, instr.operands[2].size() - 1) + "\n";
		}
		else if (cur_op.opcode == "cbr_gt")
		{
			stringstream r0, r1;
			r0 << cur_op.real_operands[0];
			r1 << cur_op.real_operands[1];
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "bgt $t0, $t1, At" + instr.operands[2].substr(1, instr.operands[2].size() - 1) + "\n";
		}
		else if (cur_op.opcode == "cbr_ge")
		{
			stringstream r0, r1;
			r0 << cur_op.real_operands[0];
			r1 << cur_op.real_operands[1];
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "bge $t0, $t1, At" + instr.operands[2].substr(1, instr.operands[2].size() - 1) + "\n";
		}
		else if (cur_op.opcode == "cbr_eq")
		{
			stringstream r0, r1;
			r0 << cur_op.real_operands[0];
			r1 << cur_op.real_operands[1];
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "beq $t0, $t1, At" + instr.operands[2].substr(1, instr.operands[2].size() - 1) + "\n";
		}
		else if (cur_op.opcode == "cbr_ne")
		{
			stringstream r0, r1;
			r0 << cur_op.real_operands[0];
			r1 << cur_op.real_operands[1];
			dot_text = dot_text + "lw $t0, reg_" + r0.str() + "\n";
			dot_text = dot_text + "lw $t1, reg_" + r1.str() + "\n";
			dot_text = dot_text + "bne $t0, $t1, At" + instr.operands[2].substr(1, instr.operands[2].size() - 1) + "\n";
		}
		else if (cur_op.opcode == "assign")
		{
			int vr, pr;
			vr = cur_op.real_operands[0];
			pr = cur_op.real_operands[1];
			stringstream Pr, Vr; Pr << pr; Vr << vr;
			dot_text = dot_text + "lw $" + Pr.str() + ", reg_" + Vr.str() + "\n";
			for (int j = 0; j < 40; ++j)
				if (physical_reg[j] == vr)
					physical_reg[j] = -1;
			physical_reg[pr] = vr;
		}
		else if (cur_op.opcode == "virtualassign")
		{
			int vr, pr;
			vr = cur_op.real_operands[0];
			pr = cur_op.real_operands[1];
			stringstream Pr, Vr; Pr << pr; Vr << vr;
			for (int j = 0; j < 40; ++j)
				if (physical_reg[j] == vr)
					physical_reg[j] = -1;
			physical_reg[pr] = vr;
		}
		else if (cur_op.opcode == "unassign")
		{
			int vr, pr;
			vr = cur_op.real_operands[0];
			pr = cur_op.real_operands[1];
			stringstream Pr, Vr; Pr << pr; Vr << vr;
			dot_text = dot_text + "sw $" + Pr.str() + ", reg_" + Vr.str() + "\n";
			physical_reg[pr] = -1;
		}
		else if (cur_op.opcode == "stackunassign")
		{
			stackUnassignInfo info;
			info.vrreg = cur_op.real_operands[0];
			info.prreg = cur_op.real_operands[1];
			st.top().push_back(info);
		}
		else
			assert(false);
	}
}


int main()
{
	st.push(vector<stackUnassignInfo>());
	string file_path = "ll_risc.ir";
	read_code(file_path);
	int this_function = -1;
	for (int i = 0; i < 40; ++i)
		physical_reg[i] = -1;
	for (int i = 0; i < lines.size(); ++i)
	{
		process_instruction(lines[i], this_function, i);
	}
	for (int i = 0; i < regs.size(); ++i)
	{
		stringstream ii; ii << i;
		dot_data = dot_data + "reg_" + ii.str() + ": .space 4\n";
	}
	ofstream fout = ofstream("lovelive.s");
	dot_data = dot_data + "__CharEnter: .space 8\n";
	dot_data = dot_data + "__CharSpace: .space 8\n";
	fout << ".text" << endl << dot_text << endl << ".data" << endl << dot_data << endl;
	fout << endl << dot_data_tail << endl;
	fout.close();
	return 0;
}