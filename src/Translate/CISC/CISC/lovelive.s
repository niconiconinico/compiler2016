.text
AtfunctionLabel___builtin_string_generate_quote_space_quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
move $25, $v0
li $24, 1
sw $24, ($25)
move $24, $25
add $24, $24, 4
li $23, 32
sw $23, ($24)
lw $23, reg_306
move $23, $25
sw $23, reg_306
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel___builtin_string_generate_quote_backslash_nquote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
move $25, $v0
li $24, 1
sw $24, ($25)
move $24, $25
add $24, $24, 4
li $23, 10
sw $23, ($24)
lw $23, reg_306
move $23, $25
sw $23, reg_306
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_origin:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_291
lw $25, reg_291
move $24, $25
mul $23, $24, 4
add $23, $23, 4
li $v0, 9
move $a0, $23
syscall
move $22, $v0
sw $24, ($22)
move $24, $22
move $22, $0
sw $22, reg_303
sw $24, reg_300
j AtLabel_for_cond_20
AtLabel_for_exp3_22:
lw $25, reg_303
add $25, $25, 1
sw $25, reg_303
AtLabel_for_cond_20:
lw $t0, reg_303
lw $t1, reg_291
bge $t0, $t1, AtLabel_for_exit_23
AtLabel_for_body_21:
lw $25, reg_291
move $24, $25
mul $23, $24, 4
add $23, $23, 4
li $v0, 9
move $a0, $23
syscall
move $22, $v0
sw $24, ($22)
lw $24, reg_303
mul $23, $24, 4
lw $21, reg_300
add $23, $23, $21
add $20, $23, 4
sw $22, ($20)
move $22, $0
sw $22, reg_304
j AtLabel_for_cond_24
AtLabel_for_exp3_26:
lw $25, reg_304
add $25, $25, 1
sw $25, reg_304
AtLabel_for_cond_24:
lw $t0, reg_304
lw $t1, reg_291
bge $t0, $t1, AtLabel_for_exit_27
AtLabel_for_body_25:
move $25, $0
lw $24, reg_303
mul $23, $24, 4
lw $22, reg_300
add $23, $23, $22
lw $21, 4($23)
lw $23, reg_304
mul $20, $23, 4
add $20, $20, $21
add $21, $20, 4
sw $25, ($21)
j AtLabel_for_exp3_26
AtLabel_for_exit_27:
j AtLabel_for_exp3_22
AtLabel_for_exit_23:
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_search:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_44
lw $t0, -8($fp)
sw $t0, reg_45
lw $t0, -12($fp)
sw $t0, reg_46
move $25, $0
move $24, $0
move $23, $0
lw $22, reg_45
sgt $21, $22, $23
sw $21, reg_52
sw $24, reg_51
sw $25, reg_50
lw $t0, reg_52
beq $t0, 1, AtLabel_32
j AtLabel_34
AtLabel_34:
move $25, $0
lw $24, reg_45
slt $23, $24, $25
sw $23, reg_54
lw $t0, reg_54
beq $t0, 1, AtLabel_32
j AtLabel_35
AtLabel_35:
j AtLabel_33
AtLabel_32:
li $25, 1
sw $25, reg_51
AtLabel_33:
lw $t0, reg_51
beq $t0, 1, AtLabel_30
j AtLabel_36
AtLabel_36:
move $25, $0
lw $24, reg_44
seq $23, $24, $25
sw $23, reg_56
lw $t0, reg_56
beq $t0, 1, AtLabel_30
j AtLabel_37
AtLabel_37:
lw $25, reg_44
sub $24, $25, 1
mul $23, $24, 4
lw $24, reg_300
add $23, $23, $24
lw $22, 4($23)
move $23, $0
mul $21, $23, 4
add $21, $21, $22
lw $22, 4($21)
sub $21, $25, 1
mul $23, $21, 4
add $23, $23, $24
lw $21, 4($23)
li $23, 1
mul $20, $23, 4
add $20, $20, $21
lw $21, 4($20)
add $20, $22, $21
sub $21, $25, 1
mul $22, $21, 4
add $22, $22, $24
lw $21, 4($22)
li $22, 2
mul $23, $22, 4
add $23, $23, $21
lw $21, 4($23)
add $20, $20, $21
li $21, 15
seq $23, $20, $21
sw $23, reg_58
lw $t0, reg_58
beq $t0, 1, AtLabel_30
j AtLabel_38
AtLabel_38:
j AtLabel_31
AtLabel_30:
li $25, 1
sw $25, reg_50
AtLabel_31:
lw $t0, reg_50
beq $t0, 1, AtLabel_if_body1_28
j AtLabel_if_exit_29
AtLabel_if_body1_28:
li $25, 1
li $24, 2
sw $24, reg_77
sw $25, reg_76
lw $t0, reg_44
lw $t1, reg_77
bne $t0, $t1, AtLabel_42
li $25, 2
sw $25, reg_78
lw $t0, reg_45
lw $t1, reg_78
bne $t0, $t1, AtLabel_42
j AtLabel_43
AtLabel_42:
move $25, $0
sw $25, reg_76
AtLabel_43:
lw $t0, reg_76
beq $t0, 1, AtLabel_if_body1_39
j AtLabel_if_body2_40
AtLabel_if_body1_39:
li $25, 45
lw $24, reg_46
sub $23, $25, $24
li $25, 2
mul $22, $25, 4
lw $25, reg_300
add $22, $22, $25
lw $21, 4($22)
li $22, 2
mul $20, $22, 4
add $20, $20, $21
add $21, $20, 4
sw $23, ($21)
move $23, $0
mul $20, $23, 4
add $20, $20, $25
lw $23, 4($20)
move $20, $0
mul $22, $20, 4
add $22, $22, $23
lw $23, 4($22)
move $22, $0
mul $20, $22, 4
add $20, $20, $25
lw $22, 4($20)
li $20, 1
mul $19, $20, 4
add $19, $19, $22
lw $22, 4($19)
add $19, $23, $22
move $22, $0
mul $23, $22, 4
add $23, $23, $25
lw $22, 4($23)
li $23, 2
mul $20, $23, 4
add $20, $20, $22
lw $22, 4($20)
add $20, $19, $22
li $22, 1
li $19, 1
mul $23, $19, 4
add $23, $23, $25
lw $19, 4($23)
move $23, $0
mul $18, $23, 4
add $18, $18, $19
lw $19, 4($18)
li $18, 1
mul $23, $18, 4
add $23, $23, $25
lw $18, 4($23)
li $23, 1
mul $17, $23, 4
add $17, $17, $18
lw $18, 4($17)
add $17, $19, $18
li $18, 1
mul $19, $18, 4
add $19, $19, $25
lw $18, 4($19)
li $19, 2
mul $23, $19, 4
add $23, $23, $18
lw $18, 4($23)
add $17, $17, $18
sw $17, reg_104
sw $20, reg_47
sw $22, reg_103
lw $t0, reg_104
lw $t1, reg_47
bne $t0, $t1, AtLabel_46
li $25, 2
mul $24, $25, 4
lw $25, reg_300
add $24, $24, $25
lw $23, 4($24)
move $24, $0
mul $22, $24, 4
add $22, $22, $23
lw $23, 4($22)
li $22, 2
mul $24, $22, 4
add $24, $24, $25
lw $22, 4($24)
li $24, 1
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
add $21, $23, $22
li $22, 2
mul $23, $22, 4
add $23, $23, $25
lw $22, 4($23)
li $23, 2
mul $24, $23, 4
add $24, $24, $22
lw $22, 4($24)
add $21, $21, $22
sw $21, reg_123
lw $t0, reg_123
lw $t1, reg_47
bne $t0, $t1, AtLabel_46
move $25, $0
mul $24, $25, 4
lw $25, reg_300
add $24, $24, $25
lw $23, 4($24)
move $24, $0
mul $22, $24, 4
add $22, $22, $23
lw $23, 4($22)
li $22, 1
mul $24, $22, 4
add $24, $24, $25
lw $22, 4($24)
move $24, $0
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
add $21, $23, $22
li $22, 2
mul $23, $22, 4
add $23, $23, $25
lw $22, 4($23)
move $23, $0
mul $24, $23, 4
add $24, $24, $22
lw $22, 4($24)
add $21, $21, $22
sw $21, reg_140
lw $t0, reg_140
lw $t1, reg_47
bne $t0, $t1, AtLabel_46
move $25, $0
mul $24, $25, 4
lw $25, reg_300
add $24, $24, $25
lw $23, 4($24)
li $24, 1
mul $22, $24, 4
add $22, $22, $23
lw $23, 4($22)
li $22, 1
mul $24, $22, 4
add $24, $24, $25
lw $22, 4($24)
li $24, 1
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
add $21, $23, $22
li $22, 2
mul $23, $22, 4
add $23, $23, $25
lw $22, 4($23)
li $23, 1
mul $24, $23, 4
add $24, $24, $22
lw $22, 4($24)
add $21, $21, $22
sw $21, reg_158
lw $t0, reg_158
lw $t1, reg_47
bne $t0, $t1, AtLabel_46
move $25, $0
mul $24, $25, 4
lw $25, reg_300
add $24, $24, $25
lw $23, 4($24)
li $24, 2
mul $22, $24, 4
add $22, $22, $23
lw $23, 4($22)
li $22, 1
mul $24, $22, 4
add $24, $24, $25
lw $22, 4($24)
li $24, 2
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
add $21, $23, $22
li $22, 2
mul $23, $22, 4
add $23, $23, $25
lw $22, 4($23)
li $23, 2
mul $24, $23, 4
add $24, $24, $22
lw $22, 4($24)
add $21, $21, $22
sw $21, reg_176
lw $t0, reg_176
lw $t1, reg_47
bne $t0, $t1, AtLabel_46
move $25, $0
mul $24, $25, 4
lw $25, reg_300
add $24, $24, $25
lw $23, 4($24)
move $24, $0
mul $22, $24, 4
add $22, $22, $23
lw $23, 4($22)
li $22, 1
mul $24, $22, 4
add $24, $24, $25
lw $22, 4($24)
li $24, 1
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
add $21, $23, $22
li $22, 2
mul $23, $22, 4
add $23, $23, $25
lw $22, 4($23)
li $23, 2
mul $24, $23, 4
add $24, $24, $22
lw $22, 4($24)
add $21, $21, $22
sw $21, reg_197
lw $t0, reg_197
lw $t1, reg_47
bne $t0, $t1, AtLabel_46
li $25, 2
mul $24, $25, 4
lw $25, reg_300
add $24, $24, $25
lw $23, 4($24)
move $24, $0
mul $22, $24, 4
add $22, $22, $23
lw $23, 4($22)
li $22, 1
mul $24, $22, 4
add $24, $24, $25
lw $22, 4($24)
li $24, 1
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
add $21, $23, $22
move $22, $0
mul $23, $22, 4
add $23, $23, $25
lw $22, 4($23)
li $23, 2
mul $24, $23, 4
add $24, $24, $22
lw $22, 4($24)
add $21, $21, $22
sw $21, reg_214
lw $t0, reg_214
lw $t1, reg_47
bne $t0, $t1, AtLabel_46
j AtLabel_47
AtLabel_46:
move $25, $0
sw $25, reg_103
AtLabel_47:
lw $t0, reg_103
beq $t0, 1, AtLabel_if_body1_44
j AtLabel_if_exit_45
AtLabel_if_body1_44:
move $25, $0
mul $24, $25, 4
lw $25, reg_302
add $24, $24, $25
lw $23, 4($24)
add $24, $23, 1
move $23, $0
mul $22, $23, 4
add $22, $22, $25
add $23, $22, 4
sw $24, ($23)
move $24, $0
sw $24, reg_48
j AtLabel_for_cond_48
AtLabel_for_exp3_50:
lw $25, reg_48
add $25, $25, 1
sw $25, reg_48
AtLabel_for_cond_48:
li $25, 2
sw $25, reg_237
lw $t0, reg_48
lw $t1, reg_237
bgt $t0, $t1, AtLabel_for_exit_51
AtLabel_for_body_49:
move $25, $0
sw $25, reg_49
j AtLabel_for_cond_52
AtLabel_for_exp3_54:
lw $25, reg_49
add $25, $25, 1
sw $25, reg_49
AtLabel_for_cond_52:
li $25, 2
sw $25, reg_238
lw $t0, reg_49
lw $t1, reg_238
bgt $t0, $t1, AtLabel_for_exit_55
AtLabel_for_body_53:
lw $25, reg_48
mul $24, $25, 4
lw $23, reg_300
add $24, $24, $23
lw $22, 4($24)
lw $24, reg_49
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
sw $22, reg_241
sw $23, reg_300
sw $24, reg_49
sw $25, reg_48
li $v0, 1
lw $a0, reg_241
syscall
la $t0, __CharSpace
sw $t0, reg_306
lw $25, reg_306
move $24, $25
sw $25, reg_306
sw $24, reg_242
li $v0, 4
lw $a0, reg_242
add $a0, $a0, 4
syscall
j AtLabel_for_exp3_54
AtLabel_for_exit_55:
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_backslash_nquote_
lw $25, reg_306
move $24, $25
sw $25, reg_306
sw $24, reg_243
li $v0, 4
lw $a0, reg_243
add $a0, $a0, 4
syscall
j AtLabel_for_exp3_50
AtLabel_for_exit_51:
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_backslash_nquote_
lw $25, reg_306
move $24, $25
sw $25, reg_306
sw $24, reg_244
li $v0, 4
lw $a0, reg_244
add $a0, $a0, 4
syscall
AtLabel_if_exit_45:
j AtLabel_if_exit_41
AtLabel_if_body2_40:
li $25, 2
sw $25, reg_245
lw $t0, reg_45
lw $t1, reg_245
bne $t0, $t1, AtLabel_if_body2_57
AtLabel_if_body1_56:
li $25, 15
lw $24, reg_44
mul $23, $24, 4
lw $22, reg_300
add $23, $23, $22
lw $21, 4($23)
move $23, $0
mul $20, $23, 4
add $20, $20, $21
lw $21, 4($20)
sub $20, $25, $21
mul $21, $24, 4
add $21, $21, $22
lw $25, 4($21)
li $21, 1
mul $23, $21, 4
add $23, $23, $25
lw $25, 4($23)
sub $20, $20, $25
mul $25, $24, 4
add $25, $25, $22
lw $23, 4($25)
lw $25, reg_45
mul $21, $25, 4
add $21, $21, $23
add $23, $21, 4
sw $20, ($23)
li $20, 1
mul $21, $24, 4
add $21, $21, $22
lw $19, 4($21)
mul $21, $25, 4
add $21, $21, $19
lw $19, 4($21)
move $21, $0
sw $19, reg_262
sw $20, reg_259
sw $21, reg_263
lw $t0, reg_262
lw $t1, reg_263
ble $t0, $t1, AtLabel_61
lw $25, reg_44
mul $24, $25, 4
lw $23, reg_300
add $24, $24, $23
lw $22, 4($24)
lw $24, reg_45
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
li $21, 10
sw $21, reg_267
sw $22, reg_266
lw $t0, reg_266
lw $t1, reg_267
bge $t0, $t1, AtLabel_61
lw $25, reg_44
mul $24, $25, 4
lw $23, reg_300
add $24, $24, $23
lw $22, 4($24)
lw $24, reg_45
mul $21, $24, 4
add $21, $21, $22
lw $22, 4($21)
mul $21, $22, 4
lw $22, reg_301
add $21, $21, $22
lw $20, 4($21)
move $21, $0
sw $20, reg_269
sw $21, reg_273
lw $t0, reg_269
lw $t1, reg_273
bne $t0, $t1, AtLabel_61
j AtLabel_62
AtLabel_61:
move $25, $0
sw $25, reg_259
AtLabel_62:
lw $t0, reg_259
beq $t0, 1, AtLabel_if_body1_59
j AtLabel_if_exit_60
AtLabel_if_body1_59:
li $25, 1
lw $24, reg_44
mul $23, $24, 4
lw $22, reg_300
add $23, $23, $22
lw $21, 4($23)
lw $23, reg_45
mul $20, $23, 4
add $20, $20, $21
lw $21, 4($20)
mul $20, $21, 4
lw $21, reg_301
add $20, $20, $21
add $19, $20, 4
sw $25, ($19)
li $25, 2
sw $25, reg_280
lw $t0, reg_45
lw $t1, reg_280
bne $t0, $t1, AtLabel_if_body2_64
AtLabel_if_body1_63:
lw $25, reg_44
add $24, $25, 1
move $23, $0
mul $22, $25, 4
lw $21, reg_300
add $22, $22, $21
lw $20, 4($22)
lw $22, reg_45
mul $19, $22, 4
add $19, $19, $20
lw $20, 4($19)
lw $19, reg_46
add $18, $19, $20
lw $t0, reg_17
sw $t0, 0($sp)
lw $t0, reg_21
sw $t0, -4($sp)
lw $t0, reg_24
sw $t0, -8($sp)
sw $25, -12($sp)
sw $22, -16($sp)
sw $19, -20($sp)
lw $t0, reg_47
sw $t0, -24($sp)
lw $t0, reg_48
sw $t0, -28($sp)
lw $t0, reg_49
sw $t0, -32($sp)
lw $t0, reg_59
sw $t0, -36($sp)
lw $t0, reg_62
sw $t0, -40($sp)
lw $t0, reg_67
sw $t0, -44($sp)
lw $t0, reg_72
sw $t0, -48($sp)
lw $t0, reg_79
sw $t0, -52($sp)
lw $t0, reg_85
sw $t0, -56($sp)
lw $t0, reg_86
sw $t0, -60($sp)
lw $t0, reg_104
sw $t0, -64($sp)
lw $t0, reg_123
sw $t0, -68($sp)
lw $t0, reg_140
sw $t0, -72($sp)
lw $t0, reg_158
sw $t0, -76($sp)
lw $t0, reg_176
sw $t0, -80($sp)
lw $t0, reg_197
sw $t0, -84($sp)
lw $t0, reg_214
sw $t0, -88($sp)
lw $t0, reg_230
sw $t0, -92($sp)
lw $t0, reg_236
sw $t0, -96($sp)
lw $t0, reg_246
sw $t0, -100($sp)
lw $t0, reg_258
sw $t0, -104($sp)
lw $t0, reg_279
sw $t0, -108($sp)
sw $24, -112($sp)
sw $18, -116($sp)
sub $sp, $sp, 120
sw $18, reg_283
sw $19, reg_46
sw $21, reg_300
sw $22, reg_45
sw $23, reg_282
sw $24, reg_281
sw $25, reg_44
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_281
sw $t0, -4($fp)
lw $t0, reg_282
sw $t0, -8($fp)
lw $t0, reg_283
sw $t0, -12($fp)
sub $sp, $sp, 16
jal AtfunctionLabel_search
add $sp, $sp, 120
lw $t0, 0($sp)
sw $t0, reg_17
lw $t0, -4($sp)
sw $t0, reg_21
lw $t0, -8($sp)
sw $t0, reg_24
lw $t0, -12($sp)
sw $t0, reg_44
lw $t0, -16($sp)
sw $t0, reg_45
lw $t0, -20($sp)
sw $t0, reg_46
lw $t0, -24($sp)
sw $t0, reg_47
lw $t0, -28($sp)
sw $t0, reg_48
lw $t0, -32($sp)
sw $t0, reg_49
lw $t0, -36($sp)
sw $t0, reg_59
lw $t0, -40($sp)
sw $t0, reg_62
lw $t0, -44($sp)
sw $t0, reg_67
lw $t0, -48($sp)
sw $t0, reg_72
lw $t0, -52($sp)
sw $t0, reg_79
lw $t0, -56($sp)
sw $t0, reg_85
lw $t0, -60($sp)
sw $t0, reg_86
lw $t0, -64($sp)
sw $t0, reg_104
lw $t0, -68($sp)
sw $t0, reg_123
lw $t0, -72($sp)
sw $t0, reg_140
lw $t0, -76($sp)
sw $t0, reg_158
lw $t0, -80($sp)
sw $t0, reg_176
lw $t0, -84($sp)
sw $t0, reg_197
lw $t0, -88($sp)
sw $t0, reg_214
lw $t0, -92($sp)
sw $t0, reg_230
lw $t0, -96($sp)
sw $t0, reg_236
lw $t0, -100($sp)
sw $t0, reg_246
lw $t0, -104($sp)
sw $t0, reg_258
lw $t0, -108($sp)
sw $t0, reg_279
lw $t0, -112($sp)
sw $t0, reg_281
lw $t0, -116($sp)
sw $t0, reg_283
j AtLabel_if_exit_65
AtLabel_if_body2_64:
lw $25, reg_45
add $24, $25, 1
lw $23, reg_44
mul $22, $23, 4
lw $21, reg_300
add $22, $22, $21
lw $20, 4($22)
mul $22, $25, 4
add $22, $22, $20
lw $20, 4($22)
lw $22, reg_46
add $19, $22, $20
lw $t0, reg_17
sw $t0, 0($sp)
lw $t0, reg_21
sw $t0, -4($sp)
lw $t0, reg_24
sw $t0, -8($sp)
sw $23, -12($sp)
sw $25, -16($sp)
sw $22, -20($sp)
lw $t0, reg_47
sw $t0, -24($sp)
lw $t0, reg_48
sw $t0, -28($sp)
lw $t0, reg_49
sw $t0, -32($sp)
lw $t0, reg_59
sw $t0, -36($sp)
lw $t0, reg_62
sw $t0, -40($sp)
lw $t0, reg_67
sw $t0, -44($sp)
lw $t0, reg_72
sw $t0, -48($sp)
lw $t0, reg_79
sw $t0, -52($sp)
lw $t0, reg_85
sw $t0, -56($sp)
lw $t0, reg_86
sw $t0, -60($sp)
lw $t0, reg_104
sw $t0, -64($sp)
lw $t0, reg_123
sw $t0, -68($sp)
lw $t0, reg_140
sw $t0, -72($sp)
lw $t0, reg_158
sw $t0, -76($sp)
lw $t0, reg_176
sw $t0, -80($sp)
lw $t0, reg_197
sw $t0, -84($sp)
lw $t0, reg_214
sw $t0, -88($sp)
lw $t0, reg_230
sw $t0, -92($sp)
lw $t0, reg_236
sw $t0, -96($sp)
lw $t0, reg_246
sw $t0, -100($sp)
lw $t0, reg_258
sw $t0, -104($sp)
lw $t0, reg_279
sw $t0, -108($sp)
lw $t0, reg_281
sw $t0, -112($sp)
lw $t0, reg_283
sw $t0, -116($sp)
sw $24, -120($sp)
sw $19, -124($sp)
sub $sp, $sp, 128
sw $19, reg_288
sw $21, reg_300
sw $22, reg_46
sw $23, reg_44
sw $24, reg_287
sw $25, reg_45
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_44
sw $t0, -4($fp)
lw $t0, reg_287
sw $t0, -8($fp)
lw $t0, reg_288
sw $t0, -12($fp)
sub $sp, $sp, 16
jal AtfunctionLabel_search
add $sp, $sp, 128
lw $t0, 0($sp)
sw $t0, reg_17
lw $t0, -4($sp)
sw $t0, reg_21
lw $t0, -8($sp)
sw $t0, reg_24
lw $t0, -12($sp)
sw $t0, reg_44
lw $t0, -16($sp)
sw $t0, reg_45
lw $t0, -20($sp)
sw $t0, reg_46
lw $t0, -24($sp)
sw $t0, reg_47
lw $t0, -28($sp)
sw $t0, reg_48
lw $t0, -32($sp)
sw $t0, reg_49
lw $t0, -36($sp)
sw $t0, reg_59
lw $t0, -40($sp)
sw $t0, reg_62
lw $t0, -44($sp)
sw $t0, reg_67
lw $t0, -48($sp)
sw $t0, reg_72
lw $t0, -52($sp)
sw $t0, reg_79
lw $t0, -56($sp)
sw $t0, reg_85
lw $t0, -60($sp)
sw $t0, reg_86
lw $t0, -64($sp)
sw $t0, reg_104
lw $t0, -68($sp)
sw $t0, reg_123
lw $t0, -72($sp)
sw $t0, reg_140
lw $t0, -76($sp)
sw $t0, reg_158
lw $t0, -80($sp)
sw $t0, reg_176
lw $t0, -84($sp)
sw $t0, reg_197
lw $t0, -88($sp)
sw $t0, reg_214
lw $t0, -92($sp)
sw $t0, reg_230
lw $t0, -96($sp)
sw $t0, reg_236
lw $t0, -100($sp)
sw $t0, reg_246
lw $t0, -104($sp)
sw $t0, reg_258
lw $t0, -108($sp)
sw $t0, reg_279
lw $t0, -112($sp)
sw $t0, reg_281
lw $t0, -116($sp)
sw $t0, reg_283
lw $t0, -120($sp)
sw $t0, reg_287
lw $t0, -124($sp)
sw $t0, reg_288
AtLabel_if_exit_65:
move $25, $0
lw $24, reg_44
mul $23, $24, 4
lw $22, reg_300
add $23, $23, $22
lw $21, 4($23)
lw $23, reg_45
mul $20, $23, 4
add $20, $20, $21
lw $21, 4($20)
mul $20, $21, 4
lw $21, reg_301
add $20, $20, $21
add $19, $20, 4
sw $25, ($19)
sw $19, reg_299
sw $21, reg_301
sw $22, reg_300
sw $23, reg_45
sw $24, reg_44
AtLabel_if_exit_60:
j AtLabel_if_exit_58
AtLabel_if_body2_57:
li $25, 1
sw $25, reg_48
j AtLabel_for_cond_66
AtLabel_for_exp3_68:
lw $25, reg_48
add $25, $25, 1
sw $25, reg_48
AtLabel_for_cond_66:
li $25, 9
sw $25, reg_0
lw $t0, reg_48
lw $t1, reg_0
bgt $t0, $t1, AtLabel_for_exit_69
AtLabel_for_body_67:
lw $25, reg_48
mul $24, $25, 4
lw $23, reg_301
add $24, $24, $23
lw $22, 4($24)
move $24, $0
sw $22, reg_2
sw $24, reg_3
lw $t0, reg_2
lw $t1, reg_3
bne $t0, $t1, AtLabel_if_exit_71
AtLabel_if_body1_70:
li $25, 1
lw $24, reg_48
mul $23, $24, 4
lw $22, reg_301
add $23, $23, $22
add $21, $23, 4
sw $25, ($21)
lw $25, reg_44
mul $23, $25, 4
lw $20, reg_300
add $23, $23, $20
lw $19, 4($23)
lw $23, reg_45
mul $18, $23, 4
add $18, $18, $19
add $19, $18, 4
sw $24, ($19)
li $18, 2
sw $18, reg_12
lw $t0, reg_45
lw $t1, reg_12
bne $t0, $t1, AtLabel_if_body2_73
AtLabel_if_body1_72:
lw $25, reg_44
add $24, $25, 1
move $23, $0
lw $22, reg_46
lw $21, reg_48
add $20, $22, $21
lw $t0, reg_7
sw $t0, 0($sp)
sw $19, -4($sp)
sw $24, -8($sp)
sw $20, -12($sp)
lw $t0, reg_17
sw $t0, -16($sp)
lw $t0, reg_21
sw $t0, -20($sp)
lw $t0, reg_24
sw $t0, -24($sp)
sw $25, -28($sp)
lw $t0, reg_45
sw $t0, -32($sp)
sw $22, -36($sp)
lw $t0, reg_47
sw $t0, -40($sp)
sw $21, -44($sp)
lw $t0, reg_49
sw $t0, -48($sp)
lw $t0, reg_59
sw $t0, -52($sp)
lw $t0, reg_62
sw $t0, -56($sp)
lw $t0, reg_67
sw $t0, -60($sp)
lw $t0, reg_72
sw $t0, -64($sp)
lw $t0, reg_79
sw $t0, -68($sp)
lw $t0, reg_85
sw $t0, -72($sp)
lw $t0, reg_86
sw $t0, -76($sp)
lw $t0, reg_104
sw $t0, -80($sp)
lw $t0, reg_123
sw $t0, -84($sp)
lw $t0, reg_140
sw $t0, -88($sp)
lw $t0, reg_158
sw $t0, -92($sp)
lw $t0, reg_176
sw $t0, -96($sp)
lw $t0, reg_197
sw $t0, -100($sp)
lw $t0, reg_214
sw $t0, -104($sp)
lw $t0, reg_230
sw $t0, -108($sp)
lw $t0, reg_236
sw $t0, -112($sp)
lw $t0, reg_246
sw $t0, -116($sp)
lw $t0, reg_258
sw $t0, -120($sp)
lw $t0, reg_279
sw $t0, -124($sp)
lw $t0, reg_281
sw $t0, -128($sp)
lw $t0, reg_283
sw $t0, -132($sp)
lw $t0, reg_287
sw $t0, -136($sp)
lw $t0, reg_288
sw $t0, -140($sp)
lw $t0, reg_299
sw $t0, -144($sp)
sub $sp, $sp, 148
sw $20, reg_15
sw $21, reg_48
sw $22, reg_46
sw $23, reg_14
sw $24, reg_13
sw $25, reg_44
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_13
sw $t0, -4($fp)
lw $t0, reg_14
sw $t0, -8($fp)
lw $t0, reg_15
sw $t0, -12($fp)
sub $sp, $sp, 16
jal AtfunctionLabel_search
add $sp, $sp, 148
lw $t0, 0($sp)
sw $t0, reg_7
lw $19, -4($sp)
lw $t0, -8($sp)
sw $t0, reg_13
lw $t0, -12($sp)
sw $t0, reg_15
lw $t0, -16($sp)
sw $t0, reg_17
lw $t0, -20($sp)
sw $t0, reg_21
lw $t0, -24($sp)
sw $t0, reg_24
lw $t0, -28($sp)
sw $t0, reg_44
lw $t0, -32($sp)
sw $t0, reg_45
lw $t0, -36($sp)
sw $t0, reg_46
lw $t0, -40($sp)
sw $t0, reg_47
lw $t0, -44($sp)
sw $t0, reg_48
lw $t0, -48($sp)
sw $t0, reg_49
lw $t0, -52($sp)
sw $t0, reg_59
lw $t0, -56($sp)
sw $t0, reg_62
lw $t0, -60($sp)
sw $t0, reg_67
lw $t0, -64($sp)
sw $t0, reg_72
lw $t0, -68($sp)
sw $t0, reg_79
lw $t0, -72($sp)
sw $t0, reg_85
lw $t0, -76($sp)
sw $t0, reg_86
lw $t0, -80($sp)
sw $t0, reg_104
lw $t0, -84($sp)
sw $t0, reg_123
lw $t0, -88($sp)
sw $t0, reg_140
lw $t0, -92($sp)
sw $t0, reg_158
lw $t0, -96($sp)
sw $t0, reg_176
lw $t0, -100($sp)
sw $t0, reg_197
lw $t0, -104($sp)
sw $t0, reg_214
lw $t0, -108($sp)
sw $t0, reg_230
lw $t0, -112($sp)
sw $t0, reg_236
lw $t0, -116($sp)
sw $t0, reg_246
lw $t0, -120($sp)
sw $t0, reg_258
lw $t0, -124($sp)
sw $t0, reg_279
lw $t0, -128($sp)
sw $t0, reg_281
lw $t0, -132($sp)
sw $t0, reg_283
lw $t0, -136($sp)
sw $t0, reg_287
lw $t0, -140($sp)
sw $t0, reg_288
lw $t0, -144($sp)
sw $t0, reg_299
j AtLabel_if_exit_74
AtLabel_if_body2_73:
lw $25, reg_45
add $24, $25, 1
lw $23, reg_46
lw $22, reg_48
add $21, $23, $22
lw $t0, reg_7
sw $t0, 0($sp)
sw $19, -4($sp)
lw $t0, reg_13
sw $t0, -8($sp)
lw $t0, reg_15
sw $t0, -12($sp)
sw $24, -16($sp)
sw $21, -20($sp)
lw $t0, reg_21
sw $t0, -24($sp)
lw $t0, reg_24
sw $t0, -28($sp)
lw $t0, reg_44
sw $t0, -32($sp)
sw $25, -36($sp)
sw $23, -40($sp)
lw $t0, reg_47
sw $t0, -44($sp)
sw $22, -48($sp)
lw $t0, reg_49
sw $t0, -52($sp)
lw $t0, reg_59
sw $t0, -56($sp)
lw $t0, reg_62
sw $t0, -60($sp)
lw $t0, reg_67
sw $t0, -64($sp)
lw $t0, reg_72
sw $t0, -68($sp)
lw $t0, reg_79
sw $t0, -72($sp)
lw $t0, reg_85
sw $t0, -76($sp)
lw $t0, reg_86
sw $t0, -80($sp)
lw $t0, reg_104
sw $t0, -84($sp)
lw $t0, reg_123
sw $t0, -88($sp)
lw $t0, reg_140
sw $t0, -92($sp)
lw $t0, reg_158
sw $t0, -96($sp)
lw $t0, reg_176
sw $t0, -100($sp)
lw $t0, reg_197
sw $t0, -104($sp)
lw $t0, reg_214
sw $t0, -108($sp)
lw $t0, reg_230
sw $t0, -112($sp)
lw $t0, reg_236
sw $t0, -116($sp)
lw $t0, reg_246
sw $t0, -120($sp)
lw $t0, reg_258
sw $t0, -124($sp)
lw $t0, reg_279
sw $t0, -128($sp)
lw $t0, reg_281
sw $t0, -132($sp)
lw $t0, reg_283
sw $t0, -136($sp)
lw $t0, reg_287
sw $t0, -140($sp)
lw $t0, reg_288
sw $t0, -144($sp)
lw $t0, reg_299
sw $t0, -148($sp)
sub $sp, $sp, 152
sw $21, reg_17
sw $22, reg_48
sw $23, reg_46
sw $24, reg_16
sw $25, reg_45
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_44
sw $t0, -4($fp)
lw $t0, reg_16
sw $t0, -8($fp)
lw $t0, reg_17
sw $t0, -12($fp)
sub $sp, $sp, 16
jal AtfunctionLabel_search
add $sp, $sp, 152
lw $t0, 0($sp)
sw $t0, reg_7
lw $19, -4($sp)
lw $t0, -8($sp)
sw $t0, reg_13
lw $t0, -12($sp)
sw $t0, reg_15
lw $t0, -16($sp)
sw $t0, reg_16
lw $t0, -20($sp)
sw $t0, reg_17
lw $t0, -24($sp)
sw $t0, reg_21
lw $t0, -28($sp)
sw $t0, reg_24
lw $t0, -32($sp)
sw $t0, reg_44
lw $t0, -36($sp)
sw $t0, reg_45
lw $t0, -40($sp)
sw $t0, reg_46
lw $t0, -44($sp)
sw $t0, reg_47
lw $t0, -48($sp)
sw $t0, reg_48
lw $t0, -52($sp)
sw $t0, reg_49
lw $t0, -56($sp)
sw $t0, reg_59
lw $t0, -60($sp)
sw $t0, reg_62
lw $t0, -64($sp)
sw $t0, reg_67
lw $t0, -68($sp)
sw $t0, reg_72
lw $t0, -72($sp)
sw $t0, reg_79
lw $t0, -76($sp)
sw $t0, reg_85
lw $t0, -80($sp)
sw $t0, reg_86
lw $t0, -84($sp)
sw $t0, reg_104
lw $t0, -88($sp)
sw $t0, reg_123
lw $t0, -92($sp)
sw $t0, reg_140
lw $t0, -96($sp)
sw $t0, reg_158
lw $t0, -100($sp)
sw $t0, reg_176
lw $t0, -104($sp)
sw $t0, reg_197
lw $t0, -108($sp)
sw $t0, reg_214
lw $t0, -112($sp)
sw $t0, reg_230
lw $t0, -116($sp)
sw $t0, reg_236
lw $t0, -120($sp)
sw $t0, reg_246
lw $t0, -124($sp)
sw $t0, reg_258
lw $t0, -128($sp)
sw $t0, reg_279
lw $t0, -132($sp)
sw $t0, reg_281
lw $t0, -136($sp)
sw $t0, reg_283
lw $t0, -140($sp)
sw $t0, reg_287
lw $t0, -144($sp)
sw $t0, reg_288
lw $t0, -148($sp)
sw $t0, reg_299
AtLabel_if_exit_74:
move $25, $0
lw $24, reg_44
mul $23, $24, 4
lw $22, reg_300
add $23, $23, $22
lw $21, 4($23)
lw $23, reg_45
mul $20, $23, 4
add $20, $20, $21
add $21, $20, 4
sw $25, ($21)
move $25, $0
lw $20, reg_48
mul $19, $20, 4
lw $18, reg_301
add $19, $19, $18
add $17, $19, 4
sw $25, ($17)
sw $17, reg_24
sw $18, reg_301
sw $20, reg_48
sw $21, reg_21
sw $22, reg_300
sw $23, reg_45
sw $24, reg_44
AtLabel_if_exit_71:
j AtLabel_for_exp3_68
AtLabel_for_exit_69:
AtLabel_if_exit_58:
AtLabel_if_exit_41:
AtLabel_if_exit_29:
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
main:
move $fp, $sp
li $t0, 32
li $t1, 1
la $a0, __CharSpace
sw $t1, ($a0)
sw $t0, 4($a0)
li $t0, 10
li $t1, 1
la $a0, __CharEnter
sw $t1, ($a0)
sw $t0, 4($a0)
sw $31, ($sp)
sub $sp, $sp, 4
la $t0, static
sw $t0, reg_305
li $25, 1
lw $24, reg_305
sw $25, 20($24)
li $25, 1
sw $25, 24($24)
li $25, 1
sw $25, 28($24)
add $25, $24, 0
add $23, $24, 16
add $22, $24, 8
add $21, $24, 4
add $20, $24, 12
li $19, 10
mul $18, $19, 4
add $18, $18, 4
li $v0, 9
move $a0, $18
syscall
move $17, $v0
sw $19, ($17)
move $21, $17
sw $21, 4($24)
li $17, 1
mul $19, $17, 4
add $19, $19, 4
li $v0, 9
move $a0, $19
syscall
move $18, $v0
sw $17, ($18)
move $22, $18
sw $22, 8($24)
li $18, 3
sw $18, reg_25
sw $20, reg_303
sw $21, reg_301
sw $22, reg_302
sw $23, reg_304
sw $24, reg_305
sw $25, reg_300
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_25
sw $t0, -4($fp)
sub $sp, $sp, 8
jal AtfunctionLabel_origin
move $25, $0
move $24, $0
move $23, $0
sw $23, reg_28
sw $24, reg_27
sw $25, reg_26
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_26
sw $t0, -4($fp)
lw $t0, reg_27
sw $t0, -8($fp)
lw $t0, reg_28
sw $t0, -12($fp)
sub $sp, $sp, 16
jal AtfunctionLabel_search
move $25, $0
mul $24, $25, 4
lw $25, reg_302
add $24, $24, $25
lw $25, 4($24)
sw $25, reg_30
li $v0, 1
lw $a0, reg_30
syscall
li $v0, 4
la $a0, __CharEnter
add $a0, $a0, 4
syscall
lw $25, reg_306
move $25, $0
sw $25, reg_306
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_print:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_32
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel___builtin_printIntln:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_38
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel___builtin_printInt:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_39
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31

.data
static:
	.space 32
reg_0: .space 4
reg_1: .space 4
reg_2: .space 4
reg_3: .space 4
reg_4: .space 4
reg_5: .space 4
reg_6: .space 4
reg_7: .space 4
reg_8: .space 4
reg_9: .space 4
reg_10: .space 4
reg_11: .space 4
reg_12: .space 4
reg_13: .space 4
reg_14: .space 4
reg_15: .space 4
reg_16: .space 4
reg_17: .space 4
reg_18: .space 4
reg_19: .space 4
reg_20: .space 4
reg_21: .space 4
reg_22: .space 4
reg_23: .space 4
reg_24: .space 4
reg_25: .space 4
reg_26: .space 4
reg_27: .space 4
reg_28: .space 4
reg_29: .space 4
reg_30: .space 4
reg_31: .space 4
reg_32: .space 4
reg_33: .space 4
reg_34: .space 4
reg_35: .space 4
reg_36: .space 4
reg_37: .space 4
reg_38: .space 4
reg_39: .space 4
reg_40: .space 4
reg_41: .space 4
reg_42: .space 4
reg_43: .space 4
reg_44: .space 4
reg_45: .space 4
reg_46: .space 4
reg_47: .space 4
reg_48: .space 4
reg_49: .space 4
reg_50: .space 4
reg_51: .space 4
reg_52: .space 4
reg_53: .space 4
reg_54: .space 4
reg_55: .space 4
reg_56: .space 4
reg_57: .space 4
reg_58: .space 4
reg_59: .space 4
reg_60: .space 4
reg_61: .space 4
reg_62: .space 4
reg_63: .space 4
reg_64: .space 4
reg_65: .space 4
reg_66: .space 4
reg_67: .space 4
reg_68: .space 4
reg_69: .space 4
reg_70: .space 4
reg_71: .space 4
reg_72: .space 4
reg_73: .space 4
reg_74: .space 4
reg_75: .space 4
reg_76: .space 4
reg_77: .space 4
reg_78: .space 4
reg_79: .space 4
reg_80: .space 4
reg_81: .space 4
reg_82: .space 4
reg_83: .space 4
reg_84: .space 4
reg_85: .space 4
reg_86: .space 4
reg_87: .space 4
reg_88: .space 4
reg_89: .space 4
reg_90: .space 4
reg_91: .space 4
reg_92: .space 4
reg_93: .space 4
reg_94: .space 4
reg_95: .space 4
reg_96: .space 4
reg_97: .space 4
reg_98: .space 4
reg_99: .space 4
reg_100: .space 4
reg_101: .space 4
reg_102: .space 4
reg_103: .space 4
reg_104: .space 4
reg_105: .space 4
reg_106: .space 4
reg_107: .space 4
reg_108: .space 4
reg_109: .space 4
reg_110: .space 4
reg_111: .space 4
reg_112: .space 4
reg_113: .space 4
reg_114: .space 4
reg_115: .space 4
reg_116: .space 4
reg_117: .space 4
reg_118: .space 4
reg_119: .space 4
reg_120: .space 4
reg_121: .space 4
reg_122: .space 4
reg_123: .space 4
reg_124: .space 4
reg_125: .space 4
reg_126: .space 4
reg_127: .space 4
reg_128: .space 4
reg_129: .space 4
reg_130: .space 4
reg_131: .space 4
reg_132: .space 4
reg_133: .space 4
reg_134: .space 4
reg_135: .space 4
reg_136: .space 4
reg_137: .space 4
reg_138: .space 4
reg_139: .space 4
reg_140: .space 4
reg_141: .space 4
reg_142: .space 4
reg_143: .space 4
reg_144: .space 4
reg_145: .space 4
reg_146: .space 4
reg_147: .space 4
reg_148: .space 4
reg_149: .space 4
reg_150: .space 4
reg_151: .space 4
reg_152: .space 4
reg_153: .space 4
reg_154: .space 4
reg_155: .space 4
reg_156: .space 4
reg_157: .space 4
reg_158: .space 4
reg_159: .space 4
reg_160: .space 4
reg_161: .space 4
reg_162: .space 4
reg_163: .space 4
reg_164: .space 4
reg_165: .space 4
reg_166: .space 4
reg_167: .space 4
reg_168: .space 4
reg_169: .space 4
reg_170: .space 4
reg_171: .space 4
reg_172: .space 4
reg_173: .space 4
reg_174: .space 4
reg_175: .space 4
reg_176: .space 4
reg_177: .space 4
reg_178: .space 4
reg_179: .space 4
reg_180: .space 4
reg_181: .space 4
reg_182: .space 4
reg_183: .space 4
reg_184: .space 4
reg_185: .space 4
reg_186: .space 4
reg_187: .space 4
reg_188: .space 4
reg_189: .space 4
reg_190: .space 4
reg_191: .space 4
reg_192: .space 4
reg_193: .space 4
reg_194: .space 4
reg_195: .space 4
reg_196: .space 4
reg_197: .space 4
reg_198: .space 4
reg_199: .space 4
reg_200: .space 4
reg_201: .space 4
reg_202: .space 4
reg_203: .space 4
reg_204: .space 4
reg_205: .space 4
reg_206: .space 4
reg_207: .space 4
reg_208: .space 4
reg_209: .space 4
reg_210: .space 4
reg_211: .space 4
reg_212: .space 4
reg_213: .space 4
reg_214: .space 4
reg_215: .space 4
reg_216: .space 4
reg_217: .space 4
reg_218: .space 4
reg_219: .space 4
reg_220: .space 4
reg_221: .space 4
reg_222: .space 4
reg_223: .space 4
reg_224: .space 4
reg_225: .space 4
reg_226: .space 4
reg_227: .space 4
reg_228: .space 4
reg_229: .space 4
reg_230: .space 4
reg_231: .space 4
reg_232: .space 4
reg_233: .space 4
reg_234: .space 4
reg_235: .space 4
reg_236: .space 4
reg_237: .space 4
reg_238: .space 4
reg_239: .space 4
reg_240: .space 4
reg_241: .space 4
reg_242: .space 4
reg_243: .space 4
reg_244: .space 4
reg_245: .space 4
reg_246: .space 4
reg_247: .space 4
reg_248: .space 4
reg_249: .space 4
reg_250: .space 4
reg_251: .space 4
reg_252: .space 4
reg_253: .space 4
reg_254: .space 4
reg_255: .space 4
reg_256: .space 4
reg_257: .space 4
reg_258: .space 4
reg_259: .space 4
reg_260: .space 4
reg_261: .space 4
reg_262: .space 4
reg_263: .space 4
reg_264: .space 4
reg_265: .space 4
reg_266: .space 4
reg_267: .space 4
reg_268: .space 4
reg_269: .space 4
reg_270: .space 4
reg_271: .space 4
reg_272: .space 4
reg_273: .space 4
reg_274: .space 4
reg_275: .space 4
reg_276: .space 4
reg_277: .space 4
reg_278: .space 4
reg_279: .space 4
reg_280: .space 4
reg_281: .space 4
reg_282: .space 4
reg_283: .space 4
reg_284: .space 4
reg_285: .space 4
reg_286: .space 4
reg_287: .space 4
reg_288: .space 4
reg_289: .space 4
reg_290: .space 4
reg_291: .space 4
reg_292: .space 4
reg_293: .space 4
reg_294: .space 4
reg_295: .space 4
reg_296: .space 4
reg_297: .space 4
reg_298: .space 4
reg_299: .space 4
reg_300: .space 4
reg_301: .space 4
reg_302: .space 4
reg_303: .space 4
reg_304: .space 4
reg_305: .space 4
reg_306: .space 4
reg_307: .space 4
reg_308: .space 4
reg_309: .space 4
reg_310: .space 4
reg_311: .space 4
reg_312: .space 4
reg_313: .space 4
reg_314: .space 4
reg_315: .space 4
reg_316: .space 4
__CharEnter: .space 8
__CharSpace: .space 8

