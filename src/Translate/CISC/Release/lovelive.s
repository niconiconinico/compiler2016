.text
AtfunctionLabel___builtin_string_generate_quote_space_Oquote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_178
li $t1, 2
sw $t1, reg_181
lw $t0, reg_181
lw $t1, reg_178
sw $t0, ($t1)
lw $t0, reg_178
sw $t0, reg_186
lw $t0, reg_186
add $t2, $t0, 4
sw $t2, reg_186
li $t1, 20256
sw $t1, reg_181
lw $t0, reg_181
lw $t1, reg_186
sw $t0, ($t1)
lw $t0, reg_178
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_space_.quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_188
li $t1, 2
sw $t1, reg_193
lw $t0, reg_193
lw $t1, reg_188
sw $t0, ($t1)
lw $t0, reg_188
sw $t0, reg_197
lw $t0, reg_197
add $t2, $t0, 4
sw $t2, reg_197
li $t1, 11808
sw $t1, reg_193
lw $t0, reg_193
lw $t1, reg_197
sw $t0, ($t1)
lw $t0, reg_188
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 8
syscall
sw $v0, reg_200
li $t1, 0
sw $t1, reg_205
lw $t0, reg_205
lw $t1, reg_200
sw $t0, ($t1)
lw $t0, reg_200
sw $t0, reg_207
lw $t0, reg_200
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_0quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_212
li $t1, 1
sw $t1, reg_216
lw $t0, reg_216
lw $t1, reg_212
sw $t0, ($t1)
lw $t0, reg_212
sw $t0, reg_219
lw $t0, reg_219
add $t2, $t0, 4
sw $t2, reg_219
li $t1, 48
sw $t1, reg_216
lw $t0, reg_216
lw $t1, reg_219
sw $t0, ($t1)
lw $t0, reg_212
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_1quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_224
li $t1, 1
sw $t1, reg_226
lw $t0, reg_226
lw $t1, reg_224
sw $t0, ($t1)
lw $t0, reg_224
sw $t0, reg_231
lw $t0, reg_231
add $t2, $t0, 4
sw $t2, reg_231
li $t1, 49
sw $t1, reg_226
lw $t0, reg_226
lw $t1, reg_231
sw $t0, ($t1)
lw $t0, reg_224
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_2quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_235
li $t1, 1
sw $t1, reg_237
lw $t0, reg_237
lw $t1, reg_235
sw $t0, ($t1)
lw $t0, reg_235
sw $t0, reg_240
lw $t0, reg_240
add $t2, $t0, 4
sw $t2, reg_240
li $t1, 50
sw $t1, reg_237
lw $t0, reg_237
lw $t1, reg_240
sw $t0, ($t1)
lw $t0, reg_235
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_3quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_242
li $t1, 1
sw $t1, reg_250
lw $t0, reg_250
lw $t1, reg_242
sw $t0, ($t1)
lw $t0, reg_242
sw $t0, reg_261
lw $t0, reg_261
add $t2, $t0, 4
sw $t2, reg_261
li $t1, 51
sw $t1, reg_250
lw $t0, reg_250
lw $t1, reg_261
sw $t0, ($t1)
lw $t0, reg_242
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_4quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_264
li $t1, 1
sw $t1, reg_265
lw $t0, reg_265
lw $t1, reg_264
sw $t0, ($t1)
lw $t0, reg_264
sw $t0, reg_266
lw $t0, reg_266
add $t2, $t0, 4
sw $t2, reg_266
li $t1, 52
sw $t1, reg_265
lw $t0, reg_265
lw $t1, reg_266
sw $t0, ($t1)
lw $t0, reg_264
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_5quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_267
li $t1, 1
sw $t1, reg_268
lw $t0, reg_268
lw $t1, reg_267
sw $t0, ($t1)
lw $t0, reg_267
sw $t0, reg_269
lw $t0, reg_269
add $t2, $t0, 4
sw $t2, reg_269
li $t1, 53
sw $t1, reg_268
lw $t0, reg_268
lw $t1, reg_269
sw $t0, ($t1)
lw $t0, reg_267
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_6quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_270
li $t1, 1
sw $t1, reg_271
lw $t0, reg_271
lw $t1, reg_270
sw $t0, ($t1)
lw $t0, reg_270
sw $t0, reg_272
lw $t0, reg_272
add $t2, $t0, 4
sw $t2, reg_272
li $t1, 54
sw $t1, reg_271
lw $t0, reg_271
lw $t1, reg_272
sw $t0, ($t1)
lw $t0, reg_270
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_7quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_273
li $t1, 1
sw $t1, reg_274
lw $t0, reg_274
lw $t1, reg_273
sw $t0, ($t1)
lw $t0, reg_273
sw $t0, reg_275
lw $t0, reg_275
add $t2, $t0, 4
sw $t2, reg_275
li $t1, 55
sw $t1, reg_274
lw $t0, reg_274
lw $t1, reg_275
sw $t0, ($t1)
lw $t0, reg_273
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_8quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_276
li $t1, 1
sw $t1, reg_277
lw $t0, reg_277
lw $t1, reg_276
sw $t0, ($t1)
lw $t0, reg_276
sw $t0, reg_278
lw $t0, reg_278
add $t2, $t0, 4
sw $t2, reg_278
li $t1, 56
sw $t1, reg_277
lw $t0, reg_277
lw $t1, reg_278
sw $t0, ($t1)
lw $t0, reg_276
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_9quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_279
li $t1, 1
sw $t1, reg_280
lw $t0, reg_280
lw $t1, reg_279
sw $t0, ($t1)
lw $t0, reg_279
sw $t0, reg_281
lw $t0, reg_281
add $t2, $t0, 4
sw $t2, reg_281
li $t1, 57
sw $t1, reg_280
lw $t0, reg_280
lw $t1, reg_281
sw $t0, ($t1)
lw $t0, reg_279
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin_string_generate_quote_sub_quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
sw $v0, reg_0
li $t1, 1
sw $t1, reg_1
lw $t0, reg_1
lw $t1, reg_0
sw $t0, ($t1)
lw $t0, reg_0
sw $t0, reg_2
lw $t0, reg_2
add $t2, $t0, 4
sw $t2, reg_2
li $t1, 45
sw $t1, reg_1
lw $t0, reg_1
lw $t1, reg_2
sw $t0, ($t1)
lw $t0, reg_0
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel_printBoard:
sw $31, ($sp)
sub $sp, $sp, 4
li $t1, 0
sw $t1, reg_5
lw $t0, reg_5
sw $t0, reg_3
j AtLabel_for_cond_18
AtLabel_for_exp3_20:
nop
lw $t0, reg_3
sw $t0, reg_6
lw $t0, reg_3
add $t2, $t0, 1
sw $t2, reg_3
AtLabel_for_cond_18:
nop
lw $t0, reg_3
lw $t1, reg_282
slt $t2, $t0, $t1
sw $t2, reg_7
lw $t0, reg_7
beq $t0, 1, AtLabel_for_body_19
j AtLabel_for_exit_21
AtLabel_for_body_19:
nop
li $t1, 0
sw $t1, reg_8
lw $t0, reg_8
sw $t0, reg_4
j AtLabel_for_cond_22
AtLabel_for_exp3_24:
nop
lw $t0, reg_4
sw $t0, reg_9
lw $t0, reg_4
add $t2, $t0, 1
sw $t2, reg_4
AtLabel_for_cond_22:
nop
lw $t0, reg_4
lw $t1, reg_282
slt $t2, $t0, $t1
sw $t2, reg_10
lw $t0, reg_10
beq $t0, 1, AtLabel_for_body_23
j AtLabel_for_exit_25
AtLabel_for_body_23:
nop
lw $t0, reg_3
mul $t2, $t0, 4
sw $t2, reg_12
lw $t0, reg_12
lw $t1, reg_284
add $t2, $t0, $t1
sw $t2, reg_12
lw $t0, reg_12
add $t2, $t0, 4
sw $t2, reg_12
lw $t0, reg_12
lw $t1, ($t0)
sw $t1, reg_13
lw $t0, reg_12
sw $t0, reg_14
lw $t0, reg_13
lw $t1, reg_4
seq $t2, $t0, $t1
sw $t2, reg_11
lw $t0, reg_11
beq $t0, 1, AtLabel_if_body1_26
j AtLabel_if_body2_27
AtLabel_if_body1_26:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_space_Oquote_
lw $t0, reg_287
sw $t0, reg_16
li $v0, 4
lw $a0, reg_16
add $a0, $a0, 4
syscall
lw $t0, reg_287
sw $t0, reg_15
j AtLabel_if_exit_28
AtLabel_if_body2_27:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_space_.quote_
lw $t0, reg_287
sw $t0, reg_18
li $v0, 4
lw $a0, reg_18
add $a0, $a0, 4
syscall
lw $t0, reg_287
sw $t0, reg_17
AtLabel_if_exit_28:
nop
j AtLabel_for_exp3_24
AtLabel_for_exit_25:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_quote_
lw $t0, reg_287
sw $t0, reg_20
li $v0, 4
lw $a0, reg_20
add $a0, $a0, 4
syscall
la $a0, __CharEnter
syscall
lw $t0, reg_287
sw $t0, reg_19
j AtLabel_for_exp3_20
AtLabel_for_exit_21:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_quote_
lw $t0, reg_287
sw $t0, reg_22
li $v0, 4
lw $a0, reg_22
add $a0, $a0, 4
syscall
la $a0, __CharEnter
syscall
lw $t0, reg_287
sw $t0, reg_21
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel_search:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_23
lw $t0, reg_23
lw $t1, reg_282
seq $t2, $t0, $t1
sw $t2, reg_24
lw $t0, reg_24
beq $t0, 1, AtLabel_if_body1_29
j AtLabel_if_body2_30
AtLabel_if_body1_29:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel_printBoard
lw $t0, reg_287
sw $t0, reg_25
j AtLabel_if_exit_31
AtLabel_if_body2_30:
nop
li $t1, 0
sw $t1, reg_27
lw $t0, reg_27
sw $t0, reg_26
j AtLabel_for_cond_32
AtLabel_for_exp3_34:
nop
lw $t0, reg_26
sw $t0, reg_28
lw $t0, reg_26
add $t2, $t0, 1
sw $t2, reg_26
AtLabel_for_cond_32:
nop
lw $t0, reg_26
lw $t1, reg_282
slt $t2, $t0, $t1
sw $t2, reg_29
lw $t0, reg_29
beq $t0, 1, AtLabel_for_body_33
j AtLabel_for_exit_35
AtLabel_for_body_33:
nop
li $t1, 1
sw $t1, reg_30
lw $t0, reg_26
mul $t2, $t0, 4
sw $t2, reg_32
lw $t0, reg_32
lw $t1, reg_283
add $t2, $t0, $t1
sw $t2, reg_32
lw $t0, reg_32
add $t2, $t0, 4
sw $t2, reg_32
lw $t0, reg_32
lw $t1, ($t0)
sw $t1, reg_33
lw $t0, reg_32
sw $t0, reg_34
li $t1, 0
sw $t1, reg_35
lw $t0, reg_33
lw $t1, reg_35
seq $t2, $t0, $t1
sw $t2, reg_31
lw $t0, reg_31
beq $t0, 1, AtLabel_40
j AtLabel_38
AtLabel_40:
nop
li $t1, 0
sw $t1, reg_39
lw $t0, reg_39
mul $t2, $t0, 4
sw $t2, reg_37
lw $t0, reg_37
lw $t1, reg_285
add $t2, $t0, $t1
sw $t2, reg_37
lw $t0, reg_37
add $t2, $t0, 4
sw $t2, reg_37
lw $t0, reg_37
lw $t1, ($t0)
sw $t1, reg_38
lw $t0, reg_37
sw $t0, reg_40
lw $t0, reg_26
lw $t1, reg_23
add $t2, $t0, $t1
sw $t2, reg_42
lw $t0, reg_42
mul $t2, $t0, 4
sw $t2, reg_37
lw $t0, reg_37
lw $t1, reg_38
add $t2, $t0, $t1
sw $t2, reg_37
lw $t0, reg_37
add $t2, $t0, 4
sw $t2, reg_37
lw $t0, reg_37
lw $t1, ($t0)
sw $t1, reg_41
lw $t0, reg_37
sw $t0, reg_43
li $t1, 0
sw $t1, reg_44
lw $t0, reg_41
lw $t1, reg_44
seq $t2, $t0, $t1
sw $t2, reg_36
lw $t0, reg_36
beq $t0, 1, AtLabel_41
j AtLabel_38
AtLabel_41:
nop
li $t1, 1
sw $t1, reg_48
lw $t0, reg_48
mul $t2, $t0, 4
sw $t2, reg_46
lw $t0, reg_46
lw $t1, reg_285
add $t2, $t0, $t1
sw $t2, reg_46
lw $t0, reg_46
add $t2, $t0, 4
sw $t2, reg_46
lw $t0, reg_46
lw $t1, ($t0)
sw $t1, reg_47
lw $t0, reg_46
sw $t0, reg_49
lw $t0, reg_26
lw $t1, reg_282
add $t2, $t0, $t1
sw $t2, reg_51
li $t1, 1
sw $t1, reg_52
lw $t0, reg_51
lw $t1, reg_52
sub $t2, $t0, $t1
sw $t2, reg_51
lw $t0, reg_51
lw $t1, reg_23
sub $t2, $t0, $t1
sw $t2, reg_51
lw $t0, reg_51
mul $t2, $t0, 4
sw $t2, reg_46
lw $t0, reg_46
lw $t1, reg_47
add $t2, $t0, $t1
sw $t2, reg_46
lw $t0, reg_46
add $t2, $t0, 4
sw $t2, reg_46
lw $t0, reg_46
lw $t1, ($t0)
sw $t1, reg_50
lw $t0, reg_46
sw $t0, reg_53
li $t1, 0
sw $t1, reg_54
lw $t0, reg_50
lw $t1, reg_54
seq $t2, $t0, $t1
sw $t2, reg_45
lw $t0, reg_45
beq $t0, 1, AtLabel_42
j AtLabel_38
AtLabel_42:
nop
j AtLabel_39
AtLabel_38:
li $t1, 0
sw $t1, reg_30
AtLabel_39:
nop
lw $t0, reg_30
beq $t0, 1, AtLabel_if_body1_36
j AtLabel_if_exit_37
AtLabel_if_body1_36:
nop
li $t1, 1
sw $t1, reg_56
li $t1, 1
sw $t1, reg_59
lw $t0, reg_59
mul $t2, $t0, 4
sw $t2, reg_57
lw $t0, reg_57
lw $t1, reg_285
add $t2, $t0, $t1
sw $t2, reg_57
lw $t0, reg_57
add $t2, $t0, 4
sw $t2, reg_57
lw $t0, reg_57
lw $t1, ($t0)
sw $t1, reg_58
lw $t0, reg_57
sw $t0, reg_61
lw $t0, reg_26
lw $t1, reg_282
add $t2, $t0, $t1
sw $t2, reg_63
li $t1, 1
sw $t1, reg_65
lw $t0, reg_63
lw $t1, reg_65
sub $t2, $t0, $t1
sw $t2, reg_63
lw $t0, reg_63
lw $t1, reg_23
sub $t2, $t0, $t1
sw $t2, reg_63
lw $t0, reg_63
mul $t2, $t0, 4
sw $t2, reg_57
lw $t0, reg_57
lw $t1, reg_58
add $t2, $t0, $t1
sw $t2, reg_57
lw $t0, reg_57
add $t2, $t0, 4
sw $t2, reg_57
lw $t0, reg_57
lw $t1, ($t0)
sw $t1, reg_62
lw $t0, reg_57
sw $t0, reg_66
lw $t0, reg_56
sw $t0, reg_62
lw $t0, reg_62
lw $t1, reg_66
sw $t0, ($t1)
li $t1, 0
sw $t1, reg_69
lw $t0, reg_69
mul $t2, $t0, 4
sw $t2, reg_67
lw $t0, reg_67
lw $t1, reg_285
add $t2, $t0, $t1
sw $t2, reg_67
lw $t0, reg_67
add $t2, $t0, 4
sw $t2, reg_67
lw $t0, reg_67
lw $t1, ($t0)
sw $t1, reg_68
lw $t0, reg_67
sw $t0, reg_70
lw $t0, reg_26
lw $t1, reg_23
add $t2, $t0, $t1
sw $t2, reg_72
lw $t0, reg_72
mul $t2, $t0, 4
sw $t2, reg_67
lw $t0, reg_67
lw $t1, reg_68
add $t2, $t0, $t1
sw $t2, reg_67
lw $t0, reg_67
add $t2, $t0, 4
sw $t2, reg_67
lw $t0, reg_67
lw $t1, ($t0)
sw $t1, reg_71
lw $t0, reg_67
sw $t0, reg_73
lw $t0, reg_62
sw $t0, reg_71
lw $t0, reg_71
lw $t1, reg_73
sw $t0, ($t1)
lw $t0, reg_26
mul $t2, $t0, 4
sw $t2, reg_74
lw $t0, reg_74
lw $t1, reg_283
add $t2, $t0, $t1
sw $t2, reg_74
lw $t0, reg_74
add $t2, $t0, 4
sw $t2, reg_74
lw $t0, reg_74
lw $t1, ($t0)
sw $t1, reg_75
lw $t0, reg_74
sw $t0, reg_77
lw $t0, reg_71
sw $t0, reg_75
lw $t0, reg_75
lw $t1, reg_77
sw $t0, ($t1)
lw $t0, reg_23
mul $t2, $t0, 4
sw $t2, reg_78
lw $t0, reg_78
lw $t1, reg_284
add $t2, $t0, $t1
sw $t2, reg_78
lw $t0, reg_78
add $t2, $t0, 4
sw $t2, reg_78
lw $t0, reg_78
lw $t1, ($t0)
sw $t1, reg_79
lw $t0, reg_78
sw $t0, reg_80
lw $t0, reg_26
sw $t0, reg_79
lw $t0, reg_79
lw $t1, reg_80
sw $t0, ($t1)
li $t1, 1
sw $t1, reg_83
lw $t0, reg_23
lw $t1, reg_83
add $t2, $t0, $t1
sw $t2, reg_82
lw $t0, reg_23
sw $t0, 0($sp)
lw $t0, reg_26
sw $t0, -4($sp)
lw $t0, reg_34
sw $t0, -8($sp)
lw $t0, reg_40
sw $t0, -12($sp)
lw $t0, reg_42
sw $t0, -16($sp)
lw $t0, reg_43
sw $t0, -20($sp)
lw $t0, reg_49
sw $t0, -24($sp)
lw $t0, reg_51
sw $t0, -28($sp)
lw $t0, reg_53
sw $t0, -32($sp)
lw $t0, reg_61
sw $t0, -36($sp)
lw $t0, reg_63
sw $t0, -40($sp)
lw $t0, reg_66
sw $t0, -44($sp)
lw $t0, reg_70
sw $t0, -48($sp)
lw $t0, reg_72
sw $t0, -52($sp)
lw $t0, reg_73
sw $t0, -56($sp)
lw $t0, reg_77
sw $t0, -60($sp)
lw $t0, reg_80
sw $t0, -64($sp)
lw $t0, reg_82
sw $t0, -68($sp)
lw $t0, reg_89
sw $t0, -72($sp)
lw $t0, reg_91
sw $t0, -76($sp)
lw $t0, reg_93
sw $t0, -80($sp)
lw $t0, reg_98
sw $t0, -84($sp)
lw $t0, reg_100
sw $t0, -88($sp)
lw $t0, reg_101
sw $t0, -92($sp)
lw $t0, reg_104
sw $t0, -96($sp)
sub $sp, $sp, 100
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_82
sw $t0, -4($fp)
sub $sp, $sp, 8
jal AtfunctionLabel_search
add $sp, $sp, 100
lw $t0, 0($sp)
sw $t0, reg_23
lw $t0, -4($sp)
sw $t0, reg_26
lw $t0, -8($sp)
sw $t0, reg_34
lw $t0, -12($sp)
sw $t0, reg_40
lw $t0, -16($sp)
sw $t0, reg_42
lw $t0, -20($sp)
sw $t0, reg_43
lw $t0, -24($sp)
sw $t0, reg_49
lw $t0, -28($sp)
sw $t0, reg_51
lw $t0, -32($sp)
sw $t0, reg_53
lw $t0, -36($sp)
sw $t0, reg_61
lw $t0, -40($sp)
sw $t0, reg_63
lw $t0, -44($sp)
sw $t0, reg_66
lw $t0, -48($sp)
sw $t0, reg_70
lw $t0, -52($sp)
sw $t0, reg_72
lw $t0, -56($sp)
sw $t0, reg_73
lw $t0, -60($sp)
sw $t0, reg_77
lw $t0, -64($sp)
sw $t0, reg_80
lw $t0, -68($sp)
sw $t0, reg_82
lw $t0, -72($sp)
sw $t0, reg_89
lw $t0, -76($sp)
sw $t0, reg_91
lw $t0, -80($sp)
sw $t0, reg_93
lw $t0, -84($sp)
sw $t0, reg_98
lw $t0, -88($sp)
sw $t0, reg_100
lw $t0, -92($sp)
sw $t0, reg_101
lw $t0, -96($sp)
sw $t0, reg_104
lw $t0, reg_287
sw $t0, reg_81
li $t1, 0
sw $t1, reg_84
li $t1, 1
sw $t1, reg_88
lw $t0, reg_88
mul $t2, $t0, 4
sw $t2, reg_86
lw $t0, reg_86
lw $t1, reg_285
add $t2, $t0, $t1
sw $t2, reg_86
lw $t0, reg_86
add $t2, $t0, 4
sw $t2, reg_86
lw $t0, reg_86
lw $t1, ($t0)
sw $t1, reg_87
lw $t0, reg_86
sw $t0, reg_89
lw $t0, reg_26
lw $t1, reg_282
add $t2, $t0, $t1
sw $t2, reg_91
li $t1, 1
sw $t1, reg_92
lw $t0, reg_91
lw $t1, reg_92
sub $t2, $t0, $t1
sw $t2, reg_91
lw $t0, reg_91
lw $t1, reg_23
sub $t2, $t0, $t1
sw $t2, reg_91
lw $t0, reg_91
mul $t2, $t0, 4
sw $t2, reg_86
lw $t0, reg_86
lw $t1, reg_87
add $t2, $t0, $t1
sw $t2, reg_86
lw $t0, reg_86
add $t2, $t0, 4
sw $t2, reg_86
lw $t0, reg_86
lw $t1, ($t0)
sw $t1, reg_90
lw $t0, reg_86
sw $t0, reg_93
lw $t0, reg_84
sw $t0, reg_90
lw $t0, reg_90
lw $t1, reg_93
sw $t0, ($t1)
li $t1, 0
sw $t1, reg_97
lw $t0, reg_97
mul $t2, $t0, 4
sw $t2, reg_94
lw $t0, reg_94
lw $t1, reg_285
add $t2, $t0, $t1
sw $t2, reg_94
lw $t0, reg_94
add $t2, $t0, 4
sw $t2, reg_94
lw $t0, reg_94
lw $t1, ($t0)
sw $t1, reg_95
lw $t0, reg_94
sw $t0, reg_98
lw $t0, reg_26
lw $t1, reg_23
add $t2, $t0, $t1
sw $t2, reg_100
lw $t0, reg_100
mul $t2, $t0, 4
sw $t2, reg_94
lw $t0, reg_94
lw $t1, reg_95
add $t2, $t0, $t1
sw $t2, reg_94
lw $t0, reg_94
add $t2, $t0, 4
sw $t2, reg_94
lw $t0, reg_94
lw $t1, ($t0)
sw $t1, reg_99
lw $t0, reg_94
sw $t0, reg_101
lw $t0, reg_90
sw $t0, reg_99
lw $t0, reg_99
lw $t1, reg_101
sw $t0, ($t1)
lw $t0, reg_26
mul $t2, $t0, 4
sw $t2, reg_102
lw $t0, reg_102
lw $t1, reg_283
add $t2, $t0, $t1
sw $t2, reg_102
lw $t0, reg_102
add $t2, $t0, 4
sw $t2, reg_102
lw $t0, reg_102
lw $t1, ($t0)
sw $t1, reg_103
lw $t0, reg_102
sw $t0, reg_104
lw $t0, reg_99
sw $t0, reg_103
lw $t0, reg_103
lw $t1, reg_104
sw $t0, ($t1)
AtLabel_if_exit_37:
nop
j AtLabel_for_exp3_34
AtLabel_for_exit_35:
nop
AtLabel_if_exit_31:
nop
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
main:
move $fp, $sp
li $t0, 32
sw $t0, __CharSpace
li $t0, 10
sw $t0, __CharEnter
sw $31, ($sp)
sub $sp, $sp, 4
la $t0, static
sw $t0, reg_286
li $t1, 1
sw $t1, reg_55
lw $t0, reg_55
lw $t2, reg_286
add $t2, $t2, 16
sw $t0, ($t2)
li $t1, 1
sw $t1, reg_60
lw $t0, reg_60
lw $t2, reg_286
add $t2, $t2, 20
sw $t0, ($t2)
li $t1, 1
sw $t1, reg_64
lw $t0, reg_64
lw $t2, reg_286
add $t2, $t2, 24
sw $t0, ($t2)
lw $t0, reg_286
add $t2, $t0, 0
sw $t2, reg_282
lw $t0, reg_286
add $t2, $t0, 12
sw $t2, reg_285
lw $t0, reg_286
add $t2, $t0, 16
sw $t2, reg_55
lw $t0, reg_286
add $t2, $t0, 8
sw $t2, reg_284
lw $t0, reg_286
add $t2, $t0, 20
sw $t2, reg_60
lw $t0, reg_286
add $t2, $t0, 24
sw $t2, reg_64
lw $t0, reg_286
add $t2, $t0, 4
sw $t2, reg_283
li $t1, 8
sw $t1, reg_76
lw $t0, reg_76
sw $t0, reg_282
lw $t0, reg_282
lw $t2, reg_286
add $t2, $t2, 0
sw $t0, ($t2)
li $t1, 8
sw $t1, reg_96
lw $t0, reg_96
sw $t0, reg_107
lw $t0, reg_107
mul $t2, $t0, 4
sw $t2, reg_111
lw $t0, reg_111
add $t2, $t0, 4
sw $t2, reg_111
li $v0, 9
lw $a0, reg_111
syscall
sw $v0, reg_85
lw $t0, reg_107
lw $t1, reg_85
sw $t0, ($t1)
lw $t0, reg_85
sw $t0, reg_283
lw $t0, reg_283
lw $t2, reg_286
add $t2, $t2, 4
sw $t0, ($t2)
li $t1, 8
sw $t1, reg_134
lw $t0, reg_134
sw $t0, reg_141
lw $t0, reg_141
mul $t2, $t0, 4
sw $t2, reg_144
lw $t0, reg_144
add $t2, $t0, 4
sw $t2, reg_144
li $v0, 9
lw $a0, reg_144
syscall
sw $v0, reg_120
lw $t0, reg_141
lw $t1, reg_120
sw $t0, ($t1)
lw $t0, reg_120
sw $t0, reg_284
lw $t0, reg_284
lw $t2, reg_286
add $t2, $t2, 8
sw $t0, ($t2)
li $t1, 2
sw $t1, reg_161
lw $t0, reg_161
sw $t0, reg_169
lw $t0, reg_169
mul $t2, $t0, 4
sw $t2, reg_174
lw $t0, reg_174
add $t2, $t0, 4
sw $t2, reg_174
li $v0, 9
lw $a0, reg_174
syscall
sw $v0, reg_152
lw $t0, reg_169
lw $t1, reg_152
sw $t0, ($t1)
lw $t0, reg_152
sw $t0, reg_285
lw $t0, reg_285
lw $t2, reg_286
add $t2, $t2, 12
sw $t0, ($t2)
li $t1, 0
sw $t1, reg_106
lw $t0, reg_106
sw $t0, reg_105
j AtLabel_for_cond_43
AtLabel_for_exp3_45:
nop
lw $t0, reg_105
sw $t0, reg_108
lw $t0, reg_105
add $t2, $t0, 1
sw $t2, reg_105
AtLabel_for_cond_43:
nop
li $t1, 2
sw $t1, reg_110
lw $t0, reg_105
lw $t1, reg_110
slt $t2, $t0, $t1
sw $t2, reg_109
lw $t0, reg_109
beq $t0, 1, AtLabel_for_body_44
j AtLabel_for_exit_46
AtLabel_for_body_44:
nop
li $t1, 8
sw $t1, reg_114
li $t1, 8
sw $t1, reg_115
lw $t0, reg_114
lw $t1, reg_115
add $t2, $t0, $t1
sw $t2, reg_113
li $t1, 1
sw $t1, reg_116
lw $t0, reg_113
lw $t1, reg_116
sub $t2, $t0, $t1
sw $t2, reg_113
lw $t0, reg_113
sw $t0, reg_117
lw $t0, reg_117
mul $t2, $t0, 4
sw $t2, reg_118
lw $t0, reg_118
add $t2, $t0, 4
sw $t2, reg_118
li $v0, 9
lw $a0, reg_118
syscall
sw $v0, reg_112
lw $t0, reg_117
lw $t1, reg_112
sw $t0, ($t1)
lw $t0, reg_105
mul $t2, $t0, 4
sw $t2, reg_119
lw $t0, reg_119
lw $t1, reg_285
add $t2, $t0, $t1
sw $t2, reg_119
lw $t0, reg_119
add $t2, $t0, 4
sw $t2, reg_119
lw $t0, reg_119
lw $t1, ($t0)
sw $t1, reg_121
lw $t0, reg_119
sw $t0, reg_122
lw $t0, reg_112
sw $t0, reg_121
lw $t0, reg_121
lw $t1, reg_122
sw $t0, ($t1)
j AtLabel_for_exp3_45
AtLabel_for_exit_46:
nop
li $t1, 0
sw $t1, reg_124
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_124
sw $t0, -4($fp)
sub $sp, $sp, 8
jal AtfunctionLabel_search
lw $t0, reg_287
sw $t0, reg_123
li $t1, 0
sw $t1, reg_125
lw $t0, reg_125
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel_print:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_126
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel_println:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_127
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel_getString:
sw $31, ($sp)
sub $sp, $sp, 4
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel_getInt:
sw $31, ($sp)
sub $sp, $sp, 4
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel_toString:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_128
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_quote_
lw $t0, reg_287
sw $t0, reg_130
lw $t0, reg_130
sw $t0, reg_129
li $t1, 0
sw $t1, reg_132
lw $t0, reg_132
sw $t0, reg_131
li $t1, 0
sw $t1, reg_135
lw $t0, reg_128
lw $t1, reg_135
slt $t2, $t0, $t1
sw $t2, reg_133
lw $t0, reg_133
beq $t0, 1, AtLabel_if_body1_47
j AtLabel_if_exit_48
AtLabel_if_body1_47:
nop
li $t1, 1
sw $t1, reg_136
lw $t0, reg_136
sw $t0, reg_131
lw $t0, reg_128
neg $t0, $t0
add $t2, $t0, 0
sw $t2, reg_137
lw $t0, reg_137
sw $t0, reg_128
AtLabel_if_exit_48:
nop
li $t1, 0
sw $t1, reg_139
lw $t0, reg_128
lw $t1, reg_139
seq $t2, $t0, $t1
sw $t2, reg_138
lw $t0, reg_138
beq $t0, 1, AtLabel_if_body1_49
j AtLabel_if_exit_50
AtLabel_if_body1_49:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_0quote_
lw $t0, reg_287
sw $t0, reg_140
lw $t0, reg_140
sw $t0, reg_129
lw $t0, reg_129
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtLabel_if_exit_50:
nop
lw $t0, reg_128
sw $t0, reg_142
AtLabel_while_cond_51:
nop
li $t1, 0
sw $t1, reg_145
lw $t0, reg_142
lw $t1, reg_145
sne $t2, $t0, $t1
sw $t2, reg_143
lw $t0, reg_143
beq $t0, 1, AtLabel_while_start_53
j AtLabel_while_end_52
AtLabel_while_start_53:
nop
li $t1, 10
sw $t1, reg_148
lw $t0, reg_142
lw $t1, reg_148
div $t2, $t0, $t1
sw $t2, reg_149
lw $t0, reg_148
lw $t1, reg_149
mul $t2, $t0, $t1
sw $t2, reg_149
lw $t0, reg_142
lw $t1, reg_149
sub $t2, $t0, $t1
sw $t2, reg_147
li $t1, 0
sw $t1, reg_150
lw $t0, reg_147
lw $t1, reg_150
seq $t2, $t0, $t1
sw $t2, reg_146
lw $t0, reg_146
beq $t0, 1, AtLabel_if_body1_54
j AtLabel_if_body2_55
AtLabel_if_body1_54:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_0quote_
lw $t0, reg_287
sw $t0, reg_153
lw $t0, reg_287
sw $t0, reg_151
lw $t0, reg_151
sw $t0, reg_129
j AtLabel_if_exit_56
AtLabel_if_body2_55:
nop
li $t1, 10
sw $t1, reg_156
lw $t0, reg_142
lw $t1, reg_156
div $t2, $t0, $t1
sw $t2, reg_157
lw $t0, reg_156
lw $t1, reg_157
mul $t2, $t0, $t1
sw $t2, reg_157
lw $t0, reg_142
lw $t1, reg_157
sub $t2, $t0, $t1
sw $t2, reg_155
li $t1, 1
sw $t1, reg_158
lw $t0, reg_155
lw $t1, reg_158
seq $t2, $t0, $t1
sw $t2, reg_154
lw $t0, reg_154
beq $t0, 1, AtLabel_if_body1_57
j AtLabel_if_body2_58
AtLabel_if_body1_57:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_1quote_
lw $t0, reg_287
sw $t0, reg_160
lw $t0, reg_287
sw $t0, reg_159
lw $t0, reg_159
sw $t0, reg_129
j AtLabel_if_exit_59
AtLabel_if_body2_58:
nop
li $t1, 10
sw $t1, reg_164
lw $t0, reg_142
lw $t1, reg_164
div $t2, $t0, $t1
sw $t2, reg_165
lw $t0, reg_164
lw $t1, reg_165
mul $t2, $t0, $t1
sw $t2, reg_165
lw $t0, reg_142
lw $t1, reg_165
sub $t2, $t0, $t1
sw $t2, reg_163
li $t1, 2
sw $t1, reg_166
lw $t0, reg_163
lw $t1, reg_166
seq $t2, $t0, $t1
sw $t2, reg_162
lw $t0, reg_162
beq $t0, 1, AtLabel_if_body1_60
j AtLabel_if_body2_61
AtLabel_if_body1_60:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_2quote_
lw $t0, reg_287
sw $t0, reg_168
lw $t0, reg_287
sw $t0, reg_167
lw $t0, reg_167
sw $t0, reg_129
j AtLabel_if_exit_62
AtLabel_if_body2_61:
nop
li $t1, 10
sw $t1, reg_172
lw $t0, reg_142
lw $t1, reg_172
div $t2, $t0, $t1
sw $t2, reg_173
lw $t0, reg_172
lw $t1, reg_173
mul $t2, $t0, $t1
sw $t2, reg_173
lw $t0, reg_142
lw $t1, reg_173
sub $t2, $t0, $t1
sw $t2, reg_171
li $t1, 3
sw $t1, reg_175
lw $t0, reg_171
lw $t1, reg_175
seq $t2, $t0, $t1
sw $t2, reg_170
lw $t0, reg_170
beq $t0, 1, AtLabel_if_body1_63
j AtLabel_if_body2_64
AtLabel_if_body1_63:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_3quote_
lw $t0, reg_287
sw $t0, reg_177
lw $t0, reg_287
sw $t0, reg_176
lw $t0, reg_176
sw $t0, reg_129
j AtLabel_if_exit_65
AtLabel_if_body2_64:
nop
li $t1, 10
sw $t1, reg_182
lw $t0, reg_142
lw $t1, reg_182
div $t2, $t0, $t1
sw $t2, reg_183
lw $t0, reg_182
lw $t1, reg_183
mul $t2, $t0, $t1
sw $t2, reg_183
lw $t0, reg_142
lw $t1, reg_183
sub $t2, $t0, $t1
sw $t2, reg_180
li $t1, 4
sw $t1, reg_184
lw $t0, reg_180
lw $t1, reg_184
seq $t2, $t0, $t1
sw $t2, reg_179
lw $t0, reg_179
beq $t0, 1, AtLabel_if_body1_66
j AtLabel_if_body2_67
AtLabel_if_body1_66:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_4quote_
lw $t0, reg_287
sw $t0, reg_187
lw $t0, reg_287
sw $t0, reg_185
lw $t0, reg_185
sw $t0, reg_129
j AtLabel_if_exit_68
AtLabel_if_body2_67:
nop
li $t1, 10
sw $t1, reg_191
lw $t0, reg_142
lw $t1, reg_191
div $t2, $t0, $t1
sw $t2, reg_192
lw $t0, reg_191
lw $t1, reg_192
mul $t2, $t0, $t1
sw $t2, reg_192
lw $t0, reg_142
lw $t1, reg_192
sub $t2, $t0, $t1
sw $t2, reg_190
li $t1, 5
sw $t1, reg_194
lw $t0, reg_190
lw $t1, reg_194
seq $t2, $t0, $t1
sw $t2, reg_189
lw $t0, reg_189
beq $t0, 1, AtLabel_if_body1_69
j AtLabel_if_body2_70
AtLabel_if_body1_69:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_5quote_
lw $t0, reg_287
sw $t0, reg_196
lw $t0, reg_287
sw $t0, reg_195
lw $t0, reg_195
sw $t0, reg_129
j AtLabel_if_exit_71
AtLabel_if_body2_70:
nop
li $t1, 10
sw $t1, reg_201
lw $t0, reg_142
lw $t1, reg_201
div $t2, $t0, $t1
sw $t2, reg_202
lw $t0, reg_201
lw $t1, reg_202
mul $t2, $t0, $t1
sw $t2, reg_202
lw $t0, reg_142
lw $t1, reg_202
sub $t2, $t0, $t1
sw $t2, reg_199
li $t1, 6
sw $t1, reg_203
lw $t0, reg_199
lw $t1, reg_203
seq $t2, $t0, $t1
sw $t2, reg_198
lw $t0, reg_198
beq $t0, 1, AtLabel_if_body1_72
j AtLabel_if_body2_73
AtLabel_if_body1_72:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_6quote_
lw $t0, reg_287
sw $t0, reg_206
lw $t0, reg_287
sw $t0, reg_204
lw $t0, reg_204
sw $t0, reg_129
j AtLabel_if_exit_74
AtLabel_if_body2_73:
nop
li $t1, 10
sw $t1, reg_210
lw $t0, reg_142
lw $t1, reg_210
div $t2, $t0, $t1
sw $t2, reg_211
lw $t0, reg_210
lw $t1, reg_211
mul $t2, $t0, $t1
sw $t2, reg_211
lw $t0, reg_142
lw $t1, reg_211
sub $t2, $t0, $t1
sw $t2, reg_209
li $t1, 7
sw $t1, reg_213
lw $t0, reg_209
lw $t1, reg_213
seq $t2, $t0, $t1
sw $t2, reg_208
lw $t0, reg_208
beq $t0, 1, AtLabel_if_body1_75
j AtLabel_if_body2_76
AtLabel_if_body1_75:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_7quote_
lw $t0, reg_287
sw $t0, reg_215
lw $t0, reg_287
sw $t0, reg_214
lw $t0, reg_214
sw $t0, reg_129
j AtLabel_if_exit_77
AtLabel_if_body2_76:
nop
li $t1, 10
sw $t1, reg_220
lw $t0, reg_142
lw $t1, reg_220
div $t2, $t0, $t1
sw $t2, reg_221
lw $t0, reg_220
lw $t1, reg_221
mul $t2, $t0, $t1
sw $t2, reg_221
lw $t0, reg_142
lw $t1, reg_221
sub $t2, $t0, $t1
sw $t2, reg_218
li $t1, 8
sw $t1, reg_222
lw $t0, reg_218
lw $t1, reg_222
seq $t2, $t0, $t1
sw $t2, reg_217
lw $t0, reg_217
beq $t0, 1, AtLabel_if_body1_78
j AtLabel_if_body2_79
AtLabel_if_body1_78:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_8quote_
lw $t0, reg_287
sw $t0, reg_225
lw $t0, reg_287
sw $t0, reg_223
lw $t0, reg_223
sw $t0, reg_129
j AtLabel_if_exit_80
AtLabel_if_body2_79:
nop
li $t1, 10
sw $t1, reg_229
lw $t0, reg_142
lw $t1, reg_229
div $t2, $t0, $t1
sw $t2, reg_230
lw $t0, reg_229
lw $t1, reg_230
mul $t2, $t0, $t1
sw $t2, reg_230
lw $t0, reg_142
lw $t1, reg_230
sub $t2, $t0, $t1
sw $t2, reg_228
li $t1, 9
sw $t1, reg_232
lw $t0, reg_228
lw $t1, reg_232
seq $t2, $t0, $t1
sw $t2, reg_227
lw $t0, reg_227
beq $t0, 1, AtLabel_if_body1_81
j AtLabel_if_exit_82
AtLabel_if_body1_81:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_9quote_
lw $t0, reg_287
sw $t0, reg_234
lw $t0, reg_287
sw $t0, reg_233
lw $t0, reg_233
sw $t0, reg_129
AtLabel_if_exit_82:
nop
AtLabel_if_exit_80:
nop
AtLabel_if_exit_77:
nop
AtLabel_if_exit_74:
nop
AtLabel_if_exit_71:
nop
AtLabel_if_exit_68:
nop
AtLabel_if_exit_65:
nop
AtLabel_if_exit_62:
nop
AtLabel_if_exit_59:
nop
AtLabel_if_exit_56:
nop
li $t1, 10
sw $t1, reg_238
lw $t0, reg_142
lw $t1, reg_238
div $t2, $t0, $t1
sw $t2, reg_236
lw $t0, reg_236
sw $t0, reg_142
j AtLabel_while_cond_51
AtLabel_while_end_52:
nop
lw $t0, reg_131
beq $t0, 1, AtLabel_if_body1_83
j AtLabel_if_exit_84
AtLabel_if_body1_83:
nop
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_sub_quote_
lw $t0, reg_287
sw $t0, reg_241
lw $t0, reg_287
sw $t0, reg_239
lw $t0, reg_239
sw $t0, reg_129
AtLabel_if_exit_84:
nop
lw $t0, reg_129
sw $t0, reg_287
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__length:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_243
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__substring:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_244
lw $t0, -8($fp)
sw $t0, reg_245
lw $t0, -12($fp)
sw $t0, reg_246
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__parseInt:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_247
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__ord:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_248
lw $t0, -8($fp)
sw $t0, reg_249
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__string_add:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_251
lw $t0, -8($fp)
sw $t0, reg_252
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__string_cmp_EQ:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_253
lw $t0, -8($fp)
sw $t0, reg_254
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__string_cmp_LT:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_255
lw $t0, -8($fp)
sw $t0, reg_256
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__string_cmp_LE:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_257
lw $t0, -8($fp)
sw $t0, reg_258
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__string_cmp_GT:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_259
lw $t0, -8($fp)
sw $t0, reg_260
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31
AtfunctionLabel___builtin__string_cmp_GE:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_262
lw $t0, -8($fp)
sw $t0, reg_263
lw $31, 4($sp)
lw $t0, ($fp)
move $sp, $fp
move $fp, $t0
jr $31

.data
static:
	.space 28
reg_0: .space 4
reg_1: .space 4
reg_2: .space 4
reg_3: .space 4
reg_4: .space 4
reg_5: .space 4
reg_6: .space 4
reg_7: .space 4
reg_8: .space 4
reg_9: .space 4
reg_10: .space 4
reg_11: .space 4
reg_12: .space 4
reg_13: .space 4
reg_14: .space 4
reg_15: .space 4
reg_16: .space 4
reg_17: .space 4
reg_18: .space 4
reg_19: .space 4
reg_20: .space 4
reg_21: .space 4
reg_22: .space 4
reg_23: .space 4
reg_24: .space 4
reg_25: .space 4
reg_26: .space 4
reg_27: .space 4
reg_28: .space 4
reg_29: .space 4
reg_30: .space 4
reg_31: .space 4
reg_32: .space 4
reg_33: .space 4
reg_34: .space 4
reg_35: .space 4
reg_36: .space 4
reg_37: .space 4
reg_38: .space 4
reg_39: .space 4
reg_40: .space 4
reg_41: .space 4
reg_42: .space 4
reg_43: .space 4
reg_44: .space 4
reg_45: .space 4
reg_46: .space 4
reg_47: .space 4
reg_48: .space 4
reg_49: .space 4
reg_50: .space 4
reg_51: .space 4
reg_52: .space 4
reg_53: .space 4
reg_54: .space 4
reg_55: .space 4
reg_56: .space 4
reg_57: .space 4
reg_58: .space 4
reg_59: .space 4
reg_60: .space 4
reg_61: .space 4
reg_62: .space 4
reg_63: .space 4
reg_64: .space 4
reg_65: .space 4
reg_66: .space 4
reg_67: .space 4
reg_68: .space 4
reg_69: .space 4
reg_70: .space 4
reg_71: .space 4
reg_72: .space 4
reg_73: .space 4
reg_74: .space 4
reg_75: .space 4
reg_76: .space 4
reg_77: .space 4
reg_78: .space 4
reg_79: .space 4
reg_80: .space 4
reg_81: .space 4
reg_82: .space 4
reg_83: .space 4
reg_84: .space 4
reg_85: .space 4
reg_86: .space 4
reg_87: .space 4
reg_88: .space 4
reg_89: .space 4
reg_90: .space 4
reg_91: .space 4
reg_92: .space 4
reg_93: .space 4
reg_94: .space 4
reg_95: .space 4
reg_96: .space 4
reg_97: .space 4
reg_98: .space 4
reg_99: .space 4
reg_100: .space 4
reg_101: .space 4
reg_102: .space 4
reg_103: .space 4
reg_104: .space 4
reg_105: .space 4
reg_106: .space 4
reg_107: .space 4
reg_108: .space 4
reg_109: .space 4
reg_110: .space 4
reg_111: .space 4
reg_112: .space 4
reg_113: .space 4
reg_114: .space 4
reg_115: .space 4
reg_116: .space 4
reg_117: .space 4
reg_118: .space 4
reg_119: .space 4
reg_120: .space 4
reg_121: .space 4
reg_122: .space 4
reg_123: .space 4
reg_124: .space 4
reg_125: .space 4
reg_126: .space 4
reg_127: .space 4
reg_128: .space 4
reg_129: .space 4
reg_130: .space 4
reg_131: .space 4
reg_132: .space 4
reg_133: .space 4
reg_134: .space 4
reg_135: .space 4
reg_136: .space 4
reg_137: .space 4
reg_138: .space 4
reg_139: .space 4
reg_140: .space 4
reg_141: .space 4
reg_142: .space 4
reg_143: .space 4
reg_144: .space 4
reg_145: .space 4
reg_146: .space 4
reg_147: .space 4
reg_148: .space 4
reg_149: .space 4
reg_150: .space 4
reg_151: .space 4
reg_152: .space 4
reg_153: .space 4
reg_154: .space 4
reg_155: .space 4
reg_156: .space 4
reg_157: .space 4
reg_158: .space 4
reg_159: .space 4
reg_160: .space 4
reg_161: .space 4
reg_162: .space 4
reg_163: .space 4
reg_164: .space 4
reg_165: .space 4
reg_166: .space 4
reg_167: .space 4
reg_168: .space 4
reg_169: .space 4
reg_170: .space 4
reg_171: .space 4
reg_172: .space 4
reg_173: .space 4
reg_174: .space 4
reg_175: .space 4
reg_176: .space 4
reg_177: .space 4
reg_178: .space 4
reg_179: .space 4
reg_180: .space 4
reg_181: .space 4
reg_182: .space 4
reg_183: .space 4
reg_184: .space 4
reg_185: .space 4
reg_186: .space 4
reg_187: .space 4
reg_188: .space 4
reg_189: .space 4
reg_190: .space 4
reg_191: .space 4
reg_192: .space 4
reg_193: .space 4
reg_194: .space 4
reg_195: .space 4
reg_196: .space 4
reg_197: .space 4
reg_198: .space 4
reg_199: .space 4
reg_200: .space 4
reg_201: .space 4
reg_202: .space 4
reg_203: .space 4
reg_204: .space 4
reg_205: .space 4
reg_206: .space 4
reg_207: .space 4
reg_208: .space 4
reg_209: .space 4
reg_210: .space 4
reg_211: .space 4
reg_212: .space 4
reg_213: .space 4
reg_214: .space 4
reg_215: .space 4
reg_216: .space 4
reg_217: .space 4
reg_218: .space 4
reg_219: .space 4
reg_220: .space 4
reg_221: .space 4
reg_222: .space 4
reg_223: .space 4
reg_224: .space 4
reg_225: .space 4
reg_226: .space 4
reg_227: .space 4
reg_228: .space 4
reg_229: .space 4
reg_230: .space 4
reg_231: .space 4
reg_232: .space 4
reg_233: .space 4
reg_234: .space 4
reg_235: .space 4
reg_236: .space 4
reg_237: .space 4
reg_238: .space 4
reg_239: .space 4
reg_240: .space 4
reg_241: .space 4
reg_242: .space 4
reg_243: .space 4
reg_244: .space 4
reg_245: .space 4
reg_246: .space 4
reg_247: .space 4
reg_248: .space 4
reg_249: .space 4
reg_250: .space 4
reg_251: .space 4
reg_252: .space 4
reg_253: .space 4
reg_254: .space 4
reg_255: .space 4
reg_256: .space 4
reg_257: .space 4
reg_258: .space 4
reg_259: .space 4
reg_260: .space 4
reg_261: .space 4
reg_262: .space 4
reg_263: .space 4
reg_264: .space 4
reg_265: .space 4
reg_266: .space 4
reg_267: .space 4
reg_268: .space 4
reg_269: .space 4
reg_270: .space 4
reg_271: .space 4
reg_272: .space 4
reg_273: .space 4
reg_274: .space 4
reg_275: .space 4
reg_276: .space 4
reg_277: .space 4
reg_278: .space 4
reg_279: .space 4
reg_280: .space 4
reg_281: .space 4
reg_282: .space 4
reg_283: .space 4
reg_284: .space 4
reg_285: .space 4
reg_286: .space 4
reg_287: .space 4
reg_288: .space 4
reg_289: .space 4
reg_290: .space 4
reg_291: .space 4
reg_292: .space 4
reg_293: .space 4
reg_294: .space 4
reg_295: .space 4
reg_296: .space 4
reg_297: .space 4
__CharEnter: .space 4
__CharSpace: .space 4

