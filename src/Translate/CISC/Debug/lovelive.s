.text
AtfunctionLabel___builtin_string_generate_quote_space_quote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
move $25, $v0
li $24, 1
sw $24, ($25)
move $24, $25
add $24, $24, 4
li $23, 32
sw $23, ($24)
lw $23, reg_138
move $23, $25
sw $23, reg_138
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel___builtin_string_generate_quote_backslash_nquote_:
sw $31, ($sp)
sub $sp, $sp, 4
li $v0, 9
li $a0, 12
syscall
move $25, $v0
li $24, 1
sw $24, ($25)
move $24, $25
add $24, $24, 4
li $23, 10
sw $23, ($24)
lw $23, reg_138
move $23, $25
sw $23, reg_138
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_exchange:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_125
lw $t0, -8($fp)
sw $t0, reg_126
lw $25, reg_125
mul $24, $25, 4
lw $23, reg_136
add $24, $24, $23
lw $22, 4($24)
move $24, $22
lw $22, reg_126
mul $21, $22, 4
add $21, $21, $23
lw $20, 4($21)
mul $21, $25, 4
add $21, $21, $23
lw $19, 4($21)
add $18, $21, 4
move $19, $20
sw $19, ($18)
mul $19, $22, 4
add $19, $19, $23
lw $20, 4($19)
add $21, $19, 4
move $20, $24
sw $20, ($21)
sw $22, reg_126
sw $23, reg_136
sw $25, reg_125
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_makeHeap:
sw $31, ($sp)
sub $sp, $sp, 4
lw $25, reg_135
sub $24, $25, 1
li $23, 2
div $22, $24, $23
move $23, $22
li $22, 0
move $24, $22
sw $23, reg_4
sw $24, reg_5
sw $25, reg_135
AtLabel_while_cond_22:
li $25, 0
sw $25, reg_10
lw $t0, reg_4
lw $t1, reg_10
blt $t0, $t1, AtLabel_while_end_23
AtLabel_while_start_24:
li $25, 2
lw $24, reg_4
mul $23, $24, $25
move $25, $23
li $23, 1
li $22, 2
mul $21, $24, $22
add $22, $21, 1
sw $22, reg_14
sw $23, reg_13
sw $24, reg_4
sw $25, reg_5
lw $t0, reg_14
lw $t1, reg_135
bge $t0, $t1, AtLabel_27
li $25, 2
lw $24, reg_4
mul $23, $24, $25
add $25, $23, 1
mul $23, $25, 4
lw $25, reg_136
add $23, $23, $25
lw $22, 4($23)
li $23, 2
mul $21, $24, $23
mul $23, $21, 4
add $23, $23, $25
lw $21, 4($23)
sw $21, reg_23
sw $22, reg_18
sw $24, reg_4
sw $25, reg_136
lw $t0, reg_18
lw $t1, reg_23
bge $t0, $t1, AtLabel_27
j AtLabel_28
AtLabel_27:
li $25, 0
sw $25, reg_13
AtLabel_28:
lw $t0, reg_13
beq $t0, 1, AtLabel_if_body1_25
j AtLabel_if_exit_26
AtLabel_if_body1_25:
li $25, 2
lw $24, reg_4
mul $23, $24, $25
add $25, $23, 1
move $23, $25
sw $23, reg_5
sw $24, reg_4
AtLabel_if_exit_26:
lw $25, reg_4
mul $24, $25, 4
lw $23, reg_136
add $24, $24, $23
lw $22, 4($24)
lw $24, reg_5
mul $21, $24, 4
add $21, $21, $23
lw $20, 4($21)
sw $20, reg_32
sw $22, reg_30
sw $23, reg_136
sw $24, reg_5
sw $25, reg_4
lw $t0, reg_30
lw $t1, reg_32
ble $t0, $t1, AtLabel_if_exit_30
AtLabel_if_body1_29:
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_4
sw $t0, -4($fp)
lw $t0, reg_5
sw $t0, -8($fp)
sub $sp, $sp, 12
jal AtfunctionLabel_exchange
AtLabel_if_exit_30:
lw $25, reg_4
sub $24, $25, 1
move $25, $24
sw $25, reg_4
j AtLabel_while_cond_22
AtLabel_while_end_23:
li $25, 0
lw $24, reg_138
move $24, $25
sw $24, reg_138
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_adjustHeap:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_35
li $25, 0
move $24, $25
move $25, $24
move $24, $25
sw $24, reg_36
AtLabel_while_cond_31:
li $25, 2
lw $24, reg_36
mul $23, $24, $25
sw $23, reg_40
sw $24, reg_36
lw $t0, reg_40
lw $t1, reg_35
bge $t0, $t1, AtLabel_while_end_32
AtLabel_while_start_33:
li $25, 2
lw $24, reg_36
mul $23, $24, $25
move $25, $23
li $23, 1
li $22, 2
mul $21, $24, $22
add $22, $21, 1
sw $22, reg_45
sw $23, reg_44
sw $24, reg_36
sw $25, reg_37
lw $t0, reg_45
lw $t1, reg_35
bge $t0, $t1, AtLabel_36
li $25, 2
lw $24, reg_36
mul $23, $24, $25
add $25, $23, 1
mul $23, $25, 4
lw $25, reg_136
add $23, $23, $25
lw $22, 4($23)
li $23, 2
mul $21, $24, $23
mul $23, $21, 4
add $23, $23, $25
lw $21, 4($23)
sw $21, reg_56
sw $22, reg_50
sw $24, reg_36
sw $25, reg_136
lw $t0, reg_50
lw $t1, reg_56
bge $t0, $t1, AtLabel_36
j AtLabel_37
AtLabel_36:
li $25, 0
sw $25, reg_44
AtLabel_37:
lw $t0, reg_44
beq $t0, 1, AtLabel_if_body1_34
j AtLabel_if_exit_35
AtLabel_if_body1_34:
li $25, 2
lw $24, reg_36
mul $23, $24, $25
add $25, $23, 1
move $23, $25
sw $23, reg_37
sw $24, reg_36
AtLabel_if_exit_35:
lw $25, reg_36
mul $24, $25, 4
lw $23, reg_136
add $24, $24, $23
lw $22, 4($24)
lw $24, reg_37
mul $21, $24, 4
add $21, $21, $23
lw $20, 4($21)
sw $20, reg_68
sw $22, reg_66
sw $23, reg_136
sw $24, reg_37
sw $25, reg_36
lw $t0, reg_66
lw $t1, reg_68
ble $t0, $t1, AtLabel_if_body2_39
AtLabel_if_body1_38:
lw $25, reg_36
mul $24, $25, 4
lw $23, reg_136
add $24, $24, $23
lw $22, 4($24)
move $24, $22
lw $22, reg_37
mul $21, $22, 4
add $21, $21, $23
lw $20, 4($21)
mul $21, $25, 4
add $21, $21, $23
lw $25, 4($21)
add $19, $21, 4
move $25, $20
sw $25, ($19)
mul $25, $22, 4
add $25, $25, $23
lw $20, 4($25)
add $21, $25, 4
move $20, $24
sw $20, ($21)
move $20, $22
sw $20, reg_36
sw $23, reg_136
j AtLabel_if_exit_40
AtLabel_if_body2_39:
j AtLabel_while_end_32
AtLabel_if_exit_40:
j AtLabel_while_cond_31
AtLabel_while_end_32:
li $25, 0
lw $24, reg_138
move $24, $25
sw $24, reg_138
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_heapSort:
sw $31, ($sp)
sub $sp, $sp, 4
li $25, 0
move $24, $25
li $25, 0
move $23, $25
sw $23, reg_86
j AtLabel_for_cond_41
AtLabel_for_exp3_43:
lw $25, reg_86
add $24, $25, 1
move $25, $24
sw $25, reg_86
AtLabel_for_cond_41:
lw $t0, reg_86
lw $t1, reg_135
bge $t0, $t1, AtLabel_for_exit_44
AtLabel_for_body_42:
li $25, 0
mul $24, $25, 4
lw $25, reg_136
add $24, $24, $25
lw $23, 4($24)
move $24, $23
lw $23, reg_135
lw $22, reg_86
sub $21, $23, $22
sub $21, $21, 1
mul $20, $21, 4
add $20, $20, $25
lw $21, 4($20)
li $20, 0
mul $19, $20, 4
add $19, $19, $25
lw $20, 4($19)
add $18, $19, 4
move $20, $21
sw $20, ($18)
sub $20, $23, $22
sub $20, $20, 1
mul $21, $20, 4
add $21, $21, $25
lw $20, 4($21)
add $19, $21, 4
move $20, $24
sw $20, ($19)
sub $20, $23, $22
sub $20, $20, 1
sw $20, reg_104
sw $22, reg_86
sw $23, reg_135
sw $25, reg_136
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_104
sw $t0, -4($fp)
sub $sp, $sp, 8
jal AtfunctionLabel_adjustHeap
j AtLabel_for_exp3_43
AtLabel_for_exit_44:
li $25, 0
lw $24, reg_138
move $24, $25
sw $24, reg_138
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
main:
move $fp, $sp
li $t0, 32
li $t1, 1
la $a0, __CharSpace
sw $t1, ($a0)
sw $t0, 4($a0)
li $t0, 10
li $t1, 1
la $a0, __CharEnter
sw $t1, ($a0)
sw $t0, 4($a0)
sw $31, ($sp)
sub $sp, $sp, 4
la $t0, static
sw $t0, reg_137
li $25, 1
lw $24, reg_137
sw $25, 8($24)
li $25, 1
sw $25, 12($24)
li $25, 1
sw $25, 16($24)
add $25, $24, 4
add $23, $24, 0
sw $23, reg_135
sw $25, reg_136
li $v0, 5
syscall
sw $v0, reg_138
lw $25, reg_138
move $24, $25
sw $25, reg_138
move $25, $24
mul $23, $25, 4
add $23, $23, 4
li $v0, 9
move $a0, $23
syscall
move $22, $v0
sw $25, ($22)
move $25, $22
li $22, 0
move $23, $22
sw $23, reg_106
sw $24, reg_135
sw $25, reg_136
j AtLabel_for_cond_45
AtLabel_for_exp3_47:
lw $25, reg_106
add $24, $25, 1
move $25, $24
sw $25, reg_106
AtLabel_for_cond_45:
lw $25, reg_136
lw $24, ($25)
sw $24, reg_112
sw $25, reg_136
lw $t0, reg_106
lw $t1, reg_112
bge $t0, $t1, AtLabel_for_exit_48
AtLabel_for_body_46:
lw $25, reg_106
mul $24, $25, 4
lw $23, reg_136
add $24, $24, $23
lw $22, 4($24)
add $21, $24, 4
move $22, $25
sw $22, ($21)
sw $23, reg_136
sw $25, reg_106
j AtLabel_for_exp3_47
AtLabel_for_exit_48:
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel_makeHeap
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel_heapSort
li $25, 0
move $24, $25
sw $24, reg_106
j AtLabel_for_cond_49
AtLabel_for_exp3_51:
lw $25, reg_106
add $24, $25, 1
move $25, $24
sw $25, reg_106
AtLabel_for_cond_49:
lw $25, reg_136
lw $24, ($25)
sw $24, reg_118
sw $25, reg_136
lw $t0, reg_106
lw $t1, reg_118
bge $t0, $t1, AtLabel_for_exit_52
AtLabel_for_body_50:
lw $25, reg_106
mul $24, $25, 4
lw $23, reg_136
add $24, $24, $23
lw $22, 4($24)
sw $22, reg_120
sw $23, reg_136
sw $25, reg_106
li $v0, 1
lw $a0, reg_120
syscall
la $t0, __CharSpace
sw $t0, reg_138
lw $25, reg_138
move $24, $25
sw $25, reg_138
sw $24, reg_121
li $v0, 4
lw $a0, reg_121
add $a0, $a0, 4
syscall
j AtLabel_for_exp3_51
AtLabel_for_exit_52:
sw $fp, ($sp)
move $fp, $sp
sub $sp, $sp, 4
jal AtfunctionLabel___builtin_string_generate_quote_backslash_nquote_
lw $25, reg_138
move $24, $25
sw $25, reg_138
sw $24, reg_122
li $v0, 4
lw $a0, reg_122
add $a0, $a0, 4
syscall
li $25, 0
lw $24, reg_138
move $24, $25
sw $24, reg_138
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_print:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_124
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_getInt:
sw $31, ($sp)
sub $sp, $sp, 4
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel___builtin_printInt:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_132
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31

.data
static:
	.space 20
reg_0: .space 4
reg_1: .space 4
reg_2: .space 4
reg_3: .space 4
reg_4: .space 4
reg_5: .space 4
reg_6: .space 4
reg_7: .space 4
reg_8: .space 4
reg_9: .space 4
reg_10: .space 4
reg_11: .space 4
reg_12: .space 4
reg_13: .space 4
reg_14: .space 4
reg_15: .space 4
reg_16: .space 4
reg_17: .space 4
reg_18: .space 4
reg_19: .space 4
reg_20: .space 4
reg_21: .space 4
reg_22: .space 4
reg_23: .space 4
reg_24: .space 4
reg_25: .space 4
reg_26: .space 4
reg_27: .space 4
reg_28: .space 4
reg_29: .space 4
reg_30: .space 4
reg_31: .space 4
reg_32: .space 4
reg_33: .space 4
reg_34: .space 4
reg_35: .space 4
reg_36: .space 4
reg_37: .space 4
reg_38: .space 4
reg_39: .space 4
reg_40: .space 4
reg_41: .space 4
reg_42: .space 4
reg_43: .space 4
reg_44: .space 4
reg_45: .space 4
reg_46: .space 4
reg_47: .space 4
reg_48: .space 4
reg_49: .space 4
reg_50: .space 4
reg_51: .space 4
reg_52: .space 4
reg_53: .space 4
reg_54: .space 4
reg_55: .space 4
reg_56: .space 4
reg_57: .space 4
reg_58: .space 4
reg_59: .space 4
reg_60: .space 4
reg_61: .space 4
reg_62: .space 4
reg_63: .space 4
reg_64: .space 4
reg_65: .space 4
reg_66: .space 4
reg_67: .space 4
reg_68: .space 4
reg_69: .space 4
reg_70: .space 4
reg_71: .space 4
reg_72: .space 4
reg_73: .space 4
reg_74: .space 4
reg_75: .space 4
reg_76: .space 4
reg_77: .space 4
reg_78: .space 4
reg_79: .space 4
reg_80: .space 4
reg_81: .space 4
reg_82: .space 4
reg_83: .space 4
reg_84: .space 4
reg_85: .space 4
reg_86: .space 4
reg_87: .space 4
reg_88: .space 4
reg_89: .space 4
reg_90: .space 4
reg_91: .space 4
reg_92: .space 4
reg_93: .space 4
reg_94: .space 4
reg_95: .space 4
reg_96: .space 4
reg_97: .space 4
reg_98: .space 4
reg_99: .space 4
reg_100: .space 4
reg_101: .space 4
reg_102: .space 4
reg_103: .space 4
reg_104: .space 4
reg_105: .space 4
reg_106: .space 4
reg_107: .space 4
reg_108: .space 4
reg_109: .space 4
reg_110: .space 4
reg_111: .space 4
reg_112: .space 4
reg_113: .space 4
reg_114: .space 4
reg_115: .space 4
reg_116: .space 4
reg_117: .space 4
reg_118: .space 4
reg_119: .space 4
reg_120: .space 4
reg_121: .space 4
reg_122: .space 4
reg_123: .space 4
reg_124: .space 4
reg_125: .space 4
reg_126: .space 4
reg_127: .space 4
reg_128: .space 4
reg_129: .space 4
reg_130: .space 4
reg_131: .space 4
reg_132: .space 4
reg_133: .space 4
reg_134: .space 4
reg_135: .space 4
reg_136: .space 4
reg_137: .space 4
reg_138: .space 4
reg_139: .space 4
reg_140: .space 4
reg_141: .space 4
reg_142: .space 4
reg_143: .space 4
reg_144: .space 4
reg_145: .space 4
reg_146: .space 4
reg_147: .space 4
reg_148: .space 4
__CharEnter: .space 8
__CharSpace: .space 8

