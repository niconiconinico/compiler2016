#include <cstdio>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <fstream>
#include <map>
#include <cassert>

using namespace std;

map<string, int> label_pc;
class operation
{
public:
	string opcode;
	vector<string> operands;
	vector<int> real_operands;
};

operation get_context(string info, int pc)
{
	if (info[0] == '@')
	{
		operation ret;
		ret.opcode = "label";
		if (info.substr(1, 8) == "function")
		{
			int cur = 0;
			string tmp;
			bool string_mode = false;
			while (cur < info.size())
			{
				if (!string_mode && info[cur] == ' ')
				{
					break;
				}
				if (info[cur] == '\"')
				{
					string_mode ^= 1;
				}
				tmp = tmp + info[cur];
				cur++;
			}
			ret.operands.push_back(tmp);
			while (cur < info.size())
			{
				tmp = "";
				cur++;
				string_mode = false;
				while (cur < info.size())
				{
					if (!string_mode && info[cur] == ' ')
					{
						break;
					}
					if (info[cur] == '\"')
					{
						string_mode ^= 1;
					}
					tmp.push_back(info[cur++]);
				}
				ret.operands.push_back(tmp);
			}
			label_pc[tmp] = pc + 1;
		}
		else
		{
			info.pop_back();
			label_pc[info] = pc + 1;
			ret.operands.push_back(info);
		}
		return ret;
	}
	else
	{
		operation ret;
		int pos = 0;
		bool string_mode = false;
		while (pos < info.size())
		{
			if (info[pos] == ' ' && !string_mode)
				break;
			if (info[pos] == '\"')
				string_mode ^= 1;
			ret.opcode.push_back(info[pos]);
			pos++;
		}
		while (pos < info.size())
		{
			string_mode = false;
			string tmp = "";
			pos++;
			while (pos < info.size())
			{
				if (info[pos] == ' ' && !string_mode)
					break;
				if (info[pos] == '\"')
					string_mode ^= 1;
				tmp.push_back(info[pos++]);
			}
			ret.operands.push_back(tmp);
		}
		return ret;
	}
}

int string_to_int(string x)
{
	int delta = 0, mul = 1, ret = 0;
	if (x[0] == '-')
		mul = -1, delta = 1;
	for (int i = delta; i < x.size(); ++i)
	{
		ret = ret * 10 + x[i] - '0';
	}
	if (mul == -1)
	{
		printf("fuck");
	}
	return ret * mul;
}

class frame
{
public:
	vector<pair<int, int> > caller_saved_regs, callee_saved_regs;
	vector<int> local_vars;
	vector<int> parameters;
	int *ra;
	int callee_return_address;
	int return_pc;
	frame() {}
};

vector<operation> lines;

stack<frame> func_stack;
vector<unsigned char> heap;
vector<pair<int, bool> > regs;


int return_reg_id;
int static_base_reg;

map<string, int> dis;
void read_code()
{
	ifstream fin("ll_after.ir");
	string tmp;
	int pc = 0;
	while (getline(fin, tmp))
	{
		if (tmp == "")
			continue;
		operation cur_op = get_context(tmp, pc++);

		{
			if (cur_op.opcode == "label")
			{
				cur_op.real_operands.resize(cur_op.operands.size());
				if (tmp.substr(1, 8) == "function")
				{
					for (int i = 1; i < cur_op.operands.size(); ++i)
						dis[cur_op.operands[i]] = 0;
				}
				label_pc[cur_op.operands[0]] = pc; //point to next
			}
			else if (cur_op.opcode == "mallocStatic")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = (string_to_int(cur_op.operands[0]));
			}
			else if (cur_op.opcode == "mallocI")
			{
				cur_op.real_operands.resize(2);
				cur_op.real_operands[0] = string_to_int(cur_op.operands[0]);
				dis[cur_op.operands[1]] = 0;
			}
			else if (cur_op.opcode == "malloc")
			{
				cur_op.real_operands.resize(2);
				dis[cur_op.operands[0]] = 0;
				dis[cur_op.operands[1]] = 0;
			}
			else if (cur_op.opcode == "call")
			{
				cur_op.real_operands.resize(cur_op.operands.size() - 1);
				for (int i = 1; i < cur_op.operands.size(); ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "return")
			{
				cur_op.real_operands.resize(1);
				dis[cur_op.operands[0]] = 0;
			}
			else if (cur_op.opcode == "nop")
			{
			}
			else if (cur_op.opcode == "add")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "sub")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "mult")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "div")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "addI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "subI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "rsubI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "multI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "divI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "rdivI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "lshift")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "rshift")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "lshiftI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "rshiftI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "and")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "or")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "xor")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "andI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "orI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "xorI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "loadI")
			{
				cur_op.real_operands.resize(2);
				dis[cur_op.operands[1]] = 0;
				cur_op.real_operands[0] = string_to_int(cur_op.operands[0]);
			}
			else if (cur_op.opcode == "loadAI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "loadA0")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "store")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "storeAI")
			{
				cur_op.real_operands.resize(3);
				dis[cur_op.operands[0]] = dis[cur_op.operands[2]] = 0;
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (cur_op.opcode == "storeA0")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "i2i")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "jump")
			{
				cur_op.real_operands.resize(1);
				dis[cur_op.operands[0]] = 0;
			}
			else if (cur_op.opcode == "jumpI")
			{
				cur_op.real_operands.resize(1);
			}
			else if (cur_op.opcode == "cbr")
			{
				cur_op.real_operands.resize(1);
				dis[cur_op.operands[0]] = 0;
			}
			else if (cur_op.opcode == "cmp_LT")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_LE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_EQ")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_GE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_GT")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "cmp_NE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else if (cur_op.opcode == "load")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					dis[cur_op.operands[i]] = 0;
			}
			else
				assert(false);
		}
		lines.push_back(cur_op);
	}
	fin.close();
	int curId = 0;
	regs.resize(dis.size() + 10);
	for (map<string, int>::iterator i = dis.begin(); i != dis.end(); ++i)
	{
		i->second = curId++;
		if (i->first == "r_{global_return_register}")
			return_reg_id = curId - 1;
		if (i->first == "r_{global_base_address_register^_^}")
		{
			static_base_reg = curId - 1;
			regs[curId - 1].first = 0;
		}
		if ((i -> first).size() >= 9 && (i->first).substr(3, 6) == "global")
		{
			regs[curId - 1].second = true;
		}
	}
	for (int i = 0; i < lines.size(); ++i)
	{
		operation &cur_op = lines[i];

		{
			if (lines[i].opcode == "label")
			{
				if (lines[i].operands[0].size() >= 9 && lines[i].operands[0].substr(1, 8) == "function")
				{
					for (int i = 1; i < cur_op.operands.size(); ++i)
						cur_op.real_operands[i] = dis[cur_op.operands[i]];
				}
			}
			else if (lines[i].opcode == "mallocStatic")
			{
			}
			else if (lines[i].opcode == "mallocI")
			{
				cur_op.real_operands.resize(2);
				cur_op.real_operands[1] = dis[cur_op.operands[1]];
			}
			else if (lines[i].opcode == "malloc")
			{
				cur_op.real_operands.resize(2);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[1] = dis[cur_op.operands[1]];
			}
			else if (lines[i].opcode == "call")
			{
				cur_op.real_operands.resize(cur_op.operands.size() - 1);
				for (int i = 1; i < cur_op.operands.size(); ++i)
					cur_op.real_operands[i - 1] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "return")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "nop")
			{
			}
			else if (lines[i].opcode == "add")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "sub")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "mult")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "div")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "addI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "subI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "rsubI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "multI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "divI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "rdivI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "lshift")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "rshift")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "lshiftI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "rshiftI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "and")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "or")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "xor")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "andI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "orI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "xorI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "loadI")
			{
				cur_op.real_operands.resize(2);
				cur_op.real_operands[1] = dis[cur_op.operands[1]];
				cur_op.real_operands[0] = string_to_int(cur_op.operands[0]);
			}
			else if (lines[i].opcode == "loadAI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "loadA0")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "store")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "storeAI")
			{
				cur_op.real_operands.resize(3);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
				cur_op.real_operands[2] = dis[cur_op.operands[2]];
				cur_op.real_operands[1] = string_to_int(cur_op.operands[1]);
			}
			else if (lines[i].opcode == "storeA0")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "i2i")
			{
				cur_op.real_operands.resize(2);
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "jump")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "jumpI")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = label_pc[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "cbr")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = dis[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "cmp_LT")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_LE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_EQ")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_GE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_GT")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "cmp_NE")
			{
				cur_op.real_operands.resize(3);
				for (int i = 0; i < 3; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else if (lines[i].opcode == "load")
			{
				for (int i = 0; i < 2; ++i)
					cur_op.real_operands[i] = dis[cur_op.operands[i]];
			}
			else
				assert(false);
		}
	}
}

int pc = 0;

void init_frame(frame &new_frame, int nextPc)
{
	new_frame.return_pc = pc + 1;
	new_frame.ra = &func_stack.top().callee_return_address;
	set<int> local_vars;
	for (int i = nextPc; i < lines.size() && (lines[i].opcode != "label" || (lines[i].opcode == "label" && lines[i].operands[0].substr(0, 9) != "@function")); i++)
	{
		operation &cur_op = lines[i];
		{
			if (lines[i].opcode == "label")
			{
				for (int j = 1; j < lines[i].real_operands.size(); ++j)
				{
					local_vars.insert(lines[i].real_operands[j]);
				}
			}
			else if (lines[i].opcode == "mallocStatic")
			{
			}
			else if (lines[i].opcode == "mallocI")
			{
				local_vars.insert(lines[i].real_operands[1]);
			}
			else if (lines[i].opcode == "malloc")
			{
				local_vars.insert(lines[i].real_operands[0]);
				local_vars.insert(lines[i].real_operands[1]);
			}
			else if (lines[i].opcode == "call")
			{
				for (int i = 1; i < cur_op.operands.size(); ++i)
					local_vars.insert(cur_op.real_operands[i - 1]);
			}
			else if (lines[i].opcode == "return")
			{
				local_vars.insert(cur_op.real_operands[0]);
			}
			else if (lines[i].opcode == "nop")
			{
			}
			else if (lines[i].opcode == "add")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "sub")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "mult")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "div")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "addI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "subI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "rsubI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "multI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "divI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "rdivI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "lshift")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "rshift")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "lshiftI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "rshiftI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "and")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "or")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "xor")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "andI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "orI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "xorI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "loadI")
			{
				local_vars.insert(cur_op.real_operands[1]);
			}
			else if (lines[i].opcode == "loadAI")
			{
				local_vars.insert(cur_op.real_operands[0]);
				local_vars.insert(cur_op.real_operands[2]);
			}
			else if (lines[i].opcode == "loadA0")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "store")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "storeAI")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "storeA0")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "i2i")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "jump")
			{
				cur_op.real_operands.resize(1);
				local_vars.insert(cur_op.real_operands[0]);
			}
			else if (lines[i].opcode == "jumpI")
			{
				cur_op.real_operands.resize(1);
				cur_op.real_operands[0] = label_pc[cur_op.operands[0]];
			}
			else if (lines[i].opcode == "cbr")
			{
				local_vars.insert(cur_op.real_operands[0]);
			}
			else if (lines[i].opcode == "cmp_LT")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_LE")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_EQ")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_GE")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_GT")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "cmp_NE")
			{
				for (int i = 0; i < 3; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else if (lines[i].opcode == "load")
			{
				for (int i = 0; i < 2; ++i)
					local_vars.insert(cur_op.real_operands[i]);
			}
			else
				assert(false);
		}
	}
	for (set<int>::iterator i = local_vars.begin(); i != local_vars.end(); i++)
	{
		if (!regs[*i].second)
			new_frame.local_vars.push_back(*i);
	}
	func_stack.push(new_frame);
	pc = nextPc;
}

void process_instruction(operation instr)
{
	operation &cur_op = instr;
	{
		if (instr.opcode == "label")
		{
			if (instr.operands[0].substr(0, 9) == "@function")
			{
				pc = func_stack.top().return_pc;
				func_stack.pop();
			}
			else
				pc++;
		}
		else if (instr.opcode == "mallocStatic")
		{
			regs[static_base_reg].first = heap.size();
			heap.resize(heap.size() + instr.real_operands[0]);
			pc++;
		}
		else if (instr.opcode == "mallocI")
		{
			regs[instr.real_operands[1]].first = heap.size();
			heap.resize(heap.size() + instr.real_operands[0]);
			pc++;
		}
		else if (instr.opcode == "malloc")
		{
			regs[instr.real_operands[1]].first = heap.size();
			heap.resize(heap.size() + regs[instr.real_operands[0]].first);
			pc++;
		}
		else if (instr.opcode == "call")
		{
			if (instr.operands[0] == "@functionLabel_print")
			{
				int size = 0;
				int pointer = regs[instr.real_operands[0]].first;
				size = ((int)heap[pointer]) + ((int)heap[pointer + 1] << 8) + ((int)heap[pointer + 2] << 16) + ((int)heap[pointer + 3] << 24);
				for (int j = 0; j < size; ++j)
					printf("%c", heap[pointer + 4 + j]);
				pc++;
			}
			else if (instr.operands[0] == "@functionLabel_println")
			{
				int size = 0;
				int pointer = regs[instr.real_operands[0]].first;
				size = ((int)heap[pointer]) + ((int)heap[pointer + 1] << 8) + ((int)heap[pointer + 2] << 16) + ((int)heap[pointer + 3] << 24);
				for (int j = 0; j < size; ++j)
					printf("%c", heap[pointer + 4 + j]);
				printf("\n");
				pc++;
			}
			else if (instr.operands[0] == "@functionLabel_getString")
			{
				string tmp;
				cin >> tmp;
				int return_ptr = heap.size();
				if (tmp.size() % 4 == 0)
					heap.resize(heap.size() + 4 + tmp.size());
				else
					heap.resize(heap.size() + 4 + tmp.size() / 4 * 4 + 4);
				heap[return_ptr] = heap.size() & 255;
				heap[return_ptr + 1] = (heap.size() >> 8) & 255;
				heap[return_ptr + 2] = (heap.size() >> 16) & 255;
				heap[return_ptr + 3] = (heap.size() >> 24) & 255;
				for (int j = 0; j < tmp.size(); ++j)
					heap[return_ptr + 4 + j] = tmp[j];
				regs[return_reg_id].first = return_ptr;
				func_stack.top().callee_return_address = return_ptr;
				pc++;
			}
			else if (instr.operands[0] == "@functionLabel_getInt")
			{
				int tmp;
				cin >> tmp;
				int return_ptr = tmp;
				regs[return_reg_id].first = return_ptr;
				func_stack.top().callee_return_address = return_ptr;
				pc++;
			}
			else if (instr.operands[0] == "@functionLabel___builtin__string_add")
			{
				int size = 0;
				int str1_ptr = regs[instr.real_operands[0]].first, str2_ptr = regs[instr.real_operands[1]].first;
				int len1 = ((int)heap[str1_ptr] << 0) + ((int)heap[str1_ptr + 1] << 8) + ((int)heap[str1_ptr + 2] << 16) + ((int)heap[str1_ptr + 3] << 24);
				int len2 = ((int)heap[str2_ptr] << 0) + ((int)heap[str2_ptr + 1] << 8) + ((int)heap[str2_ptr + 2] << 16) + ((int)heap[str2_ptr + 3] << 24);
				size = len1 + len2;
				int return_ptr = heap.size();
				if (size % 4 == 0)
					heap.resize(heap.size() + size + 4);
				else
					heap.resize(heap.size() + size / 4 * 4 + 8);
				heap[return_ptr + 0] = (size >> 0) & 255;
				heap[return_ptr + 1] = (size >> 8) & 255;
				heap[return_ptr + 2] = (size >> 16) & 255;
				heap[return_ptr + 3] = (size >> 24) & 255;
				for (int i = 0; i < len1; ++i)
					heap[return_ptr + 4 + i] = heap[str1_ptr + 4 + i];
				for (int i = 0; i < len2; ++i)
					heap[return_ptr + 4 + len1 + i] = heap[str2_ptr + 4 + i];
				regs[return_reg_id].first = return_ptr;
				func_stack.top().callee_return_address = return_ptr;
				pc++;
			}
			else
			{
				func_stack.top().caller_saved_regs.clear();
				for (int i = 0; i < func_stack.top().local_vars.size(); ++i)
				{
					int reg = func_stack.top().local_vars[i];
					if (!regs[reg].second)
					{
						func_stack.top().caller_saved_regs.push_back(make_pair(reg, regs[reg].first));
					}
				}
				int nextPc = label_pc[instr.operands[0]];
				int funcinfo = nextPc - 1;
				frame new_frame;
				vector<int> argsTemp;
				argsTemp.resize(lines[funcinfo].real_operands.size() - 1);
				for (int i = 1; i < lines[funcinfo].real_operands.size(); ++i)
					argsTemp[i - 1] = regs[instr.real_operands[i - 1]].first;
				for (int i = 1; i < lines[funcinfo].real_operands.size(); ++i)
					regs[lines[funcinfo].real_operands[i]].first = argsTemp[i - 1];
				init_frame(new_frame, nextPc);
			}
		}
		else if (instr.opcode == "return")
		{
			if (func_stack.size() == 1)
			{
				func_stack.pop();
				return;
			}
			*func_stack.top().ra = regs[instr.real_operands[0]].first;
			pc = func_stack.top().return_pc;
			func_stack.pop();
			for (int i = 0; i < func_stack.top().caller_saved_regs.size(); ++i)
			{
				pair<int, int> v = func_stack.top().caller_saved_regs[i];
				regs[v.first].first = v.second;
			}
		}
		else if (instr.opcode == "nop")
		{
			pc++;
		}
		else if (instr.opcode == "add")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first + regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "sub")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first - regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "mult")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first * regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "div")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first / regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "addI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first + instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "subI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first - instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "rsubI")
		{
			regs[instr.real_operands[2]].first = -regs[instr.real_operands[0]].first + instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "multI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first * instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "divI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first / instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "rdivI")
		{
			regs[instr.real_operands[2]].first = instr.real_operands[0] / regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "lshift")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first << regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "rshift")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first >> regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "lshiftI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first << instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "rshiftI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first >> instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "and")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first & regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "or")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first | regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "xor")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first ^ regs[instr.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "andI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first & instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "orI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first | instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "xorI")
		{
			regs[instr.real_operands[2]].first = regs[instr.real_operands[0]].first ^ instr.real_operands[1];
			pc++;
		}
		else if (instr.opcode == "loadI")
		{
			regs[cur_op.real_operands[1]].first = cur_op.real_operands[0];
			pc++;
		}
		else if (instr.opcode == "loadAI")
		{
			regs[cur_op.real_operands[2]].first = ((int)heap[regs[cur_op.real_operands[0] + cur_op.real_operands[1]].first + 0] << 0) +
				((int)heap[regs[cur_op.real_operands[0] + cur_op.real_operands[1]].first + 1] << 8) +
				((int)heap[regs[cur_op.real_operands[0] + cur_op.real_operands[1]].first + 2] << 16) +
				((int)heap[regs[cur_op.real_operands[0] + cur_op.real_operands[1]].first + 3] << 24);
			pc++;
		}
		else if (instr.opcode == "loadA0")
		{
			regs[cur_op.real_operands[2]].first = ((int)heap[regs[cur_op.real_operands[0] + regs[cur_op.real_operands[1]].first].first + 0] << 0) +
				((int)heap[regs[cur_op.real_operands[0] + regs[cur_op.real_operands[1]].first].first + 1] << 8) +
				((int)heap[regs[cur_op.real_operands[0] + regs[cur_op.real_operands[1]].first].first + 2] << 16) +
				((int)heap[regs[cur_op.real_operands[0] + regs[cur_op.real_operands[1]].first].first + 3] << 24);
			pc++;
		}
		else if (instr.opcode == "store")
		{
			heap[regs[cur_op.real_operands[1]].first] = regs[cur_op.real_operands[0]].first & (255);
			heap[regs[cur_op.real_operands[1]].first + 1] = (regs[cur_op.real_operands[0]].first >> 8) & 255;
			heap[regs[cur_op.real_operands[1]].first + 2] = (regs[cur_op.real_operands[0]].first >> 16) & 255;
			heap[regs[cur_op.real_operands[1]].first + 3] = (regs[cur_op.real_operands[0]].first >> 24) & 255;
			pc++;
		}
		else if (instr.opcode == "storeAI")
		{
			heap[regs[cur_op.real_operands[2]].first + cur_op.real_operands[1]] = (regs[cur_op.real_operands[0]].first >> 0) & 255;
			heap[regs[cur_op.real_operands[2]].first + cur_op.real_operands[1] + 1] = (regs[cur_op.real_operands[0]].first >> 8) & 255;
			heap[regs[cur_op.real_operands[2]].first + cur_op.real_operands[1] + 2] = (regs[cur_op.real_operands[0]].first >> 16) & 255;
			heap[regs[cur_op.real_operands[2]].first + cur_op.real_operands[1] + 3] = (regs[cur_op.real_operands[0]].first >> 24) & 255;
			pc++;
		}
		else if (instr.opcode == "storeA0")
		{
			heap[regs[cur_op.real_operands[0]].first + regs[cur_op.real_operands[1]].first] = (regs[cur_op.real_operands[2]].first >> 0) & 255;
			heap[regs[cur_op.real_operands[0]].first + regs[cur_op.real_operands[1]].first + 1] = (regs[cur_op.real_operands[2]].first >> 8) & 255;
			heap[regs[cur_op.real_operands[0]].first + regs[cur_op.real_operands[1]].first + 2] = (regs[cur_op.real_operands[2]].first >> 16) & 255;
			heap[regs[cur_op.real_operands[0]].first + regs[cur_op.real_operands[1]].first + 3] = (regs[cur_op.real_operands[2]].first >> 24) & 255;
			pc++;
		}
		else if (instr.opcode == "i2i")
		{
			regs[cur_op.real_operands[1]].first = regs[cur_op.real_operands[0]].first;
			pc++;
		}
		else if (instr.opcode == "jump")
		{
			pc = regs[cur_op.real_operands[0]].first;
		}
		else if (instr.opcode == "jumpI")
		{
			pc = cur_op.real_operands[0];
		}
		else if (instr.opcode == "cbr")
		{
			if (regs[cur_op.real_operands[0]].first)
			{
				pc = label_pc[cur_op.operands[1]];
			}
			else
			{
				pc = label_pc[cur_op.operands[2]];
			}
		}
		else if (instr.opcode == "cmp_LT")
		{
			regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first < regs[cur_op.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "cmp_LE")
		{
			regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first <= regs[cur_op.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "cmp_EQ")
		{
			regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first == regs[cur_op.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "cmp_GE")
		{
			regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first >= regs[cur_op.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "cmp_GT")
		{
			regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first > regs[cur_op.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "cmp_NE")
		{
			regs[cur_op.real_operands[2]].first = regs[cur_op.real_operands[0]].first != regs[cur_op.real_operands[1]].first;
			pc++;
		}
		else if (instr.opcode == "load")
		{
			int ptr = regs[cur_op.real_operands[0]].first;
			regs[cur_op.real_operands[1]].first =
				((int)heap[ptr] << 0) +
				((int)heap[ptr + 1] << 8) +
				((int)heap[ptr + 2] << 16) +
				((int)heap[ptr + 3] << 24);
			pc++;
		}
		else
			assert(false);
	}
}

int main()
{
	read_code();
	pc = label_pc["@functionLabel_main"];
	func_stack.push(frame());
	bool display = false;
	int instr_cnt = 0;
	while (func_stack.size() > 0)
	{
		if (pc == lines.size())
		{
			pc = func_stack.top().return_pc;
			func_stack.pop();
		}
		else
		{
			if (display)
			{
				cout << lines[pc].opcode;
				for (int i = 0; i < lines[pc].operands.size(); ++i)
					cout << " " << lines[pc].operands[i];
				cout << endl;
			}
 			process_instruction(lines[pc]);
			instr_cnt++;
		}
	}
	printf("\nInstruction executed: %d\n", instr_cnt);
	while (1) {}
	return 0;
}