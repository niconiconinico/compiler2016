// Generated from Muse.g4 by ANTLR 4.5.2
package Parser;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MuseParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MuseVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MuseParser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(MuseParser.ProgramContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(MuseParser.DeclarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#init_declarators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit_declarators(MuseParser.Init_declaratorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#declarators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarators(MuseParser.DeclaratorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#init_declarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit_declarator(MuseParser.Init_declaratorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#type}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType(MuseParser.TypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#array_decl}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_decl(MuseParser.Array_declContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#array_new}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArray_new(MuseParser.Array_newContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#type_non_array}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_non_array(MuseParser.Type_non_arrayContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#class_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_declaration(MuseParser.Class_declarationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#declarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarator(MuseParser.DeclaratorContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#initializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitializer(MuseParser.InitializerContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#function_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_definition(MuseParser.Function_definitionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#parameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameters(MuseParser.ParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(MuseParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(MuseParser.StatementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#assignment_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_statement(MuseParser.Assignment_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#structured_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStructured_statement(MuseParser.Structured_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#compond_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompond_statement(MuseParser.Compond_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#loop_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLoop_statement(MuseParser.Loop_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#branch_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBranch_statement(MuseParser.Branch_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#for_loop_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFor_loop_statement(MuseParser.For_loop_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#first_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFirst_expression(MuseParser.First_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#second_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSecond_expression(MuseParser.Second_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#third_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitThird_expression(MuseParser.Third_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#while_loop_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWhile_loop_statement(MuseParser.While_loop_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#if_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIf_statement(MuseParser.If_statementContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(MuseParser.ExpressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#assignment_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_expression(MuseParser.Assignment_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#equal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqual(MuseParser.EqualContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#assignment_operators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_operators(MuseParser.Assignment_operatorsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#calculation_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCalculation_expression(MuseParser.Calculation_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#logical_caclulation_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#logical_or_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#logical_and_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#bitwise_or_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#bitwise_xor_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#bitwise_and_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#isequal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIsequal(MuseParser.IsequalContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#notequal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNotequal(MuseParser.NotequalContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#equality_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquality_expression(MuseParser.Equality_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#equ_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqu_op(MuseParser.Equ_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#bigger}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBigger(MuseParser.BiggerContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#smaller}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSmaller(MuseParser.SmallerContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#bigger_e}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBigger_e(MuseParser.Bigger_eContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#smaller_e}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSmaller_e(MuseParser.Smaller_eContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#rela_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRela_op(MuseParser.Rela_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#relation_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelation_expression(MuseParser.Relation_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#lshift}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLshift(MuseParser.LshiftContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#rshift}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRshift(MuseParser.RshiftContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#plus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlus(MuseParser.PlusContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#minus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinus(MuseParser.MinusContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#shift_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShift_op(MuseParser.Shift_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#add_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd_op(MuseParser.Add_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#shift_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShift_expression(MuseParser.Shift_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#add_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdd_expression(MuseParser.Add_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#multiply}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiply(MuseParser.MultiplyContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#division}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDivision(MuseParser.DivisionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#mod}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMod(MuseParser.ModContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#mul_op}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMul_op(MuseParser.Mul_opContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#mul_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMul_expression(MuseParser.Mul_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#plusplus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlusplus(MuseParser.PlusplusContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#minusminus}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMinusminus(MuseParser.MinusminusContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#unary_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expression(MuseParser.Unary_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#pre_defined_constants}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#class_new}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitClass_new(MuseParser.Class_newContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#new_operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNew_operation(MuseParser.New_operationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#functionCall_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#arguements}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguements(MuseParser.ArguementsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#value}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitValue(MuseParser.ValueContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#not_sign}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNot_sign(MuseParser.Not_signContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#unary_operation}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operation(MuseParser.Unary_operationContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#bitwise_not}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBitwise_not(MuseParser.Bitwise_notContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#postfix_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix_expression(MuseParser.Postfix_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#getMember}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitGetMember(MuseParser.GetMemberContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix(MuseParser.PostfixContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#primary_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary_expression(MuseParser.Primary_expressionContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#type_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_name(MuseParser.Type_nameContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(MuseParser.ConstantContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#numeric_constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumeric_constant(MuseParser.Numeric_constantContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#integer_constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger_constant(MuseParser.Integer_constantContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#string_constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString_constant(MuseParser.String_constantContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#typenametokens}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypenametokens(MuseParser.TypenametokensContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#number}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNumber(MuseParser.NumberContext ctx);
	/**
	 * Visit a parse tree produced by {@link MuseParser#union_class}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnion_class(MuseParser.Union_classContext ctx);
}