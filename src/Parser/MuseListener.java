// Generated from Muse.g4 by ANTLR 4.5.2
package Parser;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MuseParser}.
 */
public interface MuseListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MuseParser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(MuseParser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(MuseParser.ProgramContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(MuseParser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(MuseParser.DeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#init_declarators}.
	 * @param ctx the parse tree
	 */
	void enterInit_declarators(MuseParser.Init_declaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#init_declarators}.
	 * @param ctx the parse tree
	 */
	void exitInit_declarators(MuseParser.Init_declaratorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#declarators}.
	 * @param ctx the parse tree
	 */
	void enterDeclarators(MuseParser.DeclaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#declarators}.
	 * @param ctx the parse tree
	 */
	void exitDeclarators(MuseParser.DeclaratorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#init_declarator}.
	 * @param ctx the parse tree
	 */
	void enterInit_declarator(MuseParser.Init_declaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#init_declarator}.
	 * @param ctx the parse tree
	 */
	void exitInit_declarator(MuseParser.Init_declaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(MuseParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(MuseParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#array_decl}.
	 * @param ctx the parse tree
	 */
	void enterArray_decl(MuseParser.Array_declContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#array_decl}.
	 * @param ctx the parse tree
	 */
	void exitArray_decl(MuseParser.Array_declContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#array_new}.
	 * @param ctx the parse tree
	 */
	void enterArray_new(MuseParser.Array_newContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#array_new}.
	 * @param ctx the parse tree
	 */
	void exitArray_new(MuseParser.Array_newContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#type_non_array}.
	 * @param ctx the parse tree
	 */
	void enterType_non_array(MuseParser.Type_non_arrayContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#type_non_array}.
	 * @param ctx the parse tree
	 */
	void exitType_non_array(MuseParser.Type_non_arrayContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#class_declaration}.
	 * @param ctx the parse tree
	 */
	void enterClass_declaration(MuseParser.Class_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#class_declaration}.
	 * @param ctx the parse tree
	 */
	void exitClass_declaration(MuseParser.Class_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#declarator}.
	 * @param ctx the parse tree
	 */
	void enterDeclarator(MuseParser.DeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#declarator}.
	 * @param ctx the parse tree
	 */
	void exitDeclarator(MuseParser.DeclaratorContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#initializer}.
	 * @param ctx the parse tree
	 */
	void enterInitializer(MuseParser.InitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#initializer}.
	 * @param ctx the parse tree
	 */
	void exitInitializer(MuseParser.InitializerContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition(MuseParser.Function_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#function_definition}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition(MuseParser.Function_definitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(MuseParser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(MuseParser.ParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(MuseParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(MuseParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(MuseParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(MuseParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#assignment_statement}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_statement(MuseParser.Assignment_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#assignment_statement}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_statement(MuseParser.Assignment_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#structured_statement}.
	 * @param ctx the parse tree
	 */
	void enterStructured_statement(MuseParser.Structured_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#structured_statement}.
	 * @param ctx the parse tree
	 */
	void exitStructured_statement(MuseParser.Structured_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#compond_statement}.
	 * @param ctx the parse tree
	 */
	void enterCompond_statement(MuseParser.Compond_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#compond_statement}.
	 * @param ctx the parse tree
	 */
	void exitCompond_statement(MuseParser.Compond_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#loop_statement}.
	 * @param ctx the parse tree
	 */
	void enterLoop_statement(MuseParser.Loop_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#loop_statement}.
	 * @param ctx the parse tree
	 */
	void exitLoop_statement(MuseParser.Loop_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#branch_statement}.
	 * @param ctx the parse tree
	 */
	void enterBranch_statement(MuseParser.Branch_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#branch_statement}.
	 * @param ctx the parse tree
	 */
	void exitBranch_statement(MuseParser.Branch_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#for_loop_statement}.
	 * @param ctx the parse tree
	 */
	void enterFor_loop_statement(MuseParser.For_loop_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#for_loop_statement}.
	 * @param ctx the parse tree
	 */
	void exitFor_loop_statement(MuseParser.For_loop_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#first_expression}.
	 * @param ctx the parse tree
	 */
	void enterFirst_expression(MuseParser.First_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#first_expression}.
	 * @param ctx the parse tree
	 */
	void exitFirst_expression(MuseParser.First_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#second_expression}.
	 * @param ctx the parse tree
	 */
	void enterSecond_expression(MuseParser.Second_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#second_expression}.
	 * @param ctx the parse tree
	 */
	void exitSecond_expression(MuseParser.Second_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#third_expression}.
	 * @param ctx the parse tree
	 */
	void enterThird_expression(MuseParser.Third_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#third_expression}.
	 * @param ctx the parse tree
	 */
	void exitThird_expression(MuseParser.Third_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#while_loop_statement}.
	 * @param ctx the parse tree
	 */
	void enterWhile_loop_statement(MuseParser.While_loop_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#while_loop_statement}.
	 * @param ctx the parse tree
	 */
	void exitWhile_loop_statement(MuseParser.While_loop_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void enterIf_statement(MuseParser.If_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#if_statement}.
	 * @param ctx the parse tree
	 */
	void exitIf_statement(MuseParser.If_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(MuseParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(MuseParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_expression(MuseParser.Assignment_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_expression(MuseParser.Assignment_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#equal}.
	 * @param ctx the parse tree
	 */
	void enterEqual(MuseParser.EqualContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#equal}.
	 * @param ctx the parse tree
	 */
	void exitEqual(MuseParser.EqualContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#assignment_operators}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_operators(MuseParser.Assignment_operatorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#assignment_operators}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_operators(MuseParser.Assignment_operatorsContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#calculation_expression}.
	 * @param ctx the parse tree
	 */
	void enterCalculation_expression(MuseParser.Calculation_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#calculation_expression}.
	 * @param ctx the parse tree
	 */
	void exitCalculation_expression(MuseParser.Calculation_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#logical_caclulation_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#logical_caclulation_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_caclulation_expression(MuseParser.Logical_caclulation_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_or_expression(MuseParser.Logical_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_or_expression(MuseParser.Logical_or_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_and_expression(MuseParser.Logical_and_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_and_expression(MuseParser.Logical_and_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#bitwise_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#bitwise_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitBitwise_or_expression(MuseParser.Bitwise_or_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#bitwise_xor_expression}.
	 * @param ctx the parse tree
	 */
	void enterBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#bitwise_xor_expression}.
	 * @param ctx the parse tree
	 */
	void exitBitwise_xor_expression(MuseParser.Bitwise_xor_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#bitwise_and_expression}.
	 * @param ctx the parse tree
	 */
	void enterBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#bitwise_and_expression}.
	 * @param ctx the parse tree
	 */
	void exitBitwise_and_expression(MuseParser.Bitwise_and_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#isequal}.
	 * @param ctx the parse tree
	 */
	void enterIsequal(MuseParser.IsequalContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#isequal}.
	 * @param ctx the parse tree
	 */
	void exitIsequal(MuseParser.IsequalContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#notequal}.
	 * @param ctx the parse tree
	 */
	void enterNotequal(MuseParser.NotequalContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#notequal}.
	 * @param ctx the parse tree
	 */
	void exitNotequal(MuseParser.NotequalContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void enterEquality_expression(MuseParser.Equality_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void exitEquality_expression(MuseParser.Equality_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#equ_op}.
	 * @param ctx the parse tree
	 */
	void enterEqu_op(MuseParser.Equ_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#equ_op}.
	 * @param ctx the parse tree
	 */
	void exitEqu_op(MuseParser.Equ_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#bigger}.
	 * @param ctx the parse tree
	 */
	void enterBigger(MuseParser.BiggerContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#bigger}.
	 * @param ctx the parse tree
	 */
	void exitBigger(MuseParser.BiggerContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#smaller}.
	 * @param ctx the parse tree
	 */
	void enterSmaller(MuseParser.SmallerContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#smaller}.
	 * @param ctx the parse tree
	 */
	void exitSmaller(MuseParser.SmallerContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#bigger_e}.
	 * @param ctx the parse tree
	 */
	void enterBigger_e(MuseParser.Bigger_eContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#bigger_e}.
	 * @param ctx the parse tree
	 */
	void exitBigger_e(MuseParser.Bigger_eContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#smaller_e}.
	 * @param ctx the parse tree
	 */
	void enterSmaller_e(MuseParser.Smaller_eContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#smaller_e}.
	 * @param ctx the parse tree
	 */
	void exitSmaller_e(MuseParser.Smaller_eContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#rela_op}.
	 * @param ctx the parse tree
	 */
	void enterRela_op(MuseParser.Rela_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#rela_op}.
	 * @param ctx the parse tree
	 */
	void exitRela_op(MuseParser.Rela_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#relation_expression}.
	 * @param ctx the parse tree
	 */
	void enterRelation_expression(MuseParser.Relation_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#relation_expression}.
	 * @param ctx the parse tree
	 */
	void exitRelation_expression(MuseParser.Relation_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#lshift}.
	 * @param ctx the parse tree
	 */
	void enterLshift(MuseParser.LshiftContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#lshift}.
	 * @param ctx the parse tree
	 */
	void exitLshift(MuseParser.LshiftContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#rshift}.
	 * @param ctx the parse tree
	 */
	void enterRshift(MuseParser.RshiftContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#rshift}.
	 * @param ctx the parse tree
	 */
	void exitRshift(MuseParser.RshiftContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#plus}.
	 * @param ctx the parse tree
	 */
	void enterPlus(MuseParser.PlusContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#plus}.
	 * @param ctx the parse tree
	 */
	void exitPlus(MuseParser.PlusContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#minus}.
	 * @param ctx the parse tree
	 */
	void enterMinus(MuseParser.MinusContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#minus}.
	 * @param ctx the parse tree
	 */
	void exitMinus(MuseParser.MinusContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#shift_op}.
	 * @param ctx the parse tree
	 */
	void enterShift_op(MuseParser.Shift_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#shift_op}.
	 * @param ctx the parse tree
	 */
	void exitShift_op(MuseParser.Shift_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#add_op}.
	 * @param ctx the parse tree
	 */
	void enterAdd_op(MuseParser.Add_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#add_op}.
	 * @param ctx the parse tree
	 */
	void exitAdd_op(MuseParser.Add_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void enterShift_expression(MuseParser.Shift_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void exitShift_expression(MuseParser.Shift_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#add_expression}.
	 * @param ctx the parse tree
	 */
	void enterAdd_expression(MuseParser.Add_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#add_expression}.
	 * @param ctx the parse tree
	 */
	void exitAdd_expression(MuseParser.Add_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#multiply}.
	 * @param ctx the parse tree
	 */
	void enterMultiply(MuseParser.MultiplyContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#multiply}.
	 * @param ctx the parse tree
	 */
	void exitMultiply(MuseParser.MultiplyContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#division}.
	 * @param ctx the parse tree
	 */
	void enterDivision(MuseParser.DivisionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#division}.
	 * @param ctx the parse tree
	 */
	void exitDivision(MuseParser.DivisionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#mod}.
	 * @param ctx the parse tree
	 */
	void enterMod(MuseParser.ModContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#mod}.
	 * @param ctx the parse tree
	 */
	void exitMod(MuseParser.ModContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#mul_op}.
	 * @param ctx the parse tree
	 */
	void enterMul_op(MuseParser.Mul_opContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#mul_op}.
	 * @param ctx the parse tree
	 */
	void exitMul_op(MuseParser.Mul_opContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#mul_expression}.
	 * @param ctx the parse tree
	 */
	void enterMul_expression(MuseParser.Mul_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#mul_expression}.
	 * @param ctx the parse tree
	 */
	void exitMul_expression(MuseParser.Mul_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#plusplus}.
	 * @param ctx the parse tree
	 */
	void enterPlusplus(MuseParser.PlusplusContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#plusplus}.
	 * @param ctx the parse tree
	 */
	void exitPlusplus(MuseParser.PlusplusContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#minusminus}.
	 * @param ctx the parse tree
	 */
	void enterMinusminus(MuseParser.MinusminusContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#minusminus}.
	 * @param ctx the parse tree
	 */
	void exitMinusminus(MuseParser.MinusminusContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary_expression(MuseParser.Unary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary_expression(MuseParser.Unary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#pre_defined_constants}.
	 * @param ctx the parse tree
	 */
	void enterPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#pre_defined_constants}.
	 * @param ctx the parse tree
	 */
	void exitPre_defined_constants(MuseParser.Pre_defined_constantsContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#class_new}.
	 * @param ctx the parse tree
	 */
	void enterClass_new(MuseParser.Class_newContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#class_new}.
	 * @param ctx the parse tree
	 */
	void exitClass_new(MuseParser.Class_newContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#new_operation}.
	 * @param ctx the parse tree
	 */
	void enterNew_operation(MuseParser.New_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#new_operation}.
	 * @param ctx the parse tree
	 */
	void exitNew_operation(MuseParser.New_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#functionCall_expression}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#functionCall_expression}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall_expression(MuseParser.FunctionCall_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#arguements}.
	 * @param ctx the parse tree
	 */
	void enterArguements(MuseParser.ArguementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#arguements}.
	 * @param ctx the parse tree
	 */
	void exitArguements(MuseParser.ArguementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(MuseParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(MuseParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#not_sign}.
	 * @param ctx the parse tree
	 */
	void enterNot_sign(MuseParser.Not_signContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#not_sign}.
	 * @param ctx the parse tree
	 */
	void exitNot_sign(MuseParser.Not_signContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#unary_operation}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operation(MuseParser.Unary_operationContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#unary_operation}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operation(MuseParser.Unary_operationContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#bitwise_not}.
	 * @param ctx the parse tree
	 */
	void enterBitwise_not(MuseParser.Bitwise_notContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#bitwise_not}.
	 * @param ctx the parse tree
	 */
	void exitBitwise_not(MuseParser.Bitwise_notContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#postfix_expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfix_expression(MuseParser.Postfix_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#postfix_expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfix_expression(MuseParser.Postfix_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#getMember}.
	 * @param ctx the parse tree
	 */
	void enterGetMember(MuseParser.GetMemberContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#getMember}.
	 * @param ctx the parse tree
	 */
	void exitGetMember(MuseParser.GetMemberContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPostfix(MuseParser.PostfixContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPostfix(MuseParser.PostfixContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void enterPrimary_expression(MuseParser.Primary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void exitPrimary_expression(MuseParser.Primary_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(MuseParser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(MuseParser.Type_nameContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(MuseParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(MuseParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#numeric_constant}.
	 * @param ctx the parse tree
	 */
	void enterNumeric_constant(MuseParser.Numeric_constantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#numeric_constant}.
	 * @param ctx the parse tree
	 */
	void exitNumeric_constant(MuseParser.Numeric_constantContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#integer_constant}.
	 * @param ctx the parse tree
	 */
	void enterInteger_constant(MuseParser.Integer_constantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#integer_constant}.
	 * @param ctx the parse tree
	 */
	void exitInteger_constant(MuseParser.Integer_constantContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#string_constant}.
	 * @param ctx the parse tree
	 */
	void enterString_constant(MuseParser.String_constantContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#string_constant}.
	 * @param ctx the parse tree
	 */
	void exitString_constant(MuseParser.String_constantContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#typenametokens}.
	 * @param ctx the parse tree
	 */
	void enterTypenametokens(MuseParser.TypenametokensContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#typenametokens}.
	 * @param ctx the parse tree
	 */
	void exitTypenametokens(MuseParser.TypenametokensContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(MuseParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(MuseParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link MuseParser#union_class}.
	 * @param ctx the parse tree
	 */
	void enterUnion_class(MuseParser.Union_classContext ctx);
	/**
	 * Exit a parse tree produced by {@link MuseParser#union_class}.
	 * @param ctx the parse tree
	 */
	void exitUnion_class(MuseParser.Union_classContext ctx);
}