// Generated from Muse.g4 by ANTLR 4.5.2
package Parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MuseParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, Comma=30, Equal=31, 
		Lbracket=32, Rbracket=33, LBigbracket=34, RBigbracket=35, LMidbracket=36, 
		RMidbracket=37, New=38, Balabalabala=39, BREAK=40, CONTINUE=41, RETURN=42, 
		FOR=43, WHILE=44, DO=45, IF=46, ELSE=47, COLON=48, QUESTION=49, Dot=50, 
		Identifier=51, SEMI=52, DecimalConstant=53, Numbertail=54, OctalConstant=55, 
		HexadecimalConstant=56, Sign=57, CharacterConstant=58, StringLiteral=59, 
		Preprocessing=60, Whitespace=61, Newline=62, BlockComment=63, LineComment=64;
	public static final int
		RULE_program = 0, RULE_declaration = 1, RULE_init_declarators = 2, RULE_declarators = 3, 
		RULE_init_declarator = 4, RULE_type = 5, RULE_array_decl = 6, RULE_array_new = 7, 
		RULE_type_non_array = 8, RULE_class_declaration = 9, RULE_declarator = 10, 
		RULE_initializer = 11, RULE_function_definition = 12, RULE_parameters = 13, 
		RULE_parameter = 14, RULE_statement = 15, RULE_assignment_statement = 16, 
		RULE_structured_statement = 17, RULE_compond_statement = 18, RULE_loop_statement = 19, 
		RULE_branch_statement = 20, RULE_for_loop_statement = 21, RULE_first_expression = 22, 
		RULE_second_expression = 23, RULE_third_expression = 24, RULE_while_loop_statement = 25, 
		RULE_if_statement = 26, RULE_expression = 27, RULE_assignment_expression = 28, 
		RULE_equal = 29, RULE_assignment_operators = 30, RULE_calculation_expression = 31, 
		RULE_logical_caclulation_expression = 32, RULE_logical_or_expression = 33, 
		RULE_logical_and_expression = 34, RULE_bitwise_or_expression = 35, RULE_bitwise_xor_expression = 36, 
		RULE_bitwise_and_expression = 37, RULE_isequal = 38, RULE_notequal = 39, 
		RULE_equality_expression = 40, RULE_equ_op = 41, RULE_bigger = 42, RULE_smaller = 43, 
		RULE_bigger_e = 44, RULE_smaller_e = 45, RULE_rela_op = 46, RULE_relation_expression = 47, 
		RULE_lshift = 48, RULE_rshift = 49, RULE_plus = 50, RULE_minus = 51, RULE_shift_op = 52, 
		RULE_add_op = 53, RULE_shift_expression = 54, RULE_add_expression = 55, 
		RULE_multiply = 56, RULE_division = 57, RULE_mod = 58, RULE_mul_op = 59, 
		RULE_mul_expression = 60, RULE_plusplus = 61, RULE_minusminus = 62, RULE_unary_expression = 63, 
		RULE_pre_defined_constants = 64, RULE_class_new = 65, RULE_new_operation = 66, 
		RULE_functionCall_expression = 67, RULE_arguements = 68, RULE_value = 69, 
		RULE_not_sign = 70, RULE_unary_operation = 71, RULE_bitwise_not = 72, 
		RULE_postfix_expression = 73, RULE_getMember = 74, RULE_postfix = 75, 
		RULE_primary_expression = 76, RULE_type_name = 77, RULE_constant = 78, 
		RULE_numeric_constant = 79, RULE_integer_constant = 80, RULE_string_constant = 81, 
		RULE_typenametokens = 82, RULE_number = 83, RULE_union_class = 84;
	public static final String[] ruleNames = {
		"program", "declaration", "init_declarators", "declarators", "init_declarator", 
		"type", "array_decl", "array_new", "type_non_array", "class_declaration", 
		"declarator", "initializer", "function_definition", "parameters", "parameter", 
		"statement", "assignment_statement", "structured_statement", "compond_statement", 
		"loop_statement", "branch_statement", "for_loop_statement", "first_expression", 
		"second_expression", "third_expression", "while_loop_statement", "if_statement", 
		"expression", "assignment_expression", "equal", "assignment_operators", 
		"calculation_expression", "logical_caclulation_expression", "logical_or_expression", 
		"logical_and_expression", "bitwise_or_expression", "bitwise_xor_expression", 
		"bitwise_and_expression", "isequal", "notequal", "equality_expression", 
		"equ_op", "bigger", "smaller", "bigger_e", "smaller_e", "rela_op", "relation_expression", 
		"lshift", "rshift", "plus", "minus", "shift_op", "add_op", "shift_expression", 
		"add_expression", "multiply", "division", "mod", "mul_op", "mul_expression", 
		"plusplus", "minusminus", "unary_expression", "pre_defined_constants", 
		"class_new", "new_operation", "functionCall_expression", "arguements", 
		"value", "not_sign", "unary_operation", "bitwise_not", "postfix_expression", 
		"getMember", "postfix", "primary_expression", "type_name", "constant", 
		"numeric_constant", "integer_constant", "string_constant", "typenametokens", 
		"number", "union_class"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'||'", "'&&'", "'|'", "'^'", "'&'", "'=='", "'!='", "'>'", "'<'", 
		"'>='", "'<='", "'<<'", "'>>'", "'+'", "'-'", "'*'", "'/'", "'%'", "'++'", 
		"'--'", "'true'", "'false'", "'null'", "'!'", "'~'", "'int'", "'bool'", 
		"'void'", "'class'", "','", "'='", "'('", "')'", "'{'", "'}'", "'['", 
		"']'", "'new'", "'...'", "'break'", "'continue'", "'return'", "'for'", 
		"'while'", "'do'", "'if'", "'else'", "':'", "'?'", "'.'", null, "';'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "Comma", "Equal", "Lbracket", "Rbracket", 
		"LBigbracket", "RBigbracket", "LMidbracket", "RMidbracket", "New", "Balabalabala", 
		"BREAK", "CONTINUE", "RETURN", "FOR", "WHILE", "DO", "IF", "ELSE", "COLON", 
		"QUESTION", "Dot", "Identifier", "SEMI", "DecimalConstant", "Numbertail", 
		"OctalConstant", "HexadecimalConstant", "Sign", "CharacterConstant", "StringLiteral", 
		"Preprocessing", "Whitespace", "Newline", "BlockComment", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Muse.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MuseParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public List<Function_definitionContext> function_definition() {
			return getRuleContexts(Function_definitionContext.class);
		}
		public Function_definitionContext function_definition(int i) {
			return getRuleContext(Function_definitionContext.class,i);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterProgram(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitProgram(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitProgram(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(172); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				setState(172);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(170);
					function_definition();
					}
					break;
				case 2:
					{
					setState(171);
					declaration();
					}
					break;
				}
				}
				setState(174); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << Identifier))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationContext extends ParserRuleContext {
		public Init_declaratorsContext init_declarators() {
			return getRuleContext(Init_declaratorsContext.class,0);
		}
		public TerminalNode SEMI() { return getToken(MuseParser.SEMI, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public Class_declarationContext class_declaration() {
			return getRuleContext(Class_declarationContext.class,0);
		}
		public DeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitDeclaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitDeclaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclarationContext declaration() throws RecognitionException {
		DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_declaration);
		try {
			setState(181);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(176);
				type();
				}
				setState(177);
				init_declarators();
				setState(178);
				match(SEMI);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(180);
				class_declaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_declaratorsContext extends ParserRuleContext {
		public Init_declaratorContext init_declarator() {
			return getRuleContext(Init_declaratorContext.class,0);
		}
		public Init_declaratorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init_declarators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterInit_declarators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitInit_declarators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitInit_declarators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Init_declaratorsContext init_declarators() throws RecognitionException {
		Init_declaratorsContext _localctx = new Init_declaratorsContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_init_declarators);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(183);
			init_declarator();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaratorsContext extends ParserRuleContext {
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public DeclaratorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterDeclarators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitDeclarators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitDeclarators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclaratorsContext declarators() throws RecognitionException {
		DeclaratorsContext _localctx = new DeclaratorsContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_declarators);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			declarator();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Init_declaratorContext extends ParserRuleContext {
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public EqualContext equal() {
			return getRuleContext(EqualContext.class,0);
		}
		public InitializerContext initializer() {
			return getRuleContext(InitializerContext.class,0);
		}
		public Init_declaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_init_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterInit_declarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitInit_declarator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitInit_declarator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Init_declaratorContext init_declarator() throws RecognitionException {
		Init_declaratorContext _localctx = new Init_declaratorContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_init_declarator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			declarator();
			setState(191);
			_la = _input.LA(1);
			if (_la==Equal) {
				{
				setState(188);
				equal();
				setState(189);
				initializer();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public Type_non_arrayContext type_non_array() {
			return getRuleContext(Type_non_arrayContext.class,0);
		}
		public Array_declContext array_decl() {
			return getRuleContext(Array_declContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitType(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitType(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_type);
		try {
			setState(195);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(193);
				type_non_array();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(194);
				array_decl();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_declContext extends ParserRuleContext {
		public Type_non_arrayContext type_non_array() {
			return getRuleContext(Type_non_arrayContext.class,0);
		}
		public List<TerminalNode> LMidbracket() { return getTokens(MuseParser.LMidbracket); }
		public TerminalNode LMidbracket(int i) {
			return getToken(MuseParser.LMidbracket, i);
		}
		public List<TerminalNode> RMidbracket() { return getTokens(MuseParser.RMidbracket); }
		public TerminalNode RMidbracket(int i) {
			return getToken(MuseParser.RMidbracket, i);
		}
		public Array_declContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_decl; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterArray_decl(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitArray_decl(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitArray_decl(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_declContext array_decl() throws RecognitionException {
		Array_declContext _localctx = new Array_declContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_array_decl);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(197);
			type_non_array();
			setState(200); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(198);
				match(LMidbracket);
				setState(199);
				match(RMidbracket);
				}
				}
				setState(202); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==LMidbracket );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Array_newContext extends ParserRuleContext {
		public Type_non_arrayContext type_non_array() {
			return getRuleContext(Type_non_arrayContext.class,0);
		}
		public List<TerminalNode> LMidbracket() { return getTokens(MuseParser.LMidbracket); }
		public TerminalNode LMidbracket(int i) {
			return getToken(MuseParser.LMidbracket, i);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> RMidbracket() { return getTokens(MuseParser.RMidbracket); }
		public TerminalNode RMidbracket(int i) {
			return getToken(MuseParser.RMidbracket, i);
		}
		public Array_newContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_array_new; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterArray_new(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitArray_new(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitArray_new(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Array_newContext array_new() throws RecognitionException {
		Array_newContext _localctx = new Array_newContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_array_new);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(204);
			type_non_array();
			setState(209); 
			_errHandler.sync(this);
			_alt = 1;
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(205);
					match(LMidbracket);
					setState(206);
					expression();
					setState(207);
					match(RMidbracket);
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(211); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			} while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER );
			setState(217);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==LMidbracket) {
				{
				{
				setState(213);
				match(LMidbracket);
				setState(214);
				match(RMidbracket);
				}
				}
				setState(219);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_non_arrayContext extends ParserRuleContext {
		public TypenametokensContext typenametokens() {
			return getRuleContext(TypenametokensContext.class,0);
		}
		public Class_declarationContext class_declaration() {
			return getRuleContext(Class_declarationContext.class,0);
		}
		public Type_non_arrayContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_non_array; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterType_non_array(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitType_non_array(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitType_non_array(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_non_arrayContext type_non_array() throws RecognitionException {
		Type_non_arrayContext _localctx = new Type_non_arrayContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_type_non_array);
		try {
			setState(222);
			switch (_input.LA(1)) {
			case T__25:
			case T__26:
			case T__27:
			case Identifier:
				enterOuterAlt(_localctx, 1);
				{
				setState(220);
				typenametokens();
				}
				break;
			case T__28:
				enterOuterAlt(_localctx, 2);
				{
				setState(221);
				class_declaration();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_declarationContext extends ParserRuleContext {
		public TerminalNode LBigbracket() { return getToken(MuseParser.LBigbracket, 0); }
		public TerminalNode RBigbracket() { return getToken(MuseParser.RBigbracket, 0); }
		public Union_classContext union_class() {
			return getRuleContext(Union_classContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public List<TypeContext> type() {
			return getRuleContexts(TypeContext.class);
		}
		public TypeContext type(int i) {
			return getRuleContext(TypeContext.class,i);
		}
		public List<DeclaratorsContext> declarators() {
			return getRuleContexts(DeclaratorsContext.class);
		}
		public DeclaratorsContext declarators(int i) {
			return getRuleContext(DeclaratorsContext.class,i);
		}
		public List<TerminalNode> SEMI() { return getTokens(MuseParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(MuseParser.SEMI, i);
		}
		public Class_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterClass_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitClass_declaration(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitClass_declaration(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_declarationContext class_declaration() throws RecognitionException {
		Class_declarationContext _localctx = new Class_declarationContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_class_declaration);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(224);
			union_class();
			}
			setState(226);
			_la = _input.LA(1);
			if (_la==Identifier) {
				{
				setState(225);
				match(Identifier);
				}
			}

			setState(228);
			match(LBigbracket);
			setState(235);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << Identifier))) != 0)) {
				{
				{
				setState(229);
				type();
				setState(230);
				declarators();
				setState(231);
				match(SEMI);
				}
				}
				setState(237);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(238);
			match(RBigbracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclaratorContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public DeclaratorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterDeclarator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitDeclarator(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitDeclarator(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DeclaratorContext declarator() throws RecognitionException {
		DeclaratorContext _localctx = new DeclaratorContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_declarator);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(240);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InitializerContext extends ParserRuleContext {
		public Assignment_expressionContext assignment_expression() {
			return getRuleContext(Assignment_expressionContext.class,0);
		}
		public InitializerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_initializer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterInitializer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitInitializer(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitInitializer(this);
			else return visitor.visitChildren(this);
		}
	}

	public final InitializerContext initializer() throws RecognitionException {
		InitializerContext _localctx = new InitializerContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_initializer);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(242);
			assignment_expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_definitionContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public TerminalNode Lbracket() { return getToken(MuseParser.Lbracket, 0); }
		public TerminalNode Rbracket() { return getToken(MuseParser.Rbracket, 0); }
		public TerminalNode LBigbracket() { return getToken(MuseParser.LBigbracket, 0); }
		public TerminalNode RBigbracket() { return getToken(MuseParser.RBigbracket, 0); }
		public ParametersContext parameters() {
			return getRuleContext(ParametersContext.class,0);
		}
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public TerminalNode SEMI() { return getToken(MuseParser.SEMI, 0); }
		public Function_definitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterFunction_definition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitFunction_definition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitFunction_definition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Function_definitionContext function_definition() throws RecognitionException {
		Function_definitionContext _localctx = new Function_definitionContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_function_definition);
		int _la;
		try {
			setState(270);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(244);
				type();
				setState(245);
				match(Identifier);
				setState(246);
				match(Lbracket);
				setState(248);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << Identifier))) != 0)) {
					{
					setState(247);
					parameters();
					}
				}

				setState(250);
				match(Rbracket);
				setState(251);
				match(LBigbracket);
				setState(256);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << Lbracket) | (1L << LBigbracket) | (1L << New) | (1L << BREAK) | (1L << CONTINUE) | (1L << RETURN) | (1L << FOR) | (1L << WHILE) | (1L << IF) | (1L << Identifier) | (1L << SEMI) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
					{
					setState(254);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
					case 1:
						{
						setState(252);
						statement();
						}
						break;
					case 2:
						{
						setState(253);
						declaration();
						}
						break;
					}
					}
					setState(258);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(259);
				match(RBigbracket);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(261);
				type();
				setState(262);
				match(Identifier);
				setState(263);
				match(Lbracket);
				setState(265);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << Identifier))) != 0)) {
					{
					setState(264);
					parameters();
					}
				}

				setState(267);
				match(Rbracket);
				setState(268);
				match(SEMI);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParametersContext extends ParserRuleContext {
		public List<ParameterContext> parameter() {
			return getRuleContexts(ParameterContext.class);
		}
		public ParameterContext parameter(int i) {
			return getRuleContext(ParameterContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(MuseParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(MuseParser.Comma, i);
		}
		public TerminalNode Balabalabala() { return getToken(MuseParser.Balabalabala, 0); }
		public ParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitParameters(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitParameters(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParametersContext parameters() throws RecognitionException {
		ParametersContext _localctx = new ParametersContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_parameters);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(272);
			parameter();
			setState(277);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(273);
					match(Comma);
					setState(274);
					parameter();
					}
					} 
				}
				setState(279);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			}
			setState(282);
			_la = _input.LA(1);
			if (_la==Comma) {
				{
				setState(280);
				match(Comma);
				setState(281);
				match(Balabalabala);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public DeclaratorContext declarator() {
			return getRuleContext(DeclaratorContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitParameter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitParameter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_parameter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(284);
			type();
			{
			setState(285);
			declarator();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public Compond_statementContext compond_statement() {
			return getRuleContext(Compond_statementContext.class,0);
		}
		public Structured_statementContext structured_statement() {
			return getRuleContext(Structured_statementContext.class,0);
		}
		public Assignment_statementContext assignment_statement() {
			return getRuleContext(Assignment_statementContext.class,0);
		}
		public DeclarationContext declaration() {
			return getRuleContext(DeclarationContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitStatement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitStatement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_statement);
		try {
			setState(291);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,18,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(287);
				compond_statement();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(288);
				structured_statement();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(289);
				assignment_statement();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(290);
				declaration();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_statementContext extends ParserRuleContext {
		public TerminalNode SEMI() { return getToken(MuseParser.SEMI, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Assignment_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterAssignment_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitAssignment_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitAssignment_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assignment_statementContext assignment_statement() throws RecognitionException {
		Assignment_statementContext _localctx = new Assignment_statementContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_assignment_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(294);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
				{
				setState(293);
				expression();
				}
			}

			setState(296);
			match(SEMI);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Structured_statementContext extends ParserRuleContext {
		public Loop_statementContext loop_statement() {
			return getRuleContext(Loop_statementContext.class,0);
		}
		public Branch_statementContext branch_statement() {
			return getRuleContext(Branch_statementContext.class,0);
		}
		public Structured_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structured_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterStructured_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitStructured_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitStructured_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Structured_statementContext structured_statement() throws RecognitionException {
		Structured_statementContext _localctx = new Structured_statementContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_structured_statement);
		try {
			setState(300);
			switch (_input.LA(1)) {
			case FOR:
			case WHILE:
				enterOuterAlt(_localctx, 1);
				{
				setState(298);
				loop_statement();
				}
				break;
			case BREAK:
			case CONTINUE:
			case RETURN:
			case IF:
				enterOuterAlt(_localctx, 2);
				{
				setState(299);
				branch_statement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Compond_statementContext extends ParserRuleContext {
		public TerminalNode LBigbracket() { return getToken(MuseParser.LBigbracket, 0); }
		public TerminalNode RBigbracket() { return getToken(MuseParser.RBigbracket, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public List<DeclarationContext> declaration() {
			return getRuleContexts(DeclarationContext.class);
		}
		public DeclarationContext declaration(int i) {
			return getRuleContext(DeclarationContext.class,i);
		}
		public Compond_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_compond_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterCompond_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitCompond_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitCompond_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Compond_statementContext compond_statement() throws RecognitionException {
		Compond_statementContext _localctx = new Compond_statementContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_compond_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(302);
			match(LBigbracket);
			setState(307);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << Lbracket) | (1L << LBigbracket) | (1L << New) | (1L << BREAK) | (1L << CONTINUE) | (1L << RETURN) | (1L << FOR) | (1L << WHILE) | (1L << IF) | (1L << Identifier) | (1L << SEMI) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
				{
				setState(305);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,21,_ctx) ) {
				case 1:
					{
					setState(303);
					statement();
					}
					break;
				case 2:
					{
					setState(304);
					declaration();
					}
					break;
				}
				}
				setState(309);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(310);
			match(RBigbracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Loop_statementContext extends ParserRuleContext {
		public For_loop_statementContext for_loop_statement() {
			return getRuleContext(For_loop_statementContext.class,0);
		}
		public While_loop_statementContext while_loop_statement() {
			return getRuleContext(While_loop_statementContext.class,0);
		}
		public Loop_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loop_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterLoop_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitLoop_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitLoop_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Loop_statementContext loop_statement() throws RecognitionException {
		Loop_statementContext _localctx = new Loop_statementContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_loop_statement);
		try {
			setState(314);
			switch (_input.LA(1)) {
			case FOR:
				enterOuterAlt(_localctx, 1);
				{
				setState(312);
				for_loop_statement();
				}
				break;
			case WHILE:
				enterOuterAlt(_localctx, 2);
				{
				setState(313);
				while_loop_statement();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Branch_statementContext extends ParserRuleContext {
		public If_statementContext if_statement() {
			return getRuleContext(If_statementContext.class,0);
		}
		public TerminalNode BREAK() { return getToken(MuseParser.BREAK, 0); }
		public TerminalNode SEMI() { return getToken(MuseParser.SEMI, 0); }
		public TerminalNode CONTINUE() { return getToken(MuseParser.CONTINUE, 0); }
		public TerminalNode RETURN() { return getToken(MuseParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Branch_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_branch_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterBranch_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitBranch_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitBranch_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Branch_statementContext branch_statement() throws RecognitionException {
		Branch_statementContext _localctx = new Branch_statementContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_branch_statement);
		int _la;
		try {
			setState(326);
			switch (_input.LA(1)) {
			case IF:
				enterOuterAlt(_localctx, 1);
				{
				setState(316);
				if_statement();
				}
				break;
			case BREAK:
				enterOuterAlt(_localctx, 2);
				{
				setState(317);
				match(BREAK);
				setState(318);
				match(SEMI);
				}
				break;
			case CONTINUE:
				enterOuterAlt(_localctx, 3);
				{
				setState(319);
				match(CONTINUE);
				setState(320);
				match(SEMI);
				}
				break;
			case RETURN:
				enterOuterAlt(_localctx, 4);
				{
				setState(321);
				match(RETURN);
				setState(323);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
					{
					setState(322);
					expression();
					}
				}

				setState(325);
				match(SEMI);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class For_loop_statementContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(MuseParser.FOR, 0); }
		public TerminalNode Lbracket() { return getToken(MuseParser.Lbracket, 0); }
		public TerminalNode Rbracket() { return getToken(MuseParser.Rbracket, 0); }
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public List<TerminalNode> SEMI() { return getTokens(MuseParser.SEMI); }
		public TerminalNode SEMI(int i) {
			return getToken(MuseParser.SEMI, i);
		}
		public First_expressionContext first_expression() {
			return getRuleContext(First_expressionContext.class,0);
		}
		public Second_expressionContext second_expression() {
			return getRuleContext(Second_expressionContext.class,0);
		}
		public Third_expressionContext third_expression() {
			return getRuleContext(Third_expressionContext.class,0);
		}
		public For_loop_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_for_loop_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterFor_loop_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitFor_loop_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitFor_loop_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final For_loop_statementContext for_loop_statement() throws RecognitionException {
		For_loop_statementContext _localctx = new For_loop_statementContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_for_loop_statement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(328);
			match(FOR);
			setState(329);
			match(Lbracket);
			{
			setState(331);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
				{
				setState(330);
				first_expression();
				}
			}

			setState(333);
			match(SEMI);
			}
			{
			setState(336);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
				{
				setState(335);
				second_expression();
				}
			}

			setState(338);
			match(SEMI);
			}
			{
			setState(341);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
				{
				setState(340);
				third_expression();
				}
			}

			}
			setState(343);
			match(Rbracket);
			setState(344);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class First_expressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public First_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_first_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterFirst_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitFirst_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitFirst_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final First_expressionContext first_expression() throws RecognitionException {
		First_expressionContext _localctx = new First_expressionContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_first_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(346);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Second_expressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Second_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_second_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterSecond_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitSecond_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitSecond_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Second_expressionContext second_expression() throws RecognitionException {
		Second_expressionContext _localctx = new Second_expressionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_second_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(348);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Third_expressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public Third_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_third_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterThird_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitThird_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitThird_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Third_expressionContext third_expression() throws RecognitionException {
		Third_expressionContext _localctx = new Third_expressionContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_third_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(350);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class While_loop_statementContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(MuseParser.WHILE, 0); }
		public TerminalNode Lbracket() { return getToken(MuseParser.Lbracket, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode Rbracket() { return getToken(MuseParser.Rbracket, 0); }
		public StatementContext statement() {
			return getRuleContext(StatementContext.class,0);
		}
		public While_loop_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_while_loop_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterWhile_loop_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitWhile_loop_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitWhile_loop_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final While_loop_statementContext while_loop_statement() throws RecognitionException {
		While_loop_statementContext _localctx = new While_loop_statementContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_while_loop_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(352);
			match(WHILE);
			setState(353);
			match(Lbracket);
			setState(354);
			expression();
			setState(355);
			match(Rbracket);
			setState(356);
			statement();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class If_statementContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(MuseParser.IF, 0); }
		public TerminalNode Lbracket() { return getToken(MuseParser.Lbracket, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode Rbracket() { return getToken(MuseParser.Rbracket, 0); }
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(MuseParser.ELSE, 0); }
		public If_statementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_if_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterIf_statement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitIf_statement(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitIf_statement(this);
			else return visitor.visitChildren(this);
		}
	}

	public final If_statementContext if_statement() throws RecognitionException {
		If_statementContext _localctx = new If_statementContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_if_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(358);
			match(IF);
			setState(359);
			match(Lbracket);
			setState(360);
			expression();
			setState(361);
			match(Rbracket);
			{
			setState(362);
			statement();
			}
			setState(365);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,29,_ctx) ) {
			case 1:
				{
				setState(363);
				match(ELSE);
				{
				setState(364);
				statement();
				}
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public Assignment_expressionContext assignment_expression() {
			return getRuleContext(Assignment_expressionContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitExpression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitExpression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(367);
			assignment_expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_expressionContext extends ParserRuleContext {
		public Calculation_expressionContext calculation_expression() {
			return getRuleContext(Calculation_expressionContext.class,0);
		}
		public List<Unary_expressionContext> unary_expression() {
			return getRuleContexts(Unary_expressionContext.class);
		}
		public Unary_expressionContext unary_expression(int i) {
			return getRuleContext(Unary_expressionContext.class,i);
		}
		public List<Assignment_operatorsContext> assignment_operators() {
			return getRuleContexts(Assignment_operatorsContext.class);
		}
		public Assignment_operatorsContext assignment_operators(int i) {
			return getRuleContext(Assignment_operatorsContext.class,i);
		}
		public Assignment_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterAssignment_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitAssignment_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitAssignment_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assignment_expressionContext assignment_expression() throws RecognitionException {
		Assignment_expressionContext _localctx = new Assignment_expressionContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_assignment_expression);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(374);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(369);
					unary_expression();
					setState(370);
					assignment_operators();
					}
					} 
				}
				setState(376);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,30,_ctx);
			}
			setState(377);
			calculation_expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class EqualContext extends ParserRuleContext {
		public EqualContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterEqual(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitEqual(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitEqual(this);
			else return visitor.visitChildren(this);
		}
	}

	public final EqualContext equal() throws RecognitionException {
		EqualContext _localctx = new EqualContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_equal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(379);
			match(Equal);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Assignment_operatorsContext extends ParserRuleContext {
		public EqualContext equal() {
			return getRuleContext(EqualContext.class,0);
		}
		public Assignment_operatorsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment_operators; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterAssignment_operators(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitAssignment_operators(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitAssignment_operators(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Assignment_operatorsContext assignment_operators() throws RecognitionException {
		Assignment_operatorsContext _localctx = new Assignment_operatorsContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_assignment_operators);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(381);
			equal();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Calculation_expressionContext extends ParserRuleContext {
		public Logical_caclulation_expressionContext logical_caclulation_expression() {
			return getRuleContext(Logical_caclulation_expressionContext.class,0);
		}
		public Calculation_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_calculation_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterCalculation_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitCalculation_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitCalculation_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Calculation_expressionContext calculation_expression() throws RecognitionException {
		Calculation_expressionContext _localctx = new Calculation_expressionContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_calculation_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(383);
			logical_caclulation_expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_caclulation_expressionContext extends ParserRuleContext {
		public Logical_or_expressionContext logical_or_expression() {
			return getRuleContext(Logical_or_expressionContext.class,0);
		}
		public Logical_caclulation_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_caclulation_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterLogical_caclulation_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitLogical_caclulation_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitLogical_caclulation_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Logical_caclulation_expressionContext logical_caclulation_expression() throws RecognitionException {
		Logical_caclulation_expressionContext _localctx = new Logical_caclulation_expressionContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_logical_caclulation_expression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(385);
			logical_or_expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_or_expressionContext extends ParserRuleContext {
		public List<Logical_and_expressionContext> logical_and_expression() {
			return getRuleContexts(Logical_and_expressionContext.class);
		}
		public Logical_and_expressionContext logical_and_expression(int i) {
			return getRuleContext(Logical_and_expressionContext.class,i);
		}
		public Logical_or_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterLogical_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitLogical_or_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitLogical_or_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Logical_or_expressionContext logical_or_expression() throws RecognitionException {
		Logical_or_expressionContext _localctx = new Logical_or_expressionContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_logical_or_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(387);
			logical_and_expression();
			setState(392);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__0) {
				{
				{
				setState(388);
				match(T__0);
				setState(389);
				logical_and_expression();
				}
				}
				setState(394);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_and_expressionContext extends ParserRuleContext {
		public List<Bitwise_or_expressionContext> bitwise_or_expression() {
			return getRuleContexts(Bitwise_or_expressionContext.class);
		}
		public Bitwise_or_expressionContext bitwise_or_expression(int i) {
			return getRuleContext(Bitwise_or_expressionContext.class,i);
		}
		public Logical_and_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterLogical_and_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitLogical_and_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitLogical_and_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Logical_and_expressionContext logical_and_expression() throws RecognitionException {
		Logical_and_expressionContext _localctx = new Logical_and_expressionContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_logical_and_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(395);
			bitwise_or_expression();
			setState(400);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__1) {
				{
				{
				setState(396);
				match(T__1);
				setState(397);
				bitwise_or_expression();
				}
				}
				setState(402);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bitwise_or_expressionContext extends ParserRuleContext {
		public List<Bitwise_xor_expressionContext> bitwise_xor_expression() {
			return getRuleContexts(Bitwise_xor_expressionContext.class);
		}
		public Bitwise_xor_expressionContext bitwise_xor_expression(int i) {
			return getRuleContext(Bitwise_xor_expressionContext.class,i);
		}
		public Bitwise_or_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitwise_or_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterBitwise_or_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitBitwise_or_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitBitwise_or_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bitwise_or_expressionContext bitwise_or_expression() throws RecognitionException {
		Bitwise_or_expressionContext _localctx = new Bitwise_or_expressionContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_bitwise_or_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(403);
			bitwise_xor_expression();
			setState(408);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__2) {
				{
				{
				setState(404);
				match(T__2);
				setState(405);
				bitwise_xor_expression();
				}
				}
				setState(410);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bitwise_xor_expressionContext extends ParserRuleContext {
		public List<Bitwise_and_expressionContext> bitwise_and_expression() {
			return getRuleContexts(Bitwise_and_expressionContext.class);
		}
		public Bitwise_and_expressionContext bitwise_and_expression(int i) {
			return getRuleContext(Bitwise_and_expressionContext.class,i);
		}
		public Bitwise_xor_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitwise_xor_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterBitwise_xor_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitBitwise_xor_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitBitwise_xor_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bitwise_xor_expressionContext bitwise_xor_expression() throws RecognitionException {
		Bitwise_xor_expressionContext _localctx = new Bitwise_xor_expressionContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_bitwise_xor_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(411);
			bitwise_and_expression();
			setState(416);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__3) {
				{
				{
				setState(412);
				match(T__3);
				setState(413);
				bitwise_and_expression();
				}
				}
				setState(418);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bitwise_and_expressionContext extends ParserRuleContext {
		public List<Equality_expressionContext> equality_expression() {
			return getRuleContexts(Equality_expressionContext.class);
		}
		public Equality_expressionContext equality_expression(int i) {
			return getRuleContext(Equality_expressionContext.class,i);
		}
		public Bitwise_and_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitwise_and_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterBitwise_and_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitBitwise_and_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitBitwise_and_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bitwise_and_expressionContext bitwise_and_expression() throws RecognitionException {
		Bitwise_and_expressionContext _localctx = new Bitwise_and_expressionContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_bitwise_and_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(419);
			equality_expression();
			setState(424);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__4) {
				{
				{
				setState(420);
				match(T__4);
				setState(421);
				equality_expression();
				}
				}
				setState(426);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IsequalContext extends ParserRuleContext {
		public IsequalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_isequal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterIsequal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitIsequal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitIsequal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final IsequalContext isequal() throws RecognitionException {
		IsequalContext _localctx = new IsequalContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_isequal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(427);
			match(T__5);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotequalContext extends ParserRuleContext {
		public NotequalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notequal; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterNotequal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitNotequal(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitNotequal(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NotequalContext notequal() throws RecognitionException {
		NotequalContext _localctx = new NotequalContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_notequal);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(429);
			match(T__6);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Equality_expressionContext extends ParserRuleContext {
		public List<Relation_expressionContext> relation_expression() {
			return getRuleContexts(Relation_expressionContext.class);
		}
		public Relation_expressionContext relation_expression(int i) {
			return getRuleContext(Relation_expressionContext.class,i);
		}
		public List<Equ_opContext> equ_op() {
			return getRuleContexts(Equ_opContext.class);
		}
		public Equ_opContext equ_op(int i) {
			return getRuleContext(Equ_opContext.class,i);
		}
		public Equality_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equality_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterEquality_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitEquality_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitEquality_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Equality_expressionContext equality_expression() throws RecognitionException {
		Equality_expressionContext _localctx = new Equality_expressionContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_equality_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(431);
			relation_expression();
			setState(437);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5 || _la==T__6) {
				{
				{
				setState(432);
				equ_op();
				setState(433);
				relation_expression();
				}
				}
				setState(439);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Equ_opContext extends ParserRuleContext {
		public IsequalContext isequal() {
			return getRuleContext(IsequalContext.class,0);
		}
		public NotequalContext notequal() {
			return getRuleContext(NotequalContext.class,0);
		}
		public Equ_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_equ_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterEqu_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitEqu_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitEqu_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Equ_opContext equ_op() throws RecognitionException {
		Equ_opContext _localctx = new Equ_opContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_equ_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(442);
			switch (_input.LA(1)) {
			case T__5:
				{
				setState(440);
				isequal();
				}
				break;
			case T__6:
				{
				setState(441);
				notequal();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BiggerContext extends ParserRuleContext {
		public BiggerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bigger; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterBigger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitBigger(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitBigger(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BiggerContext bigger() throws RecognitionException {
		BiggerContext _localctx = new BiggerContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_bigger);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(444);
			match(T__7);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SmallerContext extends ParserRuleContext {
		public SmallerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_smaller; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterSmaller(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitSmaller(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitSmaller(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SmallerContext smaller() throws RecognitionException {
		SmallerContext _localctx = new SmallerContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_smaller);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(446);
			match(T__8);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bigger_eContext extends ParserRuleContext {
		public Bigger_eContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bigger_e; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterBigger_e(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitBigger_e(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitBigger_e(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bigger_eContext bigger_e() throws RecognitionException {
		Bigger_eContext _localctx = new Bigger_eContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_bigger_e);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(448);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Smaller_eContext extends ParserRuleContext {
		public Smaller_eContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_smaller_e; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterSmaller_e(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitSmaller_e(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitSmaller_e(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Smaller_eContext smaller_e() throws RecognitionException {
		Smaller_eContext _localctx = new Smaller_eContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_smaller_e);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(450);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rela_opContext extends ParserRuleContext {
		public Smaller_eContext smaller_e() {
			return getRuleContext(Smaller_eContext.class,0);
		}
		public Bigger_eContext bigger_e() {
			return getRuleContext(Bigger_eContext.class,0);
		}
		public BiggerContext bigger() {
			return getRuleContext(BiggerContext.class,0);
		}
		public SmallerContext smaller() {
			return getRuleContext(SmallerContext.class,0);
		}
		public Rela_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rela_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterRela_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitRela_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitRela_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Rela_opContext rela_op() throws RecognitionException {
		Rela_opContext _localctx = new Rela_opContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_rela_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(456);
			switch (_input.LA(1)) {
			case T__10:
				{
				setState(452);
				smaller_e();
				}
				break;
			case T__9:
				{
				setState(453);
				bigger_e();
				}
				break;
			case T__7:
				{
				setState(454);
				bigger();
				}
				break;
			case T__8:
				{
				setState(455);
				smaller();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Relation_expressionContext extends ParserRuleContext {
		public List<Shift_expressionContext> shift_expression() {
			return getRuleContexts(Shift_expressionContext.class);
		}
		public Shift_expressionContext shift_expression(int i) {
			return getRuleContext(Shift_expressionContext.class,i);
		}
		public Rela_opContext rela_op() {
			return getRuleContext(Rela_opContext.class,0);
		}
		public Relation_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relation_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterRelation_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitRelation_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitRelation_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Relation_expressionContext relation_expression() throws RecognitionException {
		Relation_expressionContext _localctx = new Relation_expressionContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_relation_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(458);
			shift_expression();
			setState(462);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__7) | (1L << T__8) | (1L << T__9) | (1L << T__10))) != 0)) {
				{
				setState(459);
				rela_op();
				setState(460);
				shift_expression();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LshiftContext extends ParserRuleContext {
		public LshiftContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lshift; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterLshift(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitLshift(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitLshift(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LshiftContext lshift() throws RecognitionException {
		LshiftContext _localctx = new LshiftContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_lshift);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(464);
			match(T__11);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RshiftContext extends ParserRuleContext {
		public RshiftContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rshift; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterRshift(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitRshift(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitRshift(this);
			else return visitor.visitChildren(this);
		}
	}

	public final RshiftContext rshift() throws RecognitionException {
		RshiftContext _localctx = new RshiftContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_rshift);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(466);
			match(T__12);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlusContext extends ParserRuleContext {
		public PlusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterPlus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitPlus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitPlus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PlusContext plus() throws RecognitionException {
		PlusContext _localctx = new PlusContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_plus);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(468);
			match(T__13);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MinusContext extends ParserRuleContext {
		public MinusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_minus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterMinus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitMinus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitMinus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MinusContext minus() throws RecognitionException {
		MinusContext _localctx = new MinusContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_minus);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(470);
			match(T__14);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Shift_opContext extends ParserRuleContext {
		public LshiftContext lshift() {
			return getRuleContext(LshiftContext.class,0);
		}
		public RshiftContext rshift() {
			return getRuleContext(RshiftContext.class,0);
		}
		public Shift_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shift_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterShift_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitShift_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitShift_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Shift_opContext shift_op() throws RecognitionException {
		Shift_opContext _localctx = new Shift_opContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_shift_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(474);
			switch (_input.LA(1)) {
			case T__11:
				{
				setState(472);
				lshift();
				}
				break;
			case T__12:
				{
				setState(473);
				rshift();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Add_opContext extends ParserRuleContext {
		public PlusContext plus() {
			return getRuleContext(PlusContext.class,0);
		}
		public MinusContext minus() {
			return getRuleContext(MinusContext.class,0);
		}
		public Add_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_add_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterAdd_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitAdd_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitAdd_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Add_opContext add_op() throws RecognitionException {
		Add_opContext _localctx = new Add_opContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_add_op);
		try {
			setState(478);
			switch (_input.LA(1)) {
			case T__13:
				enterOuterAlt(_localctx, 1);
				{
				setState(476);
				plus();
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 2);
				{
				setState(477);
				minus();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Shift_expressionContext extends ParserRuleContext {
		public List<Add_expressionContext> add_expression() {
			return getRuleContexts(Add_expressionContext.class);
		}
		public Add_expressionContext add_expression(int i) {
			return getRuleContext(Add_expressionContext.class,i);
		}
		public List<Shift_opContext> shift_op() {
			return getRuleContexts(Shift_opContext.class);
		}
		public Shift_opContext shift_op(int i) {
			return getRuleContext(Shift_opContext.class,i);
		}
		public Shift_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_shift_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterShift_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitShift_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitShift_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Shift_expressionContext shift_expression() throws RecognitionException {
		Shift_expressionContext _localctx = new Shift_expressionContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_shift_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(480);
			add_expression();
			setState(486);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__11 || _la==T__12) {
				{
				{
				setState(481);
				shift_op();
				setState(482);
				add_expression();
				}
				}
				setState(488);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Add_expressionContext extends ParserRuleContext {
		public List<Mul_expressionContext> mul_expression() {
			return getRuleContexts(Mul_expressionContext.class);
		}
		public Mul_expressionContext mul_expression(int i) {
			return getRuleContext(Mul_expressionContext.class,i);
		}
		public List<Add_opContext> add_op() {
			return getRuleContexts(Add_opContext.class);
		}
		public Add_opContext add_op(int i) {
			return getRuleContext(Add_opContext.class,i);
		}
		public Add_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_add_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterAdd_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitAdd_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitAdd_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Add_expressionContext add_expression() throws RecognitionException {
		Add_expressionContext _localctx = new Add_expressionContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_add_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(489);
			mul_expression();
			setState(495);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__13 || _la==T__14) {
				{
				{
				setState(490);
				add_op();
				setState(491);
				mul_expression();
				}
				}
				setState(497);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MultiplyContext extends ParserRuleContext {
		public MultiplyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_multiply; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterMultiply(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitMultiply(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitMultiply(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MultiplyContext multiply() throws RecognitionException {
		MultiplyContext _localctx = new MultiplyContext(_ctx, getState());
		enterRule(_localctx, 112, RULE_multiply);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(498);
			match(T__15);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DivisionContext extends ParserRuleContext {
		public DivisionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_division; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterDivision(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitDivision(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitDivision(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DivisionContext division() throws RecognitionException {
		DivisionContext _localctx = new DivisionContext(_ctx, getState());
		enterRule(_localctx, 114, RULE_division);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(500);
			match(T__16);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModContext extends ParserRuleContext {
		public ModContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mod; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterMod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitMod(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitMod(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ModContext mod() throws RecognitionException {
		ModContext _localctx = new ModContext(_ctx, getState());
		enterRule(_localctx, 116, RULE_mod);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(502);
			match(T__17);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Mul_opContext extends ParserRuleContext {
		public MultiplyContext multiply() {
			return getRuleContext(MultiplyContext.class,0);
		}
		public DivisionContext division() {
			return getRuleContext(DivisionContext.class,0);
		}
		public ModContext mod() {
			return getRuleContext(ModContext.class,0);
		}
		public Mul_opContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mul_op; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterMul_op(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitMul_op(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitMul_op(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Mul_opContext mul_op() throws RecognitionException {
		Mul_opContext _localctx = new Mul_opContext(_ctx, getState());
		enterRule(_localctx, 118, RULE_mul_op);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(507);
			switch (_input.LA(1)) {
			case T__15:
				{
				setState(504);
				multiply();
				}
				break;
			case T__16:
				{
				setState(505);
				division();
				}
				break;
			case T__17:
				{
				setState(506);
				mod();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Mul_expressionContext extends ParserRuleContext {
		public List<Unary_expressionContext> unary_expression() {
			return getRuleContexts(Unary_expressionContext.class);
		}
		public Unary_expressionContext unary_expression(int i) {
			return getRuleContext(Unary_expressionContext.class,i);
		}
		public List<Mul_opContext> mul_op() {
			return getRuleContexts(Mul_opContext.class);
		}
		public Mul_opContext mul_op(int i) {
			return getRuleContext(Mul_opContext.class,i);
		}
		public Mul_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mul_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterMul_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitMul_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitMul_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Mul_expressionContext mul_expression() throws RecognitionException {
		Mul_expressionContext _localctx = new Mul_expressionContext(_ctx, getState());
		enterRule(_localctx, 120, RULE_mul_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(509);
			unary_expression();
			setState(515);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__15) | (1L << T__16) | (1L << T__17))) != 0)) {
				{
				{
				setState(510);
				mul_op();
				setState(511);
				unary_expression();
				}
				}
				setState(517);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PlusplusContext extends ParserRuleContext {
		public PlusplusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_plusplus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterPlusplus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitPlusplus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitPlusplus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PlusplusContext plusplus() throws RecognitionException {
		PlusplusContext _localctx = new PlusplusContext(_ctx, getState());
		enterRule(_localctx, 122, RULE_plusplus);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(518);
			match(T__18);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MinusminusContext extends ParserRuleContext {
		public MinusminusContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_minusminus; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterMinusminus(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitMinusminus(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitMinusminus(this);
			else return visitor.visitChildren(this);
		}
	}

	public final MinusminusContext minusminus() throws RecognitionException {
		MinusminusContext _localctx = new MinusminusContext(_ctx, getState());
		enterRule(_localctx, 124, RULE_minusminus);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(520);
			match(T__19);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_expressionContext extends ParserRuleContext {
		public Postfix_expressionContext postfix_expression() {
			return getRuleContext(Postfix_expressionContext.class,0);
		}
		public Unary_operationContext unary_operation() {
			return getRuleContext(Unary_operationContext.class,0);
		}
		public Unary_expressionContext unary_expression() {
			return getRuleContext(Unary_expressionContext.class,0);
		}
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public FunctionCall_expressionContext functionCall_expression() {
			return getRuleContext(FunctionCall_expressionContext.class,0);
		}
		public New_operationContext new_operation() {
			return getRuleContext(New_operationContext.class,0);
		}
		public Pre_defined_constantsContext pre_defined_constants() {
			return getRuleContext(Pre_defined_constantsContext.class,0);
		}
		public Unary_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterUnary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitUnary_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitUnary_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unary_expressionContext unary_expression() throws RecognitionException {
		Unary_expressionContext _localctx = new Unary_expressionContext(_ctx, getState());
		enterRule(_localctx, 126, RULE_unary_expression);
		try {
			setState(530);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,46,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(522);
				postfix_expression();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(523);
				unary_operation();
				setState(524);
				unary_expression();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(526);
				number();
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(527);
				functionCall_expression();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(528);
				new_operation();
				}
				break;
			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(529);
				pre_defined_constants();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Pre_defined_constantsContext extends ParserRuleContext {
		public Pre_defined_constantsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pre_defined_constants; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterPre_defined_constants(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitPre_defined_constants(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitPre_defined_constants(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Pre_defined_constantsContext pre_defined_constants() throws RecognitionException {
		Pre_defined_constantsContext _localctx = new Pre_defined_constantsContext(_ctx, getState());
		enterRule(_localctx, 128, RULE_pre_defined_constants);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(532);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__20) | (1L << T__21) | (1L << T__22))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_newContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public Class_newContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_new; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterClass_new(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitClass_new(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitClass_new(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Class_newContext class_new() throws RecognitionException {
		Class_newContext _localctx = new Class_newContext(_ctx, getState());
		enterRule(_localctx, 130, RULE_class_new);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(534);
			match(Identifier);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class New_operationContext extends ParserRuleContext {
		public TerminalNode New() { return getToken(MuseParser.New, 0); }
		public Array_newContext array_new() {
			return getRuleContext(Array_newContext.class,0);
		}
		public Class_newContext class_new() {
			return getRuleContext(Class_newContext.class,0);
		}
		public New_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_new_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterNew_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitNew_operation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitNew_operation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final New_operationContext new_operation() throws RecognitionException {
		New_operationContext _localctx = new New_operationContext(_ctx, getState());
		enterRule(_localctx, 132, RULE_new_operation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(536);
			match(New);
			setState(539);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,47,_ctx) ) {
			case 1:
				{
				setState(537);
				array_new();
				}
				break;
			case 2:
				{
				setState(538);
				class_new();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionCall_expressionContext extends ParserRuleContext {
		public TerminalNode Lbracket() { return getToken(MuseParser.Lbracket, 0); }
		public TerminalNode Rbracket() { return getToken(MuseParser.Rbracket, 0); }
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public ArguementsContext arguements() {
			return getRuleContext(ArguementsContext.class,0);
		}
		public FunctionCall_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionCall_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterFunctionCall_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitFunctionCall_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitFunctionCall_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FunctionCall_expressionContext functionCall_expression() throws RecognitionException {
		FunctionCall_expressionContext _localctx = new FunctionCall_expressionContext(_ctx, getState());
		enterRule(_localctx, 134, RULE_functionCall_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			{
			setState(541);
			match(Identifier);
			}
			setState(542);
			match(Lbracket);
			setState(544);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__14) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
				{
				setState(543);
				arguements();
				}
			}

			setState(546);
			match(Rbracket);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArguementsContext extends ParserRuleContext {
		public List<Assignment_expressionContext> assignment_expression() {
			return getRuleContexts(Assignment_expressionContext.class);
		}
		public Assignment_expressionContext assignment_expression(int i) {
			return getRuleContext(Assignment_expressionContext.class,i);
		}
		public List<TerminalNode> Comma() { return getTokens(MuseParser.Comma); }
		public TerminalNode Comma(int i) {
			return getToken(MuseParser.Comma, i);
		}
		public ArguementsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arguements; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterArguements(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitArguements(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitArguements(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ArguementsContext arguements() throws RecognitionException {
		ArguementsContext _localctx = new ArguementsContext(_ctx, getState());
		enterRule(_localctx, 136, RULE_arguements);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(548);
			assignment_expression();
			setState(553);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==Comma) {
				{
				{
				setState(549);
				match(Comma);
				setState(550);
				assignment_expression();
				}
				}
				setState(555);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ValueContext extends ParserRuleContext {
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode Lbracket() { return getToken(MuseParser.Lbracket, 0); }
		public ValueContext value() {
			return getRuleContext(ValueContext.class,0);
		}
		public TerminalNode Rbracket() { return getToken(MuseParser.Rbracket, 0); }
		public ValueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterValue(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitValue(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitValue(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ValueContext value() throws RecognitionException {
		ValueContext _localctx = new ValueContext(_ctx, getState());
		enterRule(_localctx, 138, RULE_value);
		try {
			setState(562);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,50,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(556);
				constant();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(557);
				expression();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(558);
				match(Lbracket);
				setState(559);
				value();
				setState(560);
				match(Rbracket);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Not_signContext extends ParserRuleContext {
		public Not_signContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_not_sign; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterNot_sign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitNot_sign(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitNot_sign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Not_signContext not_sign() throws RecognitionException {
		Not_signContext _localctx = new Not_signContext(_ctx, getState());
		enterRule(_localctx, 140, RULE_not_sign);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(564);
			match(T__23);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Unary_operationContext extends ParserRuleContext {
		public Not_signContext not_sign() {
			return getRuleContext(Not_signContext.class,0);
		}
		public MinusContext minus() {
			return getRuleContext(MinusContext.class,0);
		}
		public PlusplusContext plusplus() {
			return getRuleContext(PlusplusContext.class,0);
		}
		public MinusminusContext minusminus() {
			return getRuleContext(MinusminusContext.class,0);
		}
		public TerminalNode Lbracket() { return getToken(MuseParser.Lbracket, 0); }
		public Type_nameContext type_name() {
			return getRuleContext(Type_nameContext.class,0);
		}
		public TerminalNode Rbracket() { return getToken(MuseParser.Rbracket, 0); }
		public Bitwise_notContext bitwise_not() {
			return getRuleContext(Bitwise_notContext.class,0);
		}
		public Unary_operationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_unary_operation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterUnary_operation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitUnary_operation(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitUnary_operation(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Unary_operationContext unary_operation() throws RecognitionException {
		Unary_operationContext _localctx = new Unary_operationContext(_ctx, getState());
		enterRule(_localctx, 142, RULE_unary_operation);
		try {
			setState(575);
			switch (_input.LA(1)) {
			case T__23:
				enterOuterAlt(_localctx, 1);
				{
				setState(566);
				not_sign();
				}
				break;
			case T__14:
				enterOuterAlt(_localctx, 2);
				{
				setState(567);
				minus();
				}
				break;
			case T__18:
				enterOuterAlt(_localctx, 3);
				{
				setState(568);
				plusplus();
				}
				break;
			case T__19:
				enterOuterAlt(_localctx, 4);
				{
				setState(569);
				minusminus();
				}
				break;
			case Lbracket:
				enterOuterAlt(_localctx, 5);
				{
				setState(570);
				match(Lbracket);
				setState(571);
				type_name();
				setState(572);
				match(Rbracket);
				}
				break;
			case T__24:
				enterOuterAlt(_localctx, 6);
				{
				setState(574);
				bitwise_not();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Bitwise_notContext extends ParserRuleContext {
		public Bitwise_notContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bitwise_not; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterBitwise_not(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitBitwise_not(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitBitwise_not(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Bitwise_notContext bitwise_not() throws RecognitionException {
		Bitwise_notContext _localctx = new Bitwise_notContext(_ctx, getState());
		enterRule(_localctx, 144, RULE_bitwise_not);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(577);
			match(T__24);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Postfix_expressionContext extends ParserRuleContext {
		public Primary_expressionContext primary_expression() {
			return getRuleContext(Primary_expressionContext.class,0);
		}
		public List<PostfixContext> postfix() {
			return getRuleContexts(PostfixContext.class);
		}
		public PostfixContext postfix(int i) {
			return getRuleContext(PostfixContext.class,i);
		}
		public Postfix_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfix_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterPostfix_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitPostfix_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitPostfix_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Postfix_expressionContext postfix_expression() throws RecognitionException {
		Postfix_expressionContext _localctx = new Postfix_expressionContext(_ctx, getState());
		enterRule(_localctx, 146, RULE_postfix_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(579);
			primary_expression();
			setState(583);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__18) | (1L << T__19) | (1L << LMidbracket) | (1L << Dot))) != 0)) {
				{
				{
				setState(580);
				postfix();
				}
				}
				setState(585);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GetMemberContext extends ParserRuleContext {
		public GetMemberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_getMember; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterGetMember(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitGetMember(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitGetMember(this);
			else return visitor.visitChildren(this);
		}
	}

	public final GetMemberContext getMember() throws RecognitionException {
		GetMemberContext _localctx = new GetMemberContext(_ctx, getState());
		enterRule(_localctx, 148, RULE_getMember);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(586);
			match(Dot);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PostfixContext extends ParserRuleContext {
		public TerminalNode LMidbracket() { return getToken(MuseParser.LMidbracket, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode RMidbracket() { return getToken(MuseParser.RMidbracket, 0); }
		public GetMemberContext getMember() {
			return getRuleContext(GetMemberContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public FunctionCall_expressionContext functionCall_expression() {
			return getRuleContext(FunctionCall_expressionContext.class,0);
		}
		public PlusplusContext plusplus() {
			return getRuleContext(PlusplusContext.class,0);
		}
		public MinusminusContext minusminus() {
			return getRuleContext(MinusminusContext.class,0);
		}
		public PostfixContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_postfix; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterPostfix(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitPostfix(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitPostfix(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PostfixContext postfix() throws RecognitionException {
		PostfixContext _localctx = new PostfixContext(_ctx, getState());
		enterRule(_localctx, 150, RULE_postfix);
		try {
			setState(599);
			switch (_input.LA(1)) {
			case LMidbracket:
				enterOuterAlt(_localctx, 1);
				{
				setState(588);
				match(LMidbracket);
				setState(589);
				expression();
				setState(590);
				match(RMidbracket);
				}
				break;
			case Dot:
				enterOuterAlt(_localctx, 2);
				{
				setState(592);
				getMember();
				setState(595);
				_errHandler.sync(this);
				switch ( getInterpreter().adaptivePredict(_input,53,_ctx) ) {
				case 1:
					{
					setState(593);
					match(Identifier);
					}
					break;
				case 2:
					{
					setState(594);
					functionCall_expression();
					}
					break;
				}
				}
				break;
			case T__18:
				enterOuterAlt(_localctx, 3);
				{
				setState(597);
				plusplus();
				}
				break;
			case T__19:
				enterOuterAlt(_localctx, 4);
				{
				setState(598);
				minusminus();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Primary_expressionContext extends ParserRuleContext {
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public ConstantContext constant() {
			return getRuleContext(ConstantContext.class,0);
		}
		public TerminalNode Lbracket() { return getToken(MuseParser.Lbracket, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode Rbracket() { return getToken(MuseParser.Rbracket, 0); }
		public FunctionCall_expressionContext functionCall_expression() {
			return getRuleContext(FunctionCall_expressionContext.class,0);
		}
		public Primary_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_primary_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterPrimary_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitPrimary_expression(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitPrimary_expression(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Primary_expressionContext primary_expression() throws RecognitionException {
		Primary_expressionContext _localctx = new Primary_expressionContext(_ctx, getState());
		enterRule(_localctx, 152, RULE_primary_expression);
		try {
			setState(608);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,55,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(601);
				match(Identifier);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(602);
				constant();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(603);
				match(Lbracket);
				setState(604);
				expression();
				setState(605);
				match(Rbracket);
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(607);
				functionCall_expression();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Type_nameContext extends ParserRuleContext {
		public TypenametokensContext typenametokens() {
			return getRuleContext(TypenametokensContext.class,0);
		}
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public Type_nameContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type_name; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterType_name(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitType_name(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitType_name(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Type_nameContext type_name() throws RecognitionException {
		Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
		enterRule(_localctx, 154, RULE_type_name);
		try {
			setState(612);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,56,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(610);
				typenametokens();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(611);
				match(Identifier);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstantContext extends ParserRuleContext {
		public Numeric_constantContext numeric_constant() {
			return getRuleContext(Numeric_constantContext.class,0);
		}
		public String_constantContext string_constant() {
			return getRuleContext(String_constantContext.class,0);
		}
		public ConstantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterConstant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitConstant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitConstant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConstantContext constant() throws RecognitionException {
		ConstantContext _localctx = new ConstantContext(_ctx, getState());
		enterRule(_localctx, 156, RULE_constant);
		try {
			setState(616);
			switch (_input.LA(1)) {
			case DecimalConstant:
			case Sign:
				enterOuterAlt(_localctx, 1);
				{
				setState(614);
				numeric_constant();
				}
				break;
			case StringLiteral:
				enterOuterAlt(_localctx, 2);
				{
				setState(615);
				string_constant();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Numeric_constantContext extends ParserRuleContext {
		public Integer_constantContext integer_constant() {
			return getRuleContext(Integer_constantContext.class,0);
		}
		public Numeric_constantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_numeric_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterNumeric_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitNumeric_constant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitNumeric_constant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Numeric_constantContext numeric_constant() throws RecognitionException {
		Numeric_constantContext _localctx = new Numeric_constantContext(_ctx, getState());
		enterRule(_localctx, 158, RULE_numeric_constant);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(618);
			integer_constant();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Integer_constantContext extends ParserRuleContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public Integer_constantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integer_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterInteger_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitInteger_constant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitInteger_constant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Integer_constantContext integer_constant() throws RecognitionException {
		Integer_constantContext _localctx = new Integer_constantContext(_ctx, getState());
		enterRule(_localctx, 160, RULE_integer_constant);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(620);
			number();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class String_constantContext extends ParserRuleContext {
		public TerminalNode StringLiteral() { return getToken(MuseParser.StringLiteral, 0); }
		public String_constantContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_string_constant; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterString_constant(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitString_constant(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitString_constant(this);
			else return visitor.visitChildren(this);
		}
	}

	public final String_constantContext string_constant() throws RecognitionException {
		String_constantContext _localctx = new String_constantContext(_ctx, getState());
		enterRule(_localctx, 162, RULE_string_constant);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(622);
			match(StringLiteral);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypenametokensContext extends ParserRuleContext {
		public Token builtin_types;
		public TerminalNode Identifier() { return getToken(MuseParser.Identifier, 0); }
		public TypenametokensContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typenametokens; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterTypenametokens(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitTypenametokens(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitTypenametokens(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TypenametokensContext typenametokens() throws RecognitionException {
		TypenametokensContext _localctx = new TypenametokensContext(_ctx, getState());
		enterRule(_localctx, 164, RULE_typenametokens);
		int _la;
		try {
			setState(626);
			switch (_input.LA(1)) {
			case T__25:
			case T__26:
			case T__27:
				enterOuterAlt(_localctx, 1);
				{
				setState(624);
				((TypenametokensContext)_localctx).builtin_types = _input.LT(1);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27))) != 0)) ) {
					((TypenametokensContext)_localctx).builtin_types = (Token)_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
				break;
			case Identifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(625);
				match(Identifier);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public TerminalNode DecimalConstant() { return getToken(MuseParser.DecimalConstant, 0); }
		public TerminalNode Sign() { return getToken(MuseParser.Sign, 0); }
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 166, RULE_number);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(629);
			_la = _input.LA(1);
			if (_la==Sign) {
				{
				setState(628);
				match(Sign);
				}
			}

			setState(631);
			match(DecimalConstant);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Union_classContext extends ParserRuleContext {
		public Union_classContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_union_class; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).enterUnion_class(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof MuseListener ) ((MuseListener)listener).exitUnion_class(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MuseVisitor ) return ((MuseVisitor<? extends T>)visitor).visitUnion_class(this);
			else return visitor.visitChildren(this);
		}
	}

	public final Union_classContext union_class() throws RecognitionException {
		Union_classContext _localctx = new Union_classContext(_ctx, getState());
		enterRule(_localctx, 168, RULE_union_class);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(633);
			match(T__28);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3B\u027e\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\4U\tU\4V\tV\3\2\3\2\6\2\u00af\n\2\r\2\16\2\u00b0\3\3\3\3\3\3\3\3\3\3"+
		"\5\3\u00b8\n\3\3\4\3\4\3\5\3\5\3\6\3\6\3\6\3\6\5\6\u00c2\n\6\3\7\3\7\5"+
		"\7\u00c6\n\7\3\b\3\b\3\b\6\b\u00cb\n\b\r\b\16\b\u00cc\3\t\3\t\3\t\3\t"+
		"\3\t\6\t\u00d4\n\t\r\t\16\t\u00d5\3\t\3\t\7\t\u00da\n\t\f\t\16\t\u00dd"+
		"\13\t\3\n\3\n\5\n\u00e1\n\n\3\13\3\13\5\13\u00e5\n\13\3\13\3\13\3\13\3"+
		"\13\3\13\7\13\u00ec\n\13\f\13\16\13\u00ef\13\13\3\13\3\13\3\f\3\f\3\r"+
		"\3\r\3\16\3\16\3\16\3\16\5\16\u00fb\n\16\3\16\3\16\3\16\3\16\7\16\u0101"+
		"\n\16\f\16\16\16\u0104\13\16\3\16\3\16\3\16\3\16\3\16\3\16\5\16\u010c"+
		"\n\16\3\16\3\16\3\16\5\16\u0111\n\16\3\17\3\17\3\17\7\17\u0116\n\17\f"+
		"\17\16\17\u0119\13\17\3\17\3\17\5\17\u011d\n\17\3\20\3\20\3\20\3\21\3"+
		"\21\3\21\3\21\5\21\u0126\n\21\3\22\5\22\u0129\n\22\3\22\3\22\3\23\3\23"+
		"\5\23\u012f\n\23\3\24\3\24\3\24\7\24\u0134\n\24\f\24\16\24\u0137\13\24"+
		"\3\24\3\24\3\25\3\25\5\25\u013d\n\25\3\26\3\26\3\26\3\26\3\26\3\26\3\26"+
		"\5\26\u0146\n\26\3\26\5\26\u0149\n\26\3\27\3\27\3\27\5\27\u014e\n\27\3"+
		"\27\3\27\3\27\5\27\u0153\n\27\3\27\3\27\3\27\5\27\u0158\n\27\3\27\3\27"+
		"\3\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\33\3\33\3\33\3\33\3\34"+
		"\3\34\3\34\3\34\3\34\3\34\3\34\5\34\u0170\n\34\3\35\3\35\3\36\3\36\3\36"+
		"\7\36\u0177\n\36\f\36\16\36\u017a\13\36\3\36\3\36\3\37\3\37\3 \3 \3!\3"+
		"!\3\"\3\"\3#\3#\3#\7#\u0189\n#\f#\16#\u018c\13#\3$\3$\3$\7$\u0191\n$\f"+
		"$\16$\u0194\13$\3%\3%\3%\7%\u0199\n%\f%\16%\u019c\13%\3&\3&\3&\7&\u01a1"+
		"\n&\f&\16&\u01a4\13&\3\'\3\'\3\'\7\'\u01a9\n\'\f\'\16\'\u01ac\13\'\3("+
		"\3(\3)\3)\3*\3*\3*\3*\7*\u01b6\n*\f*\16*\u01b9\13*\3+\3+\5+\u01bd\n+\3"+
		",\3,\3-\3-\3.\3.\3/\3/\3\60\3\60\3\60\3\60\5\60\u01cb\n\60\3\61\3\61\3"+
		"\61\3\61\5\61\u01d1\n\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66"+
		"\3\66\5\66\u01dd\n\66\3\67\3\67\5\67\u01e1\n\67\38\38\38\38\78\u01e7\n"+
		"8\f8\168\u01ea\138\39\39\39\39\79\u01f0\n9\f9\169\u01f3\139\3:\3:\3;\3"+
		";\3<\3<\3=\3=\3=\5=\u01fe\n=\3>\3>\3>\3>\7>\u0204\n>\f>\16>\u0207\13>"+
		"\3?\3?\3@\3@\3A\3A\3A\3A\3A\3A\3A\3A\5A\u0215\nA\3B\3B\3C\3C\3D\3D\3D"+
		"\5D\u021e\nD\3E\3E\3E\5E\u0223\nE\3E\3E\3F\3F\3F\7F\u022a\nF\fF\16F\u022d"+
		"\13F\3G\3G\3G\3G\3G\3G\5G\u0235\nG\3H\3H\3I\3I\3I\3I\3I\3I\3I\3I\3I\5"+
		"I\u0242\nI\3J\3J\3K\3K\7K\u0248\nK\fK\16K\u024b\13K\3L\3L\3M\3M\3M\3M"+
		"\3M\3M\3M\5M\u0256\nM\3M\3M\5M\u025a\nM\3N\3N\3N\3N\3N\3N\3N\5N\u0263"+
		"\nN\3O\3O\5O\u0267\nO\3P\3P\5P\u026b\nP\3Q\3Q\3R\3R\3S\3S\3T\3T\5T\u0275"+
		"\nT\3U\5U\u0278\nU\3U\3U\3V\3V\3V\2\2W\2\4\6\b\n\f\16\20\22\24\26\30\32"+
		"\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080"+
		"\u0082\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098"+
		"\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\2\4\3\2\27\31\3"+
		"\2\34\36\u0278\2\u00ae\3\2\2\2\4\u00b7\3\2\2\2\6\u00b9\3\2\2\2\b\u00bb"+
		"\3\2\2\2\n\u00bd\3\2\2\2\f\u00c5\3\2\2\2\16\u00c7\3\2\2\2\20\u00ce\3\2"+
		"\2\2\22\u00e0\3\2\2\2\24\u00e2\3\2\2\2\26\u00f2\3\2\2\2\30\u00f4\3\2\2"+
		"\2\32\u0110\3\2\2\2\34\u0112\3\2\2\2\36\u011e\3\2\2\2 \u0125\3\2\2\2\""+
		"\u0128\3\2\2\2$\u012e\3\2\2\2&\u0130\3\2\2\2(\u013c\3\2\2\2*\u0148\3\2"+
		"\2\2,\u014a\3\2\2\2.\u015c\3\2\2\2\60\u015e\3\2\2\2\62\u0160\3\2\2\2\64"+
		"\u0162\3\2\2\2\66\u0168\3\2\2\28\u0171\3\2\2\2:\u0178\3\2\2\2<\u017d\3"+
		"\2\2\2>\u017f\3\2\2\2@\u0181\3\2\2\2B\u0183\3\2\2\2D\u0185\3\2\2\2F\u018d"+
		"\3\2\2\2H\u0195\3\2\2\2J\u019d\3\2\2\2L\u01a5\3\2\2\2N\u01ad\3\2\2\2P"+
		"\u01af\3\2\2\2R\u01b1\3\2\2\2T\u01bc\3\2\2\2V\u01be\3\2\2\2X\u01c0\3\2"+
		"\2\2Z\u01c2\3\2\2\2\\\u01c4\3\2\2\2^\u01ca\3\2\2\2`\u01cc\3\2\2\2b\u01d2"+
		"\3\2\2\2d\u01d4\3\2\2\2f\u01d6\3\2\2\2h\u01d8\3\2\2\2j\u01dc\3\2\2\2l"+
		"\u01e0\3\2\2\2n\u01e2\3\2\2\2p\u01eb\3\2\2\2r\u01f4\3\2\2\2t\u01f6\3\2"+
		"\2\2v\u01f8\3\2\2\2x\u01fd\3\2\2\2z\u01ff\3\2\2\2|\u0208\3\2\2\2~\u020a"+
		"\3\2\2\2\u0080\u0214\3\2\2\2\u0082\u0216\3\2\2\2\u0084\u0218\3\2\2\2\u0086"+
		"\u021a\3\2\2\2\u0088\u021f\3\2\2\2\u008a\u0226\3\2\2\2\u008c\u0234\3\2"+
		"\2\2\u008e\u0236\3\2\2\2\u0090\u0241\3\2\2\2\u0092\u0243\3\2\2\2\u0094"+
		"\u0245\3\2\2\2\u0096\u024c\3\2\2\2\u0098\u0259\3\2\2\2\u009a\u0262\3\2"+
		"\2\2\u009c\u0266\3\2\2\2\u009e\u026a\3\2\2\2\u00a0\u026c\3\2\2\2\u00a2"+
		"\u026e\3\2\2\2\u00a4\u0270\3\2\2\2\u00a6\u0274\3\2\2\2\u00a8\u0277\3\2"+
		"\2\2\u00aa\u027b\3\2\2\2\u00ac\u00af\5\32\16\2\u00ad\u00af\5\4\3\2\u00ae"+
		"\u00ac\3\2\2\2\u00ae\u00ad\3\2\2\2\u00af\u00b0\3\2\2\2\u00b0\u00ae\3\2"+
		"\2\2\u00b0\u00b1\3\2\2\2\u00b1\3\3\2\2\2\u00b2\u00b3\5\f\7\2\u00b3\u00b4"+
		"\5\6\4\2\u00b4\u00b5\7\66\2\2\u00b5\u00b8\3\2\2\2\u00b6\u00b8\5\24\13"+
		"\2\u00b7\u00b2\3\2\2\2\u00b7\u00b6\3\2\2\2\u00b8\5\3\2\2\2\u00b9\u00ba"+
		"\5\n\6\2\u00ba\7\3\2\2\2\u00bb\u00bc\5\26\f\2\u00bc\t\3\2\2\2\u00bd\u00c1"+
		"\5\26\f\2\u00be\u00bf\5<\37\2\u00bf\u00c0\5\30\r\2\u00c0\u00c2\3\2\2\2"+
		"\u00c1\u00be\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2\13\3\2\2\2\u00c3\u00c6"+
		"\5\22\n\2\u00c4\u00c6\5\16\b\2\u00c5\u00c3\3\2\2\2\u00c5\u00c4\3\2\2\2"+
		"\u00c6\r\3\2\2\2\u00c7\u00ca\5\22\n\2\u00c8\u00c9\7&\2\2\u00c9\u00cb\7"+
		"\'\2\2\u00ca\u00c8\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cc"+
		"\u00cd\3\2\2\2\u00cd\17\3\2\2\2\u00ce\u00d3\5\22\n\2\u00cf\u00d0\7&\2"+
		"\2\u00d0\u00d1\58\35\2\u00d1\u00d2\7\'\2\2\u00d2\u00d4\3\2\2\2\u00d3\u00cf"+
		"\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d3\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6"+
		"\u00db\3\2\2\2\u00d7\u00d8\7&\2\2\u00d8\u00da\7\'\2\2\u00d9\u00d7\3\2"+
		"\2\2\u00da\u00dd\3\2\2\2\u00db\u00d9\3\2\2\2\u00db\u00dc\3\2\2\2\u00dc"+
		"\21\3\2\2\2\u00dd\u00db\3\2\2\2\u00de\u00e1\5\u00a6T\2\u00df\u00e1\5\24"+
		"\13\2\u00e0\u00de\3\2\2\2\u00e0\u00df\3\2\2\2\u00e1\23\3\2\2\2\u00e2\u00e4"+
		"\5\u00aaV\2\u00e3\u00e5\7\65\2\2\u00e4\u00e3\3\2\2\2\u00e4\u00e5\3\2\2"+
		"\2\u00e5\u00e6\3\2\2\2\u00e6\u00ed\7$\2\2\u00e7\u00e8\5\f\7\2\u00e8\u00e9"+
		"\5\b\5\2\u00e9\u00ea\7\66\2\2\u00ea\u00ec\3\2\2\2\u00eb\u00e7\3\2\2\2"+
		"\u00ec\u00ef\3\2\2\2\u00ed\u00eb\3\2\2\2\u00ed\u00ee\3\2\2\2\u00ee\u00f0"+
		"\3\2\2\2\u00ef\u00ed\3\2\2\2\u00f0\u00f1\7%\2\2\u00f1\25\3\2\2\2\u00f2"+
		"\u00f3\7\65\2\2\u00f3\27\3\2\2\2\u00f4\u00f5\5:\36\2\u00f5\31\3\2\2\2"+
		"\u00f6\u00f7\5\f\7\2\u00f7\u00f8\7\65\2\2\u00f8\u00fa\7\"\2\2\u00f9\u00fb"+
		"\5\34\17\2\u00fa\u00f9\3\2\2\2\u00fa\u00fb\3\2\2\2\u00fb\u00fc\3\2\2\2"+
		"\u00fc\u00fd\7#\2\2\u00fd\u0102\7$\2\2\u00fe\u0101\5 \21\2\u00ff\u0101"+
		"\5\4\3\2\u0100\u00fe\3\2\2\2\u0100\u00ff\3\2\2\2\u0101\u0104\3\2\2\2\u0102"+
		"\u0100\3\2\2\2\u0102\u0103\3\2\2\2\u0103\u0105\3\2\2\2\u0104\u0102\3\2"+
		"\2\2\u0105\u0106\7%\2\2\u0106\u0111\3\2\2\2\u0107\u0108\5\f\7\2\u0108"+
		"\u0109\7\65\2\2\u0109\u010b\7\"\2\2\u010a\u010c\5\34\17\2\u010b\u010a"+
		"\3\2\2\2\u010b\u010c\3\2\2\2\u010c\u010d\3\2\2\2\u010d\u010e\7#\2\2\u010e"+
		"\u010f\7\66\2\2\u010f\u0111\3\2\2\2\u0110\u00f6\3\2\2\2\u0110\u0107\3"+
		"\2\2\2\u0111\33\3\2\2\2\u0112\u0117\5\36\20\2\u0113\u0114\7 \2\2\u0114"+
		"\u0116\5\36\20\2\u0115\u0113\3\2\2\2\u0116\u0119\3\2\2\2\u0117\u0115\3"+
		"\2\2\2\u0117\u0118\3\2\2\2\u0118\u011c\3\2\2\2\u0119\u0117\3\2\2\2\u011a"+
		"\u011b\7 \2\2\u011b\u011d\7)\2\2\u011c\u011a\3\2\2\2\u011c\u011d\3\2\2"+
		"\2\u011d\35\3\2\2\2\u011e\u011f\5\f\7\2\u011f\u0120\5\26\f\2\u0120\37"+
		"\3\2\2\2\u0121\u0126\5&\24\2\u0122\u0126\5$\23\2\u0123\u0126\5\"\22\2"+
		"\u0124\u0126\5\4\3\2\u0125\u0121\3\2\2\2\u0125\u0122\3\2\2\2\u0125\u0123"+
		"\3\2\2\2\u0125\u0124\3\2\2\2\u0126!\3\2\2\2\u0127\u0129\58\35\2\u0128"+
		"\u0127\3\2\2\2\u0128\u0129\3\2\2\2\u0129\u012a\3\2\2\2\u012a\u012b\7\66"+
		"\2\2\u012b#\3\2\2\2\u012c\u012f\5(\25\2\u012d\u012f\5*\26\2\u012e\u012c"+
		"\3\2\2\2\u012e\u012d\3\2\2\2\u012f%\3\2\2\2\u0130\u0135\7$\2\2\u0131\u0134"+
		"\5 \21\2\u0132\u0134\5\4\3\2\u0133\u0131\3\2\2\2\u0133\u0132\3\2\2\2\u0134"+
		"\u0137\3\2\2\2\u0135\u0133\3\2\2\2\u0135\u0136\3\2\2\2\u0136\u0138\3\2"+
		"\2\2\u0137\u0135\3\2\2\2\u0138\u0139\7%\2\2\u0139\'\3\2\2\2\u013a\u013d"+
		"\5,\27\2\u013b\u013d\5\64\33\2\u013c\u013a\3\2\2\2\u013c\u013b\3\2\2\2"+
		"\u013d)\3\2\2\2\u013e\u0149\5\66\34\2\u013f\u0140\7*\2\2\u0140\u0149\7"+
		"\66\2\2\u0141\u0142\7+\2\2\u0142\u0149\7\66\2\2\u0143\u0145\7,\2\2\u0144"+
		"\u0146\58\35\2\u0145\u0144\3\2\2\2\u0145\u0146\3\2\2\2\u0146\u0147\3\2"+
		"\2\2\u0147\u0149\7\66\2\2\u0148\u013e\3\2\2\2\u0148\u013f\3\2\2\2\u0148"+
		"\u0141\3\2\2\2\u0148\u0143\3\2\2\2\u0149+\3\2\2\2\u014a\u014b\7-\2\2\u014b"+
		"\u014d\7\"\2\2\u014c\u014e\5.\30\2\u014d\u014c\3\2\2\2\u014d\u014e\3\2"+
		"\2\2\u014e\u014f\3\2\2\2\u014f\u0150\7\66\2\2\u0150\u0152\3\2\2\2\u0151"+
		"\u0153\5\60\31\2\u0152\u0151\3\2\2\2\u0152\u0153\3\2\2\2\u0153\u0154\3"+
		"\2\2\2\u0154\u0155\7\66\2\2\u0155\u0157\3\2\2\2\u0156\u0158\5\62\32\2"+
		"\u0157\u0156\3\2\2\2\u0157\u0158\3\2\2\2\u0158\u0159\3\2\2\2\u0159\u015a"+
		"\7#\2\2\u015a\u015b\5 \21\2\u015b-\3\2\2\2\u015c\u015d\58\35\2\u015d/"+
		"\3\2\2\2\u015e\u015f\58\35\2\u015f\61\3\2\2\2\u0160\u0161\58\35\2\u0161"+
		"\63\3\2\2\2\u0162\u0163\7.\2\2\u0163\u0164\7\"\2\2\u0164\u0165\58\35\2"+
		"\u0165\u0166\7#\2\2\u0166\u0167\5 \21\2\u0167\65\3\2\2\2\u0168\u0169\7"+
		"\60\2\2\u0169\u016a\7\"\2\2\u016a\u016b\58\35\2\u016b\u016c\7#\2\2\u016c"+
		"\u016f\5 \21\2\u016d\u016e\7\61\2\2\u016e\u0170\5 \21\2\u016f\u016d\3"+
		"\2\2\2\u016f\u0170\3\2\2\2\u0170\67\3\2\2\2\u0171\u0172\5:\36\2\u0172"+
		"9\3\2\2\2\u0173\u0174\5\u0080A\2\u0174\u0175\5> \2\u0175\u0177\3\2\2\2"+
		"\u0176\u0173\3\2\2\2\u0177\u017a\3\2\2\2\u0178\u0176\3\2\2\2\u0178\u0179"+
		"\3\2\2\2\u0179\u017b\3\2\2\2\u017a\u0178\3\2\2\2\u017b\u017c\5@!\2\u017c"+
		";\3\2\2\2\u017d\u017e\7!\2\2\u017e=\3\2\2\2\u017f\u0180\5<\37\2\u0180"+
		"?\3\2\2\2\u0181\u0182\5B\"\2\u0182A\3\2\2\2\u0183\u0184\5D#\2\u0184C\3"+
		"\2\2\2\u0185\u018a\5F$\2\u0186\u0187\7\3\2\2\u0187\u0189\5F$\2\u0188\u0186"+
		"\3\2\2\2\u0189\u018c\3\2\2\2\u018a\u0188\3\2\2\2\u018a\u018b\3\2\2\2\u018b"+
		"E\3\2\2\2\u018c\u018a\3\2\2\2\u018d\u0192\5H%\2\u018e\u018f\7\4\2\2\u018f"+
		"\u0191\5H%\2\u0190\u018e\3\2\2\2\u0191\u0194\3\2\2\2\u0192\u0190\3\2\2"+
		"\2\u0192\u0193\3\2\2\2\u0193G\3\2\2\2\u0194\u0192\3\2\2\2\u0195\u019a"+
		"\5J&\2\u0196\u0197\7\5\2\2\u0197\u0199\5J&\2\u0198\u0196\3\2\2\2\u0199"+
		"\u019c\3\2\2\2\u019a\u0198\3\2\2\2\u019a\u019b\3\2\2\2\u019bI\3\2\2\2"+
		"\u019c\u019a\3\2\2\2\u019d\u01a2\5L\'\2\u019e\u019f\7\6\2\2\u019f\u01a1"+
		"\5L\'\2\u01a0\u019e\3\2\2\2\u01a1\u01a4\3\2\2\2\u01a2\u01a0\3\2\2\2\u01a2"+
		"\u01a3\3\2\2\2\u01a3K\3\2\2\2\u01a4\u01a2\3\2\2\2\u01a5\u01aa\5R*\2\u01a6"+
		"\u01a7\7\7\2\2\u01a7\u01a9\5R*\2\u01a8\u01a6\3\2\2\2\u01a9\u01ac\3\2\2"+
		"\2\u01aa\u01a8\3\2\2\2\u01aa\u01ab\3\2\2\2\u01abM\3\2\2\2\u01ac\u01aa"+
		"\3\2\2\2\u01ad\u01ae\7\b\2\2\u01aeO\3\2\2\2\u01af\u01b0\7\t\2\2\u01b0"+
		"Q\3\2\2\2\u01b1\u01b7\5`\61\2\u01b2\u01b3\5T+\2\u01b3\u01b4\5`\61\2\u01b4"+
		"\u01b6\3\2\2\2\u01b5\u01b2\3\2\2\2\u01b6\u01b9\3\2\2\2\u01b7\u01b5\3\2"+
		"\2\2\u01b7\u01b8\3\2\2\2\u01b8S\3\2\2\2\u01b9\u01b7\3\2\2\2\u01ba\u01bd"+
		"\5N(\2\u01bb\u01bd\5P)\2\u01bc\u01ba\3\2\2\2\u01bc\u01bb\3\2\2\2\u01bd"+
		"U\3\2\2\2\u01be\u01bf\7\n\2\2\u01bfW\3\2\2\2\u01c0\u01c1\7\13\2\2\u01c1"+
		"Y\3\2\2\2\u01c2\u01c3\7\f\2\2\u01c3[\3\2\2\2\u01c4\u01c5\7\r\2\2\u01c5"+
		"]\3\2\2\2\u01c6\u01cb\5\\/\2\u01c7\u01cb\5Z.\2\u01c8\u01cb\5V,\2\u01c9"+
		"\u01cb\5X-\2\u01ca\u01c6\3\2\2\2\u01ca\u01c7\3\2\2\2\u01ca\u01c8\3\2\2"+
		"\2\u01ca\u01c9\3\2\2\2\u01cb_\3\2\2\2\u01cc\u01d0\5n8\2\u01cd\u01ce\5"+
		"^\60\2\u01ce\u01cf\5n8\2\u01cf\u01d1\3\2\2\2\u01d0\u01cd\3\2\2\2\u01d0"+
		"\u01d1\3\2\2\2\u01d1a\3\2\2\2\u01d2\u01d3\7\16\2\2\u01d3c\3\2\2\2\u01d4"+
		"\u01d5\7\17\2\2\u01d5e\3\2\2\2\u01d6\u01d7\7\20\2\2\u01d7g\3\2\2\2\u01d8"+
		"\u01d9\7\21\2\2\u01d9i\3\2\2\2\u01da\u01dd\5b\62\2\u01db\u01dd\5d\63\2"+
		"\u01dc\u01da\3\2\2\2\u01dc\u01db\3\2\2\2\u01ddk\3\2\2\2\u01de\u01e1\5"+
		"f\64\2\u01df\u01e1\5h\65\2\u01e0\u01de\3\2\2\2\u01e0\u01df\3\2\2\2\u01e1"+
		"m\3\2\2\2\u01e2\u01e8\5p9\2\u01e3\u01e4\5j\66\2\u01e4\u01e5\5p9\2\u01e5"+
		"\u01e7\3\2\2\2\u01e6\u01e3\3\2\2\2\u01e7\u01ea\3\2\2\2\u01e8\u01e6\3\2"+
		"\2\2\u01e8\u01e9\3\2\2\2\u01e9o\3\2\2\2\u01ea\u01e8\3\2\2\2\u01eb\u01f1"+
		"\5z>\2\u01ec\u01ed\5l\67\2\u01ed\u01ee\5z>\2\u01ee\u01f0\3\2\2\2\u01ef"+
		"\u01ec\3\2\2\2\u01f0\u01f3\3\2\2\2\u01f1\u01ef\3\2\2\2\u01f1\u01f2\3\2"+
		"\2\2\u01f2q\3\2\2\2\u01f3\u01f1\3\2\2\2\u01f4\u01f5\7\22\2\2\u01f5s\3"+
		"\2\2\2\u01f6\u01f7\7\23\2\2\u01f7u\3\2\2\2\u01f8\u01f9\7\24\2\2\u01f9"+
		"w\3\2\2\2\u01fa\u01fe\5r:\2\u01fb\u01fe\5t;\2\u01fc\u01fe\5v<\2\u01fd"+
		"\u01fa\3\2\2\2\u01fd\u01fb\3\2\2\2\u01fd\u01fc\3\2\2\2\u01fey\3\2\2\2"+
		"\u01ff\u0205\5\u0080A\2\u0200\u0201\5x=\2\u0201\u0202\5\u0080A\2\u0202"+
		"\u0204\3\2\2\2\u0203\u0200\3\2\2\2\u0204\u0207\3\2\2\2\u0205\u0203\3\2"+
		"\2\2\u0205\u0206\3\2\2\2\u0206{\3\2\2\2\u0207\u0205\3\2\2\2\u0208\u0209"+
		"\7\25\2\2\u0209}\3\2\2\2\u020a\u020b\7\26\2\2\u020b\177\3\2\2\2\u020c"+
		"\u0215\5\u0094K\2\u020d\u020e\5\u0090I\2\u020e\u020f\5\u0080A\2\u020f"+
		"\u0215\3\2\2\2\u0210\u0215\5\u00a8U\2\u0211\u0215\5\u0088E\2\u0212\u0215"+
		"\5\u0086D\2\u0213\u0215\5\u0082B\2\u0214\u020c\3\2\2\2\u0214\u020d\3\2"+
		"\2\2\u0214\u0210\3\2\2\2\u0214\u0211\3\2\2\2\u0214\u0212\3\2\2\2\u0214"+
		"\u0213\3\2\2\2\u0215\u0081\3\2\2\2\u0216\u0217\t\2\2\2\u0217\u0083\3\2"+
		"\2\2\u0218\u0219\7\65\2\2\u0219\u0085\3\2\2\2\u021a\u021d\7(\2\2\u021b"+
		"\u021e\5\20\t\2\u021c\u021e\5\u0084C\2\u021d\u021b\3\2\2\2\u021d\u021c"+
		"\3\2\2\2\u021e\u0087\3\2\2\2\u021f\u0220\7\65\2\2\u0220\u0222\7\"\2\2"+
		"\u0221\u0223\5\u008aF\2\u0222\u0221\3\2\2\2\u0222\u0223\3\2\2\2\u0223"+
		"\u0224\3\2\2\2\u0224\u0225\7#\2\2\u0225\u0089\3\2\2\2\u0226\u022b\5:\36"+
		"\2\u0227\u0228\7 \2\2\u0228\u022a\5:\36\2\u0229\u0227\3\2\2\2\u022a\u022d"+
		"\3\2\2\2\u022b\u0229\3\2\2\2\u022b\u022c\3\2\2\2\u022c\u008b\3\2\2\2\u022d"+
		"\u022b\3\2\2\2\u022e\u0235\5\u009eP\2\u022f\u0235\58\35\2\u0230\u0231"+
		"\7\"\2\2\u0231\u0232\5\u008cG\2\u0232\u0233\7#\2\2\u0233\u0235\3\2\2\2"+
		"\u0234\u022e\3\2\2\2\u0234\u022f\3\2\2\2\u0234\u0230\3\2\2\2\u0235\u008d"+
		"\3\2\2\2\u0236\u0237\7\32\2\2\u0237\u008f\3\2\2\2\u0238\u0242\5\u008e"+
		"H\2\u0239\u0242\5h\65\2\u023a\u0242\5|?\2\u023b\u0242\5~@\2\u023c\u023d"+
		"\7\"\2\2\u023d\u023e\5\u009cO\2\u023e\u023f\7#\2\2\u023f\u0242\3\2\2\2"+
		"\u0240\u0242\5\u0092J\2\u0241\u0238\3\2\2\2\u0241\u0239\3\2\2\2\u0241"+
		"\u023a\3\2\2\2\u0241\u023b\3\2\2\2\u0241\u023c\3\2\2\2\u0241\u0240\3\2"+
		"\2\2\u0242\u0091\3\2\2\2\u0243\u0244\7\33\2\2\u0244\u0093\3\2\2\2\u0245"+
		"\u0249\5\u009aN\2\u0246\u0248\5\u0098M\2\u0247\u0246\3\2\2\2\u0248\u024b"+
		"\3\2\2\2\u0249\u0247\3\2\2\2\u0249\u024a\3\2\2\2\u024a\u0095\3\2\2\2\u024b"+
		"\u0249\3\2\2\2\u024c\u024d\7\64\2\2\u024d\u0097\3\2\2\2\u024e\u024f\7"+
		"&\2\2\u024f\u0250\58\35\2\u0250\u0251\7\'\2\2\u0251\u025a\3\2\2\2\u0252"+
		"\u0255\5\u0096L\2\u0253\u0256\7\65\2\2\u0254\u0256\5\u0088E\2\u0255\u0253"+
		"\3\2\2\2\u0255\u0254\3\2\2\2\u0256\u025a\3\2\2\2\u0257\u025a\5|?\2\u0258"+
		"\u025a\5~@\2\u0259\u024e\3\2\2\2\u0259\u0252\3\2\2\2\u0259\u0257\3\2\2"+
		"\2\u0259\u0258\3\2\2\2\u025a\u0099\3\2\2\2\u025b\u0263\7\65\2\2\u025c"+
		"\u0263\5\u009eP\2\u025d\u025e\7\"\2\2\u025e\u025f\58\35\2\u025f\u0260"+
		"\7#\2\2\u0260\u0263\3\2\2\2\u0261\u0263\5\u0088E\2\u0262\u025b\3\2\2\2"+
		"\u0262\u025c\3\2\2\2\u0262\u025d\3\2\2\2\u0262\u0261\3\2\2\2\u0263\u009b"+
		"\3\2\2\2\u0264\u0267\5\u00a6T\2\u0265\u0267\7\65\2\2\u0266\u0264\3\2\2"+
		"\2\u0266\u0265\3\2\2\2\u0267\u009d\3\2\2\2\u0268\u026b\5\u00a0Q\2\u0269"+
		"\u026b\5\u00a4S\2\u026a\u0268\3\2\2\2\u026a\u0269\3\2\2\2\u026b\u009f"+
		"\3\2\2\2\u026c\u026d\5\u00a2R\2\u026d\u00a1\3\2\2\2\u026e\u026f\5\u00a8"+
		"U\2\u026f\u00a3\3\2\2\2\u0270\u0271\7=\2\2\u0271\u00a5\3\2\2\2\u0272\u0275"+
		"\t\3\2\2\u0273\u0275\7\65\2\2\u0274\u0272\3\2\2\2\u0274\u0273\3\2\2\2"+
		"\u0275\u00a7\3\2\2\2\u0276\u0278\7;\2\2\u0277\u0276\3\2\2\2\u0277\u0278"+
		"\3\2\2\2\u0278\u0279\3\2\2\2\u0279\u027a\7\67\2\2\u027a\u00a9\3\2\2\2"+
		"\u027b\u027c\7\37\2\2\u027c\u00ab\3\2\2\2>\u00ae\u00b0\u00b7\u00c1\u00c5"+
		"\u00cc\u00d5\u00db\u00e0\u00e4\u00ed\u00fa\u0100\u0102\u010b\u0110\u0117"+
		"\u011c\u0125\u0128\u012e\u0133\u0135\u013c\u0145\u0148\u014d\u0152\u0157"+
		"\u016f\u0178\u018a\u0192\u019a\u01a2\u01aa\u01b7\u01bc\u01ca\u01d0\u01dc"+
		"\u01e0\u01e8\u01f1\u01fd\u0205\u0214\u021d\u0222\u022b\u0234\u0241\u0249"+
		"\u0255\u0259\u0262\u0266\u026a\u0274\u0277";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}