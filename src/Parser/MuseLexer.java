// Generated from Muse.g4 by ANTLR 4.5.2
package Parser;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MuseLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, T__28=29, Comma=30, Equal=31, 
		Lbracket=32, Rbracket=33, LBigbracket=34, RBigbracket=35, LMidbracket=36, 
		RMidbracket=37, New=38, Balabalabala=39, BREAK=40, CONTINUE=41, RETURN=42, 
		FOR=43, WHILE=44, DO=45, IF=46, ELSE=47, COLON=48, QUESTION=49, Dot=50, 
		Identifier=51, SEMI=52, DecimalConstant=53, Numbertail=54, OctalConstant=55, 
		HexadecimalConstant=56, Sign=57, CharacterConstant=58, StringLiteral=59, 
		Preprocessing=60, Whitespace=61, Newline=62, BlockComment=63, LineComment=64;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "T__24", 
		"T__25", "T__26", "T__27", "T__28", "Comma", "Equal", "Lbracket", "Rbracket", 
		"LBigbracket", "RBigbracket", "LMidbracket", "RMidbracket", "New", "Balabalabala", 
		"BREAK", "CONTINUE", "RETURN", "FOR", "WHILE", "DO", "IF", "ELSE", "COLON", 
		"QUESTION", "Dot", "Identifier", "IdentifierNondigit", "Nondigit", "SEMI", 
		"Digit", "HexQuad", "DecimalConstant", "Numbertail", "OctalConstant", 
		"HexadecimalConstant", "HexadecimalPrefix", "NonzeroDigit", "OctalDigit", 
		"HexadecimalDigit", "Sign", "DigitSequence", "CharacterConstant", "CharSequence", 
		"Char", "EscapeSequence", "SimpleEscapeSequence", "OctalEscapeSequence", 
		"HexadecimalEscapeSequence", "StringLiteral", "SCharSequence", "SChar", 
		"CCharSequence", "CChar", "Preprocessing", "Whitespace", "Newline", "BlockComment", 
		"LineComment"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'||'", "'&&'", "'|'", "'^'", "'&'", "'=='", "'!='", "'>'", "'<'", 
		"'>='", "'<='", "'<<'", "'>>'", "'+'", "'-'", "'*'", "'/'", "'%'", "'++'", 
		"'--'", "'true'", "'false'", "'null'", "'!'", "'~'", "'int'", "'bool'", 
		"'void'", "'class'", "','", "'='", "'('", "')'", "'{'", "'}'", "'['", 
		"']'", "'new'", "'...'", "'break'", "'continue'", "'return'", "'for'", 
		"'while'", "'do'", "'if'", "'else'", "':'", "'?'", "'.'", null, "';'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, "Comma", "Equal", "Lbracket", "Rbracket", 
		"LBigbracket", "RBigbracket", "LMidbracket", "RMidbracket", "New", "Balabalabala", 
		"BREAK", "CONTINUE", "RETURN", "FOR", "WHILE", "DO", "IF", "ELSE", "COLON", 
		"QUESTION", "Dot", "Identifier", "SEMI", "DecimalConstant", "Numbertail", 
		"OctalConstant", "HexadecimalConstant", "Sign", "CharacterConstant", "StringLiteral", 
		"Preprocessing", "Whitespace", "Newline", "BlockComment", "LineComment"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MuseLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Muse.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2B\u0213\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t="+
		"\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I"+
		"\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT"+
		"\3\2\3\2\3\2\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3"+
		"\b\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3"+
		"\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\24\3"+
		"\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3"+
		"\30\3\30\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\33\3\33\3\34\3"+
		"\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\36\3\36\3\36\3\36\3\36\3"+
		"\36\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3\'"+
		"\3\'\3(\3(\3(\3(\3)\3)\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3*\3*\3*\3+\3+\3"+
		"+\3+\3+\3+\3+\3,\3,\3,\3,\3-\3-\3-\3-\3-\3-\3.\3.\3.\3/\3/\3/\3\60\3\60"+
		"\3\60\3\60\3\60\3\61\3\61\3\62\3\62\3\63\3\63\3\64\3\64\3\64\7\64\u0150"+
		"\n\64\f\64\16\64\u0153\13\64\3\65\3\65\3\66\3\66\3\67\3\67\38\38\39\3"+
		"9\39\39\39\3:\3:\7:\u0164\n:\f:\16:\u0167\13:\3:\5:\u016a\n:\3:\3:\5:"+
		"\u016e\n:\5:\u0170\n:\3;\3;\3;\3;\3;\3;\3;\3;\3;\3;\5;\u017c\n;\3<\3<"+
		"\7<\u0180\n<\f<\16<\u0183\13<\3=\3=\6=\u0187\n=\r=\16=\u0188\3>\3>\3>"+
		"\3?\3?\3@\3@\3A\3A\3B\3B\3C\6C\u0197\nC\rC\16C\u0198\3D\3D\3D\3D\3E\6"+
		"E\u01a0\nE\rE\16E\u01a1\3F\3F\5F\u01a6\nF\3G\3G\3G\5G\u01ab\nG\3H\3H\3"+
		"H\3I\3I\3I\3I\3I\3I\3I\3I\3I\3I\3I\5I\u01bb\nI\3J\3J\3J\3J\6J\u01c1\n"+
		"J\rJ\16J\u01c2\3K\3K\5K\u01c7\nK\3K\3K\3L\6L\u01cc\nL\rL\16L\u01cd\3M"+
		"\3M\5M\u01d2\nM\3N\6N\u01d5\nN\rN\16N\u01d6\3O\3O\5O\u01db\nO\3P\3P\7"+
		"P\u01df\nP\fP\16P\u01e2\13P\3P\3P\3P\5P\u01e7\nP\3P\3P\3Q\6Q\u01ec\nQ"+
		"\rQ\16Q\u01ed\3Q\3Q\3R\3R\5R\u01f4\nR\3R\5R\u01f7\nR\3R\3R\3S\3S\3S\3"+
		"S\7S\u01ff\nS\fS\16S\u0202\13S\3S\3S\3S\3S\3S\3T\3T\3T\3T\7T\u020d\nT"+
		"\fT\16T\u0210\13T\3T\3T\3\u0200\2U\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n"+
		"\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30"+
		"/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.["+
		"/]\60_\61a\62c\63e\64g\65i\2k\2m\66o\2q\2s\67u8w9y:{\2}\2\177\2\u0081"+
		"\2\u0083;\u0085\2\u0087<\u0089\2\u008b\2\u008d\2\u008f\2\u0091\2\u0093"+
		"\2\u0095=\u0097\2\u0099\2\u009b\2\u009d\2\u009f>\u00a1?\u00a3@\u00a5A"+
		"\u00a7B\3\2\17\6\2&&C\\aac|\3\2\62;\4\2WWww\4\2ZZzz\3\2\63;\3\2\629\5"+
		"\2\62;CHch\4\2--//\6\2\f\f\17\17))^^\f\2$$))AA^^cdhhppttvvxx\6\2\f\f\17"+
		"\17$$^^\4\2\f\f\17\17\4\2\13\13\"\"\u0220\2\3\3\2\2\2\2\5\3\2\2\2\2\7"+
		"\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2"+
		"\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2"+
		"\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2"+
		"\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2"+
		"\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2"+
		"\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M"+
		"\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2"+
		"\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2"+
		"\2g\3\2\2\2\2m\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\2\u0083"+
		"\3\2\2\2\2\u0087\3\2\2\2\2\u0095\3\2\2\2\2\u009f\3\2\2\2\2\u00a1\3\2\2"+
		"\2\2\u00a3\3\2\2\2\2\u00a5\3\2\2\2\2\u00a7\3\2\2\2\3\u00a9\3\2\2\2\5\u00ac"+
		"\3\2\2\2\7\u00af\3\2\2\2\t\u00b1\3\2\2\2\13\u00b3\3\2\2\2\r\u00b5\3\2"+
		"\2\2\17\u00b8\3\2\2\2\21\u00bb\3\2\2\2\23\u00bd\3\2\2\2\25\u00bf\3\2\2"+
		"\2\27\u00c2\3\2\2\2\31\u00c5\3\2\2\2\33\u00c8\3\2\2\2\35\u00cb\3\2\2\2"+
		"\37\u00cd\3\2\2\2!\u00cf\3\2\2\2#\u00d1\3\2\2\2%\u00d3\3\2\2\2\'\u00d5"+
		"\3\2\2\2)\u00d8\3\2\2\2+\u00db\3\2\2\2-\u00e0\3\2\2\2/\u00e6\3\2\2\2\61"+
		"\u00eb\3\2\2\2\63\u00ed\3\2\2\2\65\u00ef\3\2\2\2\67\u00f3\3\2\2\29\u00f8"+
		"\3\2\2\2;\u00fd\3\2\2\2=\u0103\3\2\2\2?\u0105\3\2\2\2A\u0107\3\2\2\2C"+
		"\u0109\3\2\2\2E\u010b\3\2\2\2G\u010d\3\2\2\2I\u010f\3\2\2\2K\u0111\3\2"+
		"\2\2M\u0113\3\2\2\2O\u0117\3\2\2\2Q\u011b\3\2\2\2S\u0121\3\2\2\2U\u012a"+
		"\3\2\2\2W\u0131\3\2\2\2Y\u0135\3\2\2\2[\u013b\3\2\2\2]\u013e\3\2\2\2_"+
		"\u0141\3\2\2\2a\u0146\3\2\2\2c\u0148\3\2\2\2e\u014a\3\2\2\2g\u014c\3\2"+
		"\2\2i\u0154\3\2\2\2k\u0156\3\2\2\2m\u0158\3\2\2\2o\u015a\3\2\2\2q\u015c"+
		"\3\2\2\2s\u016f\3\2\2\2u\u017b\3\2\2\2w\u017d\3\2\2\2y\u0184\3\2\2\2{"+
		"\u018a\3\2\2\2}\u018d\3\2\2\2\177\u018f\3\2\2\2\u0081\u0191\3\2\2\2\u0083"+
		"\u0193\3\2\2\2\u0085\u0196\3\2\2\2\u0087\u019a\3\2\2\2\u0089\u019f\3\2"+
		"\2\2\u008b\u01a5\3\2\2\2\u008d\u01aa\3\2\2\2\u008f\u01ac\3\2\2\2\u0091"+
		"\u01ba\3\2\2\2\u0093\u01bc\3\2\2\2\u0095\u01c4\3\2\2\2\u0097\u01cb\3\2"+
		"\2\2\u0099\u01d1\3\2\2\2\u009b\u01d4\3\2\2\2\u009d\u01da\3\2\2\2\u009f"+
		"\u01dc\3\2\2\2\u00a1\u01eb\3\2\2\2\u00a3\u01f6\3\2\2\2\u00a5\u01fa\3\2"+
		"\2\2\u00a7\u0208\3\2\2\2\u00a9\u00aa\7~\2\2\u00aa\u00ab\7~\2\2\u00ab\4"+
		"\3\2\2\2\u00ac\u00ad\7(\2\2\u00ad\u00ae\7(\2\2\u00ae\6\3\2\2\2\u00af\u00b0"+
		"\7~\2\2\u00b0\b\3\2\2\2\u00b1\u00b2\7`\2\2\u00b2\n\3\2\2\2\u00b3\u00b4"+
		"\7(\2\2\u00b4\f\3\2\2\2\u00b5\u00b6\7?\2\2\u00b6\u00b7\7?\2\2\u00b7\16"+
		"\3\2\2\2\u00b8\u00b9\7#\2\2\u00b9\u00ba\7?\2\2\u00ba\20\3\2\2\2\u00bb"+
		"\u00bc\7@\2\2\u00bc\22\3\2\2\2\u00bd\u00be\7>\2\2\u00be\24\3\2\2\2\u00bf"+
		"\u00c0\7@\2\2\u00c0\u00c1\7?\2\2\u00c1\26\3\2\2\2\u00c2\u00c3\7>\2\2\u00c3"+
		"\u00c4\7?\2\2\u00c4\30\3\2\2\2\u00c5\u00c6\7>\2\2\u00c6\u00c7\7>\2\2\u00c7"+
		"\32\3\2\2\2\u00c8\u00c9\7@\2\2\u00c9\u00ca\7@\2\2\u00ca\34\3\2\2\2\u00cb"+
		"\u00cc\7-\2\2\u00cc\36\3\2\2\2\u00cd\u00ce\7/\2\2\u00ce \3\2\2\2\u00cf"+
		"\u00d0\7,\2\2\u00d0\"\3\2\2\2\u00d1\u00d2\7\61\2\2\u00d2$\3\2\2\2\u00d3"+
		"\u00d4\7\'\2\2\u00d4&\3\2\2\2\u00d5\u00d6\7-\2\2\u00d6\u00d7\7-\2\2\u00d7"+
		"(\3\2\2\2\u00d8\u00d9\7/\2\2\u00d9\u00da\7/\2\2\u00da*\3\2\2\2\u00db\u00dc"+
		"\7v\2\2\u00dc\u00dd\7t\2\2\u00dd\u00de\7w\2\2\u00de\u00df\7g\2\2\u00df"+
		",\3\2\2\2\u00e0\u00e1\7h\2\2\u00e1\u00e2\7c\2\2\u00e2\u00e3\7n\2\2\u00e3"+
		"\u00e4\7u\2\2\u00e4\u00e5\7g\2\2\u00e5.\3\2\2\2\u00e6\u00e7\7p\2\2\u00e7"+
		"\u00e8\7w\2\2\u00e8\u00e9\7n\2\2\u00e9\u00ea\7n\2\2\u00ea\60\3\2\2\2\u00eb"+
		"\u00ec\7#\2\2\u00ec\62\3\2\2\2\u00ed\u00ee\7\u0080\2\2\u00ee\64\3\2\2"+
		"\2\u00ef\u00f0\7k\2\2\u00f0\u00f1\7p\2\2\u00f1\u00f2\7v\2\2\u00f2\66\3"+
		"\2\2\2\u00f3\u00f4\7d\2\2\u00f4\u00f5\7q\2\2\u00f5\u00f6\7q\2\2\u00f6"+
		"\u00f7\7n\2\2\u00f78\3\2\2\2\u00f8\u00f9\7x\2\2\u00f9\u00fa\7q\2\2\u00fa"+
		"\u00fb\7k\2\2\u00fb\u00fc\7f\2\2\u00fc:\3\2\2\2\u00fd\u00fe\7e\2\2\u00fe"+
		"\u00ff\7n\2\2\u00ff\u0100\7c\2\2\u0100\u0101\7u\2\2\u0101\u0102\7u\2\2"+
		"\u0102<\3\2\2\2\u0103\u0104\7.\2\2\u0104>\3\2\2\2\u0105\u0106\7?\2\2\u0106"+
		"@\3\2\2\2\u0107\u0108\7*\2\2\u0108B\3\2\2\2\u0109\u010a\7+\2\2\u010aD"+
		"\3\2\2\2\u010b\u010c\7}\2\2\u010cF\3\2\2\2\u010d\u010e\7\177\2\2\u010e"+
		"H\3\2\2\2\u010f\u0110\7]\2\2\u0110J\3\2\2\2\u0111\u0112\7_\2\2\u0112L"+
		"\3\2\2\2\u0113\u0114\7p\2\2\u0114\u0115\7g\2\2\u0115\u0116\7y\2\2\u0116"+
		"N\3\2\2\2\u0117\u0118\7\60\2\2\u0118\u0119\7\60\2\2\u0119\u011a\7\60\2"+
		"\2\u011aP\3\2\2\2\u011b\u011c\7d\2\2\u011c\u011d\7t\2\2\u011d\u011e\7"+
		"g\2\2\u011e\u011f\7c\2\2\u011f\u0120\7m\2\2\u0120R\3\2\2\2\u0121\u0122"+
		"\7e\2\2\u0122\u0123\7q\2\2\u0123\u0124\7p\2\2\u0124\u0125\7v\2\2\u0125"+
		"\u0126\7k\2\2\u0126\u0127\7p\2\2\u0127\u0128\7w\2\2\u0128\u0129\7g\2\2"+
		"\u0129T\3\2\2\2\u012a\u012b\7t\2\2\u012b\u012c\7g\2\2\u012c\u012d\7v\2"+
		"\2\u012d\u012e\7w\2\2\u012e\u012f\7t\2\2\u012f\u0130\7p\2\2\u0130V\3\2"+
		"\2\2\u0131\u0132\7h\2\2\u0132\u0133\7q\2\2\u0133\u0134\7t\2\2\u0134X\3"+
		"\2\2\2\u0135\u0136\7y\2\2\u0136\u0137\7j\2\2\u0137\u0138\7k\2\2\u0138"+
		"\u0139\7n\2\2\u0139\u013a\7g\2\2\u013aZ\3\2\2\2\u013b\u013c\7f\2\2\u013c"+
		"\u013d\7q\2\2\u013d\\\3\2\2\2\u013e\u013f\7k\2\2\u013f\u0140\7h\2\2\u0140"+
		"^\3\2\2\2\u0141\u0142\7g\2\2\u0142\u0143\7n\2\2\u0143\u0144\7u\2\2\u0144"+
		"\u0145\7g\2\2\u0145`\3\2\2\2\u0146\u0147\7<\2\2\u0147b\3\2\2\2\u0148\u0149"+
		"\7A\2\2\u0149d\3\2\2\2\u014a\u014b\7\60\2\2\u014bf\3\2\2\2\u014c\u0151"+
		"\5i\65\2\u014d\u0150\5i\65\2\u014e\u0150\5o8\2\u014f\u014d\3\2\2\2\u014f"+
		"\u014e\3\2\2\2\u0150\u0153\3\2\2\2\u0151\u014f\3\2\2\2\u0151\u0152\3\2"+
		"\2\2\u0152h\3\2\2\2\u0153\u0151\3\2\2\2\u0154\u0155\5k\66\2\u0155j\3\2"+
		"\2\2\u0156\u0157\t\2\2\2\u0157l\3\2\2\2\u0158\u0159\7=\2\2\u0159n\3\2"+
		"\2\2\u015a\u015b\t\3\2\2\u015bp\3\2\2\2\u015c\u015d\5\u0081A\2\u015d\u015e"+
		"\5\u0081A\2\u015e\u015f\5\u0081A\2\u015f\u0160\5\u0081A\2\u0160r\3\2\2"+
		"\2\u0161\u0165\5}?\2\u0162\u0164\5o8\2\u0163\u0162\3\2\2\2\u0164\u0167"+
		"\3\2\2\2\u0165\u0163\3\2\2\2\u0165\u0166\3\2\2\2\u0166\u0169\3\2\2\2\u0167"+
		"\u0165\3\2\2\2\u0168\u016a\5u;\2\u0169\u0168\3\2\2\2\u0169\u016a\3\2\2"+
		"\2\u016a\u0170\3\2\2\2\u016b\u016d\7\62\2\2\u016c\u016e\5u;\2\u016d\u016c"+
		"\3\2\2\2\u016d\u016e\3\2\2\2\u016e\u0170\3\2\2\2\u016f\u0161\3\2\2\2\u016f"+
		"\u016b\3\2\2\2\u0170t\3\2\2\2\u0171\u017c\t\4\2\2\u0172\u0173\7n\2\2\u0173"+
		"\u017c\7n\2\2\u0174\u0175\7N\2\2\u0175\u017c\7N\2\2\u0176\u0177\7n\2\2"+
		"\u0177\u017c\7N\2\2\u0178\u0179\7N\2\2\u0179\u017c\7n\2\2\u017a\u017c"+
		"\7h\2\2\u017b\u0171\3\2\2\2\u017b\u0172\3\2\2\2\u017b\u0174\3\2\2\2\u017b"+
		"\u0176\3\2\2\2\u017b\u0178\3\2\2\2\u017b\u017a\3\2\2\2\u017cv\3\2\2\2"+
		"\u017d\u0181\7\62\2\2\u017e\u0180\5\177@\2\u017f\u017e\3\2\2\2\u0180\u0183"+
		"\3\2\2\2\u0181\u017f\3\2\2\2\u0181\u0182\3\2\2\2\u0182x\3\2\2\2\u0183"+
		"\u0181\3\2\2\2\u0184\u0186\5{>\2\u0185\u0187\5\u0081A\2\u0186\u0185\3"+
		"\2\2\2\u0187\u0188\3\2\2\2\u0188\u0186\3\2\2\2\u0188\u0189\3\2\2\2\u0189"+
		"z\3\2\2\2\u018a\u018b\7\62\2\2\u018b\u018c\t\5\2\2\u018c|\3\2\2\2\u018d"+
		"\u018e\t\6\2\2\u018e~\3\2\2\2\u018f\u0190\t\7\2\2\u0190\u0080\3\2\2\2"+
		"\u0191\u0192\t\b\2\2\u0192\u0082\3\2\2\2\u0193\u0194\t\t\2\2\u0194\u0084"+
		"\3\2\2\2\u0195\u0197\5o8\2\u0196\u0195\3\2\2\2\u0197\u0198\3\2\2\2\u0198"+
		"\u0196\3\2\2\2\u0198\u0199\3\2\2\2\u0199\u0086\3\2\2\2\u019a\u019b\7)"+
		"\2\2\u019b\u019c\5\u009bN\2\u019c\u019d\7)\2\2\u019d\u0088\3\2\2\2\u019e"+
		"\u01a0\5\u008bF\2\u019f\u019e\3\2\2\2\u01a0\u01a1\3\2\2\2\u01a1\u019f"+
		"\3\2\2\2\u01a1\u01a2\3\2\2\2\u01a2\u008a\3\2\2\2\u01a3\u01a6\n\n\2\2\u01a4"+
		"\u01a6\5\u008dG\2\u01a5\u01a3\3\2\2\2\u01a5\u01a4\3\2\2\2\u01a6\u008c"+
		"\3\2\2\2\u01a7\u01ab\5\u008fH\2\u01a8\u01ab\5\u0091I\2\u01a9\u01ab\5\u0093"+
		"J\2\u01aa\u01a7\3\2\2\2\u01aa\u01a8\3\2\2\2\u01aa\u01a9\3\2\2\2\u01ab"+
		"\u008e\3\2\2\2\u01ac\u01ad\7^\2\2\u01ad\u01ae\t\13\2\2\u01ae\u0090\3\2"+
		"\2\2\u01af\u01b0\7^\2\2\u01b0\u01bb\5\177@\2\u01b1\u01b2\7^\2\2\u01b2"+
		"\u01b3\5\177@\2\u01b3\u01b4\5\177@\2\u01b4\u01bb\3\2\2\2\u01b5\u01b6\7"+
		"^\2\2\u01b6\u01b7\5\177@\2\u01b7\u01b8\5\177@\2\u01b8\u01b9\5\177@\2\u01b9"+
		"\u01bb\3\2\2\2\u01ba\u01af\3\2\2\2\u01ba\u01b1\3\2\2\2\u01ba\u01b5\3\2"+
		"\2\2\u01bb\u0092\3\2\2\2\u01bc\u01bd\7^\2\2\u01bd\u01be\7z\2\2\u01be\u01c0"+
		"\3\2\2\2\u01bf\u01c1\5\u0081A\2\u01c0\u01bf\3\2\2\2\u01c1\u01c2\3\2\2"+
		"\2\u01c2\u01c0\3\2\2\2\u01c2\u01c3\3\2\2\2\u01c3\u0094\3\2\2\2\u01c4\u01c6"+
		"\7$\2\2\u01c5\u01c7\5\u0097L\2\u01c6\u01c5\3\2\2\2\u01c6\u01c7\3\2\2\2"+
		"\u01c7\u01c8\3\2\2\2\u01c8\u01c9\7$\2\2\u01c9\u0096\3\2\2\2\u01ca\u01cc"+
		"\5\u0099M\2\u01cb\u01ca\3\2\2\2\u01cc\u01cd\3\2\2\2\u01cd\u01cb\3\2\2"+
		"\2\u01cd\u01ce\3\2\2\2\u01ce\u0098\3\2\2\2\u01cf\u01d2\n\f\2\2\u01d0\u01d2"+
		"\5\u008dG\2\u01d1\u01cf\3\2\2\2\u01d1\u01d0\3\2\2\2\u01d2\u009a\3\2\2"+
		"\2\u01d3\u01d5\5\u009dO\2\u01d4\u01d3\3\2\2\2\u01d5\u01d6\3\2\2\2\u01d6"+
		"\u01d4\3\2\2\2\u01d6\u01d7\3\2\2\2\u01d7\u009c\3\2\2\2\u01d8\u01db\n\n"+
		"\2\2\u01d9\u01db\5\u008dG\2\u01da\u01d8\3\2\2\2\u01da\u01d9\3\2\2\2\u01db"+
		"\u009e\3\2\2\2\u01dc\u01e0\7%\2\2\u01dd\u01df\n\r\2\2\u01de\u01dd\3\2"+
		"\2\2\u01df\u01e2\3\2\2\2\u01e0\u01de\3\2\2\2\u01e0\u01e1\3\2\2\2\u01e1"+
		"\u01e6\3\2\2\2\u01e2\u01e0\3\2\2\2\u01e3\u01e7\t\r\2\2\u01e4\u01e5\7\17"+
		"\2\2\u01e5\u01e7\7\f\2\2\u01e6\u01e3\3\2\2\2\u01e6\u01e4\3\2\2\2\u01e7"+
		"\u01e8\3\2\2\2\u01e8\u01e9\bP\2\2\u01e9\u00a0\3\2\2\2\u01ea\u01ec\t\16"+
		"\2\2\u01eb\u01ea\3\2\2\2\u01ec\u01ed\3\2\2\2\u01ed\u01eb\3\2\2\2\u01ed"+
		"\u01ee\3\2\2\2\u01ee\u01ef\3\2\2\2\u01ef\u01f0\bQ\3\2\u01f0\u00a2\3\2"+
		"\2\2\u01f1\u01f3\7\17\2\2\u01f2\u01f4\7\f\2\2\u01f3\u01f2\3\2\2\2\u01f3"+
		"\u01f4\3\2\2\2\u01f4\u01f7\3\2\2\2\u01f5\u01f7\7\f\2\2\u01f6\u01f1\3\2"+
		"\2\2\u01f6\u01f5\3\2\2\2\u01f7\u01f8\3\2\2\2\u01f8\u01f9\bR\2\2\u01f9"+
		"\u00a4\3\2\2\2\u01fa\u01fb\7\61\2\2\u01fb\u01fc\7,\2\2\u01fc\u0200\3\2"+
		"\2\2\u01fd\u01ff\13\2\2\2\u01fe\u01fd\3\2\2\2\u01ff\u0202\3\2\2\2\u0200"+
		"\u0201\3\2\2\2\u0200\u01fe\3\2\2\2\u0201\u0203\3\2\2\2\u0202\u0200\3\2"+
		"\2\2\u0203\u0204\7,\2\2\u0204\u0205\7\61\2\2\u0205\u0206\3\2\2\2\u0206"+
		"\u0207\bS\2\2\u0207\u00a6\3\2\2\2\u0208\u0209\7\61\2\2\u0209\u020a\7\61"+
		"\2\2\u020a\u020e\3\2\2\2\u020b\u020d\n\r\2\2\u020c\u020b\3\2\2\2\u020d"+
		"\u0210\3\2\2\2\u020e\u020c\3\2\2\2\u020e\u020f\3\2\2\2\u020f\u0211\3\2"+
		"\2\2\u0210\u020e\3\2\2\2\u0211\u0212\bT\2\2\u0212\u00a8\3\2\2\2\36\2\u014f"+
		"\u0151\u0165\u0169\u016d\u016f\u017b\u0181\u0188\u0198\u01a1\u01a5\u01aa"+
		"\u01ba\u01c2\u01c6\u01cd\u01d1\u01d6\u01da\u01e0\u01e6\u01ed\u01f3\u01f6"+
		"\u0200\u020e\4\2\3\2\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}