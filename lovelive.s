.text
AtfunctionLabel___builtin_string_generate_quote_space_quote_:
sub $sp, $sp, 4
li $v0, 9
li $a0, 3
syscall
move $25, $v0
move $24, $6
sw $24, ($25)
move $24, $25
add $24, $24, $6
li $23, 32
sw $23, ($24)
move $23, $25
sw $23, reg_80
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_getHash:
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_69
li $25, 237
lw $24, reg_69
mul $23, $24, $25
lw $25, reg_77
div $22, $23, $25
mul $22, $25, $22
lw $21, reg_80
sub $21, $23, $22
sw $21, reg_80
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_put:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_73
lw $t0, -8($fp)
sw $t0, reg_74
move $25, $0
sw $25, reg_76
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_73
sw $t0, -4($fp)
sub $sp, $sp, 8
jal AtfunctionLabel_getHash
lw $25, reg_80
move $24, $25
mul $23, $24, $6
lw $22, reg_78
add $23, $23, $22
lw $21, 1($23)
move $23, $0
sw $21, reg_1
sw $23, reg_2
sw $24, reg_75
lw $t0, reg_1
lw $t1, reg_2
bne $t0, $t1, AtLabel_if_exit_24
AtLabel_if_body1_23:
li $v0, 9
li $a0, 4
syscall
move $25, $v0
li $24, 3
sw $24, ($25)
lw $24, reg_75
mul $23, $24, $6
lw $22, reg_78
add $23, $23, $22
add $21, $23, $6
sw $25, ($21)
mul $25, $24, $6
add $25, $25, $22
lw $23, 1($25)
move $25, $0
add $25, $25, $23
move $23, $25
lw $25, reg_73
sw $25, ($23)
mul $20, $24, $6
add $20, $20, $22
lw $19, 1($20)
move $20, $6
add $20, $20, $19
move $19, $20
lw $20, reg_74
sw $20, ($19)
move $18, $0
mul $17, $24, $6
add $17, $17, $22
lw $24, 1($17)
li $17, 2
add $17, $17, $24
move $24, $17
sw $18, ($24)
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtLabel_if_exit_24:
lw $25, reg_75
mul $24, $25, $6
lw $25, reg_78
add $24, $24, $25
lw $23, 1($24)
move $24, $23
sw $24, reg_76
sw $25, reg_78
AtLabel_while_cond_25:
move $25, $0
lw $24, reg_76
add $25, $25, $24
lw $23, ($25)
sw $23, reg_20
lw $t0, reg_20
lw $t1, reg_73
beq $t0, $t1, AtLabel_while_end_26
AtLabel_while_start_27:
li $25, 2
lw $24, reg_76
add $25, $25, $24
lw $23, ($25)
move $25, $0
sw $23, reg_22
sw $25, reg_23
lw $t0, reg_22
lw $t1, reg_23
bne $t0, $t1, AtLabel_if_exit_29
AtLabel_if_body1_28:
li $v0, 9
li $a0, 4
syscall
move $25, $v0
li $24, 3
sw $24, ($25)
li $24, 2
lw $23, reg_76
add $24, $24, $23
move $22, $24
sw $25, ($22)
li $25, 2
add $25, $25, $23
lw $24, ($25)
move $25, $0
add $25, $25, $24
move $24, $25
lw $25, reg_73
sw $25, ($24)
move $21, $0
li $20, 2
add $20, $20, $23
lw $19, ($20)
li $20, 2
add $20, $20, $19
move $19, $20
sw $21, ($19)
sw $19, reg_34
sw $22, reg_27
sw $23, reg_76
sw $24, reg_30
sw $25, reg_73
AtLabel_if_exit_29:
li $25, 2
lw $24, reg_76
add $25, $25, $24
lw $24, ($25)
move $25, $24
sw $25, reg_76
j AtLabel_while_cond_25
AtLabel_while_end_26:
move $25, $6
lw $24, reg_76
add $25, $25, $24
move $24, $25
lw $25, reg_74
sw $25, ($24)
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_get:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_39
move $25, $0
sw $25, reg_40
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_39
sw $t0, -4($fp)
sub $sp, $sp, 8
jal AtfunctionLabel_getHash
lw $25, reg_80
mul $24, $25, $6
sw $25, reg_80
lw $25, reg_78
add $24, $24, $25
lw $23, 1($24)
move $24, $23
sw $24, reg_40
sw $25, reg_78
AtLabel_while_cond_30:
move $25, $0
lw $24, reg_40
add $25, $25, $24
lw $23, ($25)
sw $23, reg_44
lw $t0, reg_44
lw $t1, reg_39
beq $t0, $t1, AtLabel_while_end_31
AtLabel_while_start_32:
li $25, 2
lw $24, reg_40
add $25, $25, $24
lw $24, ($25)
move $25, $24
sw $25, reg_40
j AtLabel_while_cond_30
AtLabel_while_end_31:
move $25, $6
lw $24, reg_40
add $25, $25, $24
lw $24, ($25)
move $25, $24
sw $25, reg_80
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
main:
move $fp, $sp
li $7, 4
li $6, 1
li $t0, 32
li $t1, 1
la $a0, __CharSpace
sw $t1, ($a0)
sw $t0, 4($a0)
li $t0, 10
li $t1, 1
la $a0, __CharEnter
sw $t1, ($a0)
sw $t0, 4($a0)
sw $31, ($sp)
sub $sp, $sp, 4
la $t0, static
sw $t0, reg_79
li $25, 5
lw $24, reg_79
sw $25, ($24)
move $25, $6
sw $25, 3($24)
move $25, $6
sw $25, 4($24)
move $25, $6
sw $25, 5($24)
add $25, $24, $6
add $23, $24, 2
li $25, 100
sw $25, 0($24)
li $22, 100
mul $21, $22, $6
add $21, $21, $6
li $v0, 9
move $a0, $21
syscall
move $20, $v0
sw $22, ($20)
move $23, $20
move $20, $0
sw $20, reg_49
sw $23, reg_78
sw $25, reg_77
j AtLabel_for_cond_33
AtLabel_for_exp3_35:
lw $25, reg_49
add $25, $25, $6
sw $25, reg_49
AtLabel_for_cond_33:
lw $t0, reg_49
lw $t1, reg_77
bge $t0, $t1, AtLabel_for_exit_36
AtLabel_for_body_34:
move $25, $0
lw $24, reg_49
mul $23, $24, $6
lw $22, reg_78
add $23, $23, $22
add $21, $23, $6
sw $25, ($21)
j AtLabel_for_exp3_35
AtLabel_for_exit_36:
move $25, $0
sw $25, reg_49
j AtLabel_for_cond_37
AtLabel_for_exp3_39:
lw $25, reg_49
add $25, $25, $6
sw $25, reg_49
AtLabel_for_cond_37:
li $25, 1000
sw $25, reg_56
lw $t0, reg_49
lw $t1, reg_56
bge $t0, $t1, AtLabel_for_exit_40
AtLabel_for_body_38:
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_49
sw $t0, -4($fp)
lw $t0, reg_49
sw $t0, -8($fp)
sub $sp, $sp, 12
jal AtfunctionLabel_put
j AtLabel_for_exp3_39
AtLabel_for_exit_40:
move $25, $0
sw $25, reg_49
j AtLabel_for_cond_41
AtLabel_for_exp3_43:
lw $25, reg_49
add $25, $25, $6
sw $25, reg_49
AtLabel_for_cond_41:
li $25, 1000
sw $25, reg_60
lw $t0, reg_49
lw $t1, reg_60
bge $t0, $t1, AtLabel_for_exit_44
AtLabel_for_body_42:
li $v0, 1
lw $a0, reg_49
syscall
la $t0, __CharSpace
sw $t0, reg_80
lw $25, reg_80
move $24, $25
sw $25, reg_80
sw $24, reg_62
li $v0, 4
lw $a0, reg_62
add $a0, $a0, 4
syscall
sw $fp, ($sp)
move $fp, $sp
lw $t0, reg_49
sw $t0, -4($fp)
sub $sp, $sp, 8
jal AtfunctionLabel_get
li $v0, 1
lw $a0, reg_80
syscall
li $v0, 4
la $a0, __CharEnter
add $a0, $a0, 4
syscall
j AtLabel_for_exp3_43
AtLabel_for_exit_44:
move $25, $0
sw $25, reg_80
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel_print:
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_63
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel___builtin_printIntln:
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_67
move $sp, $fp
lw $fp, ($fp)
jr $31
AtfunctionLabel___builtin_printInt:
sw $31, ($sp)
sub $sp, $sp, 4
lw $t0, -4($fp)
sw $t0, reg_68
lw $31, 4($sp)
move $sp, $fp
lw $fp, ($fp)
jr $31

.data
static:
	.space 6
reg_0: .space 4
reg_1: .space 4
reg_2: .space 4
reg_3: .space 4
reg_4: .space 4
reg_5: .space 4
reg_6: .space 4
reg_7: .space 4
reg_8: .space 4
reg_9: .space 4
reg_10: .space 4
reg_11: .space 4
reg_12: .space 4
reg_13: .space 4
reg_14: .space 4
reg_15: .space 4
reg_16: .space 4
reg_17: .space 4
reg_18: .space 4
reg_19: .space 4
reg_20: .space 4
reg_21: .space 4
reg_22: .space 4
reg_23: .space 4
reg_24: .space 4
reg_25: .space 4
reg_26: .space 4
reg_27: .space 4
reg_28: .space 4
reg_29: .space 4
reg_30: .space 4
reg_31: .space 4
reg_32: .space 4
reg_33: .space 4
reg_34: .space 4
reg_35: .space 4
reg_36: .space 4
reg_37: .space 4
reg_38: .space 4
reg_39: .space 4
reg_40: .space 4
reg_41: .space 4
reg_42: .space 4
reg_43: .space 4
reg_44: .space 4
reg_45: .space 4
reg_46: .space 4
reg_47: .space 4
reg_48: .space 4
reg_49: .space 4
reg_50: .space 4
reg_51: .space 4
reg_52: .space 4
reg_53: .space 4
reg_54: .space 4
reg_55: .space 4
reg_56: .space 4
reg_57: .space 4
reg_58: .space 4
reg_59: .space 4
reg_60: .space 4
reg_61: .space 4
reg_62: .space 4
reg_63: .space 4
reg_64: .space 4
reg_65: .space 4
reg_66: .space 4
reg_67: .space 4
reg_68: .space 4
reg_69: .space 4
reg_70: .space 4
reg_71: .space 4
reg_72: .space 4
reg_73: .space 4
reg_74: .space 4
reg_75: .space 4
reg_76: .space 4
reg_77: .space 4
reg_78: .space 4
reg_79: .space 4
reg_80: .space 4
reg_81: .space 4
reg_82: .space 4
reg_83: .space 4
reg_84: .space 4
reg_85: .space 4
reg_86: .space 4
reg_87: .space 4
reg_88: .space 4
reg_89: .space 4
reg_90: .space 4
__CharEnter: .space 8
__CharSpace: .space 8



