#include <cstdio>
#include <cstring>
#include <iostream>
#include <vector>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

vector<string> src_raw;

void read_src_code()
{
	string tmp;
	while(getline(cin, tmp))
		src_raw.push_back(tmp);
	ofstream out("test.ll");
	for(int i = 0; i < src_raw.size(); ++i)
	{
		out << src_raw[i] << endl;
	}
}

void read_s_code()
{
	string tmp;
	ifstream s_in("lovelive.s");
	while(getline(s_in, tmp))
	{
		cout << tmp << endl;
	}
}

int main()
{
	read_src_code();
	system("java Main");
	system("./move_code");
	system("./CFG");
	system("./Dead");
	system("./CFG");
	system("./DeadCode");
	system("./CFG");
	system("./RISClization");
	system("./CISC");
	system("./TrueDead");
	read_s_code();
	return 0;
}