all:
	mkdir -p bin
	javac -cp src/Parser/antlr-4.5.2-complete.jar src/*/*/*.java src/*/*.java src/*.java -d bin
	g++ -std=c++11 src/Optimization/CFG/CFG/main.cpp -o bin/CFG -O2
	g++ -std=c++11 src/Optimization/Dead/Dead/main.cpp -o bin/Dead -O2
	g++ -std=c++11 src/Optimization/DeadCode/DeadCode/main.cpp -o bin/DeadCode -O2
	g++ -std=c++11 src/Optimization/RISClization/RISClization/main.cpp -o bin/RISClization -O2
	g++ -std=c++11 src/Optimization/TrueDead/TrueDead/main.cpp -o bin/TrueDead -O2
	g++ -std=c++11 src/Translate/CISC/CISC/Main.cpp -o bin/CISC -O2
	g++ -std=c++11 src/Translate/move_code.cpp -o bin/move_code -O2
	g++ -std=c++11 muse.cpp -o bin/muse -O2
	cp built_in.ll bin
clean:
	rm -rf bin
