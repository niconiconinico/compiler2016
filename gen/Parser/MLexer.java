// Generated from C:/Users/���/Documents/GitHub/compiler2016/src/Parser\M.g4 by ANTLR 4.5.1
package Parser;

package Parser;
        import org.antlr.v4.runtime.Lexer;
        import org.antlr.v4.runtime.CharStream;
        import org.antlr.v4.runtime.Token;
        import org.antlr.v4.runtime.TokenStream;
        import org.antlr.v4.runtime.*;
        import org.antlr.v4.runtime.atn.*;
        import org.antlr.v4.runtime.dfa.DFA;
        import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MLexer extends Lexer {
    public static final int
            T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, T__7 = 8, T__8 = 9,
            T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, T__14 = 15, T__15 = 16, T__16 = 17,
            T__17 = 18, T__18 = 19, T__19 = 20, T__20 = 21, T__21 = 22, T__22 = 23, T__23 = 24,
            T__24 = 25, T__25 = 26, T__26 = 27, T__27 = 28, T__28 = 29, T__29 = 30, Comma = 31,
            Equal = 32, Lbracket = 33, Rbracket = 34, LBigbracket = 35, RBigbracket = 36, LMidbracket = 37,
            RMidbracket = 38, New = 39, Balabalabala = 40, BREAK = 41, CONTINUE = 42, RETURN = 43,
            FOR = 44, WHILE = 45, DO = 46, IF = 47, ELSE = 48, COLON = 49, QUESTION = 50, Dot = 51,
            Identifier = 52, SEMI = 53, DecimalConstant = 54, Numbertail = 55, OctalConstant = 56,
            HexadecimalConstant = 57, Sign = 58, CharacterConstant = 59, StringLiteral = 60,
            Preprocessing = 61, Whitespace = 62, Newline = 63, BlockComment = 64, LineComment = 65;
    public static final String[] ruleNames = {
            "T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8",
            "T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16",
            "T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "T__24",
            "T__25", "T__26", "T__27", "T__28", "T__29", "Comma", "Equal", "Lbracket",
            "Rbracket", "LBigbracket", "RBigbracket", "LMidbracket", "RMidbracket",
            "New", "Balabalabala", "BREAK", "CONTINUE", "RETURN", "FOR", "WHILE",
            "DO", "IF", "ELSE", "COLON", "QUESTION", "Dot", "Identifier", "IdentifierNondigit",
            "Nondigit", "SEMI", "Digit", "HexQuad", "DecimalConstant", "Numbertail",
            "OctalConstant", "HexadecimalConstant", "HexadecimalPrefix", "NonzeroDigit",
            "OctalDigit", "HexadecimalDigit", "Sign", "DigitSequence", "CharacterConstant",
            "CharSequence", "Char", "EscapeSequence", "SimpleEscapeSequence", "OctalEscapeSequence",
            "HexadecimalEscapeSequence", "StringLiteral", "SCharSequence", "SChar",
            "CCharSequence", "CChar", "Preprocessing", "Whitespace", "Newline", "BlockComment",
            "LineComment"
    };
    /**
     * @deprecated Use {@link #VOCABULARY} instead.
     */
    @Deprecated
    public static final String[] tokenNames;
    public static final String _serializedATN =
            "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2C\u021c\b\1\4\2\t" +
                    "\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13" +
                    "\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22" +
                    "\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31" +
                    "\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!" +
                    "\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4" +
                    ",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t" +
                    "\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t=" +
                    "\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I" +
                    "\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT" +
                    "\4U\tU\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t" +
                    "\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r\3\16\3\16\3\16" +
                    "\3\17\3\17\3\20\3\20\3\21\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\24\3\24" +
                    "\3\24\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27" +
                    "\3\27\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\33\3\33" +
                    "\3\34\3\34\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\36\3\36" +
                    "\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3 \3 \3!\3!\3\"\3\"\3#\3" +
                    "#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3(\3(\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*" +
                    "\3+\3+\3+\3+\3+\3+\3+\3+\3+\3,\3,\3,\3,\3,\3,\3,\3-\3-\3-\3-\3.\3.\3." +
                    "\3.\3.\3.\3/\3/\3/\3\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\62\3\62\3" +
                    "\63\3\63\3\64\3\64\3\65\3\65\3\65\7\65\u0159\n\65\f\65\16\65\u015c\13" +
                    "\65\3\66\3\66\3\67\3\67\38\38\39\39\3:\3:\3:\3:\3:\3;\3;\7;\u016d\n;\f" +
                    ";\16;\u0170\13;\3;\5;\u0173\n;\3;\3;\5;\u0177\n;\5;\u0179\n;\3<\3<\3<" +
                    "\3<\3<\3<\3<\3<\3<\3<\5<\u0185\n<\3=\3=\7=\u0189\n=\f=\16=\u018c\13=\3" +
                    ">\3>\6>\u0190\n>\r>\16>\u0191\3?\3?\3?\3@\3@\3A\3A\3B\3B\3C\3C\3D\6D\u01a0" +
                    "\nD\rD\16D\u01a1\3E\3E\3E\3E\3F\6F\u01a9\nF\rF\16F\u01aa\3G\3G\5G\u01af" +
                    "\nG\3H\3H\3H\5H\u01b4\nH\3I\3I\3I\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\3J\5J" +
                    "\u01c4\nJ\3K\3K\3K\3K\6K\u01ca\nK\rK\16K\u01cb\3L\3L\5L\u01d0\nL\3L\3" +
                    "L\3M\6M\u01d5\nM\rM\16M\u01d6\3N\3N\5N\u01db\nN\3O\6O\u01de\nO\rO\16O" +
                    "\u01df\3P\3P\5P\u01e4\nP\3Q\3Q\7Q\u01e8\nQ\fQ\16Q\u01eb\13Q\3Q\3Q\3Q\5" +
                    "Q\u01f0\nQ\3Q\3Q\3R\6R\u01f5\nR\rR\16R\u01f6\3R\3R\3S\3S\5S\u01fd\nS\3" +
                    "S\5S\u0200\nS\3S\3S\3T\3T\3T\3T\7T\u0208\nT\fT\16T\u020b\13T\3T\3T\3T" +
                    "\3T\3T\3U\3U\3U\3U\7U\u0216\nU\fU\16U\u0219\13U\3U\3U\3\u0209\2V\3\3\5" +
                    "\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21" +
                    "!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!" +
                    "A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k\2m\2o\67q" +
                    "\2s\2u8w9y:{;}\2\177\2\u0081\2\u0083\2\u0085<\u0087\2\u0089=\u008b\2\u008d" +
                    "\2\u008f\2\u0091\2\u0093\2\u0095\2\u0097>\u0099\2\u009b\2\u009d\2\u009f" +
                    "\2\u00a1?\u00a3@\u00a5A\u00a7B\u00a9C\3\2\17\6\2&&C\\aac|\3\2\62;\4\2" +
                    "WWww\4\2ZZzz\3\2\63;\3\2\629\5\2\62;CHch\4\2--//\6\2\f\f\17\17))^^\f\2" +
                    "$$))AA^^cdhhppttvvxx\6\2\f\f\17\17$$^^\4\2\f\f\17\17\4\2\13\13\"\"\u0229" +
                    "\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2" +
                    "\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2" +
                    "\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2" +
                    "\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2" +
                    "\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3" +
                    "\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2" +
                    "\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2" +
                    "U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3\2\2\2\2a\3" +
                    "\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2o\3\2\2\2\2u\3\2\2" +
                    "\2\2w\3\2\2\2\2y\3\2\2\2\2{\3\2\2\2\2\u0085\3\2\2\2\2\u0089\3\2\2\2\2" +
                    "\u0097\3\2\2\2\2\u00a1\3\2\2\2\2\u00a3\3\2\2\2\2\u00a5\3\2\2\2\2\u00a7" +
                    "\3\2\2\2\2\u00a9\3\2\2\2\3\u00ab\3\2\2\2\5\u00ad\3\2\2\2\7\u00af\3\2\2" +
                    "\2\t\u00b1\3\2\2\2\13\u00b3\3\2\2\2\r\u00b5\3\2\2\2\17\u00b7\3\2\2\2\21" +
                    "\u00b9\3\2\2\2\23\u00bc\3\2\2\2\25\u00bf\3\2\2\2\27\u00c2\3\2\2\2\31\u00c5" +
                    "\3\2\2\2\33\u00c8\3\2\2\2\35\u00cb\3\2\2\2\37\u00cd\3\2\2\2!\u00cf\3\2" +
                    "\2\2#\u00d2\3\2\2\2%\u00d5\3\2\2\2\'\u00d7\3\2\2\2)\u00da\3\2\2\2+\u00dd" +
                    "\3\2\2\2-\u00e2\3\2\2\2/\u00e8\3\2\2\2\61\u00ed\3\2\2\2\63\u00ef\3\2\2" +
                    "\2\65\u00f1\3\2\2\2\67\u00f5\3\2\2\29\u00fc\3\2\2\2;\u0101\3\2\2\2=\u0106" +
                    "\3\2\2\2?\u010c\3\2\2\2A\u010e\3\2\2\2C\u0110\3\2\2\2E\u0112\3\2\2\2G" +
                    "\u0114\3\2\2\2I\u0116\3\2\2\2K\u0118\3\2\2\2M\u011a\3\2\2\2O\u011c\3\2" +
                    "\2\2Q\u0120\3\2\2\2S\u0124\3\2\2\2U\u012a\3\2\2\2W\u0133\3\2\2\2Y\u013a" +
                    "\3\2\2\2[\u013e\3\2\2\2]\u0144\3\2\2\2_\u0147\3\2\2\2a\u014a\3\2\2\2c" +
                    "\u014f\3\2\2\2e\u0151\3\2\2\2g\u0153\3\2\2\2i\u0155\3\2\2\2k\u015d\3\2" +
                    "\2\2m\u015f\3\2\2\2o\u0161\3\2\2\2q\u0163\3\2\2\2s\u0165\3\2\2\2u\u0178" +
                    "\3\2\2\2w\u0184\3\2\2\2y\u0186\3\2\2\2{\u018d\3\2\2\2}\u0193\3\2\2\2\177" +
                    "\u0196\3\2\2\2\u0081\u0198\3\2\2\2\u0083\u019a\3\2\2\2\u0085\u019c\3\2" +
                    "\2\2\u0087\u019f\3\2\2\2\u0089\u01a3\3\2\2\2\u008b\u01a8\3\2\2\2\u008d" +
                    "\u01ae\3\2\2\2\u008f\u01b3\3\2\2\2\u0091\u01b5\3\2\2\2\u0093\u01c3\3\2" +
                    "\2\2\u0095\u01c5\3\2\2\2\u0097\u01cd\3\2\2\2\u0099\u01d4\3\2\2\2\u009b" +
                    "\u01da\3\2\2\2\u009d\u01dd\3\2\2\2\u009f\u01e3\3\2\2\2\u00a1\u01e5\3\2" +
                    "\2\2\u00a3\u01f4\3\2\2\2\u00a5\u01ff\3\2\2\2\u00a7\u0203\3\2\2\2\u00a9" +
                    "\u0211\3\2\2\2\u00ab\u00ac\7-\2\2\u00ac\4\3\2\2\2\u00ad\u00ae\7/\2\2\u00ae" +
                    "\6\3\2\2\2\u00af\u00b0\7,\2\2\u00b0\b\3\2\2\2\u00b1\u00b2\7\61\2\2\u00b2" +
                    "\n\3\2\2\2\u00b3\u00b4\7(\2\2\u00b4\f\3\2\2\2\u00b5\u00b6\7`\2\2\u00b6" +
                    "\16\3\2\2\2\u00b7\u00b8\7~\2\2\u00b8\20\3\2\2\2\u00b9\u00ba\7>\2\2\u00ba" +
                    "\u00bb\7>\2\2\u00bb\22\3\2\2\2\u00bc\u00bd\7@\2\2\u00bd\u00be\7@\2\2\u00be" +
                    "\24\3\2\2\2\u00bf\u00c0\7~\2\2\u00c0\u00c1\7~\2\2\u00c1\26\3\2\2\2\u00c2" +
                    "\u00c3\7(\2\2\u00c3\u00c4\7(\2\2\u00c4\30\3\2\2\2\u00c5\u00c6\7?\2\2\u00c6" +
                    "\u00c7\7?\2\2\u00c7\32\3\2\2\2\u00c8\u00c9\7#\2\2\u00c9\u00ca\7?\2\2\u00ca" +
                    "\34\3\2\2\2\u00cb\u00cc\7@\2\2\u00cc\36\3\2\2\2\u00cd\u00ce\7>\2\2\u00ce" +
                    " \3\2\2\2\u00cf\u00d0\7@\2\2\u00d0\u00d1\7?\2\2\u00d1\"\3\2\2\2\u00d2" +
                    "\u00d3\7>\2\2\u00d3\u00d4\7?\2\2\u00d4$\3\2\2\2\u00d5\u00d6\7\'\2\2\u00d6" +
                    "&\3\2\2\2\u00d7\u00d8\7-\2\2\u00d8\u00d9\7-\2\2\u00d9(\3\2\2\2\u00da\u00db" +
                    "\7/\2\2\u00db\u00dc\7/\2\2\u00dc*\3\2\2\2\u00dd\u00de\7v\2\2\u00de\u00df" +
                    "\7t\2\2\u00df\u00e0\7w\2\2\u00e0\u00e1\7g\2\2\u00e1,\3\2\2\2\u00e2\u00e3" +
                    "\7h\2\2\u00e3\u00e4\7c\2\2\u00e4\u00e5\7n\2\2\u00e5\u00e6\7u\2\2\u00e6" +
                    "\u00e7\7g\2\2\u00e7.\3\2\2\2\u00e8\u00e9\7p\2\2\u00e9\u00ea\7w\2\2\u00ea" +
                    "\u00eb\7n\2\2\u00eb\u00ec\7n\2\2\u00ec\60\3\2\2\2\u00ed\u00ee\7#\2\2\u00ee" +
                    "\62\3\2\2\2\u00ef\u00f0\7\u0080\2\2\u00f0\64\3\2\2\2\u00f1\u00f2\7k\2" +
                    "\2\u00f2\u00f3\7p\2\2\u00f3\u00f4\7v\2\2\u00f4\66\3\2\2\2\u00f5\u00f6" +
                    "\7u\2\2\u00f6\u00f7\7v\2\2\u00f7\u00f8\7t\2\2\u00f8\u00f9\7k\2\2\u00f9" +
                    "\u00fa\7p\2\2\u00fa\u00fb\7i\2\2\u00fb8\3\2\2\2\u00fc\u00fd\7d\2\2\u00fd" +
                    "\u00fe\7q\2\2\u00fe\u00ff\7q\2\2\u00ff\u0100\7n\2\2\u0100:\3\2\2\2\u0101" +
                    "\u0102\7x\2\2\u0102\u0103\7q\2\2\u0103\u0104\7k\2\2\u0104\u0105\7f\2\2" +
                    "\u0105<\3\2\2\2\u0106\u0107\7e\2\2\u0107\u0108\7n\2\2\u0108\u0109\7c\2" +
                    "\2\u0109\u010a\7u\2\2\u010a\u010b\7u\2\2\u010b>\3\2\2\2\u010c\u010d\7" +
                    ".\2\2\u010d@\3\2\2\2\u010e\u010f\7?\2\2\u010fB\3\2\2\2\u0110\u0111\7*" +
                    "\2\2\u0111D\3\2\2\2\u0112\u0113\7+\2\2\u0113F\3\2\2\2\u0114\u0115\7}\2" +
                    "\2\u0115H\3\2\2\2\u0116\u0117\7\177\2\2\u0117J\3\2\2\2\u0118\u0119\7]" +
                    "\2\2\u0119L\3\2\2\2\u011a\u011b\7_\2\2\u011bN\3\2\2\2\u011c\u011d\7p\2" +
                    "\2\u011d\u011e\7g\2\2\u011e\u011f\7y\2\2\u011fP\3\2\2\2\u0120\u0121\7" +
                    "\60\2\2\u0121\u0122\7\60\2\2\u0122\u0123\7\60\2\2\u0123R\3\2\2\2\u0124" +
                    "\u0125\7d\2\2\u0125\u0126\7t\2\2\u0126\u0127\7g\2\2\u0127\u0128\7c\2\2" +
                    "\u0128\u0129\7m\2\2\u0129T\3\2\2\2\u012a\u012b\7e\2\2\u012b\u012c\7q\2" +
                    "\2\u012c\u012d\7p\2\2\u012d\u012e\7v\2\2\u012e\u012f\7k\2\2\u012f\u0130" +
                    "\7p\2\2\u0130\u0131\7w\2\2\u0131\u0132\7g\2\2\u0132V\3\2\2\2\u0133\u0134" +
                    "\7t\2\2\u0134\u0135\7g\2\2\u0135\u0136\7v\2\2\u0136\u0137\7w\2\2\u0137" +
                    "\u0138\7t\2\2\u0138\u0139\7p\2\2\u0139X\3\2\2\2\u013a\u013b\7h\2\2\u013b" +
                    "\u013c\7q\2\2\u013c\u013d\7t\2\2\u013dZ\3\2\2\2\u013e\u013f\7y\2\2\u013f" +
                    "\u0140\7j\2\2\u0140\u0141\7k\2\2\u0141\u0142\7n\2\2\u0142\u0143\7g\2\2" +
                    "\u0143\\\3\2\2\2\u0144\u0145\7f\2\2\u0145\u0146\7q\2\2\u0146^\3\2\2\2" +
                    "\u0147\u0148\7k\2\2\u0148\u0149\7h\2\2\u0149`\3\2\2\2\u014a\u014b\7g\2" +
                    "\2\u014b\u014c\7n\2\2\u014c\u014d\7u\2\2\u014d\u014e\7g\2\2\u014eb\3\2" +
                    "\2\2\u014f\u0150\7<\2\2\u0150d\3\2\2\2\u0151\u0152\7A\2\2\u0152f\3\2\2" +
                    "\2\u0153\u0154\7\60\2\2\u0154h\3\2\2\2\u0155\u015a\5k\66\2\u0156\u0159" +
                    "\5k\66\2\u0157\u0159\5q9\2\u0158\u0156\3\2\2\2\u0158\u0157\3\2\2\2\u0159" +
                    "\u015c\3\2\2\2\u015a\u0158\3\2\2\2\u015a\u015b\3\2\2\2\u015bj\3\2\2\2" +
                    "\u015c\u015a\3\2\2\2\u015d\u015e\5m\67\2\u015el\3\2\2\2\u015f\u0160\t" +
                    "\2\2\2\u0160n\3\2\2\2\u0161\u0162\7=\2\2\u0162p\3\2\2\2\u0163\u0164\t" +
                    "\3\2\2\u0164r\3\2\2\2\u0165\u0166\5\u0083B\2\u0166\u0167\5\u0083B\2\u0167" +
                    "\u0168\5\u0083B\2\u0168\u0169\5\u0083B\2\u0169t\3\2\2\2\u016a\u016e\5" +
                    "\177@\2\u016b\u016d\5q9\2\u016c\u016b\3\2\2\2\u016d\u0170\3\2\2\2\u016e" +
                    "\u016c\3\2\2\2\u016e\u016f\3\2\2\2\u016f\u0172\3\2\2\2\u0170\u016e\3\2" +
                    "\2\2\u0171\u0173\5w<\2\u0172\u0171\3\2\2\2\u0172\u0173\3\2\2\2\u0173\u0179" +
                    "\3\2\2\2\u0174\u0176\7\62\2\2\u0175\u0177\5w<\2\u0176\u0175\3\2\2\2\u0176" +
                    "\u0177\3\2\2\2\u0177\u0179\3\2\2\2\u0178\u016a\3\2\2\2\u0178\u0174\3\2" +
                    "\2\2\u0179v\3\2\2\2\u017a\u0185\t\4\2\2\u017b\u017c\7n\2\2\u017c\u0185" +
                    "\7n\2\2\u017d\u017e\7N\2\2\u017e\u0185\7N\2\2\u017f\u0180\7n\2\2\u0180" +
                    "\u0185\7N\2\2\u0181\u0182\7N\2\2\u0182\u0185\7n\2\2\u0183\u0185\7h\2\2" +
                    "\u0184\u017a\3\2\2\2\u0184\u017b\3\2\2\2\u0184\u017d\3\2\2\2\u0184\u017f" +
                    "\3\2\2\2\u0184\u0181\3\2\2\2\u0184\u0183\3\2\2\2\u0185x\3\2\2\2\u0186" +
                    "\u018a\7\62\2\2\u0187\u0189\5\u0081A\2\u0188\u0187\3\2\2\2\u0189\u018c" +
                    "\3\2\2\2\u018a\u0188\3\2\2\2\u018a\u018b\3\2\2\2\u018bz\3\2\2\2\u018c" +
                    "\u018a\3\2\2\2\u018d\u018f\5}?\2\u018e\u0190\5\u0083B\2\u018f\u018e\3" +
                    "\2\2\2\u0190\u0191\3\2\2\2\u0191\u018f\3\2\2\2\u0191\u0192\3\2\2\2\u0192" +
                    "|\3\2\2\2\u0193\u0194\7\62\2\2\u0194\u0195\t\5\2\2\u0195~\3\2\2\2\u0196" +
                    "\u0197\t\6\2\2\u0197\u0080\3\2\2\2\u0198\u0199\t\7\2\2\u0199\u0082\3\2" +
                    "\2\2\u019a\u019b\t\b\2\2\u019b\u0084\3\2\2\2\u019c\u019d\t\t\2\2\u019d" +
                    "\u0086\3\2\2\2\u019e\u01a0\5q9\2\u019f\u019e\3\2\2\2\u01a0\u01a1\3\2\2" +
                    "\2\u01a1\u019f\3\2\2\2\u01a1\u01a2\3\2\2\2\u01a2\u0088\3\2\2\2\u01a3\u01a4" +
                    "\7)\2\2\u01a4\u01a5\5\u009dO\2\u01a5\u01a6\7)\2\2\u01a6\u008a\3\2\2\2" +
                    "\u01a7\u01a9\5\u008dG\2\u01a8\u01a7\3\2\2\2\u01a9\u01aa\3\2\2\2\u01aa" +
                    "\u01a8\3\2\2\2\u01aa\u01ab\3\2\2\2\u01ab\u008c\3\2\2\2\u01ac\u01af\n\n" +
                    "\2\2\u01ad\u01af\5\u008fH\2\u01ae\u01ac\3\2\2\2\u01ae\u01ad\3\2\2\2\u01af" +
                    "\u008e\3\2\2\2\u01b0\u01b4\5\u0091I\2\u01b1\u01b4\5\u0093J\2\u01b2\u01b4" +
                    "\5\u0095K\2\u01b3\u01b0\3\2\2\2\u01b3\u01b1\3\2\2\2\u01b3\u01b2\3\2\2" +
                    "\2\u01b4\u0090\3\2\2\2\u01b5\u01b6\7^\2\2\u01b6\u01b7\t\13\2\2\u01b7\u0092" +
                    "\3\2\2\2\u01b8\u01b9\7^\2\2\u01b9\u01c4\5\u0081A\2\u01ba\u01bb\7^\2\2" +
                    "\u01bb\u01bc\5\u0081A\2\u01bc\u01bd\5\u0081A\2\u01bd\u01c4\3\2\2\2\u01be" +
                    "\u01bf\7^\2\2\u01bf\u01c0\5\u0081A\2\u01c0\u01c1\5\u0081A\2\u01c1\u01c2" +
                    "\5\u0081A\2\u01c2\u01c4\3\2\2\2\u01c3\u01b8\3\2\2\2\u01c3\u01ba\3\2\2" +
                    "\2\u01c3\u01be\3\2\2\2\u01c4\u0094\3\2\2\2\u01c5\u01c6\7^\2\2\u01c6\u01c7" +
                    "\7z\2\2\u01c7\u01c9\3\2\2\2\u01c8\u01ca\5\u0083B\2\u01c9\u01c8\3\2\2\2" +
                    "\u01ca\u01cb\3\2\2\2\u01cb\u01c9\3\2\2\2\u01cb\u01cc\3\2\2\2\u01cc\u0096" +
                    "\3\2\2\2\u01cd\u01cf\7$\2\2\u01ce\u01d0\5\u0099M\2\u01cf\u01ce\3\2\2\2" +
                    "\u01cf\u01d0\3\2\2\2\u01d0\u01d1\3\2\2\2\u01d1\u01d2\7$\2\2\u01d2\u0098" +
                    "\3\2\2\2\u01d3\u01d5\5\u009bN\2\u01d4\u01d3\3\2\2\2\u01d5\u01d6\3\2\2" +
                    "\2\u01d6\u01d4\3\2\2\2\u01d6\u01d7\3\2\2\2\u01d7\u009a\3\2\2\2\u01d8\u01db" +
                    "\n\f\2\2\u01d9\u01db\5\u008fH\2\u01da\u01d8\3\2\2\2\u01da\u01d9\3\2\2" +
                    "\2\u01db\u009c\3\2\2\2\u01dc\u01de\5\u009fP\2\u01dd\u01dc\3\2\2\2\u01de" +
                    "\u01df\3\2\2\2\u01df\u01dd\3\2\2\2\u01df\u01e0\3\2\2\2\u01e0\u009e\3\2" +
                    "\2\2\u01e1\u01e4\n\n\2\2\u01e2\u01e4\5\u008fH\2\u01e3\u01e1\3\2\2\2\u01e3" +
                    "\u01e2\3\2\2\2\u01e4\u00a0\3\2\2\2\u01e5\u01e9\7%\2\2\u01e6\u01e8\n\r" +
                    "\2\2\u01e7\u01e6\3\2\2\2\u01e8\u01eb\3\2\2\2\u01e9\u01e7\3\2\2\2\u01e9" +
                    "\u01ea\3\2\2\2\u01ea\u01ef\3\2\2\2\u01eb\u01e9\3\2\2\2\u01ec\u01f0\t\r" +
                    "\2\2\u01ed\u01ee\7\17\2\2\u01ee\u01f0\7\f\2\2\u01ef\u01ec\3\2\2\2\u01ef" +
                    "\u01ed\3\2\2\2\u01f0\u01f1\3\2\2\2\u01f1\u01f2\bQ\2\2\u01f2\u00a2\3\2" +
                    "\2\2\u01f3\u01f5\t\16\2\2\u01f4\u01f3\3\2\2\2\u01f5\u01f6\3\2\2\2\u01f6" +
                    "\u01f4\3\2\2\2\u01f6\u01f7\3\2\2\2\u01f7\u01f8\3\2\2\2\u01f8\u01f9\bR" +
                    "\3\2\u01f9\u00a4\3\2\2\2\u01fa\u01fc\7\17\2\2\u01fb\u01fd\7\f\2\2\u01fc" +
                    "\u01fb\3\2\2\2\u01fc\u01fd\3\2\2\2\u01fd\u0200\3\2\2\2\u01fe\u0200\7\f" +
                    "\2\2\u01ff\u01fa\3\2\2\2\u01ff\u01fe\3\2\2\2\u0200\u0201\3\2\2\2\u0201" +
                    "\u0202\bS\2\2\u0202\u00a6\3\2\2\2\u0203\u0204\7\61\2\2\u0204\u0205\7," +
                    "\2\2\u0205\u0209\3\2\2\2\u0206\u0208\13\2\2\2\u0207\u0206\3\2\2\2\u0208" +
                    "\u020b\3\2\2\2\u0209\u020a\3\2\2\2\u0209\u0207\3\2\2\2\u020a\u020c\3\2" +
                    "\2\2\u020b\u0209\3\2\2\2\u020c\u020d\7,\2\2\u020d\u020e\7\61\2\2\u020e" +
                    "\u020f\3\2\2\2\u020f\u0210\bT\2\2\u0210\u00a8\3\2\2\2\u0211\u0212\7\61" +
                    "\2\2\u0212\u0213\7\61\2\2\u0213\u0217\3\2\2\2\u0214\u0216\n\r\2\2\u0215" +
                    "\u0214\3\2\2\2\u0216\u0219\3\2\2\2\u0217\u0215\3\2\2\2\u0217\u0218\3\2" +
                    "\2\2\u0218\u021a\3\2\2\2\u0219\u0217\3\2\2\2\u021a\u021b\bU\2\2\u021b" +
                    "\u00aa\3\2\2\2\36\2\u0158\u015a\u016e\u0172\u0176\u0178\u0184\u018a\u0191" +
                    "\u01a1\u01aa\u01ae\u01b3\u01c3\u01cb\u01cf\u01d6\u01da\u01df\u01e3\u01e9" +
                    "\u01ef\u01f6\u01fc\u01ff\u0209\u0217\4\2\3\2\b\2\2";
    public static final ATN _ATN =
            new ATNDeserializer().deserialize(_serializedATN.toCharArray());
    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache =
            new PredictionContextCache();
    private static final String[] _LITERAL_NAMES = {
            null, "'+'", "'-'", "'*'", "'/'", "'&'", "'^'", "'|'", "'<<'", "'>>'",
            "'||'", "'&&'", "'=='", "'!='", "'>'", "'<'", "'>='", "'<='", "'%'", "'++'",
            "'--'", "'true'", "'false'", "'null'", "'!'", "'~'", "'int'", "'string'",
            "'bool'", "'void'", "'class'", "','", "'='", "'('", "')'", "'{'", "'}'",
            "'['", "']'", "'new'", "'...'", "'break'", "'continue'", "'return'", "'for'",
            "'while'", "'do'", "'if'", "'else'", "':'", "'?'", "'.'", null, "';'"
    };
    private static final String[] _SYMBOLIC_NAMES = {
            null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, "Comma", "Equal", "Lbracket",
            "Rbracket", "LBigbracket", "RBigbracket", "LMidbracket", "RMidbracket",
            "New", "Balabalabala", "BREAK", "CONTINUE", "RETURN", "FOR", "WHILE",
            "DO", "IF", "ELSE", "COLON", "QUESTION", "Dot", "Identifier", "SEMI",
            "DecimalConstant", "Numbertail", "OctalConstant", "HexadecimalConstant",
            "Sign", "CharacterConstant", "StringLiteral", "Preprocessing", "Whitespace",
            "Newline", "BlockComment", "LineComment"
    };
    public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);
    public static String[] modeNames = {
            "DEFAULT_MODE"
    };

    static {
        RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION);
    }

    static {
        tokenNames = new String[_SYMBOLIC_NAMES.length];
        for (int i = 0; i < tokenNames.length; i++) {
            tokenNames[i] = VOCABULARY.getLiteralName(i);
            if (tokenNames[i] == null) {
                tokenNames[i] = VOCABULARY.getSymbolicName(i);
            }

            if (tokenNames[i] == null) {
                tokenNames[i] = "<INVALID>";
            }
        }
    }

    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }

    public MLexer(CharStream input) {
        super(input);
        _interp = new LexerATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    @Override
    @Deprecated
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override

    public Vocabulary getVocabulary() {
        return VOCABULARY;
    }

    @Override
    public String getGrammarFileName() {
        return "M.g4";
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public String[] getModeNames() {
        return modeNames;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }
}