// Generated from C:/Users/���/Documents/GitHub/compiler2016/src/Parser\M.g4 by ANTLR 4.5.1
package Parser;

package Parser;
        import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 *            operations with no return type.
 */
public interface MVisitor<T> extends ParseTreeVisitor<T> {
    /**
     * Visit a parse tree produced by {@link MParser#program}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitProgram(MParser.ProgramContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#declaration}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDeclaration(MParser.DeclarationContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#init_declarators}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitInit_declarators(MParser.Init_declaratorsContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#declarators}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDeclarators(MParser.DeclaratorsContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#init_declarator}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitInit_declarator(MParser.Init_declaratorContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#type}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitType(MParser.TypeContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#array_decl}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArray_decl(MParser.Array_declContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#array_new}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArray_new(MParser.Array_newContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#type_non_array}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitType_non_array(MParser.Type_non_arrayContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#class_declaration}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClass_declaration(MParser.Class_declarationContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#declarator}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDeclarator(MParser.DeclaratorContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#initializer}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitInitializer(MParser.InitializerContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#function_definition}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFunction_definition(MParser.Function_definitionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#parameters}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitParameters(MParser.ParametersContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#parameter}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitParameter(MParser.ParameterContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStatement(MParser.StatementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#assignment_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignment_statement(MParser.Assignment_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#structured_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStructured_statement(MParser.Structured_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#compond_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCompond_statement(MParser.Compond_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#loop_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLoop_statement(MParser.Loop_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#branch_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBranch_statement(MParser.Branch_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#for_loop_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFor_loop_statement(MParser.For_loop_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#while_loop_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitWhile_loop_statement(MParser.While_loop_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#do_while_loop_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDo_while_loop_statement(MParser.Do_while_loop_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#if_statement}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIf_statement(MParser.If_statementContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitExpression(MParser.ExpressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#assignment_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignment_expression(MParser.Assignment_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#equal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEqual(MParser.EqualContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#pequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPequal(MParser.PequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#miequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMiequal(MParser.MiequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#muequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMuequal(MParser.MuequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#diequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDiequal(MParser.DiequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#andequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAndequal(MParser.AndequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#xorequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitXorequal(MParser.XorequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#orequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitOrequal(MParser.OrequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#lsequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLsequal(MParser.LsequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#rsequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitRsequal(MParser.RsequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#assignment_operators}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAssignment_operators(MParser.Assignment_operatorsContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#calculation_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCalculation_expression(MParser.Calculation_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#logical_caclulation_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLogical_caclulation_expression(MParser.Logical_caclulation_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#logical_or_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLogical_or_expression(MParser.Logical_or_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#logical_and_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLogical_and_expression(MParser.Logical_and_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#bitwise_or_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitwise_or_expression(MParser.Bitwise_or_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#bitwise_xor_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitwise_xor_expression(MParser.Bitwise_xor_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#bitwise_and_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitwise_and_expression(MParser.Bitwise_and_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#isequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIsequal(MParser.IsequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#notequal}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNotequal(MParser.NotequalContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#equality_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitEquality_expression(MParser.Equality_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#bigger}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBigger(MParser.BiggerContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#smaller}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSmaller(MParser.SmallerContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#bigger_e}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBigger_e(MParser.Bigger_eContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#smaller_e}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSmaller_e(MParser.Smaller_eContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#relation_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitRelation_expression(MParser.Relation_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#lshift}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLshift(MParser.LshiftContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#rshift}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitRshift(MParser.RshiftContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#plus}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPlus(MParser.PlusContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#minus}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMinus(MParser.MinusContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#shift_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitShift_expression(MParser.Shift_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#add_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAdd_expression(MParser.Add_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#multiply}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMultiply(MParser.MultiplyContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#division}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDivision(MParser.DivisionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#mod}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMod(MParser.ModContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#mul_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMul_expression(MParser.Mul_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#cast_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCast_expression(MParser.Cast_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#plusplus}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPlusplus(MParser.PlusplusContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#minusminus}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMinusminus(MParser.MinusminusContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#unary_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUnary_expression(MParser.Unary_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#pre_defined_constants}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPre_defined_constants(MParser.Pre_defined_constantsContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#class_new}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClass_new(MParser.Class_newContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#new_operation}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNew_operation(MParser.New_operationContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#functionCall_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFunctionCall_expression(MParser.FunctionCall_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#arguements}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitArguements(MParser.ArguementsContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitValue(MParser.ValueContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#not_sign}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNot_sign(MParser.Not_signContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#getAddr}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitGetAddr(MParser.GetAddrContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#unary_operation}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUnary_operation(MParser.Unary_operationContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#bitwise_not}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBitwise_not(MParser.Bitwise_notContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#postfix_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPostfix_expression(MParser.Postfix_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#getMember}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitGetMember(MParser.GetMemberContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#postfix}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPostfix(MParser.PostfixContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#primary_expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPrimary_expression(MParser.Primary_expressionContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#type_name}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitType_name(MParser.Type_nameContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#constant}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitConstant(MParser.ConstantContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#numeric_constant}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNumeric_constant(MParser.Numeric_constantContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#integer_constant}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitInteger_constant(MParser.Integer_constantContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#string_constant}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitString_constant(MParser.String_constantContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#typenametokens}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTypenametokens(MParser.TypenametokensContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#number}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNumber(MParser.NumberContext ctx);

    /**
     * Visit a parse tree produced by {@link MParser#union_class}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUnion_class(MParser.Union_classContext ctx);
}