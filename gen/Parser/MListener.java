// Generated from C:/Users/���/Documents/GitHub/compiler2016/src/Parser\M.g4 by ANTLR 4.5.1
package Parser;

package Parser;
        import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MParser}.
 */
public interface MListener extends ParseTreeListener {
    /**
     * Enter a parse tree produced by {@link MParser#program}.
     *
     * @param ctx the parse tree
     */
    void enterProgram(MParser.ProgramContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#program}.
     *
     * @param ctx the parse tree
     */
    void exitProgram(MParser.ProgramContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#declaration}.
     *
     * @param ctx the parse tree
     */
    void enterDeclaration(MParser.DeclarationContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#declaration}.
     *
     * @param ctx the parse tree
     */
    void exitDeclaration(MParser.DeclarationContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#init_declarators}.
     *
     * @param ctx the parse tree
     */
    void enterInit_declarators(MParser.Init_declaratorsContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#init_declarators}.
     *
     * @param ctx the parse tree
     */
    void exitInit_declarators(MParser.Init_declaratorsContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#declarators}.
     *
     * @param ctx the parse tree
     */
    void enterDeclarators(MParser.DeclaratorsContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#declarators}.
     *
     * @param ctx the parse tree
     */
    void exitDeclarators(MParser.DeclaratorsContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#init_declarator}.
     *
     * @param ctx the parse tree
     */
    void enterInit_declarator(MParser.Init_declaratorContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#init_declarator}.
     *
     * @param ctx the parse tree
     */
    void exitInit_declarator(MParser.Init_declaratorContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#type}.
     *
     * @param ctx the parse tree
     */
    void enterType(MParser.TypeContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#type}.
     *
     * @param ctx the parse tree
     */
    void exitType(MParser.TypeContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#array_decl}.
     *
     * @param ctx the parse tree
     */
    void enterArray_decl(MParser.Array_declContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#array_decl}.
     *
     * @param ctx the parse tree
     */
    void exitArray_decl(MParser.Array_declContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#array_new}.
     *
     * @param ctx the parse tree
     */
    void enterArray_new(MParser.Array_newContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#array_new}.
     *
     * @param ctx the parse tree
     */
    void exitArray_new(MParser.Array_newContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#type_non_array}.
     *
     * @param ctx the parse tree
     */
    void enterType_non_array(MParser.Type_non_arrayContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#type_non_array}.
     *
     * @param ctx the parse tree
     */
    void exitType_non_array(MParser.Type_non_arrayContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#class_declaration}.
     *
     * @param ctx the parse tree
     */
    void enterClass_declaration(MParser.Class_declarationContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#class_declaration}.
     *
     * @param ctx the parse tree
     */
    void exitClass_declaration(MParser.Class_declarationContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#declarator}.
     *
     * @param ctx the parse tree
     */
    void enterDeclarator(MParser.DeclaratorContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#declarator}.
     *
     * @param ctx the parse tree
     */
    void exitDeclarator(MParser.DeclaratorContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#initializer}.
     *
     * @param ctx the parse tree
     */
    void enterInitializer(MParser.InitializerContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#initializer}.
     *
     * @param ctx the parse tree
     */
    void exitInitializer(MParser.InitializerContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#function_definition}.
     *
     * @param ctx the parse tree
     */
    void enterFunction_definition(MParser.Function_definitionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#function_definition}.
     *
     * @param ctx the parse tree
     */
    void exitFunction_definition(MParser.Function_definitionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#parameters}.
     *
     * @param ctx the parse tree
     */
    void enterParameters(MParser.ParametersContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#parameters}.
     *
     * @param ctx the parse tree
     */
    void exitParameters(MParser.ParametersContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#parameter}.
     *
     * @param ctx the parse tree
     */
    void enterParameter(MParser.ParameterContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#parameter}.
     *
     * @param ctx the parse tree
     */
    void exitParameter(MParser.ParameterContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#statement}.
     *
     * @param ctx the parse tree
     */
    void enterStatement(MParser.StatementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#statement}.
     *
     * @param ctx the parse tree
     */
    void exitStatement(MParser.StatementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#assignment_statement}.
     *
     * @param ctx the parse tree
     */
    void enterAssignment_statement(MParser.Assignment_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#assignment_statement}.
     *
     * @param ctx the parse tree
     */
    void exitAssignment_statement(MParser.Assignment_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#structured_statement}.
     *
     * @param ctx the parse tree
     */
    void enterStructured_statement(MParser.Structured_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#structured_statement}.
     *
     * @param ctx the parse tree
     */
    void exitStructured_statement(MParser.Structured_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#compond_statement}.
     *
     * @param ctx the parse tree
     */
    void enterCompond_statement(MParser.Compond_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#compond_statement}.
     *
     * @param ctx the parse tree
     */
    void exitCompond_statement(MParser.Compond_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#loop_statement}.
     *
     * @param ctx the parse tree
     */
    void enterLoop_statement(MParser.Loop_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#loop_statement}.
     *
     * @param ctx the parse tree
     */
    void exitLoop_statement(MParser.Loop_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#branch_statement}.
     *
     * @param ctx the parse tree
     */
    void enterBranch_statement(MParser.Branch_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#branch_statement}.
     *
     * @param ctx the parse tree
     */
    void exitBranch_statement(MParser.Branch_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#for_loop_statement}.
     *
     * @param ctx the parse tree
     */
    void enterFor_loop_statement(MParser.For_loop_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#for_loop_statement}.
     *
     * @param ctx the parse tree
     */
    void exitFor_loop_statement(MParser.For_loop_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#while_loop_statement}.
     *
     * @param ctx the parse tree
     */
    void enterWhile_loop_statement(MParser.While_loop_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#while_loop_statement}.
     *
     * @param ctx the parse tree
     */
    void exitWhile_loop_statement(MParser.While_loop_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#do_while_loop_statement}.
     *
     * @param ctx the parse tree
     */
    void enterDo_while_loop_statement(MParser.Do_while_loop_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#do_while_loop_statement}.
     *
     * @param ctx the parse tree
     */
    void exitDo_while_loop_statement(MParser.Do_while_loop_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#if_statement}.
     *
     * @param ctx the parse tree
     */
    void enterIf_statement(MParser.If_statementContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#if_statement}.
     *
     * @param ctx the parse tree
     */
    void exitIf_statement(MParser.If_statementContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#expression}.
     *
     * @param ctx the parse tree
     */
    void enterExpression(MParser.ExpressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#expression}.
     *
     * @param ctx the parse tree
     */
    void exitExpression(MParser.ExpressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#assignment_expression}.
     *
     * @param ctx the parse tree
     */
    void enterAssignment_expression(MParser.Assignment_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#assignment_expression}.
     *
     * @param ctx the parse tree
     */
    void exitAssignment_expression(MParser.Assignment_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#equal}.
     *
     * @param ctx the parse tree
     */
    void enterEqual(MParser.EqualContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#equal}.
     *
     * @param ctx the parse tree
     */
    void exitEqual(MParser.EqualContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#pequal}.
     *
     * @param ctx the parse tree
     */
    void enterPequal(MParser.PequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#pequal}.
     *
     * @param ctx the parse tree
     */
    void exitPequal(MParser.PequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#miequal}.
     *
     * @param ctx the parse tree
     */
    void enterMiequal(MParser.MiequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#miequal}.
     *
     * @param ctx the parse tree
     */
    void exitMiequal(MParser.MiequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#muequal}.
     *
     * @param ctx the parse tree
     */
    void enterMuequal(MParser.MuequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#muequal}.
     *
     * @param ctx the parse tree
     */
    void exitMuequal(MParser.MuequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#diequal}.
     *
     * @param ctx the parse tree
     */
    void enterDiequal(MParser.DiequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#diequal}.
     *
     * @param ctx the parse tree
     */
    void exitDiequal(MParser.DiequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#andequal}.
     *
     * @param ctx the parse tree
     */
    void enterAndequal(MParser.AndequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#andequal}.
     *
     * @param ctx the parse tree
     */
    void exitAndequal(MParser.AndequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#xorequal}.
     *
     * @param ctx the parse tree
     */
    void enterXorequal(MParser.XorequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#xorequal}.
     *
     * @param ctx the parse tree
     */
    void exitXorequal(MParser.XorequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#orequal}.
     *
     * @param ctx the parse tree
     */
    void enterOrequal(MParser.OrequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#orequal}.
     *
     * @param ctx the parse tree
     */
    void exitOrequal(MParser.OrequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#lsequal}.
     *
     * @param ctx the parse tree
     */
    void enterLsequal(MParser.LsequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#lsequal}.
     *
     * @param ctx the parse tree
     */
    void exitLsequal(MParser.LsequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#rsequal}.
     *
     * @param ctx the parse tree
     */
    void enterRsequal(MParser.RsequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#rsequal}.
     *
     * @param ctx the parse tree
     */
    void exitRsequal(MParser.RsequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#assignment_operators}.
     *
     * @param ctx the parse tree
     */
    void enterAssignment_operators(MParser.Assignment_operatorsContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#assignment_operators}.
     *
     * @param ctx the parse tree
     */
    void exitAssignment_operators(MParser.Assignment_operatorsContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#calculation_expression}.
     *
     * @param ctx the parse tree
     */
    void enterCalculation_expression(MParser.Calculation_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#calculation_expression}.
     *
     * @param ctx the parse tree
     */
    void exitCalculation_expression(MParser.Calculation_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#logical_caclulation_expression}.
     *
     * @param ctx the parse tree
     */
    void enterLogical_caclulation_expression(MParser.Logical_caclulation_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#logical_caclulation_expression}.
     *
     * @param ctx the parse tree
     */
    void exitLogical_caclulation_expression(MParser.Logical_caclulation_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#logical_or_expression}.
     *
     * @param ctx the parse tree
     */
    void enterLogical_or_expression(MParser.Logical_or_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#logical_or_expression}.
     *
     * @param ctx the parse tree
     */
    void exitLogical_or_expression(MParser.Logical_or_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#logical_and_expression}.
     *
     * @param ctx the parse tree
     */
    void enterLogical_and_expression(MParser.Logical_and_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#logical_and_expression}.
     *
     * @param ctx the parse tree
     */
    void exitLogical_and_expression(MParser.Logical_and_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#bitwise_or_expression}.
     *
     * @param ctx the parse tree
     */
    void enterBitwise_or_expression(MParser.Bitwise_or_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#bitwise_or_expression}.
     *
     * @param ctx the parse tree
     */
    void exitBitwise_or_expression(MParser.Bitwise_or_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#bitwise_xor_expression}.
     *
     * @param ctx the parse tree
     */
    void enterBitwise_xor_expression(MParser.Bitwise_xor_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#bitwise_xor_expression}.
     *
     * @param ctx the parse tree
     */
    void exitBitwise_xor_expression(MParser.Bitwise_xor_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#bitwise_and_expression}.
     *
     * @param ctx the parse tree
     */
    void enterBitwise_and_expression(MParser.Bitwise_and_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#bitwise_and_expression}.
     *
     * @param ctx the parse tree
     */
    void exitBitwise_and_expression(MParser.Bitwise_and_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#isequal}.
     *
     * @param ctx the parse tree
     */
    void enterIsequal(MParser.IsequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#isequal}.
     *
     * @param ctx the parse tree
     */
    void exitIsequal(MParser.IsequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#notequal}.
     *
     * @param ctx the parse tree
     */
    void enterNotequal(MParser.NotequalContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#notequal}.
     *
     * @param ctx the parse tree
     */
    void exitNotequal(MParser.NotequalContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#equality_expression}.
     *
     * @param ctx the parse tree
     */
    void enterEquality_expression(MParser.Equality_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#equality_expression}.
     *
     * @param ctx the parse tree
     */
    void exitEquality_expression(MParser.Equality_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#bigger}.
     *
     * @param ctx the parse tree
     */
    void enterBigger(MParser.BiggerContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#bigger}.
     *
     * @param ctx the parse tree
     */
    void exitBigger(MParser.BiggerContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#smaller}.
     *
     * @param ctx the parse tree
     */
    void enterSmaller(MParser.SmallerContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#smaller}.
     *
     * @param ctx the parse tree
     */
    void exitSmaller(MParser.SmallerContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#bigger_e}.
     *
     * @param ctx the parse tree
     */
    void enterBigger_e(MParser.Bigger_eContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#bigger_e}.
     *
     * @param ctx the parse tree
     */
    void exitBigger_e(MParser.Bigger_eContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#smaller_e}.
     *
     * @param ctx the parse tree
     */
    void enterSmaller_e(MParser.Smaller_eContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#smaller_e}.
     *
     * @param ctx the parse tree
     */
    void exitSmaller_e(MParser.Smaller_eContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#relation_expression}.
     *
     * @param ctx the parse tree
     */
    void enterRelation_expression(MParser.Relation_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#relation_expression}.
     *
     * @param ctx the parse tree
     */
    void exitRelation_expression(MParser.Relation_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#lshift}.
     *
     * @param ctx the parse tree
     */
    void enterLshift(MParser.LshiftContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#lshift}.
     *
     * @param ctx the parse tree
     */
    void exitLshift(MParser.LshiftContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#rshift}.
     *
     * @param ctx the parse tree
     */
    void enterRshift(MParser.RshiftContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#rshift}.
     *
     * @param ctx the parse tree
     */
    void exitRshift(MParser.RshiftContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#plus}.
     *
     * @param ctx the parse tree
     */
    void enterPlus(MParser.PlusContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#plus}.
     *
     * @param ctx the parse tree
     */
    void exitPlus(MParser.PlusContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#minus}.
     *
     * @param ctx the parse tree
     */
    void enterMinus(MParser.MinusContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#minus}.
     *
     * @param ctx the parse tree
     */
    void exitMinus(MParser.MinusContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#shift_expression}.
     *
     * @param ctx the parse tree
     */
    void enterShift_expression(MParser.Shift_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#shift_expression}.
     *
     * @param ctx the parse tree
     */
    void exitShift_expression(MParser.Shift_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#add_expression}.
     *
     * @param ctx the parse tree
     */
    void enterAdd_expression(MParser.Add_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#add_expression}.
     *
     * @param ctx the parse tree
     */
    void exitAdd_expression(MParser.Add_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#multiply}.
     *
     * @param ctx the parse tree
     */
    void enterMultiply(MParser.MultiplyContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#multiply}.
     *
     * @param ctx the parse tree
     */
    void exitMultiply(MParser.MultiplyContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#division}.
     *
     * @param ctx the parse tree
     */
    void enterDivision(MParser.DivisionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#division}.
     *
     * @param ctx the parse tree
     */
    void exitDivision(MParser.DivisionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#mod}.
     *
     * @param ctx the parse tree
     */
    void enterMod(MParser.ModContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#mod}.
     *
     * @param ctx the parse tree
     */
    void exitMod(MParser.ModContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#mul_expression}.
     *
     * @param ctx the parse tree
     */
    void enterMul_expression(MParser.Mul_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#mul_expression}.
     *
     * @param ctx the parse tree
     */
    void exitMul_expression(MParser.Mul_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#cast_expression}.
     *
     * @param ctx the parse tree
     */
    void enterCast_expression(MParser.Cast_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#cast_expression}.
     *
     * @param ctx the parse tree
     */
    void exitCast_expression(MParser.Cast_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#plusplus}.
     *
     * @param ctx the parse tree
     */
    void enterPlusplus(MParser.PlusplusContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#plusplus}.
     *
     * @param ctx the parse tree
     */
    void exitPlusplus(MParser.PlusplusContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#minusminus}.
     *
     * @param ctx the parse tree
     */
    void enterMinusminus(MParser.MinusminusContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#minusminus}.
     *
     * @param ctx the parse tree
     */
    void exitMinusminus(MParser.MinusminusContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#unary_expression}.
     *
     * @param ctx the parse tree
     */
    void enterUnary_expression(MParser.Unary_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#unary_expression}.
     *
     * @param ctx the parse tree
     */
    void exitUnary_expression(MParser.Unary_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#pre_defined_constants}.
     *
     * @param ctx the parse tree
     */
    void enterPre_defined_constants(MParser.Pre_defined_constantsContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#pre_defined_constants}.
     *
     * @param ctx the parse tree
     */
    void exitPre_defined_constants(MParser.Pre_defined_constantsContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#class_new}.
     *
     * @param ctx the parse tree
     */
    void enterClass_new(MParser.Class_newContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#class_new}.
     *
     * @param ctx the parse tree
     */
    void exitClass_new(MParser.Class_newContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#new_operation}.
     *
     * @param ctx the parse tree
     */
    void enterNew_operation(MParser.New_operationContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#new_operation}.
     *
     * @param ctx the parse tree
     */
    void exitNew_operation(MParser.New_operationContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#functionCall_expression}.
     *
     * @param ctx the parse tree
     */
    void enterFunctionCall_expression(MParser.FunctionCall_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#functionCall_expression}.
     *
     * @param ctx the parse tree
     */
    void exitFunctionCall_expression(MParser.FunctionCall_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#arguements}.
     *
     * @param ctx the parse tree
     */
    void enterArguements(MParser.ArguementsContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#arguements}.
     *
     * @param ctx the parse tree
     */
    void exitArguements(MParser.ArguementsContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#value}.
     *
     * @param ctx the parse tree
     */
    void enterValue(MParser.ValueContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#value}.
     *
     * @param ctx the parse tree
     */
    void exitValue(MParser.ValueContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#not_sign}.
     *
     * @param ctx the parse tree
     */
    void enterNot_sign(MParser.Not_signContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#not_sign}.
     *
     * @param ctx the parse tree
     */
    void exitNot_sign(MParser.Not_signContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#getAddr}.
     *
     * @param ctx the parse tree
     */
    void enterGetAddr(MParser.GetAddrContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#getAddr}.
     *
     * @param ctx the parse tree
     */
    void exitGetAddr(MParser.GetAddrContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#unary_operation}.
     *
     * @param ctx the parse tree
     */
    void enterUnary_operation(MParser.Unary_operationContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#unary_operation}.
     *
     * @param ctx the parse tree
     */
    void exitUnary_operation(MParser.Unary_operationContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#bitwise_not}.
     *
     * @param ctx the parse tree
     */
    void enterBitwise_not(MParser.Bitwise_notContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#bitwise_not}.
     *
     * @param ctx the parse tree
     */
    void exitBitwise_not(MParser.Bitwise_notContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#postfix_expression}.
     *
     * @param ctx the parse tree
     */
    void enterPostfix_expression(MParser.Postfix_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#postfix_expression}.
     *
     * @param ctx the parse tree
     */
    void exitPostfix_expression(MParser.Postfix_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#getMember}.
     *
     * @param ctx the parse tree
     */
    void enterGetMember(MParser.GetMemberContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#getMember}.
     *
     * @param ctx the parse tree
     */
    void exitGetMember(MParser.GetMemberContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#postfix}.
     *
     * @param ctx the parse tree
     */
    void enterPostfix(MParser.PostfixContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#postfix}.
     *
     * @param ctx the parse tree
     */
    void exitPostfix(MParser.PostfixContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#primary_expression}.
     *
     * @param ctx the parse tree
     */
    void enterPrimary_expression(MParser.Primary_expressionContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#primary_expression}.
     *
     * @param ctx the parse tree
     */
    void exitPrimary_expression(MParser.Primary_expressionContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#type_name}.
     *
     * @param ctx the parse tree
     */
    void enterType_name(MParser.Type_nameContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#type_name}.
     *
     * @param ctx the parse tree
     */
    void exitType_name(MParser.Type_nameContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#constant}.
     *
     * @param ctx the parse tree
     */
    void enterConstant(MParser.ConstantContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#constant}.
     *
     * @param ctx the parse tree
     */
    void exitConstant(MParser.ConstantContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#numeric_constant}.
     *
     * @param ctx the parse tree
     */
    void enterNumeric_constant(MParser.Numeric_constantContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#numeric_constant}.
     *
     * @param ctx the parse tree
     */
    void exitNumeric_constant(MParser.Numeric_constantContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#integer_constant}.
     *
     * @param ctx the parse tree
     */
    void enterInteger_constant(MParser.Integer_constantContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#integer_constant}.
     *
     * @param ctx the parse tree
     */
    void exitInteger_constant(MParser.Integer_constantContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#string_constant}.
     *
     * @param ctx the parse tree
     */
    void enterString_constant(MParser.String_constantContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#string_constant}.
     *
     * @param ctx the parse tree
     */
    void exitString_constant(MParser.String_constantContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#typenametokens}.
     *
     * @param ctx the parse tree
     */
    void enterTypenametokens(MParser.TypenametokensContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#typenametokens}.
     *
     * @param ctx the parse tree
     */
    void exitTypenametokens(MParser.TypenametokensContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#number}.
     *
     * @param ctx the parse tree
     */
    void enterNumber(MParser.NumberContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#number}.
     *
     * @param ctx the parse tree
     */
    void exitNumber(MParser.NumberContext ctx);

    /**
     * Enter a parse tree produced by {@link MParser#union_class}.
     *
     * @param ctx the parse tree
     */
    void enterUnion_class(MParser.Union_classContext ctx);

    /**
     * Exit a parse tree produced by {@link MParser#union_class}.
     *
     * @param ctx the parse tree
     */
    void exitUnion_class(MParser.Union_classContext ctx);
}