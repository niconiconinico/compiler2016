// Generated from C:/Users/���/Documents/GitHub/compiler2016/src/Parser\M.g4 by ANTLR 4.5.1
package Parser;

package Parser;
        import org.antlr.v4.runtime.atn.*;
        import org.antlr.v4.runtime.dfa.DFA;
        import org.antlr.v4.runtime.*;
        import org.antlr.v4.runtime.misc.*;
        import org.antlr.v4.runtime.tree.*;
        import java.util.List;
        import java.util.Iterator;
        import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MParser extends Parser {
    public static final int
            T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, T__7 = 8, T__8 = 9,
            T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14, T__14 = 15, T__15 = 16, T__16 = 17,
            T__17 = 18, T__18 = 19, T__19 = 20, T__20 = 21, T__21 = 22, T__22 = 23, T__23 = 24,
            T__24 = 25, T__25 = 26, T__26 = 27, T__27 = 28, T__28 = 29, T__29 = 30, Comma = 31,
            Equal = 32, Lbracket = 33, Rbracket = 34, LBigbracket = 35, RBigbracket = 36, LMidbracket = 37,
            RMidbracket = 38, New = 39, Balabalabala = 40, BREAK = 41, CONTINUE = 42, RETURN = 43,
            FOR = 44, WHILE = 45, DO = 46, IF = 47, ELSE = 48, COLON = 49, QUESTION = 50, Dot = 51,
            Identifier = 52, SEMI = 53, DecimalConstant = 54, Numbertail = 55, OctalConstant = 56,
            HexadecimalConstant = 57, Sign = 58, CharacterConstant = 59, StringLiteral = 60,
            Preprocessing = 61, Whitespace = 62, Newline = 63, BlockComment = 64, LineComment = 65;
    public static final int
            RULE_program = 0, RULE_declaration = 1, RULE_init_declarators = 2, RULE_declarators = 3,
            RULE_init_declarator = 4, RULE_type = 5, RULE_array_decl = 6, RULE_array_new = 7,
            RULE_type_non_array = 8, RULE_class_declaration = 9, RULE_declarator = 10,
            RULE_initializer = 11, RULE_function_definition = 12, RULE_parameters = 13,
            RULE_parameter = 14, RULE_statement = 15, RULE_assignment_statement = 16,
            RULE_structured_statement = 17, RULE_compond_statement = 18, RULE_loop_statement = 19,
            RULE_branch_statement = 20, RULE_for_loop_statement = 21, RULE_while_loop_statement = 22,
            RULE_do_while_loop_statement = 23, RULE_if_statement = 24, RULE_expression = 25,
            RULE_assignment_expression = 26, RULE_equal = 27, RULE_pequal = 28, RULE_miequal = 29,
            RULE_muequal = 30, RULE_diequal = 31, RULE_andequal = 32, RULE_xorequal = 33,
            RULE_orequal = 34, RULE_lsequal = 35, RULE_rsequal = 36, RULE_assignment_operators = 37,
            RULE_calculation_expression = 38, RULE_logical_caclulation_expression = 39,
            RULE_logical_or_expression = 40, RULE_logical_and_expression = 41, RULE_bitwise_or_expression = 42,
            RULE_bitwise_xor_expression = 43, RULE_bitwise_and_expression = 44, RULE_isequal = 45,
            RULE_notequal = 46, RULE_equality_expression = 47, RULE_bigger = 48, RULE_smaller = 49,
            RULE_bigger_e = 50, RULE_smaller_e = 51, RULE_relation_expression = 52,
            RULE_lshift = 53, RULE_rshift = 54, RULE_plus = 55, RULE_minus = 56, RULE_shift_expression = 57,
            RULE_add_expression = 58, RULE_multiply = 59, RULE_division = 60, RULE_mod = 61,
            RULE_mul_expression = 62, RULE_cast_expression = 63, RULE_plusplus = 64,
            RULE_minusminus = 65, RULE_unary_expression = 66, RULE_pre_defined_constants = 67,
            RULE_class_new = 68, RULE_new_operation = 69, RULE_functionCall_expression = 70,
            RULE_arguements = 71, RULE_value = 72, RULE_not_sign = 73, RULE_getAddr = 74,
            RULE_unary_operation = 75, RULE_bitwise_not = 76, RULE_postfix_expression = 77,
            RULE_getMember = 78, RULE_postfix = 79, RULE_primary_expression = 80,
            RULE_type_name = 81, RULE_constant = 82, RULE_numeric_constant = 83, RULE_integer_constant = 84,
            RULE_string_constant = 85, RULE_typenametokens = 86, RULE_number = 87,
            RULE_union_class = 88;
    public static final String[] ruleNames = {
            "program", "declaration", "init_declarators", "declarators", "init_declarator",
            "type", "array_decl", "array_new", "type_non_array", "class_declaration",
            "declarator", "initializer", "function_definition", "parameters", "parameter",
            "statement", "assignment_statement", "structured_statement", "compond_statement",
            "loop_statement", "branch_statement", "for_loop_statement", "while_loop_statement",
            "do_while_loop_statement", "if_statement", "expression", "assignment_expression",
            "equal", "pequal", "miequal", "muequal", "diequal", "andequal", "xorequal",
            "orequal", "lsequal", "rsequal", "assignment_operators", "calculation_expression",
            "logical_caclulation_expression", "logical_or_expression", "logical_and_expression",
            "bitwise_or_expression", "bitwise_xor_expression", "bitwise_and_expression",
            "isequal", "notequal", "equality_expression", "bigger", "smaller", "bigger_e",
            "smaller_e", "relation_expression", "lshift", "rshift", "plus", "minus",
            "shift_expression", "add_expression", "multiply", "division", "mod", "mul_expression",
            "cast_expression", "plusplus", "minusminus", "unary_expression", "pre_defined_constants",
            "class_new", "new_operation", "functionCall_expression", "arguements",
            "value", "not_sign", "getAddr", "unary_operation", "bitwise_not", "postfix_expression",
            "getMember", "postfix", "primary_expression", "type_name", "constant",
            "numeric_constant", "integer_constant", "string_constant", "typenametokens",
            "number", "union_class"
    };
    /**
     * @deprecated Use {@link #VOCABULARY} instead.
     */
    @Deprecated
    public static final String[] tokenNames;
    public static final String _serializedATN =
            "\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3C\u02d9\4\2\t\2\4" +
                    "\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t" +
                    "\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22" +
                    "\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31" +
                    "\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!" +
                    "\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4" +
                    ",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t" +
                    "\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t<\4=\t=" +
                    "\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\4E\tE\4F\tF\4G\tG\4H\tH\4I" +
                    "\tI\4J\tJ\4K\tK\4L\tL\4M\tM\4N\tN\4O\tO\4P\tP\4Q\tQ\4R\tR\4S\tS\4T\tT" +
                    "\4U\tU\4V\tV\4W\tW\4X\tX\4Y\tY\4Z\tZ\3\2\3\2\6\2\u00b7\n\2\r\2\16\2\u00b8" +
                    "\3\3\3\3\3\3\3\3\3\3\5\3\u00c0\n\3\3\4\3\4\3\4\7\4\u00c5\n\4\f\4\16\4" +
                    "\u00c8\13\4\3\5\3\5\3\5\7\5\u00cd\n\5\f\5\16\5\u00d0\13\5\3\6\3\6\3\6" +
                    "\3\6\3\6\5\6\u00d7\n\6\3\7\3\7\5\7\u00db\n\7\3\b\3\b\3\b\6\b\u00e0\n\b" +
                    "\r\b\16\b\u00e1\3\t\3\t\3\t\3\t\3\t\6\t\u00e9\n\t\r\t\16\t\u00ea\3\t\3" +
                    "\t\7\t\u00ef\n\t\f\t\16\t\u00f2\13\t\3\n\3\n\5\n\u00f6\n\n\3\13\3\13\5" +
                    "\13\u00fa\n\13\3\13\3\13\3\13\3\13\3\13\7\13\u0101\n\13\f\13\16\13\u0104" +
                    "\13\13\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\16\5\16\u0110\n\16\3" +
                    "\16\3\16\3\16\3\16\7\16\u0116\n\16\f\16\16\16\u0119\13\16\3\16\3\16\3" +
                    "\16\3\16\3\16\3\16\5\16\u0121\n\16\3\16\3\16\3\16\5\16\u0126\n\16\3\17" +
                    "\3\17\3\17\7\17\u012b\n\17\f\17\16\17\u012e\13\17\3\17\3\17\5\17\u0132" +
                    "\n\17\3\20\3\20\3\20\3\21\3\21\3\21\5\21\u013a\n\21\3\22\5\22\u013d\n" +
                    "\22\3\22\3\22\3\23\3\23\5\23\u0143\n\23\3\24\3\24\3\24\3\24\7\24\u0149" +
                    "\n\24\f\24\16\24\u014c\13\24\3\24\3\24\3\25\3\25\3\25\5\25\u0153\n\25" +
                    "\3\26\3\26\3\26\3\26\3\26\3\26\3\26\5\26\u015c\n\26\3\26\5\26\u015f\n" +
                    "\26\3\27\3\27\3\27\5\27\u0164\n\27\3\27\3\27\3\27\5\27\u0169\n\27\3\27" +
                    "\3\27\3\27\5\27\u016e\n\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30\3\30\3\30" +
                    "\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32" +
                    "\3\32\5\32\u0188\n\32\3\33\3\33\3\33\7\33\u018d\n\33\f\33\16\33\u0190" +
                    "\13\33\3\34\3\34\3\34\7\34\u0195\n\34\f\34\16\34\u0198\13\34\3\34\3\34" +
                    "\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3\37\3 \3 \3 \3!\3!\3!\3\"\3\"\3\"" +
                    "\3#\3#\3#\3$\3$\3$\3%\3%\3%\3&\3&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3" +
                    "\'\3\'\5\'\u01c3\n\'\3(\3(\3(\3(\3(\3(\7(\u01cb\n(\f(\16(\u01ce\13(\3" +
                    ")\3)\3*\3*\3*\7*\u01d5\n*\f*\16*\u01d8\13*\3+\3+\3+\7+\u01dd\n+\f+\16" +
                    "+\u01e0\13+\3,\3,\3,\7,\u01e5\n,\f,\16,\u01e8\13,\3-\3-\3-\7-\u01ed\n" +
                    "-\f-\16-\u01f0\13-\3.\3.\3.\7.\u01f5\n.\f.\16.\u01f8\13.\3/\3/\3\60\3" +
                    "\60\3\61\3\61\3\61\5\61\u0201\n\61\3\61\3\61\7\61\u0205\n\61\f\61\16\61" +
                    "\u0208\13\61\3\62\3\62\3\63\3\63\3\64\3\64\3\65\3\65\3\66\3\66\3\66\3" +
                    "\66\3\66\5\66\u0217\n\66\3\66\3\66\7\66\u021b\n\66\f\66\16\66\u021e\13" +
                    "\66\3\67\3\67\38\38\39\39\3:\3:\3;\3;\3;\5;\u022b\n;\3;\3;\7;\u022f\n" +
                    ";\f;\16;\u0232\13;\3<\3<\3<\5<\u0237\n<\3<\3<\7<\u023b\n<\f<\16<\u023e" +
                    "\13<\3=\3=\3>\3>\3?\3?\3@\3@\3@\3@\5@\u024a\n@\3@\3@\7@\u024e\n@\f@\16" +
                    "@\u0251\13@\3A\3A\3A\3A\3A\3A\5A\u0259\nA\3B\3B\3C\3C\3D\3D\3D\3D\3D\3" +
                    "D\3D\3D\3D\3D\3D\3D\3D\3D\5D\u026d\nD\3E\3E\3F\3F\3G\3G\3G\5G\u0276\n" +
                    "G\3H\3H\3H\5H\u027b\nH\3H\3H\3I\3I\3I\7I\u0282\nI\fI\16I\u0285\13I\3J" +
                    "\3J\3J\3J\3J\3J\5J\u028d\nJ\3K\3K\3L\3L\3M\3M\3M\3M\3M\3M\3M\3M\3M\3M" +
                    "\5M\u029d\nM\3N\3N\3O\3O\7O\u02a3\nO\fO\16O\u02a6\13O\3P\3P\3Q\3Q\3Q\3" +
                    "Q\3Q\3Q\3Q\5Q\u02b1\nQ\3Q\3Q\5Q\u02b5\nQ\3R\3R\3R\3R\3R\3R\3R\5R\u02be" +
                    "\nR\3S\3S\5S\u02c2\nS\3T\3T\5T\u02c6\nT\3U\3U\3V\3V\3W\3W\3X\3X\5X\u02d0" +
                    "\nX\3Y\5Y\u02d3\nY\3Y\3Y\3Z\3Z\3Z\2\2[\2\4\6\b\n\f\16\20\22\24\26\30\32" +
                    "\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNPRTVXZ\\^`bdfhjlnprtvxz|~\u0080" +
                    "\u0082\u0084\u0086\u0088\u008a\u008c\u008e\u0090\u0092\u0094\u0096\u0098" +
                    "\u009a\u009c\u009e\u00a0\u00a2\u00a4\u00a6\u00a8\u00aa\u00ac\u00ae\u00b0" +
                    "\u00b2\2\4\3\2\27\31\3\2\34\37\u02e1\2\u00b6\3\2\2\2\4\u00bf\3\2\2\2\6" +
                    "\u00c1\3\2\2\2\b\u00c9\3\2\2\2\n\u00d6\3\2\2\2\f\u00da\3\2\2\2\16\u00dc" +
                    "\3\2\2\2\20\u00e3\3\2\2\2\22\u00f5\3\2\2\2\24\u00f7\3\2\2\2\26\u0107\3" +
                    "\2\2\2\30\u0109\3\2\2\2\32\u0125\3\2\2\2\34\u0127\3\2\2\2\36\u0133\3\2" +
                    "\2\2 \u0139\3\2\2\2\"\u013c\3\2\2\2$\u0142\3\2\2\2&\u0144\3\2\2\2(\u0152" +
                    "\3\2\2\2*\u015e\3\2\2\2,\u0160\3\2\2\2.\u0172\3\2\2\2\60\u0178\3\2\2\2" +
                    "\62\u0180\3\2\2\2\64\u0189\3\2\2\2\66\u0196\3\2\2\28\u019b\3\2\2\2:\u019d" +
                    "\3\2\2\2<\u01a0\3\2\2\2>\u01a3\3\2\2\2@\u01a6\3\2\2\2B\u01a9\3\2\2\2D" +
                    "\u01ac\3\2\2\2F\u01af\3\2\2\2H\u01b2\3\2\2\2J\u01b5\3\2\2\2L\u01c2\3\2" +
                    "\2\2N\u01c4\3\2\2\2P\u01cf\3\2\2\2R\u01d1\3\2\2\2T\u01d9\3\2\2\2V\u01e1" +
                    "\3\2\2\2X\u01e9\3\2\2\2Z\u01f1\3\2\2\2\\\u01f9\3\2\2\2^\u01fb\3\2\2\2" +
                    "`\u01fd\3\2\2\2b\u0209\3\2\2\2d\u020b\3\2\2\2f\u020d\3\2\2\2h\u020f\3" +
                    "\2\2\2j\u0211\3\2\2\2l\u021f\3\2\2\2n\u0221\3\2\2\2p\u0223\3\2\2\2r\u0225" +
                    "\3\2\2\2t\u0227\3\2\2\2v\u0233\3\2\2\2x\u023f\3\2\2\2z\u0241\3\2\2\2|" +
                    "\u0243\3\2\2\2~\u0245\3\2\2\2\u0080\u0258\3\2\2\2\u0082\u025a\3\2\2\2" +
                    "\u0084\u025c\3\2\2\2\u0086\u026c\3\2\2\2\u0088\u026e\3\2\2\2\u008a\u0270" +
                    "\3\2\2\2\u008c\u0272\3\2\2\2\u008e\u0277\3\2\2\2\u0090\u027e\3\2\2\2\u0092" +
                    "\u028c\3\2\2\2\u0094\u028e\3\2\2\2\u0096\u0290\3\2\2\2\u0098\u029c\3\2" +
                    "\2\2\u009a\u029e\3\2\2\2\u009c\u02a0\3\2\2\2\u009e\u02a7\3\2\2\2\u00a0" +
                    "\u02b4\3\2\2\2\u00a2\u02bd\3\2\2\2\u00a4\u02c1\3\2\2\2\u00a6\u02c5\3\2" +
                    "\2\2\u00a8\u02c7\3\2\2\2\u00aa\u02c9\3\2\2\2\u00ac\u02cb\3\2\2\2\u00ae" +
                    "\u02cf\3\2\2\2\u00b0\u02d2\3\2\2\2\u00b2\u02d6\3\2\2\2\u00b4\u00b7\5\32" +
                    "\16\2\u00b5\u00b7\5\4\3\2\u00b6\u00b4\3\2\2\2\u00b6\u00b5\3\2\2\2\u00b7" +
                    "\u00b8\3\2\2\2\u00b8\u00b6\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\3\3\2\2\2" +
                    "\u00ba\u00bb\5\f\7\2\u00bb\u00bc\5\6\4\2\u00bc\u00bd\7\67\2\2\u00bd\u00c0" +
                    "\3\2\2\2\u00be\u00c0\5\24\13\2\u00bf\u00ba\3\2\2\2\u00bf\u00be\3\2\2\2" +
                    "\u00c0\5\3\2\2\2\u00c1\u00c6\5\n\6\2\u00c2\u00c3\7!\2\2\u00c3\u00c5\5" +
                    "\n\6\2\u00c4\u00c2\3\2\2\2\u00c5\u00c8\3\2\2\2\u00c6\u00c4\3\2\2\2\u00c6" +
                    "\u00c7\3\2\2\2\u00c7\7\3\2\2\2\u00c8\u00c6\3\2\2\2\u00c9\u00ce\5\26\f" +
                    "\2\u00ca\u00cb\7!\2\2\u00cb\u00cd\5\26\f\2\u00cc\u00ca\3\2\2\2\u00cd\u00d0" +
                    "\3\2\2\2\u00ce\u00cc\3\2\2\2\u00ce\u00cf\3\2\2\2\u00cf\t\3\2\2\2\u00d0" +
                    "\u00ce\3\2\2\2\u00d1\u00d2\5\26\f\2\u00d2\u00d3\58\35\2\u00d3\u00d4\5" +
                    "\30\r\2\u00d4\u00d7\3\2\2\2\u00d5\u00d7\5\26\f\2\u00d6\u00d1\3\2\2\2\u00d6" +
                    "\u00d5\3\2\2\2\u00d7\13\3\2\2\2\u00d8\u00db\5\22\n\2\u00d9\u00db\5\16" +
                    "\b\2\u00da\u00d8\3\2\2\2\u00da\u00d9\3\2\2\2\u00db\r\3\2\2\2\u00dc\u00df" +
                    "\5\22\n\2\u00dd\u00de\7\'\2\2\u00de\u00e0\7(\2\2\u00df\u00dd\3\2\2\2\u00e0" +
                    "\u00e1\3\2\2\2\u00e1\u00df\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2\17\3\2\2" +
                    "\2\u00e3\u00e8\5\22\n\2\u00e4\u00e5\7\'\2\2\u00e5\u00e6\5\64\33\2\u00e6" +
                    "\u00e7\7(\2\2\u00e7\u00e9\3\2\2\2\u00e8\u00e4\3\2\2\2\u00e9\u00ea\3\2" +
                    "\2\2\u00ea\u00e8\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb\u00f0\3\2\2\2\u00ec" +
                    "\u00ed\7\'\2\2\u00ed\u00ef\7(\2\2\u00ee\u00ec\3\2\2\2\u00ef\u00f2\3\2" +
                    "\2\2\u00f0\u00ee\3\2\2\2\u00f0\u00f1\3\2\2\2\u00f1\21\3\2\2\2\u00f2\u00f0" +
                    "\3\2\2\2\u00f3\u00f6\5\u00aeX\2\u00f4\u00f6\5\24\13\2\u00f5\u00f3\3\2" +
                    "\2\2\u00f5\u00f4\3\2\2\2\u00f6\23\3\2\2\2\u00f7\u00f9\5\u00b2Z\2\u00f8" +
                    "\u00fa\7\66\2\2\u00f9\u00f8\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa\u00fb\3" +
                    "\2\2\2\u00fb\u0102\7%\2\2\u00fc\u00fd\5\f\7\2\u00fd\u00fe\5\b\5\2\u00fe" +
                    "\u00ff\7\67\2\2\u00ff\u0101\3\2\2\2\u0100\u00fc\3\2\2\2\u0101\u0104\3" +
                    "\2\2\2\u0102\u0100\3\2\2\2\u0102\u0103\3\2\2\2\u0103\u0105\3\2\2\2\u0104" +
                    "\u0102\3\2\2\2\u0105\u0106\7&\2\2\u0106\25\3\2\2\2\u0107\u0108\7\66\2" +
                    "\2\u0108\27\3\2\2\2\u0109\u010a\5\66\34\2\u010a\31\3\2\2\2\u010b\u010c" +
                    "\5\f\7\2\u010c\u010d\7\66\2\2\u010d\u010f\7#\2\2\u010e\u0110\5\34\17\2" +
                    "\u010f\u010e\3\2\2\2\u010f\u0110\3\2\2\2\u0110\u0111\3\2\2\2\u0111\u0112" +
                    "\7$\2\2\u0112\u0117\7%\2\2\u0113\u0116\5 \21\2\u0114\u0116\5\4\3\2\u0115" +
                    "\u0113\3\2\2\2\u0115\u0114\3\2\2\2\u0116\u0119\3\2\2\2\u0117\u0115\3\2" +
                    "\2\2\u0117\u0118\3\2\2\2\u0118\u011a\3\2\2\2\u0119\u0117\3\2\2\2\u011a" +
                    "\u011b\7&\2\2\u011b\u0126\3\2\2\2\u011c\u011d\5\f\7\2\u011d\u011e\7\66" +
                    "\2\2\u011e\u0120\7#\2\2\u011f\u0121\5\34\17\2\u0120\u011f\3\2\2\2\u0120" +
                    "\u0121\3\2\2\2\u0121\u0122\3\2\2\2\u0122\u0123\7$\2\2\u0123\u0124\7\67" +
                    "\2\2\u0124\u0126\3\2\2\2\u0125\u010b\3\2\2\2\u0125\u011c\3\2\2\2\u0126" +
                    "\33\3\2\2\2\u0127\u012c\5\36\20\2\u0128\u0129\7!\2\2\u0129\u012b\5\36" +
                    "\20\2\u012a\u0128\3\2\2\2\u012b\u012e\3\2\2\2\u012c\u012a\3\2\2\2\u012c" +
                    "\u012d\3\2\2\2\u012d\u0131\3\2\2\2\u012e\u012c\3\2\2\2\u012f\u0130\7!" +
                    "\2\2\u0130\u0132\7*\2\2\u0131\u012f\3\2\2\2\u0131\u0132\3\2\2\2\u0132" +
                    "\35\3\2\2\2\u0133\u0134\5\f\7\2\u0134\u0135\5\26\f\2\u0135\37\3\2\2\2" +
                    "\u0136\u013a\5&\24\2\u0137\u013a\5$\23\2\u0138\u013a\5\"\22\2\u0139\u0136" +
                    "\3\2\2\2\u0139\u0137\3\2\2\2\u0139\u0138\3\2\2\2\u013a!\3\2\2\2\u013b" +
                    "\u013d\5\64\33\2\u013c\u013b\3\2\2\2\u013c\u013d\3\2\2\2\u013d\u013e\3" +
                    "\2\2\2\u013e\u013f\7\67\2\2\u013f#\3\2\2\2\u0140\u0143\5(\25\2\u0141\u0143" +
                    "\5*\26\2\u0142\u0140\3\2\2\2\u0142\u0141\3\2\2\2\u0143%\3\2\2\2\u0144" +
                    "\u014a\7%\2\2\u0145\u0149\5\4\3\2\u0146\u0149\5 \21\2\u0147\u0149\5\32" +
                    "\16\2\u0148\u0145\3\2\2\2\u0148\u0146\3\2\2\2\u0148\u0147\3\2\2\2\u0149" +
                    "\u014c\3\2\2\2\u014a\u0148\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014d\3\2" +
                    "\2\2\u014c\u014a\3\2\2\2\u014d\u014e\7&\2\2\u014e\'\3\2\2\2\u014f\u0153" +
                    "\5,\27\2\u0150\u0153\5.\30\2\u0151\u0153\5\60\31\2\u0152\u014f\3\2\2\2" +
                    "\u0152\u0150\3\2\2\2\u0152\u0151\3\2\2\2\u0153)\3\2\2\2\u0154\u015f\5" +
                    "\62\32\2\u0155\u0156\7+\2\2\u0156\u015f\7\67\2\2\u0157\u0158\7,\2\2\u0158" +
                    "\u015f\7\67\2\2\u0159\u015b\7-\2\2\u015a\u015c\5\64\33\2\u015b\u015a\3" +
                    "\2\2\2\u015b\u015c\3\2\2\2\u015c\u015d\3\2\2\2\u015d\u015f\7\67\2\2\u015e" +
                    "\u0154\3\2\2\2\u015e\u0155\3\2\2\2\u015e\u0157\3\2\2\2\u015e\u0159\3\2" +
                    "\2\2\u015f+\3\2\2\2\u0160\u0161\7.\2\2\u0161\u0163\7#\2\2\u0162\u0164" +
                    "\5\64\33\2\u0163\u0162\3\2\2\2\u0163\u0164\3\2\2\2\u0164\u0165\3\2\2\2" +
                    "\u0165\u0166\7\67\2\2\u0166\u0168\3\2\2\2\u0167\u0169\5\64\33\2\u0168" +
                    "\u0167\3\2\2\2\u0168\u0169\3\2\2\2\u0169\u016a\3\2\2\2\u016a\u016b\7\67" +
                    "\2\2\u016b\u016d\3\2\2\2\u016c\u016e\5\64\33\2\u016d\u016c\3\2\2\2\u016d" +
                    "\u016e\3\2\2\2\u016e\u016f\3\2\2\2\u016f\u0170\7$\2\2\u0170\u0171\5 \21" +
                    "\2\u0171-\3\2\2\2\u0172\u0173\7/\2\2\u0173\u0174\7#\2\2\u0174\u0175\5" +
                    "\64\33\2\u0175\u0176\7$\2\2\u0176\u0177\5 \21\2\u0177/\3\2\2\2\u0178\u0179" +
                    "\7\60\2\2\u0179\u017a\5 \21\2\u017a\u017b\7/\2\2\u017b\u017c\7#\2\2\u017c" +
                    "\u017d\5\64\33\2\u017d\u017e\7$\2\2\u017e\u017f\7\67\2\2\u017f\61\3\2" +
                    "\2\2\u0180\u0181\7\61\2\2\u0181\u0182\7#\2\2\u0182\u0183\5\64\33\2\u0183" +
                    "\u0184\7$\2\2\u0184\u0187\5 \21\2\u0185\u0186\7\62\2\2\u0186\u0188\5 " +
                    "\21\2\u0187\u0185\3\2\2\2\u0187\u0188\3\2\2\2\u0188\63\3\2\2\2\u0189\u018e" +
                    "\5\66\34\2\u018a\u018b\7!\2\2\u018b\u018d\5\66\34\2\u018c\u018a\3\2\2" +
                    "\2\u018d\u0190\3\2\2\2\u018e\u018c\3\2\2\2\u018e\u018f\3\2\2\2\u018f\65" +
                    "\3\2\2\2\u0190\u018e\3\2\2\2\u0191\u0192\5\u0086D\2\u0192\u0193\5L\'\2" +
                    "\u0193\u0195\3\2\2\2\u0194\u0191\3\2\2\2\u0195\u0198\3\2\2\2\u0196\u0194" +
                    "\3\2\2\2\u0196\u0197\3\2\2\2\u0197\u0199\3\2\2\2\u0198\u0196\3\2\2\2\u0199" +
                    "\u019a\5N(\2\u019a\67\3\2\2\2\u019b\u019c\7\"\2\2\u019c9\3\2\2\2\u019d" +
                    "\u019e\7\3\2\2\u019e\u019f\7\"\2\2\u019f;\3\2\2\2\u01a0\u01a1\7\4\2\2" +
                    "\u01a1\u01a2\7\"\2\2\u01a2=\3\2\2\2\u01a3\u01a4\7\5\2\2\u01a4\u01a5\7" +
                    "\"\2\2\u01a5?\3\2\2\2\u01a6\u01a7\7\6\2\2\u01a7\u01a8\7\"\2\2\u01a8A\3" +
                    "\2\2\2\u01a9\u01aa\7\7\2\2\u01aa\u01ab\7\"\2\2\u01abC\3\2\2\2\u01ac\u01ad" +
                    "\7\b\2\2\u01ad\u01ae\7\"\2\2\u01aeE\3\2\2\2\u01af\u01b0\7\t\2\2\u01b0" +
                    "\u01b1\7\"\2\2\u01b1G\3\2\2\2\u01b2\u01b3\7\n\2\2\u01b3\u01b4\7\"\2\2" +
                    "\u01b4I\3\2\2\2\u01b5\u01b6\7\13\2\2\u01b6\u01b7\7\"\2\2\u01b7K\3\2\2" +
                    "\2\u01b8\u01c3\58\35\2\u01b9\u01c3\5:\36\2\u01ba\u01c3\5<\37\2\u01bb\u01c3" +
                    "\5> \2\u01bc\u01c3\5@!\2\u01bd\u01c3\5B\"\2\u01be\u01c3\5D#\2\u01bf\u01c3" +
                    "\5F$\2\u01c0\u01c3\5H%\2\u01c1\u01c3\5J&\2\u01c2\u01b8\3\2\2\2\u01c2\u01b9" +
                    "\3\2\2\2\u01c2\u01ba\3\2\2\2\u01c2\u01bb\3\2\2\2\u01c2\u01bc\3\2\2\2\u01c2" +
                    "\u01bd\3\2\2\2\u01c2\u01be\3\2\2\2\u01c2\u01bf\3\2\2\2\u01c2\u01c0\3\2" +
                    "\2\2\u01c2\u01c1\3\2\2\2\u01c3M\3\2\2\2\u01c4\u01cc\5P)\2\u01c5\u01c6" +
                    "\7\64\2\2\u01c6\u01c7\5P)\2\u01c7\u01c8\7\63\2\2\u01c8\u01c9\5P)\2\u01c9" +
                    "\u01cb\3\2\2\2\u01ca\u01c5\3\2\2\2\u01cb\u01ce\3\2\2\2\u01cc\u01ca\3\2" +
                    "\2\2\u01cc\u01cd\3\2\2\2\u01cdO\3\2\2\2\u01ce\u01cc\3\2\2\2\u01cf\u01d0" +
                    "\5R*\2\u01d0Q\3\2\2\2\u01d1\u01d6\5T+\2\u01d2\u01d3\7\f\2\2\u01d3\u01d5" +
                    "\5T+\2\u01d4\u01d2\3\2\2\2\u01d5\u01d8\3\2\2\2\u01d6\u01d4\3\2\2\2\u01d6" +
                    "\u01d7\3\2\2\2\u01d7S\3\2\2\2\u01d8\u01d6\3\2\2\2\u01d9\u01de\5V,\2\u01da" +
                    "\u01db\7\r\2\2\u01db\u01dd\5V,\2\u01dc\u01da\3\2\2\2\u01dd\u01e0\3\2\2" +
                    "\2\u01de\u01dc\3\2\2\2\u01de\u01df\3\2\2\2\u01dfU\3\2\2\2\u01e0\u01de" +
                    "\3\2\2\2\u01e1\u01e6\5X-\2\u01e2\u01e3\7\t\2\2\u01e3\u01e5\5X-\2\u01e4" +
                    "\u01e2\3\2\2\2\u01e5\u01e8\3\2\2\2\u01e6\u01e4\3\2\2\2\u01e6\u01e7\3\2" +
                    "\2\2\u01e7W\3\2\2\2\u01e8\u01e6\3\2\2\2\u01e9\u01ee\5Z.\2\u01ea\u01eb" +
                    "\7\b\2\2\u01eb\u01ed\5Z.\2\u01ec\u01ea\3\2\2\2\u01ed\u01f0\3\2\2\2\u01ee" +
                    "\u01ec\3\2\2\2\u01ee\u01ef\3\2\2\2\u01efY\3\2\2\2\u01f0\u01ee\3\2\2\2" +
                    "\u01f1\u01f6\5`\61\2\u01f2\u01f3\7\7\2\2\u01f3\u01f5\5`\61\2\u01f4\u01f2" +
                    "\3\2\2\2\u01f5\u01f8\3\2\2\2\u01f6\u01f4\3\2\2\2\u01f6\u01f7\3\2\2\2\u01f7" +
                    "[\3\2\2\2\u01f8\u01f6\3\2\2\2\u01f9\u01fa\7\16\2\2\u01fa]\3\2\2\2\u01fb" +
                    "\u01fc\7\17\2\2\u01fc_\3\2\2\2\u01fd\u0206\5j\66\2\u01fe\u0201\5\\/\2" +
                    "\u01ff\u0201\5^\60\2\u0200\u01fe\3\2\2\2\u0200\u01ff\3\2\2\2\u0201\u0202" +
                    "\3\2\2\2\u0202\u0203\5j\66\2\u0203\u0205\3\2\2\2\u0204\u0200\3\2\2\2\u0205" +
                    "\u0208\3\2\2\2\u0206\u0204\3\2\2\2\u0206\u0207\3\2\2\2\u0207a\3\2\2\2" +
                    "\u0208\u0206\3\2\2\2\u0209\u020a\7\20\2\2\u020ac\3\2\2\2\u020b\u020c\7" +
                    "\21\2\2\u020ce\3\2\2\2\u020d\u020e\7\22\2\2\u020eg\3\2\2\2\u020f\u0210" +
                    "\7\23\2\2\u0210i\3\2\2\2\u0211\u021c\5t;\2\u0212\u0217\5h\65\2\u0213\u0217" +
                    "\5f\64\2\u0214\u0217\5b\62\2\u0215\u0217\5d\63\2\u0216\u0212\3\2\2\2\u0216" +
                    "\u0213\3\2\2\2\u0216\u0214\3\2\2\2\u0216\u0215\3\2\2\2\u0217\u0218\3\2" +
                    "\2\2\u0218\u0219\5t;\2\u0219\u021b\3\2\2\2\u021a\u0216\3\2\2\2\u021b\u021e" +
                    "\3\2\2\2\u021c\u021a\3\2\2\2\u021c\u021d\3\2\2\2\u021dk\3\2\2\2\u021e" +
                    "\u021c\3\2\2\2\u021f\u0220\7\n\2\2\u0220m\3\2\2\2\u0221\u0222\7\13\2\2" +
                    "\u0222o\3\2\2\2\u0223\u0224\7\3\2\2\u0224q\3\2\2\2\u0225\u0226\7\4\2\2" +
                    "\u0226s\3\2\2\2\u0227\u0230\5v<\2\u0228\u022b\5l\67\2\u0229\u022b\5n8" +
                    "\2\u022a\u0228\3\2\2\2\u022a\u0229\3\2\2\2\u022b\u022c\3\2\2\2\u022c\u022d" +
                    "\5v<\2\u022d\u022f\3\2\2\2\u022e\u022a\3\2\2\2\u022f\u0232\3\2\2\2\u0230" +
                    "\u022e\3\2\2\2\u0230\u0231\3\2\2\2\u0231u\3\2\2\2\u0232\u0230\3\2\2\2" +
                    "\u0233\u023c\5~@\2\u0234\u0237\5p9\2\u0235\u0237\5r:\2\u0236\u0234\3\2" +
                    "\2\2\u0236\u0235\3\2\2\2\u0237\u0238\3\2\2\2\u0238\u0239\5~@\2\u0239\u023b" +
                    "\3\2\2\2\u023a\u0236\3\2\2\2\u023b\u023e\3\2\2\2\u023c\u023a\3\2\2\2\u023c" +
                    "\u023d\3\2\2\2\u023dw\3\2\2\2\u023e\u023c\3\2\2\2\u023f\u0240\7\5\2\2" +
                    "\u0240y\3\2\2\2\u0241\u0242\7\6\2\2\u0242{\3\2\2\2\u0243\u0244\7\24\2" +
                    "\2\u0244}\3\2\2\2\u0245\u024f\5\u0080A\2\u0246\u024a\5x=\2\u0247\u024a" +
                    "\5z>\2\u0248\u024a\5|?\2\u0249\u0246\3\2\2\2\u0249\u0247\3\2\2\2\u0249" +
                    "\u0248\3\2\2\2\u024a\u024b\3\2\2\2\u024b\u024c\5\u0080A\2\u024c\u024e" +
                    "\3\2\2\2\u024d\u0249\3\2\2\2\u024e\u0251\3\2\2\2\u024f\u024d\3\2\2\2\u024f" +
                    "\u0250\3\2\2\2\u0250\177\3\2\2\2\u0251\u024f\3\2\2\2\u0252\u0253\7#\2" +
                    "\2\u0253\u0254\5\f\7\2\u0254\u0255\7$\2\2\u0255\u0256\5\u0080A\2\u0256" +
                    "\u0259\3\2\2\2\u0257\u0259\5\u0086D\2\u0258\u0252\3\2\2\2\u0258\u0257" +
                    "\3\2\2\2\u0259\u0081\3\2\2\2\u025a\u025b\7\25\2\2\u025b\u0083\3\2\2\2" +
                    "\u025c\u025d\7\26\2\2\u025d\u0085\3\2\2\2\u025e\u026d\5\u009cO\2\u025f" +
                    "\u0260\5\u0082B\2\u0260\u0261\5\u0086D\2\u0261\u026d\3\2\2\2\u0262\u0263" +
                    "\5\u0084C\2\u0263\u0264\5\u0086D\2\u0264\u026d\3\2\2\2\u0265\u0266\5\u0098" +
                    "M\2\u0266\u0267\5\u0080A\2\u0267\u026d\3\2\2\2\u0268\u026d\5\u00b0Y\2" +
                    "\u0269\u026d\5\u008eH\2\u026a\u026d\5\u008cG\2\u026b\u026d\5\u0088E\2" +
                    "\u026c\u025e\3\2\2\2\u026c\u025f\3\2\2\2\u026c\u0262\3\2\2\2\u026c\u0265" +
                    "\3\2\2\2\u026c\u0268\3\2\2\2\u026c\u0269\3\2\2\2\u026c\u026a\3\2\2\2\u026c" +
                    "\u026b\3\2\2\2\u026d\u0087\3\2\2\2\u026e\u026f\t\2\2\2\u026f\u0089\3\2" +
                    "\2\2\u0270\u0271\7\66\2\2\u0271\u008b\3\2\2\2\u0272\u0275\7)\2\2\u0273" +
                    "\u0276\5\20\t\2\u0274\u0276\5\u008aF\2\u0275\u0273\3\2\2\2\u0275\u0274" +
                    "\3\2\2\2\u0276\u008d\3\2\2\2\u0277\u0278\7\66\2\2\u0278\u027a\7#\2\2\u0279" +
                    "\u027b\5\u0090I\2\u027a\u0279\3\2\2\2\u027a\u027b\3\2\2\2\u027b\u027c" +
                    "\3\2\2\2\u027c\u027d\7$\2\2\u027d\u008f\3\2\2\2\u027e\u0283\5\66\34\2" +
                    "\u027f\u0280\7!\2\2\u0280\u0282\5\66\34\2\u0281\u027f\3\2\2\2\u0282\u0285" +
                    "\3\2\2\2\u0283\u0281\3\2\2\2\u0283\u0284\3\2\2\2\u0284\u0091\3\2\2\2\u0285" +
                    "\u0283\3\2\2\2\u0286\u028d\5\u00a6T\2\u0287\u028d\5\64\33\2\u0288\u0289" +
                    "\7#\2\2\u0289\u028a\5\u0092J\2\u028a\u028b\7$\2\2\u028b\u028d\3\2\2\2" +
                    "\u028c\u0286\3\2\2\2\u028c\u0287\3\2\2\2\u028c\u0288\3\2\2\2\u028d\u0093" +
                    "\3\2\2\2\u028e\u028f\7\32\2\2\u028f\u0095\3\2\2\2\u0290\u0291\7\7\2\2" +
                    "\u0291\u0097\3\2\2\2\u0292\u029d\5\u0094K\2\u0293\u029d\5r:\2\u0294\u029d" +
                    "\5\u0082B\2\u0295\u029d\5\u0084C\2\u0296\u0297\7#\2\2\u0297\u0298\5\u00a4" +
                    "S\2\u0298\u0299\7$\2\2\u0299\u029d\3\2\2\2\u029a\u029d\5\u0096L\2\u029b" +
                    "\u029d\5\u009aN\2\u029c\u0292\3\2\2\2\u029c\u0293\3\2\2\2\u029c\u0294" +
                    "\3\2\2\2\u029c\u0295\3\2\2\2\u029c\u0296\3\2\2\2\u029c\u029a\3\2\2\2\u029c" +
                    "\u029b\3\2\2\2\u029d\u0099\3\2\2\2\u029e\u029f\7\33\2\2\u029f\u009b\3" +
                    "\2\2\2\u02a0\u02a4\5\u00a2R\2\u02a1\u02a3\5\u00a0Q\2\u02a2\u02a1\3\2\2" +
                    "\2\u02a3\u02a6\3\2\2\2\u02a4\u02a2\3\2\2\2\u02a4\u02a5\3\2\2\2\u02a5\u009d" +
                    "\3\2\2\2\u02a6\u02a4\3\2\2\2\u02a7\u02a8\7\65\2\2\u02a8\u009f\3\2\2\2" +
                    "\u02a9\u02aa\7\'\2\2\u02aa\u02ab\5\64\33\2\u02ab\u02ac\7(\2\2\u02ac\u02b5" +
                    "\3\2\2\2\u02ad\u02b0\5\u009eP\2\u02ae\u02b1\7\66\2\2\u02af\u02b1\5\u008e" +
                    "H\2\u02b0\u02ae\3\2\2\2\u02b0\u02af\3\2\2\2\u02b1\u02b5\3\2\2\2\u02b2" +
                    "\u02b5\5\u0082B\2\u02b3\u02b5\5\u0084C\2\u02b4\u02a9\3\2\2\2\u02b4\u02ad" +
                    "\3\2\2\2\u02b4\u02b2\3\2\2\2\u02b4\u02b3\3\2\2\2\u02b5\u00a1\3\2\2\2\u02b6" +
                    "\u02be\7\66\2\2\u02b7\u02be\5\u00a6T\2\u02b8\u02be\5\u00acW\2\u02b9\u02ba" +
                    "\7#\2\2\u02ba\u02bb\5\64\33\2\u02bb\u02bc\7$\2\2\u02bc\u02be\3\2\2\2\u02bd" +
                    "\u02b6\3\2\2\2\u02bd\u02b7\3\2\2\2\u02bd\u02b8\3\2\2\2\u02bd\u02b9\3\2" +
                    "\2\2\u02be\u00a3\3\2\2\2\u02bf\u02c2\5\u00aeX\2\u02c0\u02c2\7\66\2\2\u02c1" +
                    "\u02bf\3\2\2\2\u02c1\u02c0\3\2\2\2\u02c2\u00a5\3\2\2\2\u02c3\u02c6\5\u00a8" +
                    "U\2\u02c4\u02c6\5\u00acW\2\u02c5\u02c3\3\2\2\2\u02c5\u02c4\3\2\2\2\u02c6" +
                    "\u00a7\3\2\2\2\u02c7\u02c8\5\u00aaV\2\u02c8\u00a9\3\2\2\2\u02c9\u02ca" +
                    "\5\u00b0Y\2\u02ca\u00ab\3\2\2\2\u02cb\u02cc\7>\2\2\u02cc\u00ad\3\2\2\2" +
                    "\u02cd\u02d0\t\3\2\2\u02ce\u02d0\7\66\2\2\u02cf\u02cd\3\2\2\2\u02cf\u02ce" +
                    "\3\2\2\2\u02d0\u00af\3\2\2\2\u02d1\u02d3\7<\2\2\u02d2\u02d1\3\2\2\2\u02d2" +
                    "\u02d3\3\2\2\2\u02d3\u02d4\3\2\2\2\u02d4\u02d5\78\2\2\u02d5\u00b1\3\2" +
                    "\2\2\u02d6\u02d7\7 \2\2\u02d7\u00b3\3\2\2\2D\u00b6\u00b8\u00bf\u00c6\u00ce" +
                    "\u00d6\u00da\u00e1\u00ea\u00f0\u00f5\u00f9\u0102\u010f\u0115\u0117\u0120" +
                    "\u0125\u012c\u0131\u0139\u013c\u0142\u0148\u014a\u0152\u015b\u015e\u0163" +
                    "\u0168\u016d\u0187\u018e\u0196\u01c2\u01cc\u01d6\u01de\u01e6\u01ee\u01f6" +
                    "\u0200\u0206\u0216\u021c\u022a\u0230\u0236\u023c\u0249\u024f\u0258\u026c" +
                    "\u0275\u027a\u0283\u028c\u029c\u02a4\u02b0\u02b4\u02bd\u02c1\u02c5\u02cf" +
                    "\u02d2";
    public static final ATN _ATN =
            new ATNDeserializer().deserialize(_serializedATN.toCharArray());
    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache =
            new PredictionContextCache();
    private static final String[] _LITERAL_NAMES = {
            null, "'+'", "'-'", "'*'", "'/'", "'&'", "'^'", "'|'", "'<<'", "'>>'",
            "'||'", "'&&'", "'=='", "'!='", "'>'", "'<'", "'>='", "'<='", "'%'", "'++'",
            "'--'", "'true'", "'false'", "'null'", "'!'", "'~'", "'int'", "'string'",
            "'bool'", "'void'", "'class'", "','", "'='", "'('", "')'", "'{'", "'}'",
            "'['", "']'", "'new'", "'...'", "'break'", "'continue'", "'return'", "'for'",
            "'while'", "'do'", "'if'", "'else'", "':'", "'?'", "'.'", null, "';'"
    };
    private static final String[] _SYMBOLIC_NAMES = {
            null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, "Comma", "Equal", "Lbracket",
            "Rbracket", "LBigbracket", "RBigbracket", "LMidbracket", "RMidbracket",
            "New", "Balabalabala", "BREAK", "CONTINUE", "RETURN", "FOR", "WHILE",
            "DO", "IF", "ELSE", "COLON", "QUESTION", "Dot", "Identifier", "SEMI",
            "DecimalConstant", "Numbertail", "OctalConstant", "HexadecimalConstant",
            "Sign", "CharacterConstant", "StringLiteral", "Preprocessing", "Whitespace",
            "Newline", "BlockComment", "LineComment"
    };
    public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

    static {
        RuntimeMetaData.checkVersion("4.5.1", RuntimeMetaData.VERSION);
    }

    static {
        tokenNames = new String[_SYMBOLIC_NAMES.length];
        for (int i = 0; i < tokenNames.length; i++) {
            tokenNames[i] = VOCABULARY.getLiteralName(i);
            if (tokenNames[i] == null) {
                tokenNames[i] = VOCABULARY.getSymbolicName(i);
            }

            if (tokenNames[i] == null) {
                tokenNames[i] = "<INVALID>";
            }
        }
    }

    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }

    public MParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    @Override
    @Deprecated
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override

    public Vocabulary getVocabulary() {
        return VOCABULARY;
    }

    @Override
    public String getGrammarFileName() {
        return "M.g4";
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public final ProgramContext program() throws RecognitionException {
        ProgramContext _localctx = new ProgramContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_program);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(180);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        setState(180);
                        switch (getInterpreter().adaptivePredict(_input, 0, _ctx)) {
                            case 1: {
                                setState(178);
                                function_definition();
                            }
                            break;
                            case 2: {
                                setState(179);
                                declaration();
                            }
                            break;
                        }
                    }
                    setState(182);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << Identifier))) != 0));
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final DeclarationContext declaration() throws RecognitionException {
        DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_declaration);
        try {
            setState(189);
            switch (getInterpreter().adaptivePredict(_input, 2, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    {
                        setState(184);
                        type();
                    }
                    setState(185);
                    init_declarators();
                    setState(186);
                    match(SEMI);
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(188);
                    class_declaration();
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Init_declaratorsContext init_declarators() throws RecognitionException {
        Init_declaratorsContext _localctx = new Init_declaratorsContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_init_declarators);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(191);
                init_declarator();
                setState(196);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == Comma) {
                    {
                        {
                            setState(192);
                            match(Comma);
                            setState(193);
                            init_declarator();
                        }
                    }
                    setState(198);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final DeclaratorsContext declarators() throws RecognitionException {
        DeclaratorsContext _localctx = new DeclaratorsContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_declarators);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(199);
                declarator();
                setState(204);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == Comma) {
                    {
                        {
                            setState(200);
                            match(Comma);
                            setState(201);
                            declarator();
                        }
                    }
                    setState(206);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Init_declaratorContext init_declarator() throws RecognitionException {
        Init_declaratorContext _localctx = new Init_declaratorContext(_ctx, getState());
        enterRule(_localctx, 8, RULE_init_declarator);
        try {
            setState(212);
            switch (getInterpreter().adaptivePredict(_input, 5, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(207);
                    declarator();
                    {
                        setState(208);
                        equal();
                        setState(209);
                        initializer();
                    }
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(211);
                    declarator();
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final TypeContext type() throws RecognitionException {
        TypeContext _localctx = new TypeContext(_ctx, getState());
        enterRule(_localctx, 10, RULE_type);
        try {
            setState(216);
            switch (getInterpreter().adaptivePredict(_input, 6, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(214);
                    type_non_array();
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(215);
                    array_decl();
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Array_declContext array_decl() throws RecognitionException {
        Array_declContext _localctx = new Array_declContext(_ctx, getState());
        enterRule(_localctx, 12, RULE_array_decl);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(218);
                type_non_array();
                setState(221);
                _errHandler.sync(this);
                _la = _input.LA(1);
                do {
                    {
                        {
                            setState(219);
                            match(LMidbracket);
                            setState(220);
                            match(RMidbracket);
                        }
                    }
                    setState(223);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                } while (_la == LMidbracket);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Array_newContext array_new() throws RecognitionException {
        Array_newContext _localctx = new Array_newContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_array_new);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(225);
                type_non_array();
                setState(230);
                _errHandler.sync(this);
                _alt = 1;
                do {
                    switch (_alt) {
                        case 1: {
                            {
                                setState(226);
                                match(LMidbracket);
                                setState(227);
                                expression();
                                setState(228);
                                match(RMidbracket);
                            }
                        }
                        break;
                        default:
                            throw new NoViableAltException(this);
                    }
                    setState(232);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 8, _ctx);
                } while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER);
                setState(238);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == LMidbracket) {
                    {
                        {
                            setState(234);
                            match(LMidbracket);
                            setState(235);
                            match(RMidbracket);
                        }
                    }
                    setState(240);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Type_non_arrayContext type_non_array() throws RecognitionException {
        Type_non_arrayContext _localctx = new Type_non_arrayContext(_ctx, getState());
        enterRule(_localctx, 16, RULE_type_non_array);
        try {
            setState(243);
            switch (_input.LA(1)) {
                case T__25:
                case T__26:
                case T__27:
                case T__28:
                case Identifier:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(241);
                    typenametokens();
                }
                break;
                case T__29:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(242);
                    class_declaration();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Class_declarationContext class_declaration() throws RecognitionException {
        Class_declarationContext _localctx = new Class_declarationContext(_ctx, getState());
        enterRule(_localctx, 18, RULE_class_declaration);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                {
                    setState(245);
                    union_class();
                }
                setState(247);
                _la = _input.LA(1);
                if (_la == Identifier) {
                    {
                        setState(246);
                        match(Identifier);
                    }
                }

                setState(249);
                match(LBigbracket);
                setState(256);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << Identifier))) != 0)) {
                    {
                        {
                            setState(250);
                            type();
                            setState(251);
                            declarators();
                            setState(252);
                            match(SEMI);
                        }
                    }
                    setState(258);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(259);
                match(RBigbracket);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final DeclaratorContext declarator() throws RecognitionException {
        DeclaratorContext _localctx = new DeclaratorContext(_ctx, getState());
        enterRule(_localctx, 20, RULE_declarator);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(261);
                match(Identifier);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final InitializerContext initializer() throws RecognitionException {
        InitializerContext _localctx = new InitializerContext(_ctx, getState());
        enterRule(_localctx, 22, RULE_initializer);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(263);
                assignment_expression();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Function_definitionContext function_definition() throws RecognitionException {
        Function_definitionContext _localctx = new Function_definitionContext(_ctx, getState());
        enterRule(_localctx, 24, RULE_function_definition);
        int _la;
        try {
            setState(291);
            switch (getInterpreter().adaptivePredict(_input, 17, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(265);
                    type();
                    setState(266);
                    match(Identifier);
                    setState(267);
                    match(Lbracket);
                    setState(269);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << Identifier))) != 0)) {
                        {
                            setState(268);
                            parameters();
                        }
                    }

                    setState(271);
                    match(Rbracket);
                    setState(272);
                    match(LBigbracket);
                    setState(277);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                    while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << Lbracket) | (1L << LBigbracket) | (1L << New) | (1L << BREAK) | (1L << CONTINUE) | (1L << RETURN) | (1L << FOR) | (1L << WHILE) | (1L << DO) | (1L << IF) | (1L << Identifier) | (1L << SEMI) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
                        {
                            setState(275);
                            switch (getInterpreter().adaptivePredict(_input, 14, _ctx)) {
                                case 1: {
                                    setState(273);
                                    statement();
                                }
                                break;
                                case 2: {
                                    setState(274);
                                    declaration();
                                }
                                break;
                            }
                        }
                        setState(279);
                        _errHandler.sync(this);
                        _la = _input.LA(1);
                    }
                    setState(280);
                    match(RBigbracket);
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(282);
                    type();
                    setState(283);
                    match(Identifier);
                    setState(284);
                    match(Lbracket);
                    setState(286);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << Identifier))) != 0)) {
                        {
                            setState(285);
                            parameters();
                        }
                    }

                    setState(288);
                    match(Rbracket);
                    setState(289);
                    match(SEMI);
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final ParametersContext parameters() throws RecognitionException {
        ParametersContext _localctx = new ParametersContext(_ctx, getState());
        enterRule(_localctx, 26, RULE_parameters);
        int _la;
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                {
                    setState(293);
                    parameter();
                    setState(298);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 18, _ctx);
                    while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                        if (_alt == 1) {
                            {
                                {
                                    setState(294);
                                    match(Comma);
                                    setState(295);
                                    parameter();
                                }
                            }
                        }
                        setState(300);
                        _errHandler.sync(this);
                        _alt = getInterpreter().adaptivePredict(_input, 18, _ctx);
                    }
                }
                setState(303);
                _la = _input.LA(1);
                if (_la == Comma) {
                    {
                        setState(301);
                        match(Comma);
                        setState(302);
                        match(Balabalabala);
                    }
                }

            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final ParameterContext parameter() throws RecognitionException {
        ParameterContext _localctx = new ParameterContext(_ctx, getState());
        enterRule(_localctx, 28, RULE_parameter);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(305);
                type();
                {
                    setState(306);
                    declarator();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final StatementContext statement() throws RecognitionException {
        StatementContext _localctx = new StatementContext(_ctx, getState());
        enterRule(_localctx, 30, RULE_statement);
        try {
            setState(311);
            switch (_input.LA(1)) {
                case LBigbracket:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(308);
                    compond_statement();
                }
                break;
                case BREAK:
                case CONTINUE:
                case RETURN:
                case FOR:
                case WHILE:
                case DO:
                case IF:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(309);
                    structured_statement();
                }
                break;
                case T__1:
                case T__4:
                case T__18:
                case T__19:
                case T__20:
                case T__21:
                case T__22:
                case T__23:
                case T__24:
                case Lbracket:
                case New:
                case Identifier:
                case SEMI:
                case DecimalConstant:
                case Sign:
                case StringLiteral:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(310);
                    assignment_statement();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Assignment_statementContext assignment_statement() throws RecognitionException {
        Assignment_statementContext _localctx = new Assignment_statementContext(_ctx, getState());
        enterRule(_localctx, 32, RULE_assignment_statement);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(314);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
                    {
                        setState(313);
                        expression();
                    }
                }

                setState(316);
                match(SEMI);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Structured_statementContext structured_statement() throws RecognitionException {
        Structured_statementContext _localctx = new Structured_statementContext(_ctx, getState());
        enterRule(_localctx, 34, RULE_structured_statement);
        try {
            setState(320);
            switch (_input.LA(1)) {
                case FOR:
                case WHILE:
                case DO:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(318);
                    loop_statement();
                }
                break;
                case BREAK:
                case CONTINUE:
                case RETURN:
                case IF:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(319);
                    branch_statement();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Compond_statementContext compond_statement() throws RecognitionException {
        Compond_statementContext _localctx = new Compond_statementContext(_ctx, getState());
        enterRule(_localctx, 36, RULE_compond_statement);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(322);
                match(LBigbracket);
                setState(328);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28) | (1L << T__29) | (1L << Lbracket) | (1L << LBigbracket) | (1L << New) | (1L << BREAK) | (1L << CONTINUE) | (1L << RETURN) | (1L << FOR) | (1L << WHILE) | (1L << DO) | (1L << IF) | (1L << Identifier) | (1L << SEMI) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
                    {
                        setState(326);
                        switch (getInterpreter().adaptivePredict(_input, 23, _ctx)) {
                            case 1: {
                                setState(323);
                                declaration();
                            }
                            break;
                            case 2: {
                                setState(324);
                                statement();
                            }
                            break;
                            case 3: {
                                setState(325);
                                function_definition();
                            }
                            break;
                        }
                    }
                    setState(330);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(331);
                match(RBigbracket);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Loop_statementContext loop_statement() throws RecognitionException {
        Loop_statementContext _localctx = new Loop_statementContext(_ctx, getState());
        enterRule(_localctx, 38, RULE_loop_statement);
        try {
            setState(336);
            switch (_input.LA(1)) {
                case FOR:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(333);
                    for_loop_statement();
                }
                break;
                case WHILE:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(334);
                    while_loop_statement();
                }
                break;
                case DO:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(335);
                    do_while_loop_statement();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Branch_statementContext branch_statement() throws RecognitionException {
        Branch_statementContext _localctx = new Branch_statementContext(_ctx, getState());
        enterRule(_localctx, 40, RULE_branch_statement);
        int _la;
        try {
            setState(348);
            switch (_input.LA(1)) {
                case IF:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(338);
                    if_statement();
                }
                break;
                case BREAK:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(339);
                    match(BREAK);
                    setState(340);
                    match(SEMI);
                }
                break;
                case CONTINUE:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(341);
                    match(CONTINUE);
                    setState(342);
                    match(SEMI);
                }
                break;
                case RETURN:
                    enterOuterAlt(_localctx, 4);
                {
                    setState(343);
                    match(RETURN);
                    setState(345);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
                        {
                            setState(344);
                            expression();
                        }
                    }

                    setState(347);
                    match(SEMI);
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final For_loop_statementContext for_loop_statement() throws RecognitionException {
        For_loop_statementContext _localctx = new For_loop_statementContext(_ctx, getState());
        enterRule(_localctx, 42, RULE_for_loop_statement);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(350);
                match(FOR);
                setState(351);
                match(Lbracket);
                {
                    setState(353);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
                        {
                            setState(352);
                            expression();
                        }
                    }

                    setState(355);
                    match(SEMI);
                }
                {
                    setState(358);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
                        {
                            setState(357);
                            expression();
                        }
                    }

                    setState(360);
                    match(SEMI);
                }
                {
                    setState(363);
                    _la = _input.LA(1);
                    if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
                        {
                            setState(362);
                            expression();
                        }
                    }

                }
                setState(365);
                match(Rbracket);
                setState(366);
                statement();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final While_loop_statementContext while_loop_statement() throws RecognitionException {
        While_loop_statementContext _localctx = new While_loop_statementContext(_ctx, getState());
        enterRule(_localctx, 44, RULE_while_loop_statement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(368);
                match(WHILE);
                setState(369);
                match(Lbracket);
                setState(370);
                expression();
                setState(371);
                match(Rbracket);
                setState(372);
                statement();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Do_while_loop_statementContext do_while_loop_statement() throws RecognitionException {
        Do_while_loop_statementContext _localctx = new Do_while_loop_statementContext(_ctx, getState());
        enterRule(_localctx, 46, RULE_do_while_loop_statement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(374);
                match(DO);
                {
                    setState(375);
                    statement();
                }
                setState(376);
                match(WHILE);
                setState(377);
                match(Lbracket);
                setState(378);
                expression();
                setState(379);
                match(Rbracket);
                setState(380);
                match(SEMI);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final If_statementContext if_statement() throws RecognitionException {
        If_statementContext _localctx = new If_statementContext(_ctx, getState());
        enterRule(_localctx, 48, RULE_if_statement);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(382);
                match(IF);
                setState(383);
                match(Lbracket);
                setState(384);
                expression();
                setState(385);
                match(Rbracket);
                {
                    setState(386);
                    statement();
                }
                setState(389);
                switch (getInterpreter().adaptivePredict(_input, 31, _ctx)) {
                    case 1: {
                        setState(387);
                        match(ELSE);
                        {
                            setState(388);
                            statement();
                        }
                    }
                    break;
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final ExpressionContext expression() throws RecognitionException {
        ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
        enterRule(_localctx, 50, RULE_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(391);
                assignment_expression();
                setState(396);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == Comma) {
                    {
                        {
                            setState(392);
                            match(Comma);
                            setState(393);
                            assignment_expression();
                        }
                    }
                    setState(398);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Assignment_expressionContext assignment_expression() throws RecognitionException {
        Assignment_expressionContext _localctx = new Assignment_expressionContext(_ctx, getState());
        enterRule(_localctx, 52, RULE_assignment_expression);
        try {
            int _alt;
            enterOuterAlt(_localctx, 1);
            {
                setState(404);
                _errHandler.sync(this);
                _alt = getInterpreter().adaptivePredict(_input, 33, _ctx);
                while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
                    if (_alt == 1) {
                        {
                            {
                                setState(399);
                                unary_expression();
                                setState(400);
                                assignment_operators();
                            }
                        }
                    }
                    setState(406);
                    _errHandler.sync(this);
                    _alt = getInterpreter().adaptivePredict(_input, 33, _ctx);
                }
                setState(407);
                calculation_expression();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final EqualContext equal() throws RecognitionException {
        EqualContext _localctx = new EqualContext(_ctx, getState());
        enterRule(_localctx, 54, RULE_equal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(409);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final PequalContext pequal() throws RecognitionException {
        PequalContext _localctx = new PequalContext(_ctx, getState());
        enterRule(_localctx, 56, RULE_pequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(411);
                match(T__0);
                setState(412);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final MiequalContext miequal() throws RecognitionException {
        MiequalContext _localctx = new MiequalContext(_ctx, getState());
        enterRule(_localctx, 58, RULE_miequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(414);
                match(T__1);
                setState(415);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final MuequalContext muequal() throws RecognitionException {
        MuequalContext _localctx = new MuequalContext(_ctx, getState());
        enterRule(_localctx, 60, RULE_muequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(417);
                match(T__2);
                setState(418);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final DiequalContext diequal() throws RecognitionException {
        DiequalContext _localctx = new DiequalContext(_ctx, getState());
        enterRule(_localctx, 62, RULE_diequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(420);
                match(T__3);
                setState(421);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final AndequalContext andequal() throws RecognitionException {
        AndequalContext _localctx = new AndequalContext(_ctx, getState());
        enterRule(_localctx, 64, RULE_andequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(423);
                match(T__4);
                setState(424);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final XorequalContext xorequal() throws RecognitionException {
        XorequalContext _localctx = new XorequalContext(_ctx, getState());
        enterRule(_localctx, 66, RULE_xorequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(426);
                match(T__5);
                setState(427);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final OrequalContext orequal() throws RecognitionException {
        OrequalContext _localctx = new OrequalContext(_ctx, getState());
        enterRule(_localctx, 68, RULE_orequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(429);
                match(T__6);
                setState(430);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final LsequalContext lsequal() throws RecognitionException {
        LsequalContext _localctx = new LsequalContext(_ctx, getState());
        enterRule(_localctx, 70, RULE_lsequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(432);
                match(T__7);
                setState(433);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final RsequalContext rsequal() throws RecognitionException {
        RsequalContext _localctx = new RsequalContext(_ctx, getState());
        enterRule(_localctx, 72, RULE_rsequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(435);
                match(T__8);
                setState(436);
                match(Equal);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Assignment_operatorsContext assignment_operators() throws RecognitionException {
        Assignment_operatorsContext _localctx = new Assignment_operatorsContext(_ctx, getState());
        enterRule(_localctx, 74, RULE_assignment_operators);
        try {
            setState(448);
            switch (_input.LA(1)) {
                case Equal:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(438);
                    equal();
                }
                break;
                case T__0:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(439);
                    pequal();
                }
                break;
                case T__1:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(440);
                    miequal();
                }
                break;
                case T__2:
                    enterOuterAlt(_localctx, 4);
                {
                    setState(441);
                    muequal();
                }
                break;
                case T__3:
                    enterOuterAlt(_localctx, 5);
                {
                    setState(442);
                    diequal();
                }
                break;
                case T__4:
                    enterOuterAlt(_localctx, 6);
                {
                    setState(443);
                    andequal();
                }
                break;
                case T__5:
                    enterOuterAlt(_localctx, 7);
                {
                    setState(444);
                    xorequal();
                }
                break;
                case T__6:
                    enterOuterAlt(_localctx, 8);
                {
                    setState(445);
                    orequal();
                }
                break;
                case T__7:
                    enterOuterAlt(_localctx, 9);
                {
                    setState(446);
                    lsequal();
                }
                break;
                case T__8:
                    enterOuterAlt(_localctx, 10);
                {
                    setState(447);
                    rsequal();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Calculation_expressionContext calculation_expression() throws RecognitionException {
        Calculation_expressionContext _localctx = new Calculation_expressionContext(_ctx, getState());
        enterRule(_localctx, 76, RULE_calculation_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(450);
                logical_caclulation_expression();
                setState(458);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == QUESTION) {
                    {
                        {
                            setState(451);
                            match(QUESTION);
                            setState(452);
                            logical_caclulation_expression();
                            setState(453);
                            match(COLON);
                            setState(454);
                            logical_caclulation_expression();
                        }
                    }
                    setState(460);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Logical_caclulation_expressionContext logical_caclulation_expression() throws RecognitionException {
        Logical_caclulation_expressionContext _localctx = new Logical_caclulation_expressionContext(_ctx, getState());
        enterRule(_localctx, 78, RULE_logical_caclulation_expression);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(461);
                logical_or_expression();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Logical_or_expressionContext logical_or_expression() throws RecognitionException {
        Logical_or_expressionContext _localctx = new Logical_or_expressionContext(_ctx, getState());
        enterRule(_localctx, 80, RULE_logical_or_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(463);
                logical_and_expression();
                setState(468);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__9) {
                    {
                        {
                            setState(464);
                            match(T__9);
                            setState(465);
                            logical_and_expression();
                        }
                    }
                    setState(470);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Logical_and_expressionContext logical_and_expression() throws RecognitionException {
        Logical_and_expressionContext _localctx = new Logical_and_expressionContext(_ctx, getState());
        enterRule(_localctx, 82, RULE_logical_and_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(471);
                bitwise_or_expression();
                setState(476);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__10) {
                    {
                        {
                            setState(472);
                            match(T__10);
                            setState(473);
                            bitwise_or_expression();
                        }
                    }
                    setState(478);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Bitwise_or_expressionContext bitwise_or_expression() throws RecognitionException {
        Bitwise_or_expressionContext _localctx = new Bitwise_or_expressionContext(_ctx, getState());
        enterRule(_localctx, 84, RULE_bitwise_or_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(479);
                bitwise_xor_expression();
                setState(484);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__6) {
                    {
                        {
                            setState(480);
                            match(T__6);
                            setState(481);
                            bitwise_xor_expression();
                        }
                    }
                    setState(486);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Bitwise_xor_expressionContext bitwise_xor_expression() throws RecognitionException {
        Bitwise_xor_expressionContext _localctx = new Bitwise_xor_expressionContext(_ctx, getState());
        enterRule(_localctx, 86, RULE_bitwise_xor_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(487);
                bitwise_and_expression();
                setState(492);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__5) {
                    {
                        {
                            setState(488);
                            match(T__5);
                            setState(489);
                            bitwise_and_expression();
                        }
                    }
                    setState(494);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Bitwise_and_expressionContext bitwise_and_expression() throws RecognitionException {
        Bitwise_and_expressionContext _localctx = new Bitwise_and_expressionContext(_ctx, getState());
        enterRule(_localctx, 88, RULE_bitwise_and_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(495);
                equality_expression();
                setState(500);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__4) {
                    {
                        {
                            setState(496);
                            match(T__4);
                            setState(497);
                            equality_expression();
                        }
                    }
                    setState(502);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final IsequalContext isequal() throws RecognitionException {
        IsequalContext _localctx = new IsequalContext(_ctx, getState());
        enterRule(_localctx, 90, RULE_isequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(503);
                match(T__11);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final NotequalContext notequal() throws RecognitionException {
        NotequalContext _localctx = new NotequalContext(_ctx, getState());
        enterRule(_localctx, 92, RULE_notequal);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(505);
                match(T__12);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Equality_expressionContext equality_expression() throws RecognitionException {
        Equality_expressionContext _localctx = new Equality_expressionContext(_ctx, getState());
        enterRule(_localctx, 94, RULE_equality_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(507);
                relation_expression();
                setState(516);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__11 || _la == T__12) {
                    {
                        {
                            setState(510);
                            switch (_input.LA(1)) {
                                case T__11: {
                                    setState(508);
                                    isequal();
                                }
                                break;
                                case T__12: {
                                    setState(509);
                                    notequal();
                                }
                                break;
                                default:
                                    throw new NoViableAltException(this);
                            }
                            setState(512);
                            relation_expression();
                        }
                    }
                    setState(518);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final BiggerContext bigger() throws RecognitionException {
        BiggerContext _localctx = new BiggerContext(_ctx, getState());
        enterRule(_localctx, 96, RULE_bigger);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(519);
                match(T__13);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final SmallerContext smaller() throws RecognitionException {
        SmallerContext _localctx = new SmallerContext(_ctx, getState());
        enterRule(_localctx, 98, RULE_smaller);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(521);
                match(T__14);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Bigger_eContext bigger_e() throws RecognitionException {
        Bigger_eContext _localctx = new Bigger_eContext(_ctx, getState());
        enterRule(_localctx, 100, RULE_bigger_e);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(523);
                match(T__15);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Smaller_eContext smaller_e() throws RecognitionException {
        Smaller_eContext _localctx = new Smaller_eContext(_ctx, getState());
        enterRule(_localctx, 102, RULE_smaller_e);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(525);
                match(T__16);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Relation_expressionContext relation_expression() throws RecognitionException {
        Relation_expressionContext _localctx = new Relation_expressionContext(_ctx, getState());
        enterRule(_localctx, 104, RULE_relation_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(527);
                shift_expression();
                setState(538);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16))) != 0)) {
                    {
                        {
                            setState(532);
                            switch (_input.LA(1)) {
                                case T__16: {
                                    setState(528);
                                    smaller_e();
                                }
                                break;
                                case T__15: {
                                    setState(529);
                                    bigger_e();
                                }
                                break;
                                case T__13: {
                                    setState(530);
                                    bigger();
                                }
                                break;
                                case T__14: {
                                    setState(531);
                                    smaller();
                                }
                                break;
                                default:
                                    throw new NoViableAltException(this);
                            }
                            setState(534);
                            shift_expression();
                        }
                    }
                    setState(540);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final LshiftContext lshift() throws RecognitionException {
        LshiftContext _localctx = new LshiftContext(_ctx, getState());
        enterRule(_localctx, 106, RULE_lshift);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(541);
                match(T__7);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final RshiftContext rshift() throws RecognitionException {
        RshiftContext _localctx = new RshiftContext(_ctx, getState());
        enterRule(_localctx, 108, RULE_rshift);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(543);
                match(T__8);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final PlusContext plus() throws RecognitionException {
        PlusContext _localctx = new PlusContext(_ctx, getState());
        enterRule(_localctx, 110, RULE_plus);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(545);
                match(T__0);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final MinusContext minus() throws RecognitionException {
        MinusContext _localctx = new MinusContext(_ctx, getState());
        enterRule(_localctx, 112, RULE_minus);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(547);
                match(T__1);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Shift_expressionContext shift_expression() throws RecognitionException {
        Shift_expressionContext _localctx = new Shift_expressionContext(_ctx, getState());
        enterRule(_localctx, 114, RULE_shift_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(549);
                add_expression();
                setState(558);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__7 || _la == T__8) {
                    {
                        {
                            setState(552);
                            switch (_input.LA(1)) {
                                case T__7: {
                                    setState(550);
                                    lshift();
                                }
                                break;
                                case T__8: {
                                    setState(551);
                                    rshift();
                                }
                                break;
                                default:
                                    throw new NoViableAltException(this);
                            }
                            setState(554);
                            add_expression();
                        }
                    }
                    setState(560);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Add_expressionContext add_expression() throws RecognitionException {
        Add_expressionContext _localctx = new Add_expressionContext(_ctx, getState());
        enterRule(_localctx, 116, RULE_add_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(561);
                mul_expression();
                setState(570);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__0 || _la == T__1) {
                    {
                        {
                            setState(564);
                            switch (_input.LA(1)) {
                                case T__0: {
                                    setState(562);
                                    plus();
                                }
                                break;
                                case T__1: {
                                    setState(563);
                                    minus();
                                }
                                break;
                                default:
                                    throw new NoViableAltException(this);
                            }
                            setState(566);
                            mul_expression();
                        }
                    }
                    setState(572);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final MultiplyContext multiply() throws RecognitionException {
        MultiplyContext _localctx = new MultiplyContext(_ctx, getState());
        enterRule(_localctx, 118, RULE_multiply);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(573);
                match(T__2);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final DivisionContext division() throws RecognitionException {
        DivisionContext _localctx = new DivisionContext(_ctx, getState());
        enterRule(_localctx, 120, RULE_division);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(575);
                match(T__3);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final ModContext mod() throws RecognitionException {
        ModContext _localctx = new ModContext(_ctx, getState());
        enterRule(_localctx, 122, RULE_mod);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(577);
                match(T__17);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Mul_expressionContext mul_expression() throws RecognitionException {
        Mul_expressionContext _localctx = new Mul_expressionContext(_ctx, getState());
        enterRule(_localctx, 124, RULE_mul_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(579);
                cast_expression();
                setState(589);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << T__17))) != 0)) {
                    {
                        {
                            setState(583);
                            switch (_input.LA(1)) {
                                case T__2: {
                                    setState(580);
                                    multiply();
                                }
                                break;
                                case T__3: {
                                    setState(581);
                                    division();
                                }
                                break;
                                case T__17: {
                                    setState(582);
                                    mod();
                                }
                                break;
                                default:
                                    throw new NoViableAltException(this);
                            }
                            setState(585);
                            cast_expression();
                        }
                    }
                    setState(591);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Cast_expressionContext cast_expression() throws RecognitionException {
        Cast_expressionContext _localctx = new Cast_expressionContext(_ctx, getState());
        enterRule(_localctx, 126, RULE_cast_expression);
        try {
            setState(598);
            switch (getInterpreter().adaptivePredict(_input, 51, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(592);
                    match(Lbracket);
                    setState(593);
                    type();
                    setState(594);
                    match(Rbracket);
                    setState(595);
                    cast_expression();
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(597);
                    unary_expression();
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final PlusplusContext plusplus() throws RecognitionException {
        PlusplusContext _localctx = new PlusplusContext(_ctx, getState());
        enterRule(_localctx, 128, RULE_plusplus);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(600);
                match(T__18);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final MinusminusContext minusminus() throws RecognitionException {
        MinusminusContext _localctx = new MinusminusContext(_ctx, getState());
        enterRule(_localctx, 130, RULE_minusminus);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(602);
                match(T__19);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Unary_expressionContext unary_expression() throws RecognitionException {
        Unary_expressionContext _localctx = new Unary_expressionContext(_ctx, getState());
        enterRule(_localctx, 132, RULE_unary_expression);
        try {
            setState(618);
            switch (getInterpreter().adaptivePredict(_input, 52, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(604);
                    postfix_expression();
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(605);
                    plusplus();
                    setState(606);
                    unary_expression();
                }
                break;
                case 3:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(608);
                    minusminus();
                    setState(609);
                    unary_expression();
                }
                break;
                case 4:
                    enterOuterAlt(_localctx, 4);
                {
                    setState(611);
                    unary_operation();
                    setState(612);
                    cast_expression();
                }
                break;
                case 5:
                    enterOuterAlt(_localctx, 5);
                {
                    setState(614);
                    number();
                }
                break;
                case 6:
                    enterOuterAlt(_localctx, 6);
                {
                    setState(615);
                    functionCall_expression();
                }
                break;
                case 7:
                    enterOuterAlt(_localctx, 7);
                {
                    setState(616);
                    new_operation();
                }
                break;
                case 8:
                    enterOuterAlt(_localctx, 8);
                {
                    setState(617);
                    pre_defined_constants();
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Pre_defined_constantsContext pre_defined_constants() throws RecognitionException {
        Pre_defined_constantsContext _localctx = new Pre_defined_constantsContext(_ctx, getState());
        enterRule(_localctx, 134, RULE_pre_defined_constants);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(620);
                _la = _input.LA(1);
                if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__20) | (1L << T__21) | (1L << T__22))) != 0))) {
                    _errHandler.recoverInline(this);
                } else {
                    consume();
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Class_newContext class_new() throws RecognitionException {
        Class_newContext _localctx = new Class_newContext(_ctx, getState());
        enterRule(_localctx, 136, RULE_class_new);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(622);
                match(Identifier);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final New_operationContext new_operation() throws RecognitionException {
        New_operationContext _localctx = new New_operationContext(_ctx, getState());
        enterRule(_localctx, 138, RULE_new_operation);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(624);
                match(New);
                setState(627);
                switch (getInterpreter().adaptivePredict(_input, 53, _ctx)) {
                    case 1: {
                        setState(625);
                        array_new();
                    }
                    break;
                    case 2: {
                        setState(626);
                        class_new();
                    }
                    break;
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final FunctionCall_expressionContext functionCall_expression() throws RecognitionException {
        FunctionCall_expressionContext _localctx = new FunctionCall_expressionContext(_ctx, getState());
        enterRule(_localctx, 140, RULE_functionCall_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                {
                    setState(629);
                    match(Identifier);
                }
                setState(630);
                match(Lbracket);
                setState(632);
                _la = _input.LA(1);
                if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__4) | (1L << T__18) | (1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22) | (1L << T__23) | (1L << T__24) | (1L << Lbracket) | (1L << New) | (1L << Identifier) | (1L << DecimalConstant) | (1L << Sign) | (1L << StringLiteral))) != 0)) {
                    {
                        setState(631);
                        arguements();
                    }
                }

                setState(634);
                match(Rbracket);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final ArguementsContext arguements() throws RecognitionException {
        ArguementsContext _localctx = new ArguementsContext(_ctx, getState());
        enterRule(_localctx, 142, RULE_arguements);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(636);
                assignment_expression();
                setState(641);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == Comma) {
                    {
                        {
                            setState(637);
                            match(Comma);
                            setState(638);
                            assignment_expression();
                        }
                    }
                    setState(643);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final ValueContext value() throws RecognitionException {
        ValueContext _localctx = new ValueContext(_ctx, getState());
        enterRule(_localctx, 144, RULE_value);
        try {
            setState(650);
            switch (getInterpreter().adaptivePredict(_input, 56, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(644);
                    constant();
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(645);
                    expression();
                }
                break;
                case 3:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(646);
                    match(Lbracket);
                    setState(647);
                    value();
                    setState(648);
                    match(Rbracket);
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Not_signContext not_sign() throws RecognitionException {
        Not_signContext _localctx = new Not_signContext(_ctx, getState());
        enterRule(_localctx, 146, RULE_not_sign);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(652);
                match(T__23);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final GetAddrContext getAddr() throws RecognitionException {
        GetAddrContext _localctx = new GetAddrContext(_ctx, getState());
        enterRule(_localctx, 148, RULE_getAddr);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(654);
                match(T__4);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Unary_operationContext unary_operation() throws RecognitionException {
        Unary_operationContext _localctx = new Unary_operationContext(_ctx, getState());
        enterRule(_localctx, 150, RULE_unary_operation);
        try {
            setState(666);
            switch (_input.LA(1)) {
                case T__23:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(656);
                    not_sign();
                }
                break;
                case T__1:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(657);
                    minus();
                }
                break;
                case T__18:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(658);
                    plusplus();
                }
                break;
                case T__19:
                    enterOuterAlt(_localctx, 4);
                {
                    setState(659);
                    minusminus();
                }
                break;
                case Lbracket:
                    enterOuterAlt(_localctx, 5);
                {
                    setState(660);
                    match(Lbracket);
                    setState(661);
                    type_name();
                    setState(662);
                    match(Rbracket);
                }
                break;
                case T__4:
                    enterOuterAlt(_localctx, 6);
                {
                    setState(664);
                    getAddr();
                }
                break;
                case T__24:
                    enterOuterAlt(_localctx, 7);
                {
                    setState(665);
                    bitwise_not();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Bitwise_notContext bitwise_not() throws RecognitionException {
        Bitwise_notContext _localctx = new Bitwise_notContext(_ctx, getState());
        enterRule(_localctx, 152, RULE_bitwise_not);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(668);
                match(T__24);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Postfix_expressionContext postfix_expression() throws RecognitionException {
        Postfix_expressionContext _localctx = new Postfix_expressionContext(_ctx, getState());
        enterRule(_localctx, 154, RULE_postfix_expression);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(670);
                primary_expression();
                setState(674);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__18) | (1L << T__19) | (1L << LMidbracket) | (1L << Dot))) != 0)) {
                    {
                        {
                            setState(671);
                            postfix();
                        }
                    }
                    setState(676);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final GetMemberContext getMember() throws RecognitionException {
        GetMemberContext _localctx = new GetMemberContext(_ctx, getState());
        enterRule(_localctx, 156, RULE_getMember);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(677);
                match(Dot);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final PostfixContext postfix() throws RecognitionException {
        PostfixContext _localctx = new PostfixContext(_ctx, getState());
        enterRule(_localctx, 158, RULE_postfix);
        try {
            setState(690);
            switch (_input.LA(1)) {
                case LMidbracket:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(679);
                    match(LMidbracket);
                    setState(680);
                    expression();
                    setState(681);
                    match(RMidbracket);
                }
                break;
                case Dot:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(683);
                    getMember();
                    setState(686);
                    switch (getInterpreter().adaptivePredict(_input, 59, _ctx)) {
                        case 1: {
                            setState(684);
                            match(Identifier);
                        }
                        break;
                        case 2: {
                            setState(685);
                            functionCall_expression();
                        }
                        break;
                    }
                }
                break;
                case T__18:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(688);
                    plusplus();
                }
                break;
                case T__19:
                    enterOuterAlt(_localctx, 4);
                {
                    setState(689);
                    minusminus();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Primary_expressionContext primary_expression() throws RecognitionException {
        Primary_expressionContext _localctx = new Primary_expressionContext(_ctx, getState());
        enterRule(_localctx, 160, RULE_primary_expression);
        try {
            setState(699);
            switch (getInterpreter().adaptivePredict(_input, 61, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(692);
                    match(Identifier);
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(693);
                    constant();
                }
                break;
                case 3:
                    enterOuterAlt(_localctx, 3);
                {
                    setState(694);
                    string_constant();
                }
                break;
                case 4:
                    enterOuterAlt(_localctx, 4);
                {
                    setState(695);
                    match(Lbracket);
                    setState(696);
                    expression();
                    setState(697);
                    match(Rbracket);
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Type_nameContext type_name() throws RecognitionException {
        Type_nameContext _localctx = new Type_nameContext(_ctx, getState());
        enterRule(_localctx, 162, RULE_type_name);
        try {
            setState(703);
            switch (getInterpreter().adaptivePredict(_input, 62, _ctx)) {
                case 1:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(701);
                    typenametokens();
                }
                break;
                case 2:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(702);
                    match(Identifier);
                }
                break;
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final ConstantContext constant() throws RecognitionException {
        ConstantContext _localctx = new ConstantContext(_ctx, getState());
        enterRule(_localctx, 164, RULE_constant);
        try {
            setState(707);
            switch (_input.LA(1)) {
                case DecimalConstant:
                case Sign:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(705);
                    numeric_constant();
                }
                break;
                case StringLiteral:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(706);
                    string_constant();
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Numeric_constantContext numeric_constant() throws RecognitionException {
        Numeric_constantContext _localctx = new Numeric_constantContext(_ctx, getState());
        enterRule(_localctx, 166, RULE_numeric_constant);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(709);
                integer_constant();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Integer_constantContext integer_constant() throws RecognitionException {
        Integer_constantContext _localctx = new Integer_constantContext(_ctx, getState());
        enterRule(_localctx, 168, RULE_integer_constant);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(711);
                number();
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final String_constantContext string_constant() throws RecognitionException {
        String_constantContext _localctx = new String_constantContext(_ctx, getState());
        enterRule(_localctx, 170, RULE_string_constant);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(713);
                match(StringLiteral);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final TypenametokensContext typenametokens() throws RecognitionException {
        TypenametokensContext _localctx = new TypenametokensContext(_ctx, getState());
        enterRule(_localctx, 172, RULE_typenametokens);
        int _la;
        try {
            setState(717);
            switch (_input.LA(1)) {
                case T__25:
                case T__26:
                case T__27:
                case T__28:
                    enterOuterAlt(_localctx, 1);
                {
                    setState(715);
                    ((TypenametokensContext) _localctx).builtin_types = _input.LT(1);
                    _la = _input.LA(1);
                    if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__25) | (1L << T__26) | (1L << T__27) | (1L << T__28))) != 0))) {
                        ((TypenametokensContext) _localctx).builtin_types = (Token) _errHandler.recoverInline(this);
                    } else {
                        consume();
                    }
                }
                break;
                case Identifier:
                    enterOuterAlt(_localctx, 2);
                {
                    setState(716);
                    match(Identifier);
                }
                break;
                default:
                    throw new NoViableAltException(this);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final NumberContext number() throws RecognitionException {
        NumberContext _localctx = new NumberContext(_ctx, getState());
        enterRule(_localctx, 174, RULE_number);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(720);
                _la = _input.LA(1);
                if (_la == Sign) {
                    {
                        setState(719);
                        match(Sign);
                    }
                }

                setState(722);
                match(DecimalConstant);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public final Union_classContext union_class() throws RecognitionException {
        Union_classContext _localctx = new Union_classContext(_ctx, getState());
        enterRule(_localctx, 176, RULE_union_class);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(724);
                match(T__29);
            }
        } catch (RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ProgramContext extends ParserRuleContext {
        public ProgramContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Function_definitionContext> function_definition() {
            return getRuleContexts(Function_definitionContext.class);
        }

        public Function_definitionContext function_definition(int i) {
            return getRuleContext(Function_definitionContext.class, i);
        }

        public List<DeclarationContext> declaration() {
            return getRuleContexts(DeclarationContext.class);
        }

        public DeclarationContext declaration(int i) {
            return getRuleContext(DeclarationContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_program;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterProgram(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitProgram(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitProgram(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class DeclarationContext extends ParserRuleContext {
        public DeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Init_declaratorsContext init_declarators() {
            return getRuleContext(Init_declaratorsContext.class, 0);
        }

        public TerminalNode SEMI() {
            return getToken(MParser.SEMI, 0);
        }

        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public Class_declarationContext class_declaration() {
            return getRuleContext(Class_declarationContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_declaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterDeclaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitDeclaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitDeclaration(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Init_declaratorsContext extends ParserRuleContext {
        public Init_declaratorsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Init_declaratorContext> init_declarator() {
            return getRuleContexts(Init_declaratorContext.class);
        }

        public Init_declaratorContext init_declarator(int i) {
            return getRuleContext(Init_declaratorContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(MParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(MParser.Comma, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_init_declarators;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterInit_declarators(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitInit_declarators(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitInit_declarators(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class DeclaratorsContext extends ParserRuleContext {
        public DeclaratorsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<DeclaratorContext> declarator() {
            return getRuleContexts(DeclaratorContext.class);
        }

        public DeclaratorContext declarator(int i) {
            return getRuleContext(DeclaratorContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(MParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(MParser.Comma, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_declarators;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterDeclarators(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitDeclarators(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitDeclarators(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Init_declaratorContext extends ParserRuleContext {
        public Init_declaratorContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public DeclaratorContext declarator() {
            return getRuleContext(DeclaratorContext.class, 0);
        }

        public EqualContext equal() {
            return getRuleContext(EqualContext.class, 0);
        }

        public InitializerContext initializer() {
            return getRuleContext(InitializerContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_init_declarator;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterInit_declarator(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitInit_declarator(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitInit_declarator(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class TypeContext extends ParserRuleContext {
        public TypeContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Type_non_arrayContext type_non_array() {
            return getRuleContext(Type_non_arrayContext.class, 0);
        }

        public Array_declContext array_decl() {
            return getRuleContext(Array_declContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_type;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterType(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitType(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitType(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Array_declContext extends ParserRuleContext {
        public Array_declContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Type_non_arrayContext type_non_array() {
            return getRuleContext(Type_non_arrayContext.class, 0);
        }

        public List<TerminalNode> LMidbracket() {
            return getTokens(MParser.LMidbracket);
        }

        public TerminalNode LMidbracket(int i) {
            return getToken(MParser.LMidbracket, i);
        }

        public List<TerminalNode> RMidbracket() {
            return getTokens(MParser.RMidbracket);
        }

        public TerminalNode RMidbracket(int i) {
            return getToken(MParser.RMidbracket, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_array_decl;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterArray_decl(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitArray_decl(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitArray_decl(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Array_newContext extends ParserRuleContext {
        public Array_newContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Type_non_arrayContext type_non_array() {
            return getRuleContext(Type_non_arrayContext.class, 0);
        }

        public List<TerminalNode> LMidbracket() {
            return getTokens(MParser.LMidbracket);
        }

        public TerminalNode LMidbracket(int i) {
            return getToken(MParser.LMidbracket, i);
        }

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        public List<TerminalNode> RMidbracket() {
            return getTokens(MParser.RMidbracket);
        }

        public TerminalNode RMidbracket(int i) {
            return getToken(MParser.RMidbracket, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_array_new;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterArray_new(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitArray_new(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitArray_new(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Type_non_arrayContext extends ParserRuleContext {
        public Type_non_arrayContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TypenametokensContext typenametokens() {
            return getRuleContext(TypenametokensContext.class, 0);
        }

        public Class_declarationContext class_declaration() {
            return getRuleContext(Class_declarationContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_type_non_array;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterType_non_array(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitType_non_array(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitType_non_array(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Class_declarationContext extends ParserRuleContext {
        public Class_declarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode LBigbracket() {
            return getToken(MParser.LBigbracket, 0);
        }

        public TerminalNode RBigbracket() {
            return getToken(MParser.RBigbracket, 0);
        }

        public Union_classContext union_class() {
            return getRuleContext(Union_classContext.class, 0);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        public List<TypeContext> type() {
            return getRuleContexts(TypeContext.class);
        }

        public TypeContext type(int i) {
            return getRuleContext(TypeContext.class, i);
        }

        public List<DeclaratorsContext> declarators() {
            return getRuleContexts(DeclaratorsContext.class);
        }

        public DeclaratorsContext declarators(int i) {
            return getRuleContext(DeclaratorsContext.class, i);
        }

        public List<TerminalNode> SEMI() {
            return getTokens(MParser.SEMI);
        }

        public TerminalNode SEMI(int i) {
            return getToken(MParser.SEMI, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_class_declaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterClass_declaration(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitClass_declaration(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitClass_declaration(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class DeclaratorContext extends ParserRuleContext {
        public DeclaratorContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_declarator;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterDeclarator(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitDeclarator(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitDeclarator(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class InitializerContext extends ParserRuleContext {
        public InitializerContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Assignment_expressionContext assignment_expression() {
            return getRuleContext(Assignment_expressionContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_initializer;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterInitializer(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitInitializer(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitInitializer(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Function_definitionContext extends ParserRuleContext {
        public Function_definitionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        public TerminalNode LBigbracket() {
            return getToken(MParser.LBigbracket, 0);
        }

        public TerminalNode RBigbracket() {
            return getToken(MParser.RBigbracket, 0);
        }

        public ParametersContext parameters() {
            return getRuleContext(ParametersContext.class, 0);
        }

        public List<StatementContext> statement() {
            return getRuleContexts(StatementContext.class);
        }

        public StatementContext statement(int i) {
            return getRuleContext(StatementContext.class, i);
        }

        public List<DeclarationContext> declaration() {
            return getRuleContexts(DeclarationContext.class);
        }

        public DeclarationContext declaration(int i) {
            return getRuleContext(DeclarationContext.class, i);
        }

        public TerminalNode SEMI() {
            return getToken(MParser.SEMI, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_function_definition;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterFunction_definition(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitFunction_definition(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitFunction_definition(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class ParametersContext extends ParserRuleContext {
        public ParametersContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<ParameterContext> parameter() {
            return getRuleContexts(ParameterContext.class);
        }

        public ParameterContext parameter(int i) {
            return getRuleContext(ParameterContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(MParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(MParser.Comma, i);
        }

        public TerminalNode Balabalabala() {
            return getToken(MParser.Balabalabala, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_parameters;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterParameters(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitParameters(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitParameters(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class ParameterContext extends ParserRuleContext {
        public ParameterContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public DeclaratorContext declarator() {
            return getRuleContext(DeclaratorContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_parameter;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterParameter(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitParameter(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitParameter(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class StatementContext extends ParserRuleContext {
        public StatementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Compond_statementContext compond_statement() {
            return getRuleContext(Compond_statementContext.class, 0);
        }

        public Structured_statementContext structured_statement() {
            return getRuleContext(Structured_statementContext.class, 0);
        }

        public Assignment_statementContext assignment_statement() {
            return getRuleContext(Assignment_statementContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterStatement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitStatement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitStatement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Assignment_statementContext extends ParserRuleContext {
        public Assignment_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode SEMI() {
            return getToken(MParser.SEMI, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_assignment_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterAssignment_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitAssignment_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitAssignment_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Structured_statementContext extends ParserRuleContext {
        public Structured_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Loop_statementContext loop_statement() {
            return getRuleContext(Loop_statementContext.class, 0);
        }

        public Branch_statementContext branch_statement() {
            return getRuleContext(Branch_statementContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_structured_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterStructured_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitStructured_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitStructured_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Compond_statementContext extends ParserRuleContext {
        public Compond_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode LBigbracket() {
            return getToken(MParser.LBigbracket, 0);
        }

        public TerminalNode RBigbracket() {
            return getToken(MParser.RBigbracket, 0);
        }

        public List<DeclarationContext> declaration() {
            return getRuleContexts(DeclarationContext.class);
        }

        public DeclarationContext declaration(int i) {
            return getRuleContext(DeclarationContext.class, i);
        }

        public List<StatementContext> statement() {
            return getRuleContexts(StatementContext.class);
        }

        public StatementContext statement(int i) {
            return getRuleContext(StatementContext.class, i);
        }

        public List<Function_definitionContext> function_definition() {
            return getRuleContexts(Function_definitionContext.class);
        }

        public Function_definitionContext function_definition(int i) {
            return getRuleContext(Function_definitionContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_compond_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterCompond_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitCompond_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitCompond_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Loop_statementContext extends ParserRuleContext {
        public Loop_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public For_loop_statementContext for_loop_statement() {
            return getRuleContext(For_loop_statementContext.class, 0);
        }

        public While_loop_statementContext while_loop_statement() {
            return getRuleContext(While_loop_statementContext.class, 0);
        }

        public Do_while_loop_statementContext do_while_loop_statement() {
            return getRuleContext(Do_while_loop_statementContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_loop_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterLoop_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitLoop_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitLoop_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Branch_statementContext extends ParserRuleContext {
        public Branch_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public If_statementContext if_statement() {
            return getRuleContext(If_statementContext.class, 0);
        }

        public TerminalNode BREAK() {
            return getToken(MParser.BREAK, 0);
        }

        public TerminalNode SEMI() {
            return getToken(MParser.SEMI, 0);
        }

        public TerminalNode CONTINUE() {
            return getToken(MParser.CONTINUE, 0);
        }

        public TerminalNode RETURN() {
            return getToken(MParser.RETURN, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_branch_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterBranch_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitBranch_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitBranch_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class For_loop_statementContext extends ParserRuleContext {
        public For_loop_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode FOR() {
            return getToken(MParser.FOR, 0);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        public List<TerminalNode> SEMI() {
            return getTokens(MParser.SEMI);
        }

        public TerminalNode SEMI(int i) {
            return getToken(MParser.SEMI, i);
        }

        public List<ExpressionContext> expression() {
            return getRuleContexts(ExpressionContext.class);
        }

        public ExpressionContext expression(int i) {
            return getRuleContext(ExpressionContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_for_loop_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterFor_loop_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitFor_loop_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitFor_loop_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class While_loop_statementContext extends ParserRuleContext {
        public While_loop_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode WHILE() {
            return getToken(MParser.WHILE, 0);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_while_loop_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterWhile_loop_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitWhile_loop_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitWhile_loop_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Do_while_loop_statementContext extends ParserRuleContext {
        public Do_while_loop_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode DO() {
            return getToken(MParser.DO, 0);
        }

        public TerminalNode WHILE() {
            return getToken(MParser.WHILE, 0);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        public TerminalNode SEMI() {
            return getToken(MParser.SEMI, 0);
        }

        public StatementContext statement() {
            return getRuleContext(StatementContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_do_while_loop_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterDo_while_loop_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitDo_while_loop_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor)
                return ((MVisitor<? extends T>) visitor).visitDo_while_loop_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class If_statementContext extends ParserRuleContext {
        public If_statementContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode IF() {
            return getToken(MParser.IF, 0);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        public List<StatementContext> statement() {
            return getRuleContexts(StatementContext.class);
        }

        public StatementContext statement(int i) {
            return getRuleContext(StatementContext.class, i);
        }

        public TerminalNode ELSE() {
            return getToken(MParser.ELSE, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_if_statement;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterIf_statement(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitIf_statement(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitIf_statement(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class ExpressionContext extends ParserRuleContext {
        public ExpressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Assignment_expressionContext> assignment_expression() {
            return getRuleContexts(Assignment_expressionContext.class);
        }

        public Assignment_expressionContext assignment_expression(int i) {
            return getRuleContext(Assignment_expressionContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(MParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(MParser.Comma, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterExpression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitExpression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitExpression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Assignment_expressionContext extends ParserRuleContext {
        public Assignment_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Calculation_expressionContext calculation_expression() {
            return getRuleContext(Calculation_expressionContext.class, 0);
        }

        public List<Unary_expressionContext> unary_expression() {
            return getRuleContexts(Unary_expressionContext.class);
        }

        public Unary_expressionContext unary_expression(int i) {
            return getRuleContext(Unary_expressionContext.class, i);
        }

        public List<Assignment_operatorsContext> assignment_operators() {
            return getRuleContexts(Assignment_operatorsContext.class);
        }

        public Assignment_operatorsContext assignment_operators(int i) {
            return getRuleContext(Assignment_operatorsContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_assignment_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterAssignment_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitAssignment_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitAssignment_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class EqualContext extends ParserRuleContext {
        public EqualContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_equal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterEqual(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitEqual(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitEqual(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class PequalContext extends ParserRuleContext {
        public PequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_pequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterPequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitPequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitPequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class MiequalContext extends ParserRuleContext {
        public MiequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_miequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterMiequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitMiequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitMiequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class MuequalContext extends ParserRuleContext {
        public MuequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_muequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterMuequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitMuequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitMuequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class DiequalContext extends ParserRuleContext {
        public DiequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_diequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterDiequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitDiequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitDiequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class AndequalContext extends ParserRuleContext {
        public AndequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_andequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterAndequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitAndequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitAndequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class XorequalContext extends ParserRuleContext {
        public XorequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_xorequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterXorequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitXorequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitXorequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class OrequalContext extends ParserRuleContext {
        public OrequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_orequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterOrequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitOrequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitOrequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class LsequalContext extends ParserRuleContext {
        public LsequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_lsequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterLsequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitLsequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitLsequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class RsequalContext extends ParserRuleContext {
        public RsequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_rsequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterRsequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitRsequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitRsequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Assignment_operatorsContext extends ParserRuleContext {
        public Assignment_operatorsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public EqualContext equal() {
            return getRuleContext(EqualContext.class, 0);
        }

        public PequalContext pequal() {
            return getRuleContext(PequalContext.class, 0);
        }

        public MiequalContext miequal() {
            return getRuleContext(MiequalContext.class, 0);
        }

        public MuequalContext muequal() {
            return getRuleContext(MuequalContext.class, 0);
        }

        public DiequalContext diequal() {
            return getRuleContext(DiequalContext.class, 0);
        }

        public AndequalContext andequal() {
            return getRuleContext(AndequalContext.class, 0);
        }

        public XorequalContext xorequal() {
            return getRuleContext(XorequalContext.class, 0);
        }

        public OrequalContext orequal() {
            return getRuleContext(OrequalContext.class, 0);
        }

        public LsequalContext lsequal() {
            return getRuleContext(LsequalContext.class, 0);
        }

        public RsequalContext rsequal() {
            return getRuleContext(RsequalContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_assignment_operators;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterAssignment_operators(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitAssignment_operators(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitAssignment_operators(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Calculation_expressionContext extends ParserRuleContext {
        public Calculation_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Logical_caclulation_expressionContext> logical_caclulation_expression() {
            return getRuleContexts(Logical_caclulation_expressionContext.class);
        }

        public Logical_caclulation_expressionContext logical_caclulation_expression(int i) {
            return getRuleContext(Logical_caclulation_expressionContext.class, i);
        }

        public List<TerminalNode> QUESTION() {
            return getTokens(MParser.QUESTION);
        }

        public TerminalNode QUESTION(int i) {
            return getToken(MParser.QUESTION, i);
        }

        public List<TerminalNode> COLON() {
            return getTokens(MParser.COLON);
        }

        public TerminalNode COLON(int i) {
            return getToken(MParser.COLON, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_calculation_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterCalculation_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitCalculation_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitCalculation_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Logical_caclulation_expressionContext extends ParserRuleContext {
        public Logical_caclulation_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Logical_or_expressionContext logical_or_expression() {
            return getRuleContext(Logical_or_expressionContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_logical_caclulation_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterLogical_caclulation_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitLogical_caclulation_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor)
                return ((MVisitor<? extends T>) visitor).visitLogical_caclulation_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Logical_or_expressionContext extends ParserRuleContext {
        public Logical_or_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Logical_and_expressionContext> logical_and_expression() {
            return getRuleContexts(Logical_and_expressionContext.class);
        }

        public Logical_and_expressionContext logical_and_expression(int i) {
            return getRuleContext(Logical_and_expressionContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_logical_or_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterLogical_or_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitLogical_or_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitLogical_or_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Logical_and_expressionContext extends ParserRuleContext {
        public Logical_and_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Bitwise_or_expressionContext> bitwise_or_expression() {
            return getRuleContexts(Bitwise_or_expressionContext.class);
        }

        public Bitwise_or_expressionContext bitwise_or_expression(int i) {
            return getRuleContext(Bitwise_or_expressionContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_logical_and_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterLogical_and_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitLogical_and_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitLogical_and_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Bitwise_or_expressionContext extends ParserRuleContext {
        public Bitwise_or_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Bitwise_xor_expressionContext> bitwise_xor_expression() {
            return getRuleContexts(Bitwise_xor_expressionContext.class);
        }

        public Bitwise_xor_expressionContext bitwise_xor_expression(int i) {
            return getRuleContext(Bitwise_xor_expressionContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_bitwise_or_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterBitwise_or_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitBitwise_or_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitBitwise_or_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Bitwise_xor_expressionContext extends ParserRuleContext {
        public Bitwise_xor_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Bitwise_and_expressionContext> bitwise_and_expression() {
            return getRuleContexts(Bitwise_and_expressionContext.class);
        }

        public Bitwise_and_expressionContext bitwise_and_expression(int i) {
            return getRuleContext(Bitwise_and_expressionContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_bitwise_xor_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterBitwise_xor_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitBitwise_xor_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitBitwise_xor_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Bitwise_and_expressionContext extends ParserRuleContext {
        public Bitwise_and_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Equality_expressionContext> equality_expression() {
            return getRuleContexts(Equality_expressionContext.class);
        }

        public Equality_expressionContext equality_expression(int i) {
            return getRuleContext(Equality_expressionContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_bitwise_and_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterBitwise_and_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitBitwise_and_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitBitwise_and_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class IsequalContext extends ParserRuleContext {
        public IsequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_isequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterIsequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitIsequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitIsequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class NotequalContext extends ParserRuleContext {
        public NotequalContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_notequal;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterNotequal(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitNotequal(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitNotequal(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Equality_expressionContext extends ParserRuleContext {
        public Equality_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Relation_expressionContext> relation_expression() {
            return getRuleContexts(Relation_expressionContext.class);
        }

        public Relation_expressionContext relation_expression(int i) {
            return getRuleContext(Relation_expressionContext.class, i);
        }

        public List<IsequalContext> isequal() {
            return getRuleContexts(IsequalContext.class);
        }

        public IsequalContext isequal(int i) {
            return getRuleContext(IsequalContext.class, i);
        }

        public List<NotequalContext> notequal() {
            return getRuleContexts(NotequalContext.class);
        }

        public NotequalContext notequal(int i) {
            return getRuleContext(NotequalContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_equality_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterEquality_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitEquality_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitEquality_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class BiggerContext extends ParserRuleContext {
        public BiggerContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_bigger;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterBigger(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitBigger(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitBigger(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class SmallerContext extends ParserRuleContext {
        public SmallerContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_smaller;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterSmaller(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitSmaller(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitSmaller(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Bigger_eContext extends ParserRuleContext {
        public Bigger_eContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_bigger_e;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterBigger_e(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitBigger_e(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitBigger_e(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Smaller_eContext extends ParserRuleContext {
        public Smaller_eContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_smaller_e;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterSmaller_e(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitSmaller_e(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitSmaller_e(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Relation_expressionContext extends ParserRuleContext {
        public Relation_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Shift_expressionContext> shift_expression() {
            return getRuleContexts(Shift_expressionContext.class);
        }

        public Shift_expressionContext shift_expression(int i) {
            return getRuleContext(Shift_expressionContext.class, i);
        }

        public List<Smaller_eContext> smaller_e() {
            return getRuleContexts(Smaller_eContext.class);
        }

        public Smaller_eContext smaller_e(int i) {
            return getRuleContext(Smaller_eContext.class, i);
        }

        public List<Bigger_eContext> bigger_e() {
            return getRuleContexts(Bigger_eContext.class);
        }

        public Bigger_eContext bigger_e(int i) {
            return getRuleContext(Bigger_eContext.class, i);
        }

        public List<BiggerContext> bigger() {
            return getRuleContexts(BiggerContext.class);
        }

        public BiggerContext bigger(int i) {
            return getRuleContext(BiggerContext.class, i);
        }

        public List<SmallerContext> smaller() {
            return getRuleContexts(SmallerContext.class);
        }

        public SmallerContext smaller(int i) {
            return getRuleContext(SmallerContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_relation_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterRelation_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitRelation_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitRelation_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class LshiftContext extends ParserRuleContext {
        public LshiftContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_lshift;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterLshift(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitLshift(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitLshift(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class RshiftContext extends ParserRuleContext {
        public RshiftContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_rshift;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterRshift(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitRshift(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitRshift(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class PlusContext extends ParserRuleContext {
        public PlusContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_plus;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterPlus(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitPlus(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitPlus(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class MinusContext extends ParserRuleContext {
        public MinusContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_minus;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterMinus(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitMinus(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitMinus(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Shift_expressionContext extends ParserRuleContext {
        public Shift_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Add_expressionContext> add_expression() {
            return getRuleContexts(Add_expressionContext.class);
        }

        public Add_expressionContext add_expression(int i) {
            return getRuleContext(Add_expressionContext.class, i);
        }

        public List<LshiftContext> lshift() {
            return getRuleContexts(LshiftContext.class);
        }

        public LshiftContext lshift(int i) {
            return getRuleContext(LshiftContext.class, i);
        }

        public List<RshiftContext> rshift() {
            return getRuleContexts(RshiftContext.class);
        }

        public RshiftContext rshift(int i) {
            return getRuleContext(RshiftContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_shift_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterShift_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitShift_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitShift_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Add_expressionContext extends ParserRuleContext {
        public Add_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Mul_expressionContext> mul_expression() {
            return getRuleContexts(Mul_expressionContext.class);
        }

        public Mul_expressionContext mul_expression(int i) {
            return getRuleContext(Mul_expressionContext.class, i);
        }

        public List<PlusContext> plus() {
            return getRuleContexts(PlusContext.class);
        }

        public PlusContext plus(int i) {
            return getRuleContext(PlusContext.class, i);
        }

        public List<MinusContext> minus() {
            return getRuleContexts(MinusContext.class);
        }

        public MinusContext minus(int i) {
            return getRuleContext(MinusContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_add_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterAdd_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitAdd_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitAdd_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class MultiplyContext extends ParserRuleContext {
        public MultiplyContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_multiply;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterMultiply(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitMultiply(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitMultiply(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class DivisionContext extends ParserRuleContext {
        public DivisionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_division;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterDivision(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitDivision(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitDivision(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class ModContext extends ParserRuleContext {
        public ModContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_mod;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterMod(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitMod(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitMod(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Mul_expressionContext extends ParserRuleContext {
        public Mul_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Cast_expressionContext> cast_expression() {
            return getRuleContexts(Cast_expressionContext.class);
        }

        public Cast_expressionContext cast_expression(int i) {
            return getRuleContext(Cast_expressionContext.class, i);
        }

        public List<MultiplyContext> multiply() {
            return getRuleContexts(MultiplyContext.class);
        }

        public MultiplyContext multiply(int i) {
            return getRuleContext(MultiplyContext.class, i);
        }

        public List<DivisionContext> division() {
            return getRuleContexts(DivisionContext.class);
        }

        public DivisionContext division(int i) {
            return getRuleContext(DivisionContext.class, i);
        }

        public List<ModContext> mod() {
            return getRuleContexts(ModContext.class);
        }

        public ModContext mod(int i) {
            return getRuleContext(ModContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_mul_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterMul_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitMul_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitMul_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Cast_expressionContext extends ParserRuleContext {
        public Cast_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        public Cast_expressionContext cast_expression() {
            return getRuleContext(Cast_expressionContext.class, 0);
        }

        public Unary_expressionContext unary_expression() {
            return getRuleContext(Unary_expressionContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_cast_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterCast_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitCast_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitCast_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class PlusplusContext extends ParserRuleContext {
        public PlusplusContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_plusplus;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterPlusplus(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitPlusplus(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitPlusplus(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class MinusminusContext extends ParserRuleContext {
        public MinusminusContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_minusminus;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterMinusminus(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitMinusminus(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitMinusminus(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Unary_expressionContext extends ParserRuleContext {
        public Unary_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Postfix_expressionContext postfix_expression() {
            return getRuleContext(Postfix_expressionContext.class, 0);
        }

        public PlusplusContext plusplus() {
            return getRuleContext(PlusplusContext.class, 0);
        }

        public Unary_expressionContext unary_expression() {
            return getRuleContext(Unary_expressionContext.class, 0);
        }

        public MinusminusContext minusminus() {
            return getRuleContext(MinusminusContext.class, 0);
        }

        public Unary_operationContext unary_operation() {
            return getRuleContext(Unary_operationContext.class, 0);
        }

        public Cast_expressionContext cast_expression() {
            return getRuleContext(Cast_expressionContext.class, 0);
        }

        public NumberContext number() {
            return getRuleContext(NumberContext.class, 0);
        }

        public FunctionCall_expressionContext functionCall_expression() {
            return getRuleContext(FunctionCall_expressionContext.class, 0);
        }

        public New_operationContext new_operation() {
            return getRuleContext(New_operationContext.class, 0);
        }

        public Pre_defined_constantsContext pre_defined_constants() {
            return getRuleContext(Pre_defined_constantsContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_unary_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterUnary_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitUnary_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitUnary_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Pre_defined_constantsContext extends ParserRuleContext {
        public Pre_defined_constantsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_pre_defined_constants;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterPre_defined_constants(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitPre_defined_constants(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitPre_defined_constants(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Class_newContext extends ParserRuleContext {
        public Class_newContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_class_new;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterClass_new(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitClass_new(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitClass_new(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class New_operationContext extends ParserRuleContext {
        public New_operationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode New() {
            return getToken(MParser.New, 0);
        }

        public Array_newContext array_new() {
            return getRuleContext(Array_newContext.class, 0);
        }

        public Class_newContext class_new() {
            return getRuleContext(Class_newContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_new_operation;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterNew_operation(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitNew_operation(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitNew_operation(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class FunctionCall_expressionContext extends ParserRuleContext {
        public FunctionCall_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        public ArguementsContext arguements() {
            return getRuleContext(ArguementsContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_functionCall_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterFunctionCall_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitFunctionCall_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor)
                return ((MVisitor<? extends T>) visitor).visitFunctionCall_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class ArguementsContext extends ParserRuleContext {
        public ArguementsContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public List<Assignment_expressionContext> assignment_expression() {
            return getRuleContexts(Assignment_expressionContext.class);
        }

        public Assignment_expressionContext assignment_expression(int i) {
            return getRuleContext(Assignment_expressionContext.class, i);
        }

        public List<TerminalNode> Comma() {
            return getTokens(MParser.Comma);
        }

        public TerminalNode Comma(int i) {
            return getToken(MParser.Comma, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_arguements;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterArguements(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitArguements(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitArguements(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class ValueContext extends ParserRuleContext {
        public ValueContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public ConstantContext constant() {
            return getRuleContext(ConstantContext.class, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public ValueContext value() {
            return getRuleContext(ValueContext.class, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_value;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterValue(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitValue(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitValue(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Not_signContext extends ParserRuleContext {
        public Not_signContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_not_sign;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterNot_sign(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitNot_sign(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitNot_sign(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class GetAddrContext extends ParserRuleContext {
        public GetAddrContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_getAddr;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterGetAddr(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitGetAddr(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitGetAddr(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Unary_operationContext extends ParserRuleContext {
        public Unary_operationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Not_signContext not_sign() {
            return getRuleContext(Not_signContext.class, 0);
        }

        public MinusContext minus() {
            return getRuleContext(MinusContext.class, 0);
        }

        public PlusplusContext plusplus() {
            return getRuleContext(PlusplusContext.class, 0);
        }

        public MinusminusContext minusminus() {
            return getRuleContext(MinusminusContext.class, 0);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public Type_nameContext type_name() {
            return getRuleContext(Type_nameContext.class, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        public GetAddrContext getAddr() {
            return getRuleContext(GetAddrContext.class, 0);
        }

        public Bitwise_notContext bitwise_not() {
            return getRuleContext(Bitwise_notContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_unary_operation;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterUnary_operation(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitUnary_operation(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitUnary_operation(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Bitwise_notContext extends ParserRuleContext {
        public Bitwise_notContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_bitwise_not;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterBitwise_not(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitBitwise_not(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitBitwise_not(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Postfix_expressionContext extends ParserRuleContext {
        public Postfix_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Primary_expressionContext primary_expression() {
            return getRuleContext(Primary_expressionContext.class, 0);
        }

        public List<PostfixContext> postfix() {
            return getRuleContexts(PostfixContext.class);
        }

        public PostfixContext postfix(int i) {
            return getRuleContext(PostfixContext.class, i);
        }

        @Override
        public int getRuleIndex() {
            return RULE_postfix_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterPostfix_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitPostfix_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitPostfix_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class GetMemberContext extends ParserRuleContext {
        public GetMemberContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_getMember;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterGetMember(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitGetMember(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitGetMember(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class PostfixContext extends ParserRuleContext {
        public PostfixContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode LMidbracket() {
            return getToken(MParser.LMidbracket, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public TerminalNode RMidbracket() {
            return getToken(MParser.RMidbracket, 0);
        }

        public GetMemberContext getMember() {
            return getRuleContext(GetMemberContext.class, 0);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        public FunctionCall_expressionContext functionCall_expression() {
            return getRuleContext(FunctionCall_expressionContext.class, 0);
        }

        public PlusplusContext plusplus() {
            return getRuleContext(PlusplusContext.class, 0);
        }

        public MinusminusContext minusminus() {
            return getRuleContext(MinusminusContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_postfix;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterPostfix(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitPostfix(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitPostfix(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Primary_expressionContext extends ParserRuleContext {
        public Primary_expressionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        public ConstantContext constant() {
            return getRuleContext(ConstantContext.class, 0);
        }

        public String_constantContext string_constant() {
            return getRuleContext(String_constantContext.class, 0);
        }

        public TerminalNode Lbracket() {
            return getToken(MParser.Lbracket, 0);
        }

        public ExpressionContext expression() {
            return getRuleContext(ExpressionContext.class, 0);
        }

        public TerminalNode Rbracket() {
            return getToken(MParser.Rbracket, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_primary_expression;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterPrimary_expression(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitPrimary_expression(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitPrimary_expression(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Type_nameContext extends ParserRuleContext {
        public Type_nameContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TypenametokensContext typenametokens() {
            return getRuleContext(TypenametokensContext.class, 0);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_type_name;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterType_name(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitType_name(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitType_name(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class ConstantContext extends ParserRuleContext {
        public ConstantContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Numeric_constantContext numeric_constant() {
            return getRuleContext(Numeric_constantContext.class, 0);
        }

        public String_constantContext string_constant() {
            return getRuleContext(String_constantContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_constant;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterConstant(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitConstant(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitConstant(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Numeric_constantContext extends ParserRuleContext {
        public Numeric_constantContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public Integer_constantContext integer_constant() {
            return getRuleContext(Integer_constantContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_numeric_constant;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterNumeric_constant(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitNumeric_constant(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitNumeric_constant(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Integer_constantContext extends ParserRuleContext {
        public Integer_constantContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public NumberContext number() {
            return getRuleContext(NumberContext.class, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_integer_constant;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterInteger_constant(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitInteger_constant(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitInteger_constant(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class String_constantContext extends ParserRuleContext {
        public String_constantContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode StringLiteral() {
            return getToken(MParser.StringLiteral, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_string_constant;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterString_constant(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitString_constant(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitString_constant(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class TypenametokensContext extends ParserRuleContext {
        public Token builtin_types;

        public TypenametokensContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode Identifier() {
            return getToken(MParser.Identifier, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_typenametokens;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterTypenametokens(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitTypenametokens(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitTypenametokens(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class NumberContext extends ParserRuleContext {
        public NumberContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        public TerminalNode DecimalConstant() {
            return getToken(MParser.DecimalConstant, 0);
        }

        public TerminalNode Sign() {
            return getToken(MParser.Sign, 0);
        }

        @Override
        public int getRuleIndex() {
            return RULE_number;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterNumber(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitNumber(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitNumber(this);
            else return visitor.visitChildren(this);
        }
    }

    public static class Union_classContext extends ParserRuleContext {
        public Union_classContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_union_class;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).enterUnion_class(this);
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MListener) ((MListener) listener).exitUnion_class(this);
        }

        @Override
        public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
            if (visitor instanceof MVisitor) return ((MVisitor<? extends T>) visitor).visitUnion_class(this);
            else return visitor.visitChildren(this);
        }
    }
}